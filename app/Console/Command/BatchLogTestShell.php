<?php
App::uses('DailyShell', 'Console/Command');

/**
 *
 * ログ出力のテストバッチ
 *
 * sh app/Console/cake BatchLogTest
 */
class BatchLogTestShell extends AppShell {

	/**
	 * Modelの宣言。テスト用
	 */
	public $uses = array('User');

	/**
	 * BatchLogTaskの使用を宣言
	 */
	 public $tasks = array('BatchLog');

	/**
	 * 初期設定
	 */
	public function startup(){
		parent::startup();

		// 当クラス名を登録
		$this->BatchLog->settingShellName(get_class($this));
		// ログ出力するレベルを設定。宣言なしの場合は全て
		$this->BatchLog->settingConsoleOutLevel(array('info', 'error', 'warning', 'critical', 'alert', 'etc_level'));
		// エラーなどカウントするKeyを宣言
		$this->BatchLog->settingCountKey(array('count', 'fizz_count', 'buzz_count', 'fizzbuzz_count'));
	}

	public function main(){
		// バッチの開始をログ出力
		$this->BatchLog->shellStart();

		// infoのログ出力
		$this->BatchLog->infoLog('情報: 実行時の何らかの注目すべき事象');
		// warningのログ出力
		$this->BatchLog->warningLog('警告: 処理継続可能だが、後から環境面や入力等について確認を行ったほうが良い状態');
		// errorのログ出力
		$this->BatchLog->errorLog('エラー: 処理継続不能なため処理を中断。予期しない実行時エラー');

		$maxCount = 10;

		for ( $i = 1; $i <= $maxCount; $i++ ) {
			$logMessage = 'id: '. $i;
			// カウント
			$this->BatchLog->countup('count');

			if ( $i % 15 == 0 ) {
				$this->BatchLog->infoLog('FizzBuzz');
				$this->BatchLog->countup('fizzbuzz_count');

			} else if ( $i % 3 == 0 ) {
				$this->BatchLog->infoLog('Fizz');
				$this->BatchLog->countup('fizz_count');

			} else if ( $i % 5 == 0 ) {
				$this->BatchLog->infoLog('Buzz');
				$this->BatchLog->countup('buzz_count');
			}

			$this->BatchLog->infoLog('更新しました。('.$logMessage.')');
		}

		// バリデーションエラーの表示テスト
		$this->User->set(array('id'=>'test_test_test'));
		$this->User->validates();
		$this->BatchLog->validationError($this->User);

		// バッチ終了のログ出力 カウントの結果も出力される
		$this->BatchLog->shellEnd();
	}


}