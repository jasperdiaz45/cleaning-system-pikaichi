<?php

/**
 * ログ出力ヘルパー
 *
 * 概要
 *  メッセージをコンソールとファイルに出力します。
 *
 *
 * 手順
 *  1. このファイルを｢/app/Console/Command/Task/｣に追加してください。
 *  2. ｢ public $tasks = array('BatchLogTask'); ｣をxxxShell.phpファイルに追加してください。
 *     AppShell.phpに追加しますと全てのシェルPHPファイルに反映されます。
 *  3. 以下のコードをxxxShell.phpファイルに追加してください。
 *     AppShell.phpに追加しますと全てのシェルのPHPファイルに反映されます。
 *			public function startup(){
 *				parent::startup();
 *				// 当クラス名を登録
 *				$this->BatchLog->settingShellName(get_class($this));
 *				// ログ出力するレベルを設定。宣言なしの場合は全て
 *				$this->BatchLog->settingConsoleOutLevel(array('info', 'error', 'warning', 'critical', 'alert', 'etc_level'));
 *				// エラーなどカウントするKeyを宣言
 *				$this->BatchLog->settingCountKey(array('count', 'fizz_count', 'buzz_count', 'fizzbuzz_count'));
 *			}
 *
 * $logLevel
 *     ・alert: 今すぐ行動する必要がある
 *     ・critical: 致命的な状態
 *     ・error: エラー状態
 *     ・warning: 警告状態
 *     ・notice: 正常であるが、重大な状態
 *     ・info: インフォメーションメッセージ
 *     ・debug: デバッグレベルメッセージ
 */
class BatchLogTask extends Shell {

	/**
	 * @var string $shellName 実行バッチ名
	 */
	public $shellName = 'Shell';

	/**
	 * @var array $consoleOutLevel コンソールに出力するログレベル
	 */
	public $consoleOutLevel = array('info', 'error', 'warning', 'critical', 'alert');

	/**
	 * @var array $resultCount エラーなどカウントする配列
	 */
	public $resultCount = array();

	/**
	 * 初期設定
	 */
	public function initialize(){
		parent::initialize();
		$this->settingCakeLogConfig(); // ログファイルの設定
	}

	/**
	 * シェル名を登録
	 */
	public function settingShellName($name){
		$this->shellName = $name;
	}

	/**
	 * コンソールに出力するログレベルの設定
	 */
	public function settingConsoleOutLevel($level){
		$this->consoleOutLevel = $level;
	}

	/**
	 * エラーなどカウントする配列の設定
	 */
	public function settingCountKey($countKey){
		if(is_array($countKey)){
			$this->resultCount = array();
			foreach($countKey as $key){
				$this->resultCount[$key] = 0;
			}
		}
	}

	/**
	 * カウントする
	 */
	public function countup($key){
		if(isset($this->resultCount[$key])){
			$this->resultCount[$key]++;
		}
	}

	/**
	 * Modelのバリデーションエラーを出力
	 *
	 */
	public function validationError(Model $model){
		$message = 'バリデーションエラー: Model: '.$model->name;
		$this->logOut('error', $message);
		$this->logOut('error', $model->validationErrors);
	}

	/**
	 * シェルの開始案内
	 */
	public function shellStart(){
		$this->logOut('info', '---------------------------------------------------------------');
		$message = $this->shellName.' シェルの開始';
		$this->logOut('info', $message);
	}

	/**
	 * シェルの開始案内
	 */
	public function shellEnd(){
		$message = $this->shellName.' シェルの終了';
		$this->logOut('info', $message);
		if(empty($this->resultCount)){
			$this->logOut('info', 'resultCount: empty');
		}else{
			$this->logOut('info', $this->resultCount);
		}
		$this->logOut('info', '---------------------------------------------------------------');
	}

	/**
	 * Errorレベルのログ出力
	 */
	public function errorLog($message){
		$this->logOut('error', $message);
	}

	/**
	 * Errorレベルのログ出力
	 */
	public function warningLog($message){
		$this->logOut('warning', $message);
	}

	/**
	 * Infoレベルのログ出力
	 */
	public function infoLog($message){
		$this->logOut('info', $message);
	}

	/**
	 * ログ出力
	 * コンソール出力とログ出力を行う
	 *
	 * @param string $logLevel
	 * @param object $message 出力内容
	 *
	 */
	protected function logOut($logLevel, $message){
		if(is_array($message)){
			$message =json_encode($message, JSON_UNESCAPED_UNICODE);
		}else{
			$message = print_r($message, true);
		}
		if(in_array($logLevel, $this->consoleOutLevel)){

			$consoleOutMessage = $logLevel .': '. $message;
			// コンソール出力のスタイル
			if($logLevel === 'info'){
				$consoleOutMessage = '<info>'.$consoleOutMessage.'</info>';
			}else if($logLevel === 'warning'){
				$consoleOutMessage = '<warning>'.$consoleOutMessage.'</warning>';
			}else if(in_array($logLevel, array('error', 'critical', 'alert'))){
				$consoleOutMessage = '<error>'.$consoleOutMessage.'</error>';
			}

			$this->out($consoleOutMessage);
		}

		$message = $logLevel. ': ['.$this->shellName.']: ' . $message;
		CakeLog::write('batchLog', $message);
	}

	/**
	 * ログファイルの設定
	 */
	private function settingCakeLogConfig(){
		CakeLog::config('batchLog', array(
			'engine' => 'FileLog',
			'types' => array(),
			'scopes' => array('batchLog'),
			'file' => 'batchLog_'.date( 'Ymd'),
			'size' => false,
		));
	}
}
