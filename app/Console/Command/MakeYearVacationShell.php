<?php

class MakeYearVacationShell extends AppShell {

	public $uses = array('Vacation', 'Employee';

	public function startup() {
		parent::startup();
	}

	public function main() {
		echo 'test!!!!!!';
	}


	public function aaa() {
		// calculation year
		$dateNow = CakeTime::format($dateTimeNow, '%Y-%m-%d');
		$dateNowArray = explode('-', $dateNow);
		$dateNowYear = intval($dateNowArray[0]);
		$dateNowMonth = intval($dateNowArray[1]);
		$dateNowDay = intval($dateNowArray[2]);
		if ($dateNowMonth < YEAR_START_MONTH) $dateNowYear--;
		$year = sprintf('%d-%02d-%02d', $dateNowYear, $dateNowMonth, $dateNowDay);

		// get Employee info
		$employees = $this->Employee->find('all');

		foreach($employees as $employee) {

//			$number_of_days_that_were_taken_during_the_year = xxxxx;
			
//			$last_year_days_remaining_on_holiday = xxxxx;

//			$days_remaining_on_holiday = 
//			if ($last_year_days_remaining_on_holiday > $number_of_days_that_were_taken_during_the_year)


			$this->Vacation->save(
				array(
					'employee_no' => $employee['Employee']['id'],
					'year' => $year,
					'number_of_working_days' => 0,
					'number_of_days_that_were_taken_during_the_year' => 0,
					'annual_number' => 0,
					'days_remaining_on_holiday' => 0,
					'days_on_holiday' => 0,
					'absents_without_notice' => 0,
					'holiday_working_days' => 0,
					'compensation_day_off' => 0,
					'time_off' => 0
				)
			);
		}
	}
}
