<?php

class MatterSummaryMailShell extends AppShell {

	public $tasks = array('MakeMonthlyOperations');
	
	public function startup() {
		parent::startup();
	}

	public function main() {
		
		$this->MakeMonthlyOperations->MakeMonthlyOperations();
	}
}
