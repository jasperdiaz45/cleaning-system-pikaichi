<?php
App::uses('ComponentCollection', 'Controller');
App::uses('SendEmailComponent', 'Controller/Component'); 
App::import('Vendor', 'DateTimeUtil');

/**
 *
 * ログ出力のテストバッチ
 *
 * sh app/Console/cake MatterSummaryMail
 */
class MatterSummaryMailShell extends AppShell {

	/**
	 * Modelの宣言
	 */
	public $uses = array('WorkMatter','User');

	/**
	 * BatchLogTaskの使用を宣言
	 */
	public $tasks = array('BatchLog');

	/**
	 * 初期設定
	 */
	public function startup(){
		parent::startup();

		// 当クラス名を登録
		$this->BatchLog->settingShellName(get_class($this));
		// ログ出力するレベルを設定。宣言なしの場合は全て
		$this->BatchLog->settingConsoleOutLevel(array('info', 'error', 'warning', 'critical', 'alert', 'etc_level'));
		// エラーなどカウントするKeyを宣言
		$this->BatchLog->settingCountKey(array('send_count', 'error_count'));
	}

	public function main(){
		// コレクション、コンポーネントの宣言
		$collection = new ComponentCollection();
		$this->SendMail = new SendEmailComponent($collection);
		
		// バッチの開始をログ出力
		$this->BatchLog->shellStart();
		
		// 管理者（集計可）ユーザーを取得
		$admins = $this->User->find('all', array(
			'fields' => array('User.email'),
			'conditions' => array('role' => '1'),
			'recursive' => -1
		));
		
		$this->BatchLog->infoLog('送信対象ユーザー： '.count($admins).'件');
		
		$now = DateTimeUtil::format('Y/m/d');
		$startDate = DateTimeUtil::calcDate('Y/m/d', '-7 days', $now);
		
		$target = $startDate . ' ～ ' . $now . ' 集計分';
		$workMatters = $this->getData($startDate, $now);
		
		foreach ((array)$admins as $admin) {
			$toMail = $admin['User']['email'];
			
			$errMessage = $this->SendMail->sendSummaryReport($toMail, $workMatters, $target);
			if (empty($errMessage)) {
				$this->BatchLog->countup('send_count');
			} else {
				$this->BatchLog->errorLog($errMessage);
				$this->BatchLog->countup('error_count');
			}
		}

		// バッチ終了のログ出力 カウントの結果も出力される
		$this->BatchLog->shellEnd();
	}

	private function getData($startDate, $now) {
		// 直近1週間の指定
		$where = 'wr.work_date >= "' . $startDate . ' 00:00:00" AND wr.work_date <= "' . $now . ' 23:59:59" AND wm.deleted = 0 AND wr.deleted = 0';
		
		// 直近1週間の件数が多い順にソート 全体の工数取得＋サブクエリで1週間分の工数を取得
		$query = 'SELECT WorkMatter.matter_id, Matter.matter_name, Matter.customer_name, Matter.estimate_count, Matter.maintenance_year_month, SUM(WorkMatter.work_count) AS sum_work_count, sq.period_count AS period_work_count, bm.before_maintenance_count AS before_maintenance_count ';
		$query .= 'FROM work_matters WorkMatter LEFT JOIN matters Matter ON WorkMatter.matter_id = Matter.id LEFT JOIN work_reports WorkReport ON WorkMatter.work_report_id = WorkReport.id ';
		$query .= 'LEFT JOIN (SELECT SUM(work_count) AS period_count, wm.matter_id as matter_id FROM work_matters wm LEFT JOIN work_reports wr ON wm.work_report_id = wr.id WHERE ' . $where . ' GROUP BY wm.matter_id) AS sq ON sq.matter_id = WorkMatter.matter_id ';
		$query .= 'LEFT JOIN ( ';
		$query .= 'SELECT SUM(bm_inner.work_count) AS before_maintenance_count, bm_inner.matter_id AS matter_id ';
		$query .= 'FROM (SELECT wm.work_count, wm.matter_id AS matter_id, DATE_FORMAT( wr.work_date,  "%Y/%m" ) AS work_yearmonth FROM work_matters wm LEFT JOIN work_reports wr ON wm.work_report_id = wr.id LEFT JOIN matters m ON m.id = wm.matter_id WHERE (m.maintenance_year_month IS NULL OR m.maintenance_year_month =  "" OR m.maintenance_year_month > DATE_FORMAT( wr.work_date,  "%Y/%m" )) AND wm.deleted = 0 AND wr.deleted = 0 AND m.deleted = 0) AS bm_inner GROUP BY bm_inner.matter_id ';
		$query .= ') AS bm ON bm.matter_id = WorkMatter.matter_id ';
		$query .= 'WHERE WorkMatter.deleted = 0 AND WorkReport.deleted = 0 AND Matter.deleted = 0 GROUP BY WorkMatter.matter_id ORDER BY sq.period_count DESC, WorkMatter.matter_id;';
		
		$ary = $this->WorkMatter->query($query);
		$this->BatchLog->infoLog('Summaryデータ取得 ' . $startDate . ' ～ ' . $now .' 分');
		
		return $ary;
	}

}