<?php

// ログ設定
App::uses('CakeLog', 'Log');
CakeLog::config('debug', array(
	'engine' => 'File',
	'types' => array('notice', 'info', 'debug'),
	'file' => 'debug',
	'size' => '10MB',
	'rotate' => 10,
));
CakeLog::config('error', array(
	'engine' => 'File',
	'types' => array('warning', 'error', 'critical', 'alert', 'emergency'),
	'file' => 'error',
	'size' => '10MB',
	'rotate' => 10,
));

// セッション保存
Configure::write('Session', array( 
	'defaults' => 'database', 
	'cookie' => 'SID', 
	//セッションの保持時間（min） 
	'timeout' => 52560000, 
));

// メール送信情報
Configure::write('mailSettingProduction', 'false');

// ファイルのクエリストリング有効化
//Configure::write('Asset.timestamp', 'force');

// アクセスログ出力
Configure::write('writeAccsessLog', 'false');

// 進捗管理外部サイト連携URL
Configure::write('externalUrl', 'https://sample/projects/%s/issues');

// 直近稼働 取得日数
Configure::write('lasterReportDays', 9);

// 権限の設定
require_once APP. '..' .DS. 'app'. DS . 'Config'. DS . 'config_authority.php';