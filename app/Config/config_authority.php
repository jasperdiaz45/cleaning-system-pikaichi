<?php

// 権限設定
/**
 * authorityNo1:管理者（集計可）
 * authorityNo2:登録者（集計以外）
 * authorityNo3:報告者（入力,一覧のみ）
 */
$authorityList = array(
	'authorityNo1' => array(
		'workReports'=>true,
		'matters'=>true,
		'users'=>true,
		'workMatters'=>true
	),
	'authorityNo2' => array(
		'workReports'=>true,
		'matters'=>true,
		'users'=>true,
		'workMatters'=>false
	),
	'authorityNo3' => array(
		'workReports'=>true,
		'matters'=>false,
		'users'=>false,
		'workMatters'=>false
	),
);


Configure::write('authority', $authorityList);


