<?php

$config = array();

// access log
define("ACCESS_LOG_ENABLE", true);						// ログを出力するかしないか (true:する false:しない)
define("ACCESS_LOG_OUT_DIR", LOGS . "accesslogs/");		// ログ出力先
define("ACCESS_LOG_DAYS_LEFT", 2);						// 何日前までのログを残すか

// operation status ID
define("OPERATION_STATUS_ID_BEF_WORK", 1);				// 出社前
define("OPERATION_STATUS_ID_NOW_WORK", 2);				// 勤務中
define("OPERATION_STATUS_ID_LEAVE_WORK", 3);			// 途中退出中
define("OPERATION_STATUS_ID_OUT_WORK", 4);				// 退社済み
define("OPERATION_STATUS_ID_BEF_WORK_FORGOT_OUT", 5);	// 出社前(退勤打刻忘れ)

// overtime kind
define("OVERTIME_KIND_NORMAL", 1);			// 残業
define("OVERTIME_KIND_NIGHT_SHIFT", 2);		// 夜勤
define("OVERTIME_KIND_HOLIDAY_WORK", 3);	// 休出

// vacation kind
define("VACATION_KIND_SALARY", 1);			// 有休
define("VACATION_KIND_SALARY_AM", 2);		// 午前半休
define("VACATION_KIND_SALARY_PM", 3);		// 午後半休
define("VACATION_KIND_TIME", 4);			// 時間休
define("VACATION_KIND_COMPENSATION", 5);	// 代休
define("VACATION_KIND_CONGRA_CONDOL", 6);	// 慶弔休暇
define("VACATION_KIND_SUMMER", 7);			// 夏期休暇
define("VACATION_KIND_END_YEAR", 8);		// 年末休暇
define("VACATION_KIND_ABSENCE", 9);			// 欠勤

// vacation reason
define("REASON_COMPENSATION_FORHOLIDAY_WORK", "%sの休出の代休です。");		// 休出のための代休の理由

// approval
define('APPROVAL_PRESIDENT_APPROVER_NO', 2);			// 社長承認者ID
define('APPROVAL_SECTION_DEFAULT_APPROVER_NO', 2);		// 課内承認者デフォルト承認者ID
define("APPROVAL_APPLYING", 0);	// 申請中
define("APPROVAL_ALREADY", 1);	// 承認済
define("APPROVAL_REMAND", 2);	// 差戻し
define("APPROVAL_SECTION", 1);	// 課内承認者
define("APPROVAL_SENIOR", 2);	// 上席承認者
define("APPROVAL_FINAL", 3);	// 最終承認者
define("STR_APPROVAL_ALREADY_SECTION", "課内承認済");
define("STR_APPROVAL_REMAND_SECTION", "課内承認差戻し");
define("STR_APPROVAL_ALREADY_SENIOR", "上席承認済");
define("STR_APPROVAL_REMAND_SENIOR", "未承認(差戻し)");
define("STR_APPROVAL_ALREADY_FINAL", "最終承認済");
define("STR_APPROVAL_REMAND_FINAL", "未承認(差戻し)");
define("STR_APPROVAL_WAIT_PRESIDENT", "社長承認待ち");

//HolidayWithSalary for year
define("HOLIDAY_WITH_SALARY_FOR_YEAR" , 5);

// start business day
define("START_BUSINESS_DAY", 1);

// Year start month (年度開始月)
define("YEAR_START_MONTH", 4);

// page limit
define("PAGE_LIMIT_ADD_OPPORTUNITY_MODAL" , 10);
define("PAGE_LIMIT_ADD_REPORT_DESTINATION_MODAL" , 10);
define("PAGE_LIMIT_DAILY_REPORT_MODAL" , 10);
define("PAGE_LIMIT_MESSAGE_MODAL" , 10);

// define PitTouch response.
define("PIT_TOUCH_RES_OK", "00");
define("PIT_TOUCH_RES_ERR", "99");

// define PitTouch sts
define("PIT_TOUCH_STS_IN", "01");				// 出勤
define("PIT_TOUCH_STS_OUT", "02");				// 退勤
define("PIT_TOUCH_STS_LEAVE_MIDWAY", "03");		// 外出
define("PIT_TOUCH_STS_RETURN_MIDWAY", "04");	// 戻り

// ajax url base path
// 環境に応じてdefault以外のパスを直接指定してください。
//define("AJAX_URL_BASE_PATH", "/works");				// デモ環境
define("AJAX_URL_BASE_PATH", '');					// default

// define PitTouch sound.
define("PIT_TOUCH_SND_SUCCESS_1", "1001");			// 成功音1
define("PIT_TOUCH_SND_SUCCESS_2", "1002");			// 成功音2
define("PIT_TOUCH_SND_FAILURE_1", "1003");			// 失敗音1
define("PIT_TOUCH_SND_FAILURE_2", "1004");			// 失敗音2
define("PIT_TOUCH_SND_GOOD_MORNING", "1008");		// おはようございます
define("PIT_TOUCH_SND_WELCOME_HOME", "1009");		// お帰りなさい
define("PIT_TOUCH_SND_I_WAS_RELIEVED", "1010");		// おつかれさまでした
define("PIT_TOUCH_SND_HAVE_A_BLAST", "1011");		// いってらっしゃい
define("PIT_TOUCH_SND_GOODBYE", "1012");			// さようなら
define("PIT_TOUCH_SND_NO_REGISTRATION", "1013");	// 登録がありません
define("PIT_TOUCH_SND_IN", "1014");					// 出勤です
define("PIT_TOUCH_SND_OUT", "1015");				// 退勤です
define("PIT_TOUCH_SND_LEAVE_MIDWAY", "1016");		// 外出です
define("PIT_TOUCH_SND_RETURN_MIDWAY", "1017");		// 戻りです
define("PIT_TOUCH_SND_ERROR", "1018");				// エラーです
define("PIT_TOUCH_SND_GOOD WORK", "1019");			// おつかれさまです

// define PitTouch dsp msg.
define("PIT_TOUCH_DSP_IN", "出勤です");
define("PIT_TOUCH_DSP_OUT", "退勤です");
define("PIT_TOUCH_DSP_LEAVE_MIDWAY", "外出です");
define("PIT_TOUCH_DSP_RETURN_MIDWAY", "戻りです");
define("PIT_TOUCH_DSP_ERR_NO_REGISTRATION", "登録がありません");
define("PIT_TOUCH_DSP_ERR_IN", "既に出社打刻済みです");
define("PIT_TOUCH_DSP_ERR_OUT", "勤務中でないため退社できません");
define("PIT_TOUCH_DSP_ERR_LEAVE", "勤務中以外の途中退出はできません");
define("PIT_TOUCH_DSP_ERR_RETURN_MIDWAY", "途中退出中ではありません");
