<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Emails.html
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::import('Vendor', 'CountMeterUtil');
?>
<div class="row">
	<div class="col-md-12">
		<div class="col-md-12">
			<h3>集計</h3>
		</div>
		<div class="col-md-3">
			<br>
			<div><?php echo $target; ?></div>
			<br>
		</div>
		<div class="table-render col-sm-12">
			<table cellspacing="0" cellpadding="4" class="table table-bordered" style="border: 1px #ddd solid;">
				<thead>
				<tr role="row" class="heading">
					<th class="sorting_asc" style="border: 1px #ddd solid;">案件名</th>
					<th class="sorting_asc" style="border: 1px #ddd solid;">稼働工数 / 予算工数 (直近1週間の工数)</th>
				</tr>
				</thead>
				<tbody>
					<?php foreach ($workMatters as $row): ?>
					<tr>
						<td style="border: 1px #ddd solid;">
							<?php echo h($row['Matter']['customer_name']) . '<br/>' . h($row['Matter']['matter_name']); ?>&nbsp;
						</td>
						<td style="border: 1px #ddd solid;">
							<?php
								$beforeMaintenanceCount = empty($row['bm']['before_maintenance_count']) ? 0 : $row['bm']['before_maintenance_count'];
								$sumWorkCount = empty($row[0]['sum_work_count']) ? 0 : $row[0]['sum_work_count'];
								
								echo !is_null($row['bm']['before_maintenance_count']) ? number_format($row['bm']['before_maintenance_count']) : '0';
								echo ' / ';
								echo !is_null($row['Matter']['estimate_count']) ? number_format($row['Matter']['estimate_count']) : '未設定';
								echo ' (直近';
								echo !is_null($row['sq']['period_work_count']) ? number_format($row['sq']['period_work_count']) : '0';
								
								$maintenanceCountMes = '';
								$maintenanceCount = $sumWorkCount - $beforeMaintenanceCount;
								if (!empty($row['Matter']['maintenance_year_month']) && $maintenanceCount > 0) {
									$maintenanceCountMes = '&nbsp;保守' . $row['Matter']['maintenance_year_month'] . 'から' . $maintenanceCount;
								}
								
								echo $maintenanceCountMes . ')';
								
								if (!is_null($row['Matter']['estimate_count'])) {
									echo '<br/>' . CountMeterUtil::meterAndPer($row['bm']['before_maintenance_count'], $row['Matter']['estimate_count']);
								}
							?>&nbsp;
						</td>
					</tr>
					<?php endforeach; ?>
				</tbody>
			</table>

		</div>
	</div>
</div>
