<?php
App::uses('AppHelper', 'View/Helper');

class VersionHelper extends AppHelper {

	public function extVarParam($path) {

        return $path . '?ver' . filemtime(realpath(null) . $path);
	}
}