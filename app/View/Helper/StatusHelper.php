<?php
App::uses('AppHelper', 'View/Helper');

class StatusHelper extends AppHelper {

	/**
	 * 有効フラグ
	 */
	public function getStatusEffectiveDivision($isList = true, $key = null) {
		$option = array(
			'1' => '有効',
			'0' => '無効',
		);
		return $this->getListOrValue($option, $isList, $key);
	}

	/**
	 * 権限
	 */
	public function getStatusAuthority($isList = true, $key = null) {
		$option = array(
			'1' => '管理者（集計可）',
			'2' => '登録者（集計以外）',
			'3' => '報告者（入力,一覧のみ）',
		);
		return $this->getListOrValue($option, $isList, $key);
	}
	
	
	private function getListOrValue($list, $isList, $key){
		if(is_bool($key)){
			$key = $key ? 1 : 0;
		}
		if (empty($key) && $isList === true) {
			return $list;
		} else if (array_key_exists($key, $list)) {
			return $list[$key];
		} else {
			return '';
		}
	}

}