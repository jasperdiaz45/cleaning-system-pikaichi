<?php
App::uses('AppHelper', 'View/Helper');

class TimeHelper extends AppHelper {

	//１０進数から６０進数変換
	public function timeFormat60($time10) {

		if($time10 == null || $time10 == ''){
			return '';
		}

		$hour = floor($time10);
		$mins = $time10 - $hour;

		$mins = str_pad(round($mins * 60,0),2,0,STR_PAD_LEFT);

		$time60 = strval($hour) . ':' . $mins;

		return $time60;
	}
}