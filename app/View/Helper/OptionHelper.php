<?php
App::uses('AppHelper', 'View/Helper');

class OptionHelper extends AppHelper {


	//payment timing option 
	// 支払タイミング
	public function paymentTimingOption($isList = true, $key = null) {
		$option = array( 0 => null
						, 1 => '都度'
						, 2 => '前払い'
						, 3 => '掛払い');

		return $this->getListOrValue($option, $isList, $key);
	}

	//Individual Classification
	// 個人締め区分
	public function IndividualClassificationOption($isList = true, $key = null) {
		$option = array( 0 => null
						, 1 => '店舗締め'
						, 2 => '個人締め');

		return $this->getListOrValue($option, $isList, $key);
	}

	//LastDay
	// め日
	public function LastDayOption($isList = true, $key = null) {
		$option = array( 0 => null
						, 1 => '毎月10日締め'
						, 2 => '毎月15日締め'
						, 3 => '毎月20日締め'
						, 4 => '毎月25日締め'
						, 5 => '毎月末日締め'
						, 6 => '毎週末締め'
						, 7 => '代金引換え');

		return $this->getListOrValue($option, $isList, $key);
	}

	//Collection cycle
	// 回収サイクル
	public function CollectionCycleOption($isList = true, $key = null) {
		$option = array( 0 => '当月'
						, 1 => '翌月10'
						, 2 => '翌月15'
						, 3 => '翌月20'
						, 4 => '翌月末'
						, 5 => '翌々月10'
						, 6 => '翌々月15'
						, 7 => '翌々月20'
						, 8 => '翌々月末');

		return $this->getListOrValue($option, $isList, $key);
	}

	//Payment Method
	// 支払方法
	public function PaymentMethodOption($isList = true, $key = null) {
		$option = array( 0 => '現金');

		return $this->getListOrValue($option, $isList, $key);
	}
	

	//Prefectures
	// 都道府県
	public function PrefecturesOption($isList = true, $key = null) {
		$option = array( 0 => '東京都'
						, 1 => '神奈川県'
						, 2 => '埼玉県'
						, 3 => '千葉県'
						, 4 => '群馬県'
						, 5 => '静岡県');

		return $this->getListOrValue($option, $isList, $key);
	}


	//SpecifyDeliveryDate
	// 配送日指定
	public function SpecifyDeliveryDateOption($isList = true, $key = null) {
		$option = array( 0 => '普通'
						, 1 => 'お急ぎ'
						, 2 => '指定');

		return $this->getListOrValue($option, $isList, $key);
	}


	
	
	private function getListOrValue($list, $isList, $key){
		if(is_bool($key)){
			$key = $key ? 1 : 0;
		}
		if (empty($key) && $isList === true) {
			return $list;
		} else if (array_key_exists($key, $list)) {
			return $list[$key];
		} else {
			return '';
		}
	}

}