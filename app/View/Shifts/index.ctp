<span id="ajax-full-base-url" style="display:none;"><?= $ajaxFullBaseUrl ?></span>
<span id="f-company-no" style="display:none;"><?= $f_company_no ?></span>
<span id="f-department-no" style="display:none;"><?= $f_department_no ?></span>
<span id="f-section-no" style="display:none;"><?= $f_section_no ?></span>

<!--<body class="page-header-fixed page-quick-sidebar-over-content page-header-fixed-mobile page-footer-fixed1">-->
<?php echo $this->assign('title', $title); ?>
<!-- BEGIN CONTAINER -->
<input type="hidden" id="server_host" value="<?= $server ?>">
<button class="btn btn-primary" onclick="get_link('johnluis');">Try</button>
<div id="img_qr"></div>
<div class="page-container">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="contentRow">
					<div class="col-md-12">
                    	<div class="col-md-9">
                    		<h3>シフト登録</h3>
						</div>
						<div class="col-md-3">
							<a onclick = 'window.history.back()' class="btn btn-default pull-right" style="width:75px; margin-top:35px; margin-right:15px; background: #91A3AE;color:#FFF;">戻る</a>
						</div>
					</div>
					<div class="name-container">
						<?= $this->Html->link("シフト情報アップロード",['controller' => 'Shifts', 'action' => 'upload'], ['class' => 'btn btn-success', "style" => "style'=>'background: #9ACD32; width: 200px;margin-left: 30px;"]); ?>
					</div>
					<div class="col-md-12">
                    	<div class="col-md-9">
                    		<h3>シフト編集</h3>
						</div>
					</div>
				</div>	
				
		<div><?php echo $this->Form->create('Shifts'); ?></div>

				<div class="contentRow">		
					<div class="col-md-12">
						<div class="col-sm-1 name-container">
							<label>社員番号</label>
						</div>
						<div class="col-sm-2">
	                    	<?php echo $this->Form->input('search',array('label'=>false,'type'=>'text','class'=>'form-control','style'=>'width:100%;margin-left:2px;')); ?>
						</div>
					</div>
				</div>
				<div class="contentRow">		
					<div class="col-md-12 workDate">
						<div class="col-sm-1">
							<label>対象年月</label>
						</div>
						<div class="col-md-2">
					    	<?php echo $this->Form->input('work_date', array('label' => false, 'class'=>'form-control','style'=>'width:100%;margin-left:2px;', 'options' => $yearMonthList, 'value' =>$yearMonth)); ?>
			            </div>
					</div>
				</div>
                <div class="contentRow">
                    <div class="col-md-12" style="margin-top: 10px; margin-left:170px;">
                       	<div class="col-md-6">
                    			&nbsp
						</div>
						<div class="col-sm-2"></div>
                            <div class="custom-color-normal button-render col-sm-4 ">
                            	<?php echo $this->Form->button('検索', array('name'=>'search_button','class'=>'btn btn-skyBlue','style'=>'width:80px;')); ?>
                            	<?php echo $this->Form->button('更新', array('name'=>'update','class'=>'btn btn-skyBlue','style'=>'width:80px;')); ?>
                            </div>
                        </div>
                    </div>
				</div>
				
				<div class="contentRow">		
					<div class="col-sm-4 name-container">
						<?php if(!empty($shifts)): ?>
							<label><?= $employeeName ?></label>
						<?php endif;?>
					</div>
				</div>
				
				<div class="contentRow">
                    <div class="col-md-12">
						<table class="table table-bordered" style="margin-top: 15px;">
                            <thead style="background-color: #DDE1E3;">
                            <tr role="row" class="heading">
                                <th class="col-xs-2">日付</th>
                                <th class="col-xs-2">曜日</th>
                                <th class="col-xs-2">始業時刻</th>
                                <th class="col-xs-2">終業時刻</th>
                                <th class="col-xs-2">稼働時間</th>
                                <th class="col-xs-2">休憩時間</th>
                            </tr>
                            </thead>
                            <tbody>
                            	 <?php 
                            	 $day;
                            	 $month = date('m',strtotime($YmdAarray[0]));
                            	 $year = date('Y',strtotime($YmdAarray[0]));
                            	 $days = cal_days_in_month(CAL_GREGORIAN, $month, $year);
                            	 $count = count($shifts);
                            	 for($i=1;$i<=$days;$i++) { 
                            	 	$day =sprintf("%04d-%02d-%02d", $year, $month, $i);
                            	 	for($x=0;$x<$count;$x++){
                            	 		if($day==$shifts[$x]['Shifts']['date']){
                            	 			$data=array('date'=>$shifts[$x]['Shifts']['date']);
		                            	 ?>
											<tr>
												<td><?php echo $this->Form->input('Shifts.'.$i.'.empDate', array('type'=>'text','label' => false, 'class'=>'form-control','style'=>'border:none;border-width: 0;box-shadow:none;', 'value' => date('Y/m/d',strtotime($shifts[$x]['Shifts']['date'])),'readonly'=>'readonly')); ?></td>
												<td><?= $day_format[date('D',strtotime($shifts[$x]['Shifts']['date']))]?></td>
												<td><?php echo $this->Form->input('Shifts.'.$i.'.start_time', array('type'=>'text','id' => 'start_'.$shifts[$x]['Shifts']['id'],'oninput' => 'get_time_total('.strval($shifts[$x]['Shifts']['id']).')','label' => false, 'class'=>'form-control','style'=>'border:none;border-width: 0', 'value' => date('H:i', strtotime($shifts[$x]['Shifts']['start_time'])))); ?></td>
												<td><?php echo $this->Form->input('Shifts.'.$i.'.closing_time', array('type'=>'text','id' => 'end_'.$shifts[$x]['Shifts']['id'],'oninput' => 'get_time_total('.strval($shifts[$x]['Shifts']['id']).')','label' => false, 'class'=>'form-control','style'=>'border:none;border-width: 0', 'value' => date('H:i', strtotime($shifts[$x]['Shifts']['closing_time'])))); ?></td>
												<td id="basic_uptime_<?= $shifts[$x]['Shifts']['id'] ?>"><?= $this->Time->timeFormat60($shifts[$x]['Shifts']['basic_uptime']) ?></td>
												<td><?php echo $this->Form->input('Shifts.'.$i.'.break_deduction', array('type'=>'text','id' => 'break_'.$shifts[$x]['Shifts']['id'],'oninput' => 'get_time_total('.strval($shifts[$x]['Shifts']['id']).')','label' => false, 'class'=>'form-control','style'=>'border:none;border-width: 0', 'value' => $this->Time->timeFormat60($shifts[$x]['Shifts']['break_deduction']))); ?></td>
					      					</tr>
										<?php 
										$i++; 
										$day =sprintf("%04d-%02d-%02d", $year, $month, $i);
										}?>
									<?php }
									if($i<=$days){
									?>
									<tr>
										<td><?php echo $this->Form->input('Shifts.'.$i.'.empDate', array('type'=>'text','label' => false, 'class'=>'form-control','style'=>'border:none;border-width: 0;box-shadow:none;', 'value' => date('Y/m/d',strtotime($day)),'readonly'=>'readonly')); ?></td>
										<td><?= $day_format[date('D',strtotime($day))]?></td>
										<td><?php echo $this->Form->input('Shifts.'.$i.'.start_time', array('type'=>'text','id' => 'start_'.$i,'oninput' => 'get_time_total('.$i.')','label' => false, 'class'=>'form-control','style'=>'border:none;border-width: 0', 'value' => '')); ?></td>
										<td><?php echo $this->Form->input('Shifts.'.$i.'.closing_time', array('type'=>'text','id' => 'end_'.$i,'oninput' => 'get_time_total('.$i.')','label' => false, 'class'=>'form-control','style'=>'border:none;border-width: 0', 'value' => '')); ?></td>
										<td id="basic_uptime_<?= $i ?>"></td>
										<td><?php echo $this->Form->input('Shifts.'.$i.'.break_deduction', array('type'=>'text','id' => 'break_'.$i,'oninput' => 'get_time_total('.$i.')','label' => false, 'class'=>'form-control','style'=>'border:none;border-width: 0', 'value' => '')); ?></td>
			      				
									</tr>
							<?php } } ?>
                            </tbody>
                        </table>
                    </div>
               	</div>
             
				</div>
			</div>	
		<div><?php echo $this->Form->end(); ?></div>
			<!-- END PAGE CONTENT-->
		
		<div style="display:none;"></div>
	</div>
	<!-- END CONTENT -->
</div>

<!-- END CONTAINER -->

<script type="text/javascript">
	function get_link(text){
		var link = $("#server_host").val() + 'Shifts/qrcode/' + text;
		$.ajax({
			type:"GET",
			url:link,
			data:'',
			success:function(data){
				// alert(data);
				$("#img_qr").append('<img id="img_preview" src="/Pikaichi/environment/img/qrCode/'+data+'" class="img-fluid" width="150">');
			}
		});
	}
</script>


<style>
	.workDate{
		padding-top:15px;
	}
	.col-sm-1.name-container {
    	padding-right: 0px;
	}
	label {
		margin-top:8px;
	}
	input[readonly]{
	    background-color: white !important;
	}
</style>

<?php echo $this->Html->script('/js/AjaxCommons/ajax_common.js'); ?>
<?php echo $this->Html->script('/js/Shifts/index.js'); ?>
