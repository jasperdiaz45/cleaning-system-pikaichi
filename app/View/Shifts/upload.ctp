

<!--<body class="page-header-fixed page-quick-sidebar-over-content page-header-fixed-mobile page-footer-fixed1">-->
	<?php echo $this->assign('title', $title); ?>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="contentRow">
					<div class="col-md-12">
                    	<div class="col-md-9">
                    		<h3>シフト登録</h3>
							<?php //var_dump($test); ?>
						</div>
						<div class="col-sm-1">
							<a onclick = 'window.history.back()' class="btn btn-default" style="width:100%; background: #91A3AE;color:#FFF;">戻る</a>
						</div>
					</div>
				</div>	
				
        <div><?php echo $this->Form->create('upload',array('type' => 'file')); ?></div>

		<div class="contentRow">		
				<div class="col-md-12">
				<div class="col-sm-6">
					<label for="検索">Excel ファイルをアップロード</label>
					<?php echo $this->Form->input('CSVfile', array('id' => 'uploadfile', 'type' => 'file', 'label' => false, 'class'=>'form-control','style'=>'display:none')); ?>
			     	<?php echo $this->Form->input('select_file', array('readonly','id' => 'select_file' ,'label' => false, 'class'=>'form-control')); ?>
				</div>
        		
                <div class="col-sm-2">
                	&nbsp;
                    <?php echo $this->Form->button('アップロードする', array('disabled','id' => 'submit_btn','type'=>'submit', 'name'=>'search_button','class'=>'btn btn-default','style'=>'width:100%; margin-top:13px; background: #3399FF;color: #FFF;')); ?>
				</div>
				</div>
		</div>
             
			</div>
			</div>	
            <div><?php echo $this->Form->end(); ?></div>
			<!-- END PAGE CONTENT-->
		
		<div style="display:none;"></div>
	</div>
	<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->


<style>
	label {
		margin-top:8px;
	}
</style>

<?php echo $this->Html->script('/js/AjaxCommons/ajax_common.js'); ?>
<?php echo $this->Html->script('/js/AttendanceManagements/index.js'); ?>
<script type="text/javascript">
	$("#select_file").on('dblclick', function(){
		$("#uploadfile").click();
	});
	
	$("#uploadfile").on('change', function(e){
	e.preventDefault();
		if(this.value != null){
			$("#submit_btn").attr('disabled', false);
			var fullPath = this.value;
			var startIndex = (fullPath.indexOf('\\') >= 0 ? fullPath.lastIndexOf('\\') : fullPath.lastIndexOf('/'));
		    var filename = fullPath.substring(startIndex);
		    if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
		        filename = filename.substring(1);
		    }
		    //get file extension 
		    var extensions = filename.split('.').pop();
		    
		    if(extensions == 'xlsx' || extensions == 'csv'){
		    	 $("#select_file").val(filename);
		    }
		    else{
		    	alert("Excel/CSV ファイルを選択してください");
		    }
			
		}else{
			$("#submit_btn").attr('disabled', true);
			$("#select_file").val('');
		}
	});
</script>