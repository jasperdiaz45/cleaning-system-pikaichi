<!-- BEGIN CONTAINER -->
<?php echo $this->assign('title', $title); ?>
<div class="page-container">
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">
        <!-- BEGIN PAGE CONTENT-->
             <div class="row">
				<div class="col-md-12">
            <!-- Start Operation total -->
            <div class="contentRow">
                  <?php echo $this->Form->create('Employee'); ?>
                           	<div class="col-md-9">
                           	    <h3>社員詳細</h3>
                           	</div>
                           	<div class="col-md-2 form-level2">
                           	    <a onclick = 'window.history.back()' class="btn btn-default" style="width:100%; background: #91A3AE;color:#FFF;">戻る</a>
                           	</div>
				        </div>
				        
                        <!-- 1st GROUP -->  
                        <div class="contentRow">
                        <font size="2px">    
                            <div class="col-md-11" style="margin-top: 10px;">
                                <div class="col-md-2" style="color: #FFF; background-color: #0092C4">
                                    <label>姓</label> 
                                </div>
                                <div class="col-md-10">
                                    <?php echo $this->Form->text('last_name', array('label' => false, 'class'=>'form-control form-level')); ?>
                                </div>
                            </div>
                            <div class="col-md-11" style="margin-top: 10px;">
                                <div class="col-md-2" style="color: #FFF; background-color: #0092C4">
                                    <label>名</label> 
                                </div>
                                <div class="col-md-10">
                                    <?php echo $this->Form->text('first_name', array('label' => false, 'class'=>'form-control form-level')); ?>
                                </div>
                            </div>
                            <!-- 2ND GROUP -->  
                            <div class="col-md-11" style="margin-top: 40px;">
                                <div class="col-md-2" style="color: #FFF; background-color: #0092C4">
                                    <label>事業部</label> 
                                </div>
                                <div class="col-md-10">
                                    <?php
                                        echo $this->Form->input('division_no', array('label' => false, 'class'=>'form-control form-level', 'selected'=>$employee[0]['Employee']['division_no'],'options'=>$division));
                                    ?>  
                                </div>
                            </div>
                            
                            <div class="col-md-11" style="margin-top: 10px;">
                                <div class="col-md-2" style="color: #FFF; background-color: #0092C4">
                                    <label>部</label> 
                                </div>
                                <div class="col-md-10">
                                    <?php
                                        echo $this->Form->input('department_no', array('label' => false, 'class'=>'form-control form-level', 'options' => $department , 'selected'=>$employee[0]['Employee']['department_no']));
                                    ?> 
                                </div>
                            </div>
                            
                            <div class="col-md-11" style="margin-top: 10px;">
                                <div class="col-md-2" style="color: #FFF; background-color: #0092C4">
                                    <label>課</label> 
                                </div>
                                <div class="col-md-10">
                                    <?php
                                        echo $this->Form->input('section_no', array('label' => false, 'class'=>'form-control form-level','options' => $section , 'selected'=>$employee[0]['Employee']['section_no'] ));
                                    ?>  
                                </div>
                            </div>
                            <!-- 3RD GROUP -->  
                            <div class="col-md-11" style="margin-top: 40px;">
                                <div class="col-md-2" style="color: #FFF; background-color: #0092C4">
                                    <label>メールアドレス</label> 
                                </div>
                                <div class="col-md-10">
                                    <?php echo $this->Form->input('mail_address', array('label' => false, 'class'=>'form-control')); ?>
                                </div>
                            </div> 
                            <div class="col-md-11" style="margin-top: 10px;">
                                <div class="col-md-2" style="color: #FFF; background-color: #0092C4">
                                    <label>パスワード</label> 
                                </div>
                                <div class="col-md-10">
                                    <?php echo $this->Form->input('new_password', array('label' => false, 'class'=>'form-control', 'type' => 'password')); ?>
                                </div>
                            </div> 
                            
                            <div class="col-md-11" style="margin-top: 10px;">
                                <div class="col-md-2" style="color: #FFF; background-color: #0092C4">
                                    <label>従業員区分</label> 
                                </div>
                                <div class="col-md-10">
                                    <?php echo $this->Form->input('employee_category', array('label'=>false,'class'=>'form-control', 'options'=>$employee_category, 'selected'=>$employee[0]['Employee']['employee_category'])); ?>
                                </div>
                            </div>  

                            <div class="col-md-11" style="margin-top: 10px;">
                                <div class="col-md-2" style="color: #FFF; background-color: #0092C4">
                                    <label>役職</label> 
                                </div>
                                <div class="col-md-10">
                                    <?php echo $this->Form->input('position_no', array('label'=>false,'class'=>'form-control', 'options'=>$position_no, 'required' => true , 'selected'=>$employee[0]['Employee']['position_no'])); ?>
                                </div>
                            </div>  
                            
                            
                            <div class="col-md-11" style="margin-top: 10px;">
                                <div class="col-md-2" style="color: #FFF; background-color: #0092C4">
                                    <label>職責</label> 
                                </div>
                                <div class="col-md-10">
                                    <?php echo $this->Form->input('responsibility', array('label'=>false,'class'=>'form-control', 'options'=>$responsibility, 'selected'=>$employee[0]['Employee']['responsibility'])); ?>
                                </div>
                            </div>  
                            <!-- 4TH GROUP -->              
                            <div class="col-md-11" style="margin-top: 40px;">
                                <div class="col-md-2" style="color: #FFF; background-color: #0092C4">
                                    <label>就業条件調整有り</label> 
                                </div>
                                <div class="col-md-10">
                                    <?php echo $this->Form->checkbox("attendance_adjustment",array('name'=>'adjustAttendance', 'id'=>'adjustAttendance' ,'checked'=>$chk_attendance_adjustment, 'onChange' => 'chgStatusAdjustment()')); ?>
                                    <br><br>
                                </div>
                            </div> 
                            <div class="col-md-11" style="margin-top: 10px;">
                                <div class="col-md-2" style="color: #FFF; background-color: #0092C4; ">
                                    <label>始業時刻</label> 
                                </div>
                                <div class="col-md-2">
                                    <div class="pickerWrap">
                                    	<div class="input-group date timepicker custom-picker" id="timepicker1" data-target-input="nearest">
                                    	    <?php echo $this->Form->input('start_time',array('action'=>'add', 'label' => false,  'name'=>'start_time', 'div' => false, 'class'=>'check_adjustment form-control  datetimepicker-input', 'type' => 'text', 'style' => 'height: 26px;')); ?>
                                    		<div class="input-group-append custom-picker-r" style="" data-target="#timepicker1" data-toggle="datetimepicker">
                                    			<div class="input-group-text"><i class="far fa-clock custom-picker-far"></i></div>
                                    		</div>
                                    	</div>
                                    </div>
                                    <?php //echo $this->Form->input('start_time', array('label' => false, 'class'=>'form-control check_adjustment', 'type' => 'text', 'name'=>'start_time', 'disabled' => true)); ?>
                                </div>
                                <div class="col-md-2" style="color: #FFF; background-color: #0092C4; ">
                                    <label>就業時刻</label> 
                                </div>
                                <div class="col-md-2">
                                    <div class="pickerWrap">
                                    	<div class="input-group date timepicker custom-picker" id="timepicker2" data-target-input="nearest">
                                    	    <?php echo $this->Form->input('closing_time',array('action'=>'add', 'label' => false,  'name'=>'closing_time', 'div' => false, 'class'=>'check_adjustment form-control datetimepicker-input', 'type' => 'text', 'style' => 'height: 26px;')); ?>
                                    		<div class="input-group-append custom-picker-r" style="" data-target="#timepicker2" data-toggle="datetimepicker">
                                    			<div class="input-group-text"><i class="far fa-clock custom-picker-far"></i></div>
                                    		</div>
                                    	</div>
                                    </div>
                                    <?php //echo $this->Form->input('closing_time', array('label' => false, 'class'=>'form-control check_adjustment', 'type' => 'text','name'=>'closing_time', 'disabled' => true)); ?>
                                </div> 
                                
                                <div class="col-md-2" style="color: #FFF; background-color: #0092C4; ">
                                    <label>就業時間</label> 
                                </div>
                                <div class="col-md-2">
                                    <div class="pickerWrap">
                                    	<div class="input-group date timepicker custom-picker" id="timepicker3" data-target-input="nearest">
                                    	    <?php echo $this->Form->input('basic_uptime',array('action'=>'add', 'label' => false,  'name'=>'basic_uptime', 'div' => false, 'class'=>'check_adjustment form-control datetimepicker-input', 'type' => 'text', 'style' => 'height: 26px;')); ?>
                                    		<div class="input-group-append custom-picker-r" style="" data-target="#timepicker3" data-toggle="datetimepicker">
                                    			<div class="input-group-text"><i class="far fa-clock custom-picker-far"></i></div>
                                    		</div>
                                    	</div>
                                    </div>
                                    <?php //echo $this->Form->input('basic_uptime', array('label' => false, 'class'=>'form-control check_adjustment', 'type' => 'text','name'=>'basic_uptime', 'disabled' => true)); ?>
                                </div> 
                            </div>
                            <!-- 5TH GROUP -->  
                            
                            <div class="col-md-11" style="margin-top: 20px;">
                                <div class="col-md-2" style="color: #FFF; background-color: #0092C4">
                                    <label>生年月日</label> 
                                </div>
                                <div class="col-md-10">
                                    <div class="pickerWrap">
                                    	<div class="input-group date datepicker custom-picker" id="datepicker1" data-target-input="nearest">
                                    	    <?php echo $this->Form->input('birthday', array('label' => false, 'div' => false, 'class'=>'form-control datetimepicker-input', 'data-target' => '#datepicker1', 'type' => 'text', 'style' => 'height: 26px;','default'=>  $employee[0]['Employee']['birthday'])); ?>
                                    		<div class="input-group-append custom-picker-r" style="" data-target="#datepicker1" data-toggle="datetimepicker">
                                    			<div class="input-group-text"><i class="far fa-calendar-alt custom-picker-far"></i></div>
                                    		</div>
                                    	</div>
                                    </div> 
                                    <?php //echo $this->Form->input('birthday', array('label' => false, 'div' => false,'style'=>'height: 35px;', 'class'=>'datepicker form-control form-level', 'type' => 'picker','default'=> $employee[0]['Employee']['birthday'] )); ?>
                                </div>
                            </div>
                            <!--<?php //echo $this->Form->input('birthday', array('label' => false, 'div' => false,'style'=>'height: 35px;', 'class'=>'datepicker form-control form-level', 'type' => 'picker','default'=> $employee[0]['Employee']['birthday'] )); ?>-->
                            <div class="col-md-11" style="margin-top: 10px;">
                                <div class="col-md-2" style="color: #FFF; background-color: #0092C4">
                                    <label>入社年月日</label> 
                                </div>
                                <div class="col-md-10 ">
                                     <div class="pickerWrap">
                                    	<div class="input-group date datepicker custom-picker" id="datepicker3" data-target-input="nearest">
                                    	    <?php echo $this->Form->input('job_entry_date', array('label' => false, 'div' => false, 'class'=>'form-control datetimepicker-input', 'data-target' => '#datepicker3', 'type' => 'text', 'style' => 'height: 26px;')); ?>
                                    		<div class="input-group-append custom-picker-r" style="" data-target="#datepicker3" data-toggle="datetimepicker">
                                    			<div class="input-group-text"><i class="far fa-calendar-alt custom-picker-far"></i></div>
                                    		</div>
                                    	</div>
                                    </div> 
                                    <?php //echo $this->Form->input('job_entry_date', array('label' => false, 'class'=>'form-control', 'type' => 'text')); ?>
                                </div>
                            </div>
                            <!-- 6TH GROUP -->
                            
                            <div class="col-md-11" style="margin-top: 40px;">
                                <div class="col-md-2" style="color: #FFF; background-color: #0092C4">
                                    <label>休職中</label> 
                                </div>
                                
                                <div class="col-md-3">
                                    <?php echo $this->Form->checkbox('on_leave', array('id' => 'on_leave', 'checked'=>$chk_on_leave)); ?>
                                    <br><br>
                                </div>
                            </div> 
                            
                            <div><?php echo $this->Form->input('leave_of_absences_idx_max', array('label' => false, 'type' => 'hidden', 'class'=>'form-control', 'id' => 'leave-of-absences-idx-max', 'value' => count($leaveOfAbsences) == 0 ? 1 : count($leaveOfAbsences))); ?></div>

                            <div class="col-md-12" style="margin-top: 10px; display:none;" id="base-leave-of-absences">
                                <div class="col-md-1" style="color: #FFF; background-color: #0092C4; ">
                                    <label>休職</label> 
                                </div>
                                <?php echo $this->Form->input('leave_of_absence_id_XXXX', array('type' => 'hidden', 'label' => false, 'value' => '')); ?>

                                <div class="col-md-2 inputRender">
                                    <?php echo $this->Form->input('leave_of_absence_type_XXXX', array('label' => false, 'class' => 'form-control', 'options' => $leaveoptions, 'selected' => null)); ?>
                                </div>
                                <div class="col-md-1" style="color: #FFF; background-color: #0092C4; ">
                                    <label>期間FROM</label> 
                                </div>
                                    
                                <div class="col-md-2 inputRender">
                                   <div class="pickerWrap">
                                    	<div class="input-group date datepicker custom-picker" id="start_date_XXXX" data-target-input="nearest">
                                    	    <?php echo $this->Form->input('start_date_XXXX', array('label' => false, 'div' => false, 'class'=>'form-control datetimepicker-input', 'data-target' => '#start_date_XXXX', 'type' => 'text', 'style' => 'height: 26px;')); ?>
                                    		<div class="input-group-append custom-picker-r" style="" data-target="#start_date_XXXX" data-toggle="datetimepicker">
                                    			<div class="input-group-text"><i class="far fa-calendar-alt custom-picker-far"></i></div>
                                    		</div>
                                    	</div>
                                    </div>  
                                    <?php  //echo $this->Form->input('start_date_XXXX', array('label' => false, 'div' => false,'style'=>'height: 35px;', 'class'=>'datepicker form-control check_leave','type' => 'picker')); ?>
                                </div> 
                                    
                                <div class="col-md-1" style="color: #FFF; background-color: #0092C4; ">
                                    <label>期間TO</label> 
                                </div>
                                    
                                <div class="col-md-2 inputRender">
                                    <div class="pickerWrap">
                                    	<div class="input-group date datepicker custom-picker" id="end_date_XXXX" data-target-input="nearest">
                                    	    <?php echo $this->Form->input('end_date_XXXX', array('label' => false, 'div' => false, 'class'=>'form-control datetimepicker-input', 'data-target' => '#end_date_XXXX', 'type' => 'text', 'style' => 'height: 26px;')); ?>
                                    		<div class="input-group-append custom-picker-r" style="" data-target="#end_date_XXXX" data-toggle="datetimepicker">
                                    			<div class="input-group-text"><i class="far fa-calendar-alt custom-picker-far"></i></div>
                                    		</div>
                                    	</div>
                                    </div>  
                                    <?php  //echo $this->Form->input('end_date_XXXX', array('label' => false, 'div' => false,'style'=>'height: 35px;', 'class'=>'datepicker form-control', 'type' => 'picker')); ?>
                                </div>

                                <div class="col-md-1" style="color: #FFF; background-color: #0092C4; ">
                                    <label>復帰日</label> 
                                </div>
                                    
                                <div class="col-md-2 inputRender">
                                    <div class="pickerWrap">
                                    	<div class="input-group date datepicker custom-picker" id="return_date_XXXX" data-target-input="nearest">
                                    	    <?php echo $this->Form->input('return_date_XXXX', array('label' => false, 'div' => false, 'class'=>'form-control datetimepicker-input', 'data-target' => '#return_date_XXXX', 'type' => 'text', 'style' => 'height: 26px;')); ?>
                                    		<div class="input-group-append custom-picker-r" style="" data-target="#return_date_XXXX" data-toggle="datetimepicker">
                                    			<div class="input-group-text"><i class="far fa-calendar-alt custom-picker-far"></i></div>
                                    		</div>
                                    	</div>
                                    </div> 
                                    <?php  //echo $this->Form->input('return_date_XXXX', array('label' => false, 'div' => false,'style'=>'height: 35px;', 'class'=>'datepicker form-control', 'type' => 'picker')); ?>
                                </div>
                            </div>

                            <div id="leave-of-absences">

                                <?php 
                                    for ($idx = 0; $idx == 0 || $idx < count($leaveOfAbsences); $idx++) {
                                        $enableEempty = ($idx == 0 && count($leaveOfAbsences) == 0);
                                        $nameIdx = $idx + 1;
                                ?>
                                    <div class="col-md-12" style="margin-top: 10px;">
                                        <div class="col-md-1" style="color: #FFF; background-color: #0092C4; ">
                                            <label>休職</label> 
                                        </div>
    
                                        <?php echo $this->Form->input('leave_of_absence_id_' . $nameIdx, array('type' => 'hidden', 'label' => false, 'value' => $enableEempty ? '' : $leaveOfAbsences[$idx]['LeaveOfAbsence']['id'])); ?>
                                        <div class="col-sm-2 ">
                                            <?php echo $this->Form->input('leave_of_absence_type_' . $nameIdx, array('label' => false, 'class' => 'form-control', 'options' => $leaveoptions, 'selected' => $enableEempty ? null : $leaveOfAbsences[$idx]['LeaveOfAbsence']['leave_of_absence_type'])); ?>
                                        </div>
                                        <div class="col-sm-1" style="color: #FFF; background-color: #0092C4; ">
                                            <label>期間FROM</label> 
                                        </div>
                                            
                                        <div class="col-md-2 inputRender">
                                             <div class="pickerWrap">
                                            	<div class="input-group date datepicker custom-picker" id="<?php echo $nameIdx?>_picker_1" data-target-input="nearest">
                                            	    <?php echo $this->Form->input('start_date_' . $nameIdx, array('label' => false, 'div' => false, 'class'=>'form-control datetimepicker-input', 'data-target' => '#'.$nameIdx.'_picker_1', 'type' => 'text', 'style' => 'height: 26px;', 'value' => $enableEempty ? '' : $leaveOfAbsences[$idx]['LeaveOfAbsence']['start_date'])); ?>
                                            		<div class="input-group-append custom-picker-r" style="" data-target="#<?php echo $nameIdx?>_picker_1" data-toggle="datetimepicker">
                                            			<div class="input-group-text"><i class="far fa-calendar-alt custom-picker-far"></i></div>
                                            		</div>
                                            	</div>
                                            </div> 
                                            <?php  //echo $this->Form->input('start_date_' . $nameIdx, array('label' => false, 'div' => false,'style'=>'height: 35px;', 'class'=>'datepicker form-control','type' => 'picker','value' => $enableEempty ? '' : $leaveOfAbsences[$idx]['LeaveOfAbsence']['start_date'] )); ?>
                                        </div> 
                                            
                                        <div class="col-md-1" style="color: #FFF; background-color: #0092C4; ">
                                            <label>期間TO</label> 
                                        </div>
                                            
                                        <div class="col-md-2 inputRender">
                                            <div class="pickerWrap">
                                            	<div class="input-group date datepicker custom-picker" id="<?php echo $nameIdx?>_picker_2" data-target-input="nearest">
                                            	    <?php echo $this->Form->input('end_date_' . $nameIdx, array('label' => false, 'div' => false, 'class'=>'form-control datetimepicker-input', 'data-target' => '#'.$nameIdx.'_picker_2', 'type' => 'text', 'style' => 'height: 26px;', 'value' => $enableEempty ? '' : $leaveOfAbsences[$idx]['LeaveOfAbsence']['end_date'])); ?>
                                            		<div class="input-group-append custom-picker-r" style="" data-target="#<?php echo $nameIdx?>_picker_2" data-toggle="datetimepicker">
                                            			<div class="input-group-text"><i class="far fa-calendar-alt custom-picker-far"></i></div>
                                            		</div>
                                            	</div>
                                            </div> 
                                            <?php  //echo $this->Form->input('end_date_' . $nameIdx, array('label' => false, 'div' => false,'style'=>'height: 35px;', 'class'=>'datepicker form-control', 'type' => 'picker','value' => $enableEempty ? '' : $leaveOfAbsences[$idx]['LeaveOfAbsence']['end_date'] )); ?>
                                        </div>
    
                                        <div class="col-md-1" style="color: #FFF; background-color: #0092C4; ">
                                            <label>復帰日</label> 
                                        </div>
                                            
                                        <div class="col-md-2 inputRender ">
                                             <div class="pickerWrap">
                                            	<div class="input-group date datepicker custom-picker" id="<?php echo $nameIdx?>_picker_3" data-target-input="nearest">
                                            	    <?php echo $this->Form->input('return_date_' . $nameIdx, array('label' => false, 'div' => false, 'class'=>'form-control datetimepicker-input', 'data-target' => '#'.$nameIdx.'_picker_3', 'type' => 'text', 'style' => 'height: 26px;', 'value' => $enableEempty ? '' : $leaveOfAbsences[$idx]['LeaveOfAbsence']['return_date'])); ?>
                                            		<div class="input-group-append custom-picker-r" data-target="#<?php echo $nameIdx?>_picker_3" data-toggle="datetimepicker">
                                            			<div class="input-group-text"><i class="far fa-calendar-alt custom-picker-far"></i></div>
                                            		</div>
                                            	</div>
                                            </div>
                                            <?php  //echo $this->Form->input('return_date_' . $nameIdx, array('label' => false, 'div' => false,'style'=>'height: 35px;', 'class'=>'datepicker form-control', 'type' => 'picker','value' => $enableEempty ? '' : $leaveOfAbsences[$idx]['LeaveOfAbsence']['return_date'] )); ?>
                                        </div>
                                    </div>
                                            
                                <?php
                                    }
                                ?>
                            </div>                            
                       
                            <div class="col-md-12">
                                <a href='#' onClick="addLeaveOfAbsences(); return false;">休職を追加</a>
                            </div>
                            
                            <!-- 7TH GROUP -->              
                            <div class="col-md-11" style="margin-top: 40px;">
                                <div class="col-md-2" style="color: #FFF; background-color: #0092C4">
                                    <label>退職</label> 
                                </div>
                                
                                <div class="col-md-1">
                                    <?php echo $this->Form->checkbox("retirement", array('id' => 'retirement_checkbox', 'checked'=>$chk_retirement, 'onChange' => 'chgStatusRetirement()')); ?>
                                </div>
                            </div> 
                            
                            <div class="col-md-11" style="margin-top: 10px;">
                                <div class="col-md-2" style="color: #FFF; background-color: #0092C4; ">
                                    <label>退職日</label> 
                                </div>  
                                <div class="col-md-10">
                                     <div class="pickerWrap">
                                    	<div class="input-group date datepicker custom-picker" id="datepicker2" data-target-input="nearest">
                                    	    <?php echo $this->Form->input('retirement_date', array('label' => false, 'div' => false,'class'=>'form-control datetimepicker-input check_retirement', 'data-target' => '#datepicker2', 'type' => 'text', 'style' => 'height: 26px;','default'=>  $employee[0]['Employee']['retirement_date'])); ?>
                                    		<div class="input-group-append custom-picker-r"  data-target="#datepicker2" data-toggle="datetimepicker">
                                    			<div class="input-group-text"><i class="far fa-calendar-alt custom-picker-far"></i></div>
                                    		</div>
                                    	</div>
                                    </div> 
                                    <?php //echo $this->Form->input('retirement_date', array('label' => false, 'class'=>'form-control check_retirement datepicker', 'type' => 'text')); ?>
                                </div> 
                            </div>
                            <!-- 8TH GROUP -->  
                            <div class="col-md-11" style="margin-top: 20px;">
                                <div class="col-md-2" style="color: #FFF; background-color: #0092C4">
                                    <label>ICカードキー</label> 
                                </div>
                                <div class="col-md-10">
                                    <?php echo $this->Form->input('ic_card_key', array('label' => false, 'class'=>'form-control', 'type' => 'text')); ?>
                                </div>
                            </div> 
                            
                            <!--
                    	    <div class="col-md-11" style="margin-top:20px;">
                    	    	<div class="col-md-2" style="color: #FFF; background-color: #0092C4;">
                                	<label>説明</label> 
                                </div>
						    	<div class="col-md-6">
                                	<?php 
						    	echo $this->Form->input('description', array('label' => false, 'class'=>'form-control')); ?>
                                </div>
                    	    </div>
                    	    <div class="col-md-11" style="margin-top: 20px;">
                    	    	<div class="col-md-2" style="color: #FFF; background-color: #0092C4;">
                                	<label>有効</label> 
                                </div>
                                <div class="col-md-2">
                                	<?php $options = array('有効', '無効');
						    		echo $this->Form->input('effectiveness', array('label' => false, 'class'=>'form-control', 'options' => $options)); ?>  
                                </div>
						    </div> -->
						    </div>
                            <!-- BUTTON -->  
                            <div class="contentRow">
                                <div class="col-md-5" style="margin-top: 20px;">
                                    <?php echo $this->Form->button('登録', array('class'=>'btn btn-default','style' => 'width:100%; background: #0092C4;color:#FFF ;')); ?>
                                </div>
                            </div>
                            
                        </font>
                    </div>
                   <?php echo $this->Form->end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END CONTAINER --> 

<style>
label {
	margin-top:8px;
}

.form-level{
	margin-top:-1px;
}
.form-level1{
	margin-top:23px;
}
.form-level2{
    margin-top:22px;
}
</style>


<?php echo $this->Html->script('/js/Users/edit.js'); ?>
