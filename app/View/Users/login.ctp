<?php echo $this->assign('title', $title); ?>
<h3>ログイン</h3>
<div class="errerMessage" style="display:block;"><?php echo $this->Session->flash(); ?></div>
<?php echo $this->Form->create('Employee', ["name" => "login_form"]); ?>
<div class="panelBody">
	<label>メールアドレス</label>
	<div class="row input">
		<?php echo $this->Form->input('mail_address', array('label'=>false, 'id'=>'UserEmail', 'type'=>'email', 'div' => false)); ?>
	</div>
	<label>パスワード</label>
	<div class="row input">
		<?php echo $this->Form->input('password', array('label'=>false, 'id'=>'UserPassword', 'type'=>'password', 'div' => false)); ?>
	</div>
	<div class="row checkbox">
		<label><label class="checkbox-inline"><?php echo $this->Form->input('autologin', array('label'=>false, 'div'=>false, 'id'=>'autologin', 'type'=>'checkbox', 'class'=>'autologin')); ?> ログイン情報を記憶する</label>
	</div>
	<div class="btnArea">
		<?php echo $this->Html->link(__('ログイン'), ['action' => ''],['class' => 'btn btn-blue loginBtn', 'onClick' => 'login_form.submit(); return false;']); ?>
	</div>
	<div class="btnArea">
		&nbsp
	</div>
	<div class="btnArea">
		
		<?php echo $this->Html->link(__('パスワードを変更する'), ['action' => 'change_password']); ?>	
	</div>
	
</div>
<?php echo $this->Form->end(); ?>
