
<?php echo $this->assign('title', $title); ?>
<div class="users view">
<h3><?php echo __('ユーザー'); ?></h3>
</br>
</br>
	<dl>
		<dt><?php echo __('ID'); ?></dt></br>
		<dd>
			<?php echo h($user['User']['id']); ?>
			&nbsp;
		</dd>
		
		<dt><?php echo __('苗字'); ?></dt></br>
		<dd>
			<?php echo h($user['User']['last_name']); ?>
			&nbsp;
		</dd>
		
		<dt><?php echo __('ファーストネーム'); ?></dt></br>
		<dd>
			<?php echo h($user['User']['first_name']); ?>
			&nbsp;
		</dd>
		
		<dt><?php echo __('会社名'); ?></dt></br>
		<dd>
			<?php echo h($user['User']['company_name']); ?>
			&nbsp;
		</dd>
		
		<dt><?php echo __('メールアドレス'); ?></dt></br>
		<dd>
			<?php echo h($user['User']['email']); ?>
			&nbsp;
		</dd>
		
		<dt><?php echo __('パスワード'); ?></dt></br>
		<dd>
			<?php echo h($user['User']['password']); ?>
			&nbsp;
		</dd>
		
		<dt><?php echo __('役割'); ?></dt></br>
		<dd>
			<?php echo h($user['User']['role']); ?>
			&nbsp;
		</dd>
		
		<dt><?php echo __('効果的な部門'); ?></dt></br>
		<dd>
			<?php echo h($user['User']['effective_div']); ?>
			&nbsp;
		</dd>
		
		<dt><?php echo __('備考'); ?></dt></br>
		<dd>
			<?php echo h($user['User']['remarks']); ?>
			&nbsp;
		</dd>
		
		<dt><?php echo __('削除済み'); ?></dt></br>
		<dd>
			<?php echo h($user['User']['deleted']); ?>
			&nbsp;
		</dd>
		
		<dt><?php echo __('削除された日付'); ?></dt></br>
		<dd>
			<?php echo h($user['User']['deleted_date']); ?>
			&nbsp;
		</dd>
		
		<dt><?php echo __('作成した'); ?></dt></br>
		<dd>
			<?php echo h($user['User']['created']); ?>
			&nbsp;
		</dd>
		
		<dt><?php echo __('変更された'); ?></dt></br>
		<dd>
			<?php echo h($user['User']['modified']); ?>
			&nbsp;
		</dd>
		
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('アクション'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('ユーザーの編集'), array('action' => 'edit', $user['User']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('ユーザーを削除'), array('action' => 'delete', $user['User']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $user['User']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('リストユーザー'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('新しいユーザー'), array('action' => 'add')); ?> </li>
	</ul>
</div>
