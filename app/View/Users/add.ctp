<!-- BEGIN CONTAINER -->
<?php //echo $this->assign('title', $title); ?>
<div class="page-container">
    <!-- BEGIN CONTENT -->
    <div class="row">
		<div class="html-render ng-binding ng-scope">
        <!-- BEGIN PAGE CONTENT-->
            <div class="row">
				<div class="col-md-11">
            <!-- Start Operation total -->
            <div class="contentRow">
                    	<div><?php echo $this->Form->create(); ?></div>
                    		<div class="col-md-9">
                    			<h3>部門詳細ユーザー詳細細</h3>	
                    		</div>
                    		<div class="col-md-2 form-level1">
                    			<a onclick = 'window.history.back()' class="btn btn-default" style="width:100%; background: #91A3AE;color:#FFF;">戻る</a>
                    		</div>
                    </div>
                    
                        <!-- 1st GROUP -->  
                      <div><?php echo $this->Form->create('Employee'); ?></div>
                        <div class="contentRow">
                        <font size="2px">        
                            <div class="col-md-11" style="margin-top: 10px;">
                                <div class="col-md-2" style="color: #FFF; background-color: #0092C4">
                                    <label>姓</label> 
                                </div>
                                <div class="col-md-10">
                                    <?php echo $this->Form->input('last_name', array('label' => false, 'class'=>'form-control', 'type' => 'text')); ?>
                                </div>
                            </div>
                            
                            <div class="col-md-11" style="margin-top: 10px;">
                                <div class="col-md-2" style="color: #FFF; background-color: #0092C4">
                                    <label>名</label> 
                                </div>
                                <div class="col-md-10">
                                    <?php echo $this->Form->input('first_name', array('label' => false, 'class'=>'form-control', 'type' => 'text')); ?>
                                </div>
                            </div>
                            <!-- 2ND GROUP -->  
                            <div class="col-md-11" style="margin-top: 40px;">
                                <div class="col-md-2" style="color: #FFF; background-color: #0092C4">
                                    <label>事業部</label> 
                                </div>
                                <div class="col-md-10">
                                    <div class="input-box">
                                        <?php
                                        echo $this->Form->input('division_no', array('label' => false, 'class'=>'form-control', 'options'=>$divisionLists));
                                        ?>  
                                    </div>  
                                </div>
                            </div> 
                            
                            <div class="col-md-11" style="margin-top: 10px;">
                                <div class="col-md-2" style="color: #FFF; background-color: #0092C4">
                                    <label>部</label> 
                                </div>
                                <div class="col-md-10">
                                    <div class="input-box">
                                    <?php
                                    echo $this->Form->input('department_no', array('label' => false, 'class'=>'form-control','options'=>$departmentLists));
                                    ?>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-md-11" style="margin-top: 10px;">
                                <div class="col-md-2" style="color: #FFF; background-color: #0092C4">
                                    <label>課</label> 
                                </div>
                                <div class="col-md-10">
                                    <div class="input-box">
                                        <?php
                                        echo $this->Form->input('section_no', array('label' => false, 'class'=>'form-control','options'=>$sectionLists));
                                        ?> 
                                    </div>
                                </div>
                            </div> 
                            
                            <!-- 3RD GROUP -->  
                            <div class="col-md-11" style="margin-top: 40px;">
                                <div class="col-md-2" style="color: #FFF; background-color: #0092C4">
                                    <label>メールアドレス</label> 
                                </div>
                                <div class="col-md-10">
                                    <?php echo $this->Form->input('mail_address', array('label' => false, 'class'=>'form-control', 'type' => 'text')); ?>
                                </div>
                            </div> 
                            
                            <div class="col-md-11" style="margin-top: 10px;">
                                <div class="col-md-2" style="color: #FFF; background-color: #0092C4">
                                    <label>パスワード</label> 
                                </div>
                                <div class="col-md-10">
                                    <?php echo $this->Form->input('password', array('label' => false, 'class'=>'form-control', 'type' => 'password')); ?>
                                </div>
                            </div> 

                            <div class="col-md-11" style="margin-top: 10px;">
                                <div class="col-md-2" style="color: #FFF; background-color: #0092C4">
                                    <label>従業員区分</label> 
                                </div>
                                <div class="col-md-10">
                                    <div class="input-box">
                                        <?php
                                        echo $this->Form->input('employee_category', array('label' => false, 'class'=>'form-control','options'=>$employee_category));
                                        ?>
                                    </div>
                                </div>
                            </div>  

                            <div class="col-md-11" style="margin-top: 10px;">
                                <div class="col-md-2" style="color: #FFF; background-color: #0092C4">
                                    <label>役職</label> 
                                </div>
                                <div class="col-md-10">
                                    <div class="input-box">
                                        <?php
                                        echo $this->Form->input('position_no', array('label' => false, 'class'=>'form-control','options'=>$position_no, 'required' => true));
                                        ?>
                                    </div>
                                </div>
                            </div>  
                            
                            <div class="col-md-11" style="margin-top: 10px;">
                                <div class="col-md-2" style="color: #FFF; background-color: #0092C4">
                                    <label>職責</label> 
                                </div>
                                <div class="col-md-10">
                                    <div class="input-box">
                                        <?php
                                        echo $this->Form->input('responsibility', array('label' => false, 'class'=>'form-control','options'=>$responsibility));
                                        ?>
                                    </div>
                                </div>
                            </div>  

                            <!-- 4TH GROUP -->              
                            <div class="col-md-11" style="margin-top: 40px;">
                                <div class="col-md-2" style="color: #FFF; background-color: #0092C4">
                                    <label>就業条件調整有り</label> 
                                </div>
                                <div class="col-md-1">
                                    <?php echo $this->Form->checkbox("attendance_adjustment",  array('id'=>'adjustAttendance' , 'checked' => false)); ?>
                                </div>
                            </div> 

                            <div class="col-md-11" style="margin-top: 10px;">
                                <div class="col-md-2" style="color: #FFF; background-color: #0092C4; ">
                                    <label>始業時刻</label> 
                                </div>
                                <div class="col-md-2">
                                    <div class="pickerWrap">
                                    	<div class="input-group date timepicker custom-picker" id="timepicker1" data-target-input="nearest">
                                    	    <?php echo $this->Form->input('start_time',array('action'=>'add', 'label' => false, 'div' => false, 'class'=>'check_adjustment form-control  datetimepicker-input', 'type' => 'text', 'style' => 'height: 26px;')); ?>
                                    		<div class="input-group-append custom-picker-r" data-target="#timepicker1" data-toggle="datetimepicker">
                                    			<div class="input-group-text"><i class="far fa-clock custom-picker-far"></i></div>
                                    		</div>
                                    	</div>
                                    </div>
                                    <?php //echo $this->Form->input('start_time', array('label' => false, 'class'=>'form-control check_adjustment', 'type' => 'TIME' , 'disabled' => true)); ?>
                                </div>
                                
                                <div class="col-md-2" style="color: #FFF; background-color: #0092C4; ">
                                    <label>就業時刻</label> 
                                </div>
                                <div class="col-md-2">
                                     <div class="pickerWrap">
                                    	<div class="input-group date timepicker custom-picker" id="timepicker2" data-target-input="nearest">
                                    	    <?php echo $this->Form->input('closing_time',array('action'=>'add', 'label' => false, 'div' => false, 'class'=>'check_adjustment form-control  datetimepicker-input ', 'type' => 'text', 'style' => 'height: 26px;')); ?>
                                    		<div class="input-group-append custom-picker-r" data-target="#timepicker2" data-toggle="datetimepicker">
                                    			<div class="input-group-text"><i class="far fa-clock custom-picker-far"></i></div>
                                    		</div>
                                    	</div>
                                    </div>
                                    <?php //echo $this->Form->input('closing_time', array('label' => false, 'class'=>'form-control check_adjustment', 'type' => 'TIME' , 'disabled' => true)); ?>
                                </div> 
                                
                                <div class="col-md-2" style="color: #FFF; background-color: #0092C4; ">
                                    <label>就業時間</label> 
                                </div>
                                <div class="col-md-2">
                                    <div class="pickerWrap">
                                    	<div class="input-group date timepicker custom-picker" id="timepicker3" data-target-input="nearest">
                                    	    <?php echo $this->Form->input('basic_uptime',array('action'=>'add', 'label' => false, 'div' => false, 'class'=>'check_adjustment form-control datetimepicker-input', 'type' => 'text', 'style' => 'height: 26px;')); ?>
                                    		<div class="input-group-append custom-picker-r" data-target="#timepicker3" data-toggle="datetimepicker">
                                    			<div class="input-group-text"><i class="far fa-clock custom-picker-far"></i></div>
                                    		</div>
                                    	</div>
                                    </div>
                                    <?php //echo $this->Form->input('basic_uptime', array('label' => false, 'class'=>'form-control check_adjustment', 'type' => 'TIME' , 'disabled' => true)); ?>
                                </div> 
                            </div>
                            <!-- 5TH GROUP -->  
                            <div class="col-md-11" style="margin-top: 40px;">
                                <div class="col-md-2" style="color: #FFF; background-color: #0092C4">
                                    <label>生年月日</label> 
                                </div>
                                <div class="col-md-10">
                                    <div class="pickerWrap">
                                    	<div class="input-group date datepicker custom-picker" id="datepicker1" data-target-input="nearest">
                                    	    <?php echo $this->Form->input('birthday', array('label' => false, 'div' => false, 'class'=>'form-control datetimepicker-input', 'data-target' => '#datepicker1', 'type' => 'text', 'style' => 'height: 26px;')); ?>
                                    		<div class="input-group-append custom-picker-r" data-target="#datepicker1" data-toggle="datetimepicker">
                                    			<div class="input-group-text"><i class="far fa-calendar-alt custom-picker-far"></i></div>
                                    		</div>
                                    	</div>
                                    </div>
                                    <?php //echo $this->Form->input('birthday', array('label' => false, 'div' => false,'style'=>'height: 35px;', 'class'=>'datepicker form-control ', 'type' => 'DATE' )); ?>
                                </div>
                            </div>
                            
                            <div class="col-md-11" style="margin-top: 10px;">
                                <div class="col-md-2" style="color: #FFF; background-color: #0092C4">
                                    <label>入社年月日</label> 
                                </div>
                                <div class="col-md-10">
                                    <div class="pickerWrap">
                                    	<div class="input-group date datepicker custom-picker" id="datepicker2" data-target-input="nearest">
                                    	    <?php echo $this->Form->input('job_entry_date', array('label' => false, 'div' => false, 'class'=>'form-control datetimepicker-input', 'data-target' => '#datepicker2', 'type' => 'text', 'style' => 'height: 26px;')); ?>
                                    		<div class="input-group-append custom-picker-r" data-target="#datepicker2" data-toggle="datetimepicker">
                                    			<div class="input-group-text"><i class="far fa-calendar-alt custom-picker-far"></i></div>
                                    		</div>
                                    	</div>
                                    </div>
                                    <?php //echo $this->Form->input('job_entry_date', array('label' => false, 'class'=>'datepicker form-control ', 'type' => 'DATE')); ?>
                                </div>
                            </div>
                            <!-- 6TH GROUP -->              
                            <div class="col-md-11" style="margin-top: 40px;">
                                <div class="col-md-2" style="color: #FFF; background-color: #0092C4">
                                    <label>休職中</label> 
                                </div>                        
                                <div class="col-md-2">
                                    <?php echo $this->Form->checkbox("on_leave", ['id' => 'on_leave','checked' => false]); ?>
                                </div>
                            </div>   
                            
                            <div><?php $leaveOfAbsences = 0; echo $this->Form->input('leave_of_absences_idx_max', array('label' => false, 'type' => 'hidden', 'class'=>'form-control', 'id' => 'leave-of-absences-idx-max', 'value' => count($leaveOfAbsences) == 0 ? 1 : count($leaveOfAbsences))); ?></div>
                            
                            <div class="col-md-11" style="margin-top: 10px; display:none;" id="base-leave-of-absences">
                                <div class="col-md-2" style="color: #FFF; background-color: #0092C4; ">
                                    <label>休職</label> 
                                </div>
                                <?php echo $this->Form->input('leave_of_absence_id_XXXX', array('type' => 'hidden', 'label' => false, 'value' => '')); ?>

                                <div class="col-md-2">
                                  <div class="input-box">
                                    <?php echo $this->Form->input('leave_of_absence_type_XXXX', array('label' => false, 'class' => 'form-control', 'options' => $leaveoptions, 'selected' => null)); ?>
                                  </div>
                                </div>
                                <div class="col-md-2" style="color: #FFF; background-color: #0092C4; ">
                                    <label>期間FROM</label> 
                                </div>
                                    
                                <div class="col-md-2">
                                   <div class="pickerWrap">
                                    	<div class="input-group date datepicker custom-picker" id="start_date_XXXX" data-target-input="nearest">
                                    	    <?php echo $this->Form->input('start_date_XXXX', array('label' => false, 'div' => false, 'class'=>'form-control datetimepicker-input', 'data-target' => '#start_date_XXXX', 'type' => 'text', 'style' => 'height: 26px;')); ?>
                                    		<div class="input-group-append custom-picker-r" style="" data-target="#start_date_XXXX" data-toggle="datetimepicker">
                                    			<div class="input-group-text"><i class="far fa-calendar-alt custom-picker-far"></i></div>
                                    		</div>
                                    	</div>
                                    </div>  
                                    <?php  //echo $this->Form->input('start_date_XXXX', array('label' => false, 'div' => false,'style'=>'height: 35px;', 'class'=>'datepicker form-control check_leave','type' => 'picker')); ?>
                                </div> 
                                    
                                <div class="col-md-2" style="color: #FFF; background-color: #0092C4; ">
                                    <label>期間TO</label> 
                                </div>
                                    
                                <div class="col-md-2 inputRender">
                                    <div class="pickerWrap">
                                    	<div class="input-group date datepicker custom-picker" id="end_date_XXXX" data-target-input="nearest">
                                    	    <?php echo $this->Form->input('end_date_XXXX', array('label' => false, 'div' => false, 'class'=>'form-control datetimepicker-input', 'data-target' => '#end_date_XXXX', 'type' => 'text', 'style' => 'height: 26px;')); ?>
                                    		<div class="input-group-append custom-picker-r" style="" data-target="#end_date_XXXX" data-toggle="datetimepicker">
                                    			<div class="input-group-text"><i class="far fa-calendar-alt custom-picker-far"></i></div>
                                    		</div>
                                    	</div>
                                    </div>  
                                    <?php  //echo $this->Form->input('end_date_XXXX', array('label' => false, 'div' => false,'style'=>'height: 35px;', 'class'=>'datepicker form-control', 'type' => 'picker')); ?>
                                </div>

                                <!--<div class="col-md-1" style="color: #FFF; background-color: #0092C4; ">-->
                                <!--    <label>復帰日</label> -->
                                <!--</div>-->
                                    
                                <!--<div class="col-md-2 inputRender">-->
                                <!--    <div class="pickerWrap">-->
                                <!--    	<div class="input-group date datepicker custom-picker" id="return_date_XXXX" data-target-input="nearest">-->
                                <!--    	    <?php //echo $this->Form->input('return_date_XXXX', array('label' => false, 'div' => false, 'class'=>'form-control datetimepicker-input', 'data-target' => '#return_date_XXXX', 'type' => 'text', 'style' => 'height: 26px;')); ?>-->
                                <!--    		<div class="input-group-append custom-picker-r" style="" data-target="#return_date_XXXX" data-toggle="datetimepicker">-->
                                <!--    			<div class="input-group-text"><i class="far fa-calendar-alt custom-picker-far"></i></div>-->
                                <!--    		</div>-->
                                <!--    	</div>-->
                                <!--    </div> -->
                                    <?php  //echo $this->Form->input('return_date_XXXX', array('label' => false, 'div' => false,'style'=>'height: 35px;', 'class'=>'datepicker form-control', 'type' => 'picker')); ?>
                                <!--</div>-->
                            </div>
                            
                            <div id="leave-of-absences">
                                <?php 
                                $leaveOfAbsences = 0;
                                    for ($idx = 0; $idx == 0 || $idx < count($leaveOfAbsences); $idx++) {
                                        $enableEempty = ($idx == 0 && count($leaveOfAbsences) == 0);
                                        $nameIdx = $idx + 1;
                                ?>
                            
                            <div class="col-md-11" style="margin-top: 10px;">
                                <div class="col-md-2" style="color: #FFF; background-color: #0092C4; ">
                                    <label>休職</label> 
                                </div>
                                <div class="col-md-2">
                                    <div class="input-box">
                                        <?php echo $this->Form->input('leave_of_absence_type', array('label'=>false,'class'=>'form-control check_leave', 'options'=>$leaveOptions , 'disabled' => true)); ?>
                                    </div>
                                </div>
                            
                                <div class="col-md-2" style="color: #FFF; background-color: #0092C4; ">
                                    <label>期間FROM</label> 
                                </div>
                                <div class="col-md-2">
                                    <div class="pickerWrap">
                                    	<div class="input-group date datepicker custom-picker" id="datepicker3" data-target-input="nearest">
                                    	    <?php echo $this->Form->input('start_date', array('label' => false, 'div' => false, 'class'=>'form-control datetimepicker-input check_leave', 'data-target' => '#datepicker3', 'type' => 'text', 'style' => 'height: 26px;')); ?>
                                    		<div class="input-group-append custom-picker-r" data-target="#datepicker3" data-toggle="datetimepicker">
                                    			<div class="input-group-text"><i class="far fa-calendar-alt custom-picker-far"></i></div>
                                    		</div>
                                    	</div>
                                    </div>
                                    <?php //echo $this->Form->input('start_date', array('label' => false, 'div' => false,'style'=>'height: 35px;', 'class'=>'datepicker form-control check_leave', 'type' => 'DATE', 'disabled' => true )); ?>
                                </div> 
                                
                                <div class="col-md-2" style="color: #FFF; background-color: #0092C4; ">
                                    <label>期間TO</label> 
                                </div>
                                <div class="col-md-2">
                                    <div class="pickerWrap">
                                    	<div class="input-group date datepicker custom-picker" id="datepicker4" data-target-input="nearest">
                                    	    <?php echo $this->Form->input('end_date', array('label' => false, 'div' => false, 'class'=>'form-control datetimepicker-input check_leave', 'data-target' => '#datepicker4', 'type' => 'text', 'style' => 'height: 26px;')); ?>
                                    		<div class="input-group-append custom-picker-r" data-target="#datepicker4" data-toggle="datetimepicker">
                                    			<div class="input-group-text"><i class="far fa-calendar-alt custom-picker-far"></i></div>
                                    		</div>
                                    	</div>
                                    </div>
                                    <?php //echo $this->Form->input('end_date', array('label' => false, 'div' => false,'style'=>'height: 35px;', 'class'=>'datepicker form-control check_leave', 'type' => 'DATE', 'disabled' => true )); ?>
                                </div> 
                            </div>
                                <?php
                                    }
                                ?>
                            </div> 
                            
                            <!--<div class="col-md-12">-->
                            <!--    <a href='#' onClick="addLeaveOfAbsences(); return false;">休職を追加</a>-->
                            <!--</div>-->
                            <!-- 7TH GROUP -->              
                            <div class="col-md-11" style="margin-top: 40px;">
                                <div class="col-md-2" style="color: #FFF; background-color: #0092C4">
                                    <label>退職</label> 
                                </div>
                                <div class="col-md-1">
                                    <?php echo $this->Form->checkbox("retirement", ['id' => 'retirement_checkbox', 'checked' => false]); ?>
                                </div>
                            </div> 
                            
                            <div class="col-md-11" style="margin-top: 10px;">
                                <div class="col-md-2" style="color: #FFF; background-color: #0092C4; ">
                                    <label>退職日</label> 
                                </div>  
                                <div class="col-md-10">
                                    <div class="pickerWrap">
                                    	<div class="input-group date datepicker custom-picker" id="datepicker5" data-target-input="nearest">
                                    	    <?php echo $this->Form->input('retirement_date', array('label' => false, 'div' => false, 'class'=>'form-control datetimepicker-input', 'data-target' => '#datepicker5', 'type' => 'text','style' => 'height: 26px;')); ?>
                                    		<div class="input-group-append custom-picker-r" data-target="#datepicker5" data-toggle="datetimepicker">
                                    			<div class="input-group-text"><i class="far fa-calendar-alt custom-picker-far"></i></div>
                                    		</div>
                                    	</div>
                                    </div>
                                    <?php //echo $this->Form->input('retirement_date', array('label' => false, 'div' => false,'style'=>'height: 35px;', 'class'=>'datepicker check_retirement form-control', 'type' => 'DATE' , 'disabled' => true )); ?>
                                </div> 
                            </div>
                            <!-- 8TH GROUP -->  
                            <div class="col-md-11" style="margin-top: 20px;">
                                <div class="col-md-2" style="color: #FFF; background-color: #0092C4">
                                    <label>ICカードキー</label> 
                                </div>
                                <div class="col-md-10">
                                    <?php echo $this->Form->input('ic_card_key', array('label' => false, 'class'=>'form-control check_retirement', 'type' => 'number','min'=>'1', 'disabled' => true)); ?>
                                </div>
                            </div>
                            </div>
                    
                            <!-- BUTTON -->  
                            <div class="col-md-11" style="margin-top: 10px;">
                                <div class="col-md-5" style="margin-top: 20px;">
                                    <?php echo $this->Form->button('登録', array('class'=>'btn btn-default','style' => 'width:100%; background: #0092C4;color:#FFF ;')); ?>
                                </div>
                            </div>
                        </font>
                </div>
            </div>
        </div>
    </div>
    </div>

 <div><?php echo $this->Form->end(); ?></div>
<!-- END CONTAINER --> 

<style>
label {
	margin-top:8px;
}

.form-level{
	margin-top:18px;
}
.form-level1{
	margin-top:23px;
}
</style>


<script type="text/javascript">
    $(document).ready(function(){
      //checkbox adjustmentAttendance
    var ckbox = $('#adjustAttendance');
    
    
     $('input').click(function () {
        if (ckbox.is(':checked')) {
            $(".check_adjustment").attr('disabled', false);
            $(".check_adjustment").attr('required', true);
        } else {
            $(".check_adjustment").attr('disabled', true);
             $(".check_adjustment").attr('required', false);
        }
        
    }); 
    
     $('input').ready(function () {
        if (ckbox.is(':checked')) {
            $(".check_adjustment").attr('disabled', false);
            $(".check_adjustment").attr('required', true);
        } else {
            $(".check_adjustment").attr('disabled', true);
             $(".check_adjustment").attr('required', false);
        }
        
    }); 
     
   
   //Onleave Checkbox
    var ckbox2 = $('#on_leave');
      $('input').click(function () {
        if (ckbox2.is(':checked')) {
            $(".check_leave").attr('disabled', false);
             $(".check_leave").attr('required', true);
        } else {
            $(".check_leave").attr('disabled', true);
              $(".check_leave").attr('required', false);
        }
        
    });
    
     $('input').ready(function () {
        if (ckbox2.is(':checked')) {
            $(".check_leave").attr('disabled', false);
             $(".check_leave").attr('required', true);
        } else {
            $(".check_leave").attr('disabled', true);
              $(".check_leave").attr('required', false);
        }
        
    });
    
    //Retirement checkbox
    var ckbox3 = $('#retirement_checkbox');
    
    
     $('input').click(function () {
        if (ckbox3.is(':checked')) {
            // alert();
            $(".check_retirement").attr('disabled', false);
             $(".check_retirement").attr('required', true);
        } else {
            $(".check_retirement").attr('disabled', true);
             $(".check_retirement").attr('required', false);
        }
        
    });
    
     $('input').ready(function () {
        if (ckbox3.is(':checked')) {
            // alert();
            $(".check_retirement").attr('disabled', false);
             $(".check_retirement").attr('required', true);
        } else {
            $(".check_retirement").attr('disabled', true);
             $(".check_retirement").attr('required', false);
        }
        
    });
     
        
    });
</script>

<?php echo $this->Html->script('/js/Users/add.js'); ?>