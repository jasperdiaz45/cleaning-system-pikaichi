<span id="f-company-no" style="display:none;"><?= $f_company_no ?></span>
<span id="f-department-no" style="display:none;"><?= $f_department_no ?></span>
<span id="f-section-no" style="display:none;"><?= $f_section_no ?></span>

<!-- BEGIN CONTAINER -->
<?php echo $this->assign('title', $title); ?>
<div class="page-container">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE CONTENT-->
			
	<div class="row">		
		<div class="contentRow">
			
			<div class="col-md-12">
				<div class="col-md-2">
					<h3 style="color:; background: ; margin-bottom: 30px;">社員管理</h3>
				</div>
			</div>
	<div><?php echo $this->Form->create(null); ?></div>
	<div class="contentRow">
		<font size="2px">
		<div class="col-md-12" style="margin-top: 10px; margin-left:10x;">
			
    <div class="col-md-1" style="color: #FFF; background-color: #0092C4;">
            <label>部</label> 
    </div>
    
    <div class="col-md-2">
		<select name="department_no" id="search-department-id" class="form-control" onChange="getSearchSectionList('search-section-id', 'search-department-id');"><option value="0"> </option></select>
    </div>
                         
    <div class="col-md-1" style="color: #FFF; background-color: #0092C4;width:100px;">
        	<label>課</label> 
    </div>

    <div class="col-md-2">
    	<select name="section_no" id="search-section-id" class="form-control"><option value="0"> </option></select>
    </div>
				            
	<div class="col-md-1" style="color: #FFF; background-color: #0092C4;">
            <label>職責</label> 
    </div>
        <div class="col-md-1">
          <?php
				echo $this->Form->input('responsibility', array('label' => false, 'class'=>'form-control', 'options' => array(null => '', $responsibility)));
			?>   
    </div>
    
			<div class="col-md-1" style="color: #FFF; background-color: #0092C4; ">
                     <label>退職者含む</label> 
             </div>
             <div class="col-md-1">
                   	<?php echo $this->Form->checkbox("retirement"); ?>
             </div>
             
			<div class="custom-color-normal  col-sm-1 col-sm-offset-0">
				<?php echo $this->Form->button('検索', array('name'=>'search_button','class'=>'btn btn-default','style'=>'width:100%; background: #3399FF;color: #FFF; margin-bottom: 10px;')); ?>
			</div>
	</div>
	</font>
</div>
	<div><?php echo $this->Form->end(); ?></div>

                            
		<div class="col-md-12">
			<div class="custom-color-normal  col-sm-2 col-sm-offset-0">
				<?php echo $this->Html->link(__('追加'), array('action' => 'add'), array('class'=>'btn btn-default', 'style'=>'width:100%; background: #3399FF;color:#FFF ; margin-bottom: 10px;')); ?>
			</div>
		<div class="col-md-offset-5">
				  <div class="col-md-3">
				      <a class="remandOvertime btn btn-primary" data-toggle="modal" data-target="#onLeaveEmployees">休職者一覧</a>
			</div>
		</div>
		</div>
		
		<div class="col-md-12">
			<div class="table-render col-md-8">
				<table class="table table-bordered">
                            
                            <thead style="background-color: #DDE1E3;">
                                <tr>
									<th class="col-xs-6">名前</th>
									<th class="col-xs-1">部門</th>
									<th class="col-xs-2">課</th>
									<th class="col-xs-1">役職</th>
									<th class="col-xs-1">ステータス</th>
									<th class="col-xs-1">詳細</th>
                                </tr>
                            </thead>
                            
					<tbody>
						<?php foreach ($employee as $data): ?>
							<tr>
							<td><?= $data['Employee']['last_name'] ?><?= $data['Employee']['first_name'] ?></td>
							<td><?= $data['Employee']['department_name'] ?></td>
							<td><?= $data['Employee']['section_name'] ?></td>
							<td><?= $data['Employee']['position_no'] ?></td>
							<td><?= $data['Employee']['retirement'] ?></td>
						<td>
							<?php 
							echo $this->Html->link(__('編集'), ['action' => 'edit', $data['Employee']['id']],['class' => 'btn btn-primary']); ?>
						</td>
						</tr>
							<?php endforeach; ?>
					</tbody>												
                </table>
			</div>
        </div>
    </div>
   </div>
</div>
</div>
</div>

<!-- LIST OF EMPLOYEES ON LEAVE -->
<div class="modal fade" id="onLeaveEmployees" tabindex="-1" role="dialog" aria-labelledby="remandVacationModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h3 class="modal-title" id="remandVacationModalLabel">休職者一覧</h3>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true"><i class="fas fa-times-circle"></i></span>
				</button>
			</div>
			<div class="modal-body">
                <section>
                	<div class="row">
                		<div class="table-render col-md-12">
				            <table class="table table-bordered">
                                <thead style="background-color: #DDE1E3;">
                                    <tr>
								        <th class="col-xs-6">名前</th>
								        <th class="col-xs-1">事業部</th>
								        <th class="col-xs-1">部門</th>
								        <th class="col-xs-2">課</th>
								        <th class="col-xs-1">役職</th>
								        <th class="col-xs-1">詳細</th>
                                    </tr>
                                 </thead>
                                 
				            	<tbody>
				            		<?php foreach ($employeeLeaveOfAbsence as $data): ?>
				            			<tr>
				            			<td><?= $data['Employee']['last_name'] ?><?= $data['Employee']['first_name'] ?></td>
				            			<td><?= $data['Employee']['division_no'] ?></td>
				            			<td><?= $data['Employee']['department_no'] ?></td>
				            			<td><?= $data['Employee']['section_no'] ?></td>
				            			<td><?= $data['Employee']['position_no'] ?></td>
				            		<td>
			            				<?php echo $this->Html->link(__('編集'), ['action' => 'edit', $data['Employee']['id']],['class' => 'btn btn-primary']); ?>
			            			</td>
			            			</tr>
			            				<?php endforeach; ?>
			            		</tbody>												
                            </table>
			            </div>
                	</div>
                </section>
			</div>
			<div class="modal-footer"></div>
		</div>
	</div>
</div>
<!-- LIST OF EMPLOYEES ON LEAVE END -->

<style>
label {
	margin-top:8px;
}
</style>

<?php echo $this->Html->script('/js/AjaxCommons/ajax_common.js'); ?>
<?php echo $this->Html->script('/js/Users/index.js'); ?>
