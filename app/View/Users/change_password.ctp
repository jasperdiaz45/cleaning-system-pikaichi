<?php echo $this->assign('title', $title); ?>
<h3>パスワードを変更する</h3>
<div class="errerMessage" <?php if($color == true) { ?> style="display:block; color:blue;" <?php } ?> > <?php echo $this->Session->flash(); ?> </div>
<?php echo $this->Form->create('Employee', ["name" => "change_password_form"]); ?>
<div class="panelBody">
	<label>メールアドレス</label>
	<div class="row input">
		<?php echo $this->Form->input('mail_address', array('label'=>false, 'id'=>'UserEmail', 'type'=>'email', 'div' => false)); ?>
	</div>
	<label>パスワード</label>
	<div class="row input">
		<?php echo $this->Form->input('password', array('label'=>false, 'id'=>'UserPassword', 'type'=>'password', 'div' => false)); ?>
	</div>
	
	<label>新しいパスワード</label>
	<div class="row input">
		<?php echo $this->Form->input('new_password', array('label'=>false, 'id'=>'UserNewPassword', 'type'=>'password', 'div' => false)); ?>
	</div>
	<lable>新しいパスワードをもう一度入力してください</lable>
	<div class="row input">
		<?php echo $this->Form->input('confirm_password', array('label'=>false, 'id'=>'UserConfirmPassword', 'type'=>'password', 'div' => false)); ?>
	</div>
	<div class="btnArea">
		<?php echo $this->Html->link(__('パスワード変更'), ['action' => ''],['class' => 'btn btn-blue loginBtn', 'onClick' => 'change_password_form.submit(); return false;']); ?>
	</div>
	<div class="btnArea">
			 &nbsp
	</div>
	<div class="btnArea">
		<?php echo $this->Html->link(__('戻る'), ['action' => 'login'],['class' => 'btn btn-gray']); ?>
	</div>
	
</div>
<?php echo $this->Form->end(); ?>
