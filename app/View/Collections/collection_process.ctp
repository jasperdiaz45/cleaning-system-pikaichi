
<?php echo $this->assign('title', $title); ?>
<h3><?php echo $title ?></h3>
<div class="page-container">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class ="col-sm-6"></div>

				<div class="col-sm-2 no-padding text-right">
		           <?= $this->html->link('戻る', array('controller' => 'Routes', 'action' => 'SearchByRoutes'),array('class' => 'btn btn-gray', 'style' => 'width:90px;')) ?>
		            </div>


				<div class="col-sm-12 no-padding">
					<div><?php echo $this->Form->create('Shifts'); ?></div>
					
						<div class="contentRow">

		                  <div class="col-md-12 no-padding">
		           			<br> <br> 
							<div class="row">
								<div class="col-sm-3 no-padding  mb-2">
									<label class="label">回収日</label>
									<div class="pickerWrap">
										<div class="input-group date datepicker" id="datepicker2" data-target-input="nearest">
										    <?php echo $this->Form->input('', array('div' => false,'label' => false, 'class'=>'form-control datetimepicker-input', 'type' => 'text','default'=>  date('Y/m/d') )); ?>
											<div class="input-group-append" data-target="#datepicker2" data-toggle="datetimepicker">
												<div class="input-group-text"><i class="far fa-calendar-alt"></i></div>

											</div>
										</div>
									</div>

								</div>
								<div class="col-sm-3 no-padding mb-2">
									<label class="label">店舗・個人名</label>
									<?= $this->form->input('', array('class' => '' , 'label' => false,'div' => false,'size'=>'4')); ?>
		                    	</div>
		                    	<div class="col-sm-1 no-padding mb-2">
		                    		 <?= $this->html->link('戻る', array('controller' => '', 'action' => 'index'),array('class' => 'btn btn-primary', 'style' => 'width:90px;')) ?>
		                    	</div>
						    </div>
		               	</div>
		            	 <div><?php echo $this->Form->end(); ?></div>
					</div>
				</div>
				<div class="col-sm-9 no-padding" style="padding: 0px;">
					
				<div class="col-sm-3"></div>

					<div class="col-sm-10 no-padding">
						<br> <br>
							<section class="attendanceDetail">

	                    	<div class="table">
								<table class="table table-bordered table-sm table-striped">
									<thead style="background: #dde1e3; ">
										<tr>
											<th>店舗/個人名</th>
											<th>支払方法</th>
											<th>未入金額	</th>
											<th>集金</th>
										</tr>
									</thead>
									<tbody>
										<?php for ($i=0; $i <10; $i++) { ?>
											<tr>
												<td>REEKS</td>
												<td>現金</td>
												<td>14,000</td>
												<td><?= $this->html->link('集金', array('controller' => 'Delivery', 'action' => 'CashOnDelivery'),array('class' => 'btn btn-primary', 'style' => 'width:90px;')) ?></td>
												
											</tr>
										<?php } ?>
										
									</tbody>
								</table>
							</div>
						</section>
					</div>
					<div class="col-sm-2"></div>
				</div>
				</div>
			</div>	
		
			<!-- END PAGE CONTENT-->
	</div>