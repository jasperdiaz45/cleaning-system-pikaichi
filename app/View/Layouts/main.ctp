<?php
$list=false;
	$enable_old_design = true;
	switch ($this->name) {
		case 'Inputs':
				$enable_old_design = false;
				break;
		case 'Lists':
			$enable_old_design = false;
			$list = false;
			break;
		case 'Applications':
			$enable_old_design = false;
			break;
		default:
			$enable_old_design = false;
			$list = false;
			break;
	}

?>

<?php if ($enable_old_design) { ?>

<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php // echo $this->fetch('title'); ?>
		<?php echo '就業管理システム｜ピカいち'; ?>
	</title>

	<?php
		echo $this->Html->meta('icon');

		echo $this->Html->css('cake.generic');
		echo $this->Html->css('style');
		echo $this->Html->css('layout');
		echo $this->Html->css('form');
		echo $this->Html->css('w3');
		echo $this->Html->css('responsive');
		echo $this->Html->css('common');
		echo $this->Html->css('main');
		echo $this->Html->css('custom-picker');

		echo $this->fetch('meta');
		echo $this->fetch('css');
		
		// jquery library
		echo $this->Html->script('https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js');
		echo $this->Html->css('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css');
		echo $this->Html->css('//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css');
		echo $this->Html->script('//code.jquery.com/ui/1.11.4/jquery-ui.js');
		echo $this->Html->script('dist/dependencies/pikaday-responsive-modernizr.js');
		

		echo $this->fetch('script');
//		echo '<button class="btn menuBtn"><i class="fas fa-bars"></i></button>';
	?>
</head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<body>

<!-- Navbar -->
<div class="w3-top" style="z-index:1000;">
  <div class="w3-bar w3-theme w3-top w3-left-align w3-large">
    <a class="w3-bar-item w3-button w3-right w3-hide-large w3-hover-white w3-large w3-theme-l1" href="javascript:void(0)" onclick="w3_open()"><i class="fa fa-bars"></i></a>
    <a href="#" class="w3-bar-item"><!--<img src="/img/logo.png" style="height:24px; margin: 2.5%;" />--></a>ピカいち
	<div class="" style="float: right; padding: 10px">
		<i class="fa fa-user-circle"></i>
		<span><?php echo AuthComponent::user('last_name') . ' ' . AuthComponent::user('first_name'); ?></span>
	</div>
  </div>
</div>

<!-- Sidebar -->
<nav class="w3-sidebar w3-bar-block w3-collapse w3-large w3-theme-l5" id="mySidebar">
  <a href="javascript:void(0)" onclick="w3_close()" class="w3-right w3-xlarge w3-padding-large w3-hover-black w3-hide-large" title="Close Menu">
    <i class="fa fa-remove"></i>
  </a>
  <?php echo $this->element('sidebar'); ?>
</nav>

<!-- Overlay effect when opening sidebar on small screens -->
<div class="w3-overlay w3-hide-large" onclick="w3_close()" style="cursor:pointer" title="close side menu" id="myOverlay"></div>
<div class="w3-main" style="margin-left:200px">
  	<div class="w3-row w3-padding-64">
    	<div class="container-fluid">
    		<?php echo $this->Flash->render(); ?>

			<?php echo $this->fetch('content'); ?>
  		</div>
	</div>
</div>

<style type="text/css">
.w3-theme {
    color: #fff !important;
    background-color: rgb(84, 102, 114) !important;
}
</style>


<script>
// Get the Sidebar
var mySidebar = document.getElementById("mySidebar");

// Get the DIV with overlay effect
var overlayBg = document.getElementById("myOverlay");

// Toggle between showing and hiding the sidebar, and add overlay effect
function w3_open() {
    if (mySidebar.style.display === 'block') {
        mySidebar.style.display = 'none';
        overlayBg.style.display = "none";
    } else {
        mySidebar.style.display = 'block';
        mySidebar.style.width = '100%';
        sidebar.style.width = '100%';
        // overlayBg.style.display = "block";
    }
}

// Close the sidebar with the close button
function w3_close() {
    mySidebar.style.display = "none";
    overlayBg.style.display = "none";
}
</script>

</body>

<?php 
	echo $this->Html->script('https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js');
	echo $this->Html->script('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js');
	echo $this->Html->script('https://use.fontawesome.com/releases/v5.0.9/js/all.js');
	echo $this->Html->script('/js/lib/moment-with-locales.min.js');
	echo $this->Html->script('/js/lib/tempusdominus-bootstrap-4.min.js');
	echo $this->Html->script('/js/main.js');
?>
</html>

<?php } ?>

<?php if (!$enable_old_design) { ?>

<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<meta name="author" content="">
<meta name="description" content="">
<meta name="format-detection" content="telephone=no, address=no, email=no">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="publisher" href="">
<link rel="canonical" href="">
<link rel="icon" href="">
<link rel="apple-touch-icon" href="">

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP" crossorigin="anonymous">

<?php
//	echo $this->Html->css('cake.generic');
	echo $this->Html->css('/css/lib/bootstrap.min.css');
	echo $this->Html->css('/css/lib/tempusdominus-bootstrap-4.min.css');
	echo $this->Html->css('/css/common.css');
	echo $this->Html->css('/css/responsive.css');
	echo $this->Html->css('main');
	echo $this->Html->css('/datepicker/pikaday-package.css');
	echo $this->Html->css('/datepiker/demo-style.css');
	$useCssFile = '';
	switch ($this->name) {
		case 'Inputs':
			$useCssFile = 'input.css';
			break;
		case 'Lists':
			$useCssFile = 'input.css';
			break;
		case 'Applications':
			$useCssFile = 'input.css';
			break;
		default:
			$useCssFile = 'input.css';
		break;
	}
	if ($useCssFile != '') echo $this->Html->css('/css/' . $useCssFile);
	echo $this->Html->css('/css/modal.css');
	echo $this->Html->css('/css/flash-message.css');
?>

<title>就業管理システム｜ピカいち</title>

</head>
<body>
	
	<?php echo $this->Html->script('/js/lib/jquery-3.3.1.min.js'); ?>
	<?php echo $this->Html->script('/js/lib/bootstrap.bundle.min.js'); ?>
	<?php echo $this->Html->script('/js/lib/moment-with-locales.min.js'); ?>
	<?php echo $this->Html->script('/js/lib/tempusdominus-bootstrap-4.min.js'); ?>
	<?php echo $this->Html->script('/js/lib/ja.js'); ?>
	<?php echo $this->Html->script('/js/main.js'); ?>
	<?php echo $this->Html->script('/datepicker/dependencies/moment.min.js'); ?>
	<?php echo $this->Html->script('/datepicker/dependencies/pikaday.min.js'); ?>
	<?php echo $this->Html->script('/datepicker/pikaday-responsive.js'); ?>

<header>
	<h1 class="logo"><img src="img/logo.png" alt="ピカいち"></h1>
	<button class="btn menuBtn"><i class="fas fa-bars"></i></button>
</header>
	
	<header style="z-index:100;">
		<h1 class="logo">
			<?php  echo $this->Html->image('logo.png', array('alt' => 'ピカいち')); ?>
			<div class="" style="float: right; padding-top: 7px; padding-right: 35px;">
				<i class="fa fa-user-circle"></i>
				<span><?php echo AuthComponent::user('last_name') . ' ' . AuthComponent::user('first_name'); ?></span>
			</div>
		</h1>
		<button class="btn menuBtn"><i class="fas fa-bars"></i></button>
	</header>
		
	<main>
	
	<?php echo $this->element('sidebar'); ?>
		
	<!-- BEGIN CONTAINER -->
	<section class="contents">
	<div class="container-fluid">
	
	<?php
		echo $this->Flash->render();
		echo $this->fetch('content');
	?>
	
	</div><!-- /.container-fluid -->
	</section>
	</main>

</body>
</html>

<?php } ?>
