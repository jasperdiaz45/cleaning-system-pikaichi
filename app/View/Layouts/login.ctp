<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = __d('cake_dev', 'CakePHP: the rapid development php framework');
$cakeVersion = __d('cake_dev', 'CakePHP %s', Configure::version())
?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<meta name="author" content="">
<meta name="description" content="">
<meta name="format-detection" content="telephone=no, address=no, email=no">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="publisher" href="">
<link rel="canonical" href="">
<link rel="icon" href="">
<link rel="apple-touch-icon" href="">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP" crossorigin="anonymous">

<?php
	echo $this->Html->css('/css/lib/bootstrap.min.css');
	echo $this->Html->css('/css/lib/tempusdominus-bootstrap-4.min.css');
	echo $this->Html->css('/css/common.css');
	echo $this->Html->css('/css/login.css');
	echo $this->Html->css('/css/modal.css');
?>

<title>就業管理システム｜ピカいち</title>

</head>
<body>
	
	<main>
		<section class="contents">

			<div class="container logo">
				<h1>
					<?php echo $this->Html->image('logo.png', array('alt' => 'ピカいち')); ?>
				</h1>
			</div>
			<div class="container loginPanel">
				<?php echo $this->Flash->render(); ?>
				<?php echo $this->fetch('content'); ?>
			</div>

		</section>
	</main>

<?php echo $this->Html->script('/js/lib/jquery-3.3.1.min.js'); ?>
<?php echo $this->Html->script('/js/lib/bootstrap.bundle.min.js'); ?>
<?php echo $this->Html->script('/js/lib/moment-with-locales.min.js'); ?>
<?php echo $this->Html->script('/js/lib/tempusdominus-bootstrap-4.min.js'); ?>
<?php echo $this->Html->script('/js/lib/ja.js'); ?>
<?php echo $this->Html->script('/js/main.js'); ?>

</body>
</html>

