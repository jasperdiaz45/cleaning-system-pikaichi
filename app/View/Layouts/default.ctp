<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = __d('cake_dev', 'PNT WORK');
$cakeVersion = __d('cake_dev', 'CakePHP %s', Configure::version())
?>
<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $cakeDescription ?>:
		<?php echo $this->fetch('title'); ?>
	</title>
	<?php
		echo $this->Html->meta('icon');

		echo $this->Html->css('cake.generic');
		echo $this->Html->css('style');
		echo $this->Html->css('index');

		echo $this->fetch('meta');
		echo $this->fetch('css');
		
		// jquery library
		echo $this->Html->script('https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js');
		echo $this->Html->css('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css');
		echo $this->Html->css('//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css');
		echo $this->Html->script('//code.jquery.com/ui/1.11.4/jquery-ui.js');
		
		echo $this->fetch('script');
	?>
</head>
<body>
	<div id="container">
		<div id="header-full" class="fixed-header">
			<div class="logo"><?php echo $cakeDescription; ?></div>
			<div class="float-right top-username text">
				<i class="fa fa-user-circle"></i>
				<span><?php echo AuthComponent::user('last_name') . ' ' . AuthComponent::user('first_name'); ?></span>
			</div>
		</div>
		<div id="main_content">
			<div id="sidebar" class="sidebar">
				<ul class="sidebar-menu">
					<li>
						<a href="<?php echo $siteURL ?>WorkReports/input">
						<span class="title">入力</span>
						</a>
					</li>
					<li>
						<a href="<?php echo $siteURL ?>WorkReports/">
						<span class="title">一覧</span>
						</a>
					</li>
					<li>
						<a href="<?php echo $siteURL ?>matters/">
						<span class="title">案件管理</span>
						</a>
					</li>
					<li>
						<a href="<?php echo $siteURL ?>users/">
						<span class="title">ユーザ管理</span>
						</a>
					</li>
					<li>
						<a href="<?php echo $siteURL ?>">
						<span class="title">集計</span>
						</a>
					</li>
				</ul>
			</div>
			
			<div class="primary_content">
				<div class="container-fluid">
					<div class="html-render ng-binding ng-scope" ng-bind-html="htmlRender" compile-template="">
						<?php echo $this->Flash->render(); ?>

						<?php echo $this->fetch('content'); ?>
					</div>
				</div>
			</div>
		</div>
		<div id="footer">
			
		</div>
	</div>
</body>

<?php 
	//echo $this->Html->script('https://code.jquery.com/jquery-3.2.1.slim.min.js');
	
	echo $this->Html->script('https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js');
	echo $this->Html->script('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js');
	
	echo $this->Html->script('https://use.fontawesome.com/releases/v5.0.9/js/all.js');
	
	
?>
<script>
  var $date1 = $("#date1");
  var instance1 = pikadayResponsive($date1);
  $date1.on("change", function() {
    $("#output1").html($(this).val());
  });

  var $date2 = $("#date2");
  var instance2 = pikadayResponsive($date2, {
    outputFormat: "X"
  });
  $date2.on("change", function() {
    $("#output2").html($(this).val());
  });

  var $date3 = $("#date3");
  var instance3 = pikadayResponsive($date3, {
    format: "Do MMM YYYY",
    outputFormat: "X"
  });

  var $date4 = $("#date4");
  var today = new Date();
  var minDate = new Date();
  var maxDate = new Date();
  minDate.setDate(today.getDate() + 3);
  maxDate.setDate(today.getDate() + 365);
  var instance4 = pikadayResponsive($date4, {
    format: "DD/MM/YYYY",
    outputFormat: "DD/MM/YYYY",
    pikadayOptions: {
      minDate: minDate,
      maxDate: maxDate,
    },
  });
  instance4.setDate(minDate);

  $date3.on("change", function() {
    $("#output3").html($(this).val());
  });

  $date4.on("change", function() {
    $("#output4").html($(this).val());
  });

  $("#clear").click(function() {
    instance3.setDate(null);
  });

  $("#today").click(function() {
    instance3.setDate(moment());
  });

</script>
</html>