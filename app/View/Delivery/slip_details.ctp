<!-- <h3><?= $screenTitle ?></h3> -->

<!-- <h3><?= $screenTitle ?></h3> -->
<?php echo $this->assign('title', $title); ?>

<div class="page-container">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-sm-9" style="padding: 0px;">
					
					<div><?php echo $this->Form->create('Shifts'); ?></div>
					
					<div class="contentRow">

	                    <div class="col-md-12">
	           			
						<h3><b>伝票詳細</b></h3>
						

                    	
						
						<div class="row">
							<div class="col-sm-9"></div>

							<div class="col-sm-3 no-padding text-right">
		                	    <?= $this->html->link('戻る', array('controller' => 'Store','action' => 'index'), array('class' => 'btn btn-gray mb-1', 'style' => 'width:90px;')) ?>
		                	</div>
							
							<div class="col-sm-7 no-padding mb-2">
								<label class="label label-long">仕上り予定日</label>
								<?= $this->form->input('', array('class' => 'form-lable' , 'label' => false,'div' => false, 'readonly', 'value' => 'AA20190428123423001')); ?>
							</div>

							<div class="col-sm-5 no-padding mb-2"></div>

							<div class="col-sm-7 no-padding mb-2">
								<label class="label label-long">店舗・個人名</label>
								<?= $this->form->input('', array('class' => 'form-lable' , 'label' => false,'div' => false, 'readonly', 'value' => '優希')); ?>
							</div>

							<div class="col-sm-5 no-padding mb-2"></div>

							<div class="col-sm-7 no-padding mb-2">
								<label class="label label-long">金額合計（税込）</label>
								<?= $this->form->input('', array('class' => 'form-lable' , 'label' => false,'div' => false, 'readonly', 'value' => '12,960')); ?>
							</div>

							<div class="col-sm-5 no-padding mb-2"></div>

							<div class="col-sm-7 no-padding mb-2">
								<label class="label label-long">未収額</label>
								<?= $this->form->input('', array('class' => 'form-lable' , 'label' => false,'div' => false, 'readonly', 'value' => '12,960')); ?>
							</div>

							<div class="col-sm-5 no-padding mb-2"></div>

							<div class="col-sm-8"></div>
							<div class="col-sm-3 no-padding mb-2">
		                   		<?= $this->form->button('商品追加', array('class' => 'btn btn-info', 'style' => 'width:100%;')) ?>
		                    </div>

						</div>
					
	                    </div>
	               	</div>
	            	 <div><?php echo $this->Form->end(); ?></div>
				</div>
				<div class="col-sm-3"></div>

				<div class="col-sm-12 no-padding">
					<ul class="pagination">
						<li><a>全99件｜1〜99件表示｜</a></li>
						<li><a href="">1</a></li>
						<li><a href="">2</a></li>
						<li><a href="">3</a></li>
						<li><a href="">4</a></li>
						<li><a href="">5</a></li>
					</ul>
				</div>
					<div class="col-sm-10">
						 <br>
							<section class="attendanceDetail">

	                    	<div class="table">
								<table class="table table-bordered table-sm table-striped">
									<thead style="background: #dde1e3; ">
										<tr>
											<th>商品</th>
											<th width="10%">数量</th>
											<th width="5%">単価</th>
											<th>金額</th>
											<th width="10%">削除</th>
										</tr>
									</thead>

									<tbody>
										<?php for ($i=0; $i <10 ; $i++) { ?> 
											<tr>
												<td>ワイシャツ</td>
												<td><?= $this->Form->input('value',array('label'=>false,'style'=>'width: 50px;','value'=>'1'));?></td>
												<td>1,200</td>
												<td>1,200</td>
												<td class="text-center"><?= $this->html->link('削除', array('action' => ''), array('class' => 'btn btn-primary','width'=>'100%')) ?></td>
												
											</tr>
										<?php } ?>
									
									</tbody>
								</table>
							</div>
						</section>
					</div>
					<div class="col-sm-2"></div>
				</div>
				</div>
			</div>	
		
			<!-- END PAGE CONTENT-->
	</div>