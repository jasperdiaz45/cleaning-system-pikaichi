<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE CONTENT-->
			<div class="col-sm-12 no-padding">

  				<div class="col-sm-12 no-padding" style="padding: 0px;">
                    
                    <div><?php echo $this->Form->create('Shifts'); ?></div>
                    
                    <div class="contentRow">
                        <div class="col-sm-12 no-padding">
                            <div class="row">
                                <div class="col-sm-6 no-padding">
                                    <h3><b>配送処理</b></h3>
                                </div>
                                <div class="col-sm-2">
                                    <?= $this->html->link('集荷処理', array('controller' => 'Pickup', 'action' => 'index'), array('class' => 'btn btn-lime btn-block mb-2')) ?>
                                </div>
                                <div class="col-sm-2">
                                    <?= $this->html->link('集金処理', array('controller' => 'Collections', 'action' => 'CollectionProcess'), array('class' => 'btn btn-red btn-block mb-2')) ?>
                                </div>
                                <div class="col-sm-2">
                                    <?= $this->html->link('戻る', array('controller' => 'Routes', 'action' => 'SearchByRoutes'), array('class' => 'btn btn-return btn-block mb-2')) ?>
                                </div>
                            </div>
                        
                                     
                        </div>
                    </div>
                     <div><?php echo $this->Form->end(); ?></div>
                </div>
				<!--  -->


				<div class="contentRow">
					<div class="row mt-3">
						<div class="col-sm-9 no-padding">	
							 <label class="label">店舗名</label>
                             <?= $this->form->input('', array('class' => 'form-lable form-sm' , 'label' => false,'div' => false, 'readonly', 'value' => 'REEX')); ?>
							<?= $this->html->link('次の店', array('action' => ''), array('class' => 'btn btn-primary mt-1','style'=>'width: 90px;')) ?>
						</div>
                		
					</div>
					<div class="row mt-4">
						<div class="col-sm-7 no-padding">	
							<label class="label">仕上り予定日</label>
							<div class="pickerWrap">
									<div class="input-group date datepicker" id="datepicker2" data-target-input="nearest">
									    <?php echo $this->Form->input('', array('div' => false,'label' => false, 'class'=>'form-control datetimepicker-input', 'type' => 'text','default'=>  date('Y/m/d') )); ?>
										<div class="input-group-append" data-target="#datepicker2" data-toggle="datetimepicker">
											<div class="input-group-text"><i class="far fa-calendar-alt"></i></div>
										</div>
									</div>
								</div>
						</div>
					</div>

					<div class="row">

						<div class="col-sm-12 no-padding">
							<br><br>
						<section class="attendanceDetail">
							<table class="table table-bordered table-striped table-sm">
								<thead style="background: #dde1e3; ">
									<tr>
										<th>伝票番号	</th>
										<th>仕上り予定日</th>
										<th>点数</th>
										<th>金額</th>
										<th >詳細</th>
									</tr>
								</thead>
								<tbody>
									<?php for ($i=0; $i < 10; $i++) { ?>
										<tr>
											<td>A000001140312014448</td>
											<td>2014/04/27</td>
											<td>2</td>
											<td>12,000</td>
											<td class="text-center"><?= $this->html->link('詳細', array('controller'=>'Delivery','action' => 'index'), array('class' => 'btn btn-primary')) ?></td>
										</tr>
									<?php } ?>
								</tbody>
							</table>
						</section>		
						</div>
					</div>

				</div>
			</div>
		</div>

	</div>

</div>
	     
    