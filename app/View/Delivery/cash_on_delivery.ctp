<span id="ajax-url-base-path" style="display:none;">../Applications/</span>
<?php echo $this->assign('title', $title); ?>
<h3><b><?php echo $title ?></b></h3>
<?php echo $this->Form->create(null, [ "name" => "list_form" ]); ?>
<div class="page-container">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class ="col-sm-6"></div>
				<div class="col-sm-2 no-padding text-right">
					<?= $this->html->link('戻る', array('controller' => 'Collections', 'action' => 'CollectionProcess'),array('class' => 'btn btn-gray', 'style' => 'width:90px;')) ?>
				</div>
				
				<div><?php echo $this->Form->create('Shifts'); ?></div>
				
				<div class="contentRow">
					<div class="col-md-12 no-padding">
						<br>
						<div class="row">
							<div class="col-sm-12 no-padding">
								<label class="label">店舗・個人名</label>
								<?= $this->form->input('', array('class' => 'form-lable' , 'label' => false,'div' => false, 'readonly', 'value' => '優希')); ?>
							</div>
							<div class="col-sm-12 no-padding">
								<div class="mt-2">
									<label class="label">金額合計（税込）</label>
									<?= $this->form->input('', array('class' => 'form-lable' , 'label' => false,'div' => false, 'readonly', 'value' => '12,960')); ?>
									
								</div>
							</div>
							<div class="col-sm-0 no-padding">
								<div class="mt-2">
									<label class="label">入金額</label>
									<?= $this->form->input('', array('class' => '' , 'label' => false,'div' => false,'style' => 'width:180px;')); ?>
								</div>
							</div>
							<div class="mt-2">
								<div class="col-sm-9">
									<?= $this->form->button('登録', array('class' => 'btn btn-yellow', 'style' => 'width:90px;')) ?>
								</div>
							</div>
							<div class="col-sm-12 no-padding">
								<div class="mt-1">
									<label class="label">未収額</label>
									<?= $this->form->input('', array('class' => 'form-lable' , 'label' => false,'div' => false, 'readonly', 'value' => '12,960')); ?>
									
								</div>
							</div>
						</div>
						<div><?php echo $this->Form->end(); ?></div>
					</div>
				</div>
				<div class="col-sm-9" style="padding: 0px;">
					
					<div class="col-sm-15">
						<br>
						<section class="attendanceDetail">
							<div class="col-sm-0 no-padding">
								<?= $this->form->button('全てチェック', array('id' => 'btn_check','class' => 'btn btn-primary', 'style' => 'width:90px;')) ?>
							</div>
							<div class="mt-2">
								<div class="table">
									<table class="table table-bordered table-sm table-striped">
										<thead style="background: #dde1e3; ">
											<tr>
												<th>渡し	</th>
												<th>商品</th>
												<th>数量</th>
												<th>単価</th>
												<th>金額</th>
											</tr>
										</thead>
										<tbody>
											<?php for ($i=0; $i < 5; $i++) { ?>
											<tr>
												<td><?= $this->form->checkbox('', array('class' => 'checkbox','id' => 'checkbox_'.$i , 'label' => false,'div' => false,'size'=>'4')); ?></td>
												<td>ワイシャツ</td>
												<td>1</td>
												<td>1,200</td>
												<td>1,200</td>
											</tr>
											<?php } ?>
										</tbody>
									</table>
								</div>
							</section>
						</div>
						<div class="col-sm-2"></div>
					</div>
				</div>
			</div>
			
			<!-- END PAGE CONTENT-->
		</div>
		<?= $this->Html->script($this->Version->extVarParam("/js/Delivery/cash_on_delivery.js")) ?>