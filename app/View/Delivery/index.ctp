<!-- <h3><?= $screenTitle ?></h3> -->
<?php echo $this->assign('title', $title); ?>

<div class="page-container">
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">
            <!-- BEGIN PAGE CONTENT-->
            <div class="row">
                <div class="col-sm-12 no-padding" style="padding: 0px;">
                    
                    <div><?php echo $this->Form->create('Shifts'); ?></div>
                    
                    <div class="contentRow">
                        <div class="col-sm-12">
                            <div class="row">
                                <div class="col-sm-12 no-padding">
                                    <h3><b>集荷処理</b></h3>
                                </div>
                                <div class="col-sm-6"></div>
                                <div class="col-sm-2">
                                    
                                </div>
                                <div class="col-sm-2">
                                    <?= $this->html->link('次の伝票へ', array('controller' => 'Collections', 'action' => 'CollectionProcess'), array('class' => 'btn btn-primary btn-block')) ?>
                                </div>
                                <div class="col-sm-2">
                                    <?= $this->html->link('戻る', array( 'action' => 'DeliveryProcessing'), array('class' => 'btn btn-return btn-block')) ?>
                                </div>      
                            </div>
                        
                            <br>
                            <div class="row">
                                <div class="col-sm-12 no-padding ">
                                    <label class="label label-long">伝票番号</label>
                                    <?= $this->form->input('', array('class' => 'form-lable' , 'label' => false,'div' => false, 'readonly', 'value' => 'AA20190428123423001')); ?>
                                </div>
                                <div class="col-sm-12 no-padding ">
                                    <div class="mt-2">
                                    <label class="label label-long">店舗・個人名</label>
                                    <?= $this->form->input('', array('class' => 'form-lable' , 'label' => false,'div' => false, 'readonly', 'value' => '優希')); ?>
                                </div>
                                </div>
                                <div class="col-sm-12 no-padding ">
                                    <div class="mt-2">
                                    <label class="label label-long">金額合計（税込）</label>
                                    <?= $this->form->input('', array('class' => 'form-lable' , 'label' => false,'div' => false, 'readonly', 'value' => '12,960')); ?>
                                </div>
                                </div>
                                <div class="col-sm-12 no-padding ">
                                    <div class="mt-2">
                                    <label class="label label-long">未収額</label>
                                    <?= $this->form->input('', array('class' => 'form-lable' , 'label' => false,'div' => false, 'readonly', 'value' => '12,960')); ?>
                                </div>
                                </div>
                            </div>              
                        </div>
                    </div>
                     <div><?php echo $this->Form->end(); ?></div>
                
                

                    <div class="col-sm-12">
                        <section class="attendanceDetail">
                            <br>
                            <div class=" mb-2">
                                <?= $this->html->link('全てチェック', array('action' => ''), array('class' => 'btn btn-primary' , 'style' => 'width:100px;')) ?>
                            </div>
                            <div class="table">
                                <table class="table table-bordered table-sm table-striped">
                                    <thead style="background: #dde1e3; ">
                                        <tr>
                                            
                                            <th class="text-center" width="50">渡し</th>
                                            <th>商品</th>
                                            <th>数量</th>
                                            <th>単価</th>
                                            <th>金額</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php for ($i=0; $i <10; $i++) { ?>
                                            <tr>
                                                <td>
                                                    <?= $this->form->checkbox('', array('class' => '' , 'label' => false,'div' => false)); ?>
                                                </td>
                                                <td>
                                                    <p>ワイシャツ</p>
                                                </td>
                                                 <td>
                                                    <p>1</p>
                                                </td>
                                                <td>
                                                    <p>1,200</p>
                                                </td>
                                                <td>
                                                    <p>1,200</p>
                                                </td>   
                                            </tr>
                                        <?php } ?>
                                        
                                    </tbody>
                                </table>
                            </div>
                        </section>

                    </div>
                    <div class="row">
                        <div class="col-sm-2 no-padding">
                                <div class="mt-2 ml-3">
                                    <label class="label label-long">メモ</label>
                                </div>
                            </div>
                            <div class="col-sm-10 no-padding">
                                <div class="mt-2">
                                        <?php echo $this->Form->input('texarea', array('label' => false, 'class'=>'comment  form-control','type'=>'textarea','style'=>'height:40px; margin-left:20px; ')); ?>    
                                </div>
                            </div>
                            
                    </div>
                    <div class="row">
                        <div class="col-sm-4 no-padding">
                                <div class=" mb-2 ml-3">
                                    <br>
                                    <?= $this->html->link('配送済み', array('action' => ''), array('class' => 'btn btn-primary' , 'style' => 'width:200px;')) ?>
                                </div>
                            </div>
                    </div>
                    </div>
                </div>
                </div>
            </div>  
        
            <!-- END PAGE CONTENT-->
    </div>


