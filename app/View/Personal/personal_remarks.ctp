<!-- <h3><?= $screenTitle ?></h3> -->
<?php echo $this->assign('title', $title); ?>
<style type="text/css">
	.label{
		width:100px;
	}
</style>
<div class="page-container">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-sm-9" style="padding: 0px;">
					
					<div><?php echo $this->Form->create('Shifts'); ?></div>
					
					<div class="contentRow">
						<div class="col-md-12">
							
							<h3><b>個人備考</b></h3>
							<br>
							
							
							<div class="row">
								
								<div class="col-sm-12 no-padding mb-2">
									<label class="label">店舗ID</label>
									<?= $this->form->input('', array('class' => 'form-lable' , 'label' => false,'div' => false, 'readonly', 'value' => 't143')); ?>
								</div>
								<div class="col-sm-12 no-padding">
									<div class="mt-2">
										<label class="label">店舗名</label>
										<?= $this->form->input('', array('class' => 'form-lable' , 'label' => false,'div' => false, 'readonly', 'value' => 'REEK')); ?>
									</div>
								</div>
								<div class="col-sm-12 no-padding">
									<div class="mt-2">
										<label class="label">個人名</label>
										<?= $this->form->input('', array('class' => 'form-lable' , 'label' => false,'div' => false, 'readonly', 'value' => '優希')); ?>
									</div>
								</div>
								<div class="col-sm-3 no-padding">
									<div class="mt-2">
										<label class="label">備考</label>
									</div>
								</div>
								<div class="col-sm-9 no-padding">
									<div class="mt-2">
										<?php echo $this->Form->input('texarea', array('label' => false, 'class'=>'comment 	form-control','type'=>'textarea','style'=>'height:40px; ')); ?>
									</div>
								</div>
								<div class="col-sm-3 no-padding	">
									<div class="mt-2">
										<?= $this->html->link('登録', array(), array('class' => 'btn btn-orange btn-block')) ?>
									</div>
								</div>
								<div class="col-sm-3 no-padding	">	</div>
								<div class="col-sm-3 no-padding">
									<div class="mt-2">
										<?= $this->html->link('戻る', array('controller' => 'Pickup', 'action' => 'index'), array('class' => 'btn btn-return btn-block')) ?>
									</div>
								</div>
							</div>
							
						</div>
					</div>
					<div><?php echo $this->Form->end(); ?></div>
				</div>
				
				<div class="col-sm-2"></div>
			</div>
		</div>
	</div>
	
	<!-- END PAGE CONTENT-->
</div>