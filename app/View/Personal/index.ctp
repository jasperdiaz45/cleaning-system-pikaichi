<!-- <h3><?= $screenTitle ?></h3> -->
<?php echo $this->assign('title', $title); ?>
<?= $this->Html->css($this->Version->extVarParam("/css/Personal/index.css")) ?>
<div class="page-container">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-sm-9" style="padding: 0px;">
					<div><?php echo $this->Form->create('Shifts'); ?></div>
					<div class="contentRow">
						<div class="col-sm-12">
							<h3><b>個人情報</b></h3>
						</div>
					</div>
					<div><?php echo $this->Form->end(); ?></div>
				</div>
				<div class="col-sm-3"></div>
				<div class="col-sm-10">
					<div class="mt-2">
						<label class="label">店舗ID</label>
						<?= $this->form->input('', array('div' => false, 'label' => false,'class' => '')) ?>
					</div>
					<div class="mt-2">
						<label class="label">店舗名</label>
						<?= $this->form->input('', array('div' => false, 'label' => false,'class' => '')) ?>
					</div>
					<div class="mt-2">
						<label class="label">個人名</label>
						<?= $this->form->input('', array('div' => false, 'label' => false,'class' => '')) ?>
					</div>
					<div class="mt-2">
						<div class="row">
							<div class="">
								<label class="label">備考</label>
							</div>
							<div class="ml-1" style="margin-top: -10px !important;">
								<?= $this->form->input('', array('type' => 'textarea','div' => false, 'label' => false,'class' => 'textarea-form text-width')) ?>
							</div>
						</div>
					</div>
					<div class="mt-2">
						<label class="label">フリガナ</label>
						<?= $this->form->input('', array('div' => false, 'label' => false,'class' => '')) ?>
					</div>
					<div class="mt-2">
						<label class="label">メールアドレス</label>
						<?= $this->form->input('', array('div' => false, 'label' => false,'class' => 'text-width')) ?>
					</div>
					<div class="mt-2">
						<label class="label">支払タイミング</label>
						<?= $this->form->input('', array('div' => false, 'label' => false, 'options' =>$this->Option->paymentTimingOption() , 'class' => 'drop-width' )) ?>
					</div>
					<div class="mt-2">
						<label class="label">個人締め区分</label>
						<?= $this->form->input('', array('div' => false, 'label' => false, 'options' =>$this->Option->IndividualClassificationOption() , 'class' => 'drop-width' )) ?>
					</div>
					<div class="mt-2">
						<label class="label">め日</label>
						<?= $this->form->input('', array('div' => false, 'label' => false, 'options' =>$this->Option->LastDayOption() , 'class' => 'drop-width' )) ?>
					</div>
					<div class="mt-2">
						<label class="label">回収サイクル</label>
						
						<?= $this->form->input('', array('div' => false, 'label' => false, 'options' =>$this->Option->CollectionCycleOption() , 'class' => 'drop-width' )) ?>
					</div>
					<div class="mt-2">
						<label class="label">支払方法</label>
						<?= $this->form->input('', array('div' => false, 'label' => false, 'options' =>$this->Option->PaymentMethodOption() , 'class' => 'drop-width' )) ?>
					</div>
					<div class="mt-2">
						<label class="label">電話番号</label>
						<?= $this->form->input('', array('div' => false, 'label' => false,'class' => '')) ?>
					</div>
					<div class="mt-2">
						<label class="label">郵便番号</label>
						<?= $this->form->input('', array('div' => false, 'label' => false,'class' => 'drop-width')) ?>
					</div>
					<div class="mt-2">
						<label class="label">都道府県</label>
						<?= $this->form->input('', array('div' => false, 'label' => false, 'options' =>$this->Option->PrefecturesOption() , 'class' => 'drop-width' )) ?>
					</div>
					<div class="mt-2">
						<div class="row">
							<div class="" >
								<label class="label">住所</label>
							</div>
							<div class="ml-1" style="margin-top: -10px !important;">
								<?= $this->form->input('', array('type' => 'textarea','div' => false, 'label' => false,'class' => 'textarea-form text-width')) ?>
							</div>
						</div>
					</div>
					<div class="mt-3">
						<div class="row">
							<div class="col-sm-6">
								<?= $this->html->link('登録', array('action' => 'index'), array('class' => 'btn btn-orange btn-block drop-width')) ?>
							</div>
							<div class="col-sm-6">
								<?= $this->html->link('戻る', array('action' => 'PersonalRemarks'), array('class' => 'btn btn-return btn-block drop-width')) ?>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-2"></div>
			</div>
		</div>
	</div>
	
	<!-- END PAGE CONTENT-->
</div>