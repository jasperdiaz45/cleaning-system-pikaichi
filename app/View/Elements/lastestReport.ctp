<?php if ($user['role'] != '3') : ?>
<div class="col-md-12">
	<h3>直近<?php echo Configure::read('lasterReportDays'); ?>日間の稼働</h3>
</div>
<div class="contentRow">
	<div class="row">
		<div class="col-md-12">
			<div class="table-render table-scrollable">
				<table class="table table-bordered">
					<?php
						$headSrc = $bodySrc = '<tr>';
						$userId = 0;
						$externalUrl = Configure::read('externalUrl');
						foreach ((array)$latestReports as $data) {
							if ($userId != $data['User']['id']) {
								if ($userId > 0) {
									$bodySrc .= '</table></td>';
								}
								$headSrc .= '<td>' . $data['User']['last_name'] . '&nbsp;' . $data['User']['first_name'] . '</td>';
								$bodySrc .= '<td><table class="inner-list">';
								$userId = $data['User']['id'];
							}
							
							if (empty($data['WorkReport']['sum_work_count'])) {
								continue;
							}
							
							$linkSrc = '';
							if (!empty($data['Matter']['identifier'])) {
								$linkSrc .= '<a href="'.sprintf($externalUrl, $data['Matter']['identifier']).'" target="_blank">' . $data['Matter']['matter_name'] . '</a>';
							} else {
								$linkSrc .= $data['Matter']['matter_name'];
							}
							
							$bodySrc .= '<tr><td>' . $linkSrc . '</td><td>：' . number_format($data['WorkReport']['sum_work_count']) . '</td></tr>';
						}
						
						if ($userId != 0) {
							$bodySrc .= '</table></td>';
						}
						
						$headSrc .= '</tr>';
						$bodySrc .= '</tr>';
						
						echo '<thead>' . $headSrc . '</thead>';
						echo '<tbody>' . $bodySrc . '</tbody>';
					?>
				</table>
			</div>
		</div>
	</div>
</div>
<?php endif; ?>