
<div class="text-left">
	<span>所属: <?php
		echo($department.'-'.$section);?>&nbsp;&nbsp;
	</span>
	<span>氏名: <?php echo($employee);?></span>
</div>
<table border="1" width="100%" class="tbl tbl-bordered bold">
	<thead>
		<tr>
			<th class="text-center" rowspan="2" width="4%">日</th>
			<th class="text-center" rowspan="2" width="4%">曜日</th>
			<th class="text-center" rowspan="2" width="7%">開始時刻</th>
			<th class="text-center" rowspan="2" width="7%">終了時刻</th>
			<th class="text-center" rowspan="2" width="7%">勤務時間</th>
			<th class="text-center" colspan="2" width="29%">休暇</th>
			<th class="text-center" rowspan="2" width="7%">休日出勤</th>
			<th class="text-center" rowspan="2" width="14%">早朝・普通残業時間</th>
			<th class="text-center" rowspan="2" width="10%">深夜残業時間</th>
			<th class="text-center" rowspan="2" width="10%">夜勤時間</th>
		</tr>
		<tr>
			<th class="text-center" width="10%">種別</th>
			<th class="text-center" width="19%">時間休時間・対象休出日</th>
		</tr>
	</thead>
	<tbody>
		<?php 
		$start_month = date('m',strtotime($YmdAarray[0]));
		$end_month = date('m',strtotime($YmdAarray[1]));
		$start_number = cal_days_in_month(CAL_GREGORIAN, $start_month, $year);

		$d = count($data);
		$start = constant('START_BUSINESS_DAY');
		for($i=$start;$i<=$start_number;$i++){ 
			for($x=0;$x<$d;$x++){
				$day = $day_format[date('D',strtotime($year.'-'.$start_month.'-'.$i))];
				$opt_day = date('d',strtotime($data[$x]['employment_date']));
				$start_datetime =($data[$x]['start_datetime']!=null) ? date('h:i',strtotime($data[$x]['start_datetime'])):'';
				$end_datetime = ($data[$x]['end_datetime']!=null) ? date('H:i',strtotime($data[$x]['end_datetime'])):'';
				$actual_operating_time = $data[$x]['actual_operating_time'];
				$type = $data[$x]['holidaywork']['type'];
				$value = $data[$x]['holidaywork']['value'];
				$overtime = (!empty($data[$x]['otlot']['over_time_decimal'])) ? $data[$x]['otlot']['over_time_decimal'] : '0:00';
				$lateovertime = (!empty($data[$x]['otlot']['late_over_time_decimal'])) ? $data[$x]['otlot']['late_over_time_decimal'] : '0:00';
				$late_night_overtime = $data[$x]['late_night_overtime'];
				$holidayWorkKbn = $data[$x]['holidayWorkKbn'];
//						 <td class="text-center" width="7%">' . $this->Time->timeFormat60($actual_operating_time) . '</td>
				if($opt_day ==$i){ 
					echo '<tr>
						 <td class="text-center" width="4%">'.$i.'</td>
						 <td class="text-center" width="4%">'.$day.'</td>
						 <td class="text-center" width="7%">'.$start_datetime.'</td>
						 <td class="text-center" width="7%">'.$end_datetime.'</td>
						 <td class="text-center" width="7%">'.$actual_operating_time. '</td>
						 <td class="text-center" width="10%">'.$type.'</td>
						 <td class="text-center" width="19%">'.$value.'</td>
						 <td class="text-center" width="7%">'.$holidayWorkKbn.'</td>
						 <td class="text-center" width="14%">'. $this->Time->timeFormat60($overtime)  .'</td>
						<td class="text-center" width="10%">' . $this->Time->timeFormat60($lateovertime)   .'</td>
						 <td class="text-center" width="10%">'. $this->Time->timeFormat60($late_night_overtime) .'</td></tr>';
						 $i++;
						 $day = $day_format[date('D',strtotime($year.'-'.$start_month.'-'.$i))];
				}
			}
			if($i<=$start_number){
				echo '<tr>
						 <td class="text-center" width="4%">'.$i.'</td>
						 <td class="text-center" width="4%">'.$day.'</td>
						 <td width="7%"></td>
						 <td width="7%"></td>
						 <td width="7%"></td>
						 <td width="10%"></td>
						 <td width="19%"></td>
						 <td width="7%"></td>
						 <td width="14%"></td>
						 <td width="10%"></td>
						 <td width="10%"></td></tr>';
			}
		}?>
	</tbody>
	<tfoot>
	    <tr>
			<td class="text-center" colspan="2">合計</td>
			<td colspan="2"></td>
			<td class="text-center"><?php echo $this->Time->timeFormat60($Sum_actual_operating_time); ?></td>
			<td colspan="3"></td>
			<td class="text-center"><?php echo (!empty($Sum_OT)) ? $this->Time->timeFormat60($Sum_OT) : '0:00'; ?></td>
			<td class="text-center"><?php echo (!empty($Sum_LOT)) ? $this->Time->timeFormat60($Sum_LOT) : '0:00'; ?></td>
			<td class="text-center"><?php echo $this->Time->timeFormat60($Sum_late_night_overtime); ?></td>
	    </tr>
	</tfoot>
</table>
<style>
    .text-center {
    	text-align: center;
    }
</style>