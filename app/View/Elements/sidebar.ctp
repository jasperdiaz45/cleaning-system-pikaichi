<div id="sidebar" class="sidebar">
    <ul class="sidebar-menu">
        <?php
			$authorityPath = 'authority.authorityNo'.$auth->user('role');
			$authority = Configure::read($authorityPath.'.workReports');

			echo '<li id="inputs">';
				echo '<a href="'.$siteURL.'Routes/'.'">';
				echo '集配業務';
				echo '</a>';
			echo '</li>';
			echo '<li>';
				echo '<a href="'.$siteURL.'Store/'.'">';
				echo '預り伝票改定  ';
				echo '</a>';
			echo '</li>';
			echo '<li>';
				echo '<a href="'.$siteURL.'Collections/'.'">';
				echo '回収金額確認  ';
				echo '</a>';
			echo '</li>';
			
			//課内承認者・上席承認・最終承認者のみ
			// if($auth->user('responsibility') == 1 || $auth->user('responsibility') == 2 || $auth->user('responsibility') == 3){
			// 	echo '<li>';
			// 		echo '<a href="'.$siteURL.'ApplicationApprovals'.'">';
			// 		echo '<span class="title">承認</span>';
			// 		echo '</a>';
			// 	echo '</li>';
			// 	echo '<li>';
			// 		echo '<a href="'.$siteURL.'AttendanceManagements'.'">';
			// 		echo '<span class="title">就業管理</span>';
			// 		echo '</a>';
			// 	echo '</li>';
			// 	echo '<li>';
			// 		echo '<a href="'.$siteURL.'Shifts'.'">';
			// 		echo '<span class="title">シフト登録</span>';
			// 		echo '</a>';
			// 	echo '</li>';
			// }

			// //上席承認・最終承認者のみ
			// if($auth->user('responsibility') == 2 || $auth->user('responsibility') == 3){
			// 	echo '<li>';
			// 		echo '<a href="'.$siteURL.'MonthlyProcessings'.'">';
			// 		echo '<span class="title">月次処理</span>';
			// 		echo '</a>';
			// 	echo '</li>';
			// }

//			echo '<li>';
//				echo '<a href="'.$siteURL.'ProjectManagements'.'">';
//				echo '<span class="title">商談管理</span>';
//				echo '</a>';
//			echo '</li>';


//			if($auth->user('responsibility') == 1 || $auth->user('responsibility') == 2 || $auth->user('responsibility') == 3){
//				echo '<li>';
//					echo '<a href="'.$siteURL.'DivisionDetails'.'">';
//					echo '<span class="title">部門管理</span>';
//					echo '</a>';
//				echo '</li>';
//				echo '<li>';
//					echo '<a href="'.$siteURL.'WorkOperationSummarys'.'">';
//					echo '<span class="title">ユーザー別稼働集計</span>';
//					echo '</a>';
//				echo '</li>';
//				echo '<li>';
//					echo '<a href="'.$siteURL.'ProjectAttendanceSummarys'.'">';
//					echo '<span class="title">商談別稼働集計</span>';
//					echo '</a>';
//				echo '</li>';
//				//システム管理者のみ
//				if($auth->user('authority_no') == 1){
//					echo '<li>';
//						echo '<a href="'.$siteURL.'users'.'">';
//						echo '<span class="title">社員管理</span>';
//						echo '</a>';
//					echo '</li>';
//					echo '<li>';
//						echo '<a href="'.$siteURL.'Organizations'.'">';
//						echo '<span class="title">組織管理</span>';
//						echo '</a>';
//					echo '</li>';
//				}
//			}

			echo '<li>';
			echo '<a href="'.$siteURL.'Users/Logout'.'">';
			echo '<span class="title">ログアウト (Logout)</span>';
			echo '</a>';
			echo '</li>';
		?>
    </ul>
</div>

<style>
h3{
	padding-left: 3px;
	border-left: 5px solid #0092C4;
}
</style>
<script>
function activeMenu() {
    var curr_url = window.location.pathname ;
    var curr_menu = $("a[href$='" + curr_url + "']");
    $(curr_menu).parent("li").css("background", "#DAD8DC");
}
activeMenu();

</script>