<!-- <h3><?= $screenTitle ?></h3> -->
<?php echo $this->assign('title', $title); ?>

<div class="page-container">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-sm-9" style="padding: 0px;">
					
					<div><?php echo $this->Form->create('Shifts'); ?></div>
					
					<div class="contentRow">

	                    <div class="col-md-12">
	           			
						<h3><b>伝票一覧</b></h3>
						<br> <br> 

                    	
						
						<div class="row">
							
							<div class="col-sm-5 no-padding mb-2">
								<label class="label">仕上り予定日</label>
								<div class="pickerWrap">
									<div class="input-group date datepicker" id="datepicker2" data-target-input="nearest">
									    <?php echo $this->Form->input('', array('div' => false,'label' => false, 'class'=>'form-control datetimepicker-input', 'type' => 'text','default'=>  date('Y/m/d') )); ?>
										<div class="input-group-append" data-target="#datepicker2" data-toggle="datetimepicker">
											<div class="input-group-text"><i class="far fa-calendar-alt"></i></div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-sm-7 no-padding">
								<label class="label">店舗/個人名</label>
								<?= $this->form->input('', array('class' => '' , 'label' => false,'div' => false)); ?>
							</div>

							<div class="col-sm-8 no-padding">
								<div class="mt-2">
									<label class="label">店舗/個人名</label>
								<?= $this->form->input('', array('class' => '' , 'label' => false,'div' => false)); ?>
								</div>
							</div>
							<div class="col-sm-4 text-right mb-2">
								<?= $this->form->button('検索', array('class' => 'btn btn-info', 'style' => 'width:90px;')) ?>
							</div>
						</div>
					
	                    </div>
	               	</div>
	            	 <div><?php echo $this->Form->end(); ?></div>
				</div>
				<div class="col-sm-3"></div>

				<div class="col-sm-12 no-padding">
					<ul class="pagination">
						<li><a>全99件｜1〜99件表示｜</a></li>
						<li><a href="">1</a></li>
						<li><a href="">2</a></li>
						<li><a href="">3</a></li>
						<li><a href="">4</a></li>
						<li><a href="">5</a></li>
					</ul>
				</div>
					<div class="col-sm-10">
						 <br>
							<section class="attendanceDetail">

	                    	<div class="table">
								<table class="table table-bordered table-sm table-striped" >
									<thead>
										<tr>
											<th >店舗/個人名</th>
											<th>伝票番号</th>
											<th>仕上り予定日</th>
											<th>点数</th>
											<th>金額</th>
											<th class="text-center">詳細</th>
										</tr>
									</thead>
									<tbody>
										<?php for ($i=0; $i < 10; $i++) { ?>
											<tr>
												<td>REEKS</td>
												<td>AA20190420121231</td>
												<td>2019/04/28</td>
												<td>2</td>
												<td>14,000</td>
												<td class="text-center"><?= $this->html->link('詳細', array('controller' =>'Delivery','action' => 'SlipDetails'), array('class' => 'btn btn-primary')) ?></td>
											</tr>
										<?php  }?>
									</tbody>
								</table>
							</div>
						</section>
					</div>
					<div class="col-sm-2"></div>
				</div>
				</div>
			</div>	
		
			<!-- END PAGE CONTENT-->
	</div>

	


