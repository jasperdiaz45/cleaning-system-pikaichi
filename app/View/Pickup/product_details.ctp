<!-- <h3><?= $screenTitle ?></h3> -->
<?php echo $this->assign('title', $title); ?>
<div class="page-container">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-sm-9" style="padding: 0px;">
					
					<div><?php echo $this->Form->create('Shifts'); ?></div>
					
					<div class="contentRow">
						<div class="col-md-12">
							
							<h3><b>個人備考</b></h3>
							
							<div class="row">
								<div class=" col-sm-12 text-right no-padding">
									
									<?= $this->html->link('戻る', array('controller' => 'Pickup', 'action' => 'PickupInput'), array('class' => 'btn btn-return' ,'style'=>'width: 90px;')) ?>
									
								</div>
								<div class="col-sm-12 no-padding mt-2">
									<label class="label-long label">大分類</label>
									<?php $option4 = array( 0 => 'シャツ類', 1 => '布類', 2 => '男性黒物', 3 => '女性黒物', 4 => '付随着物', 5 => '男性私服', 6 => '女性私服', 7 => '衣装', 8 => 'その他', 9=>'旧価格', 10 =>'MUSEO'); ?>
									<?= $this->form->input('', array('div' => false, 'label' => false, 'options' =>$option4 , 'class' => 'drop-width' )) ?>
								</div>
								<div class="col-sm-12 no-padding mt-2">
									<label class="label-long label">小分類</label>
									<?php $option4 = array( 0 => 'Yシャツ', 1 => 'ウイングYシャツ', 2 => 'オープンシャツ', 3 => 'Tシャツ', 4 => 'ポロシャツ', 5 => 'ブラウス'); ?>
									<?= $this->form->input('', array('div' => false, 'label' => false, 'options' =>$option4 , 'class' => 'drop-width','style'=>'width: 80px;' )) ?>
								</div>
								
								<div class="col-sm-7 no-padding mt-2">
									<label class="label-long label">商品</label>
									<?php $option4 = array( 0 => '・ウイングYシャツ', 1 => ''); ?>
									<?= $this->form->input('', array('div' => false, 'label' => false, 'options' =>$option4 , 'class' => 'drop-width','style'=>'width: 160px;' )) ?>
								</div>
								<div class="col-sm-5 no-padding mt-2">
									<label class="label-long label">価格</label>
									<?= $this->form->input('', array('class' => 'form-lable form-sm' , 'label' => false,'div' => false, 'readonly', 'value' => '300円')); ?>
								</div>
								<div class="col-sm-12 no-padding mt-2">
									<label class="label-long label">色</label>
									<?php $option4 = array( 0 => ''); ?>
									<?= $this->form->input('', array('div' => false, 'label' => false, 'options' =>$option4 , 'class' => 'drop-width','style'=>'width: 160px;' )) ?>
								</div>
								<div class="col-sm-12 no-padding mt-2">
									<label class="label-long label">柄</label>
									<?php $option4 = array( 0 => ''); ?>
									<?= $this->form->input('', array('div' => false, 'label' => false, 'options' =>$option4 , 'class' => 'drop-width','style'=>'width: 160px;' )) ?>
								</div>
								<div class="col-sm-12 no-padding mt-2">
									<label class="label-long label">生地</label>
									<?php $option4 = array( 0 => ''); ?>
									<?= $this->form->input('', array('div' => false, 'label' => false, 'options' =>$option4 , 'class' => 'drop-width','style'=>'width: 160px;' )) ?>
								</div>
								<div class="col-sm-12 no-padding mt-2">
									<label class="label-long label">生地</label>
									<?= $this->form->input('', array('class' => '' , 'label' => false,'div' => false,'style'=>'width: 80px;')); ?>
								</div>
								
								
								<div class="col-sm-3 no-padding	">
									<br>
									<div class="mt-2">
										<?= $this->html->link('登録', array('controller'=>'Pickup','action'=>'PickupInput'), array('class' => 'btn btn-orange btn-block')) ?>
									</div>
								</div>
								<div class="col-sm-3 no-padding	">	</div>
								
							</div>
							
						</div>
					</div>
					<div><?php echo $this->Form->end(); ?></div>
				</div>
				
				<div class="col-sm-2"></div>
			</div>
		</div>
	</div>
	
	<!-- END PAGE CONTENT-->
</div>