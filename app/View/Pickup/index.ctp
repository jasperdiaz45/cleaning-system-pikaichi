<!-- <h3><?= $screenTitle ?></h3> -->
<?php echo $this->assign('title', $title); ?>

<div class="page-container">
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">
            <!-- BEGIN PAGE CONTENT-->
            <div class="row">
                <div class="col-sm-12 no-padding" style="padding: 0px;">
                    
                    <div><?php echo $this->Form->create('Shifts'); ?></div>
                    
                    <div class="contentRow">
                        <div class="col-sm-12">
                            <div class="row">
                                <div class="col-sm-6 no-padding">
                                    <h3><b>集荷処理</b></h3>
                                </div>
                                <div class="col-sm-2">
                                    <?= $this->html->link('配送処理', array('controller' => 'Delivery', 'action' => 'DeliveryProcessing'), array('class' => 'btn btn-lime btn-block mb-2')) ?>
                                </div>
                                <div class="col-sm-2">
                                    <?= $this->html->link('集金処理', array('controller' => 'Collections', 'action' => 'CollectionProcess'), array('class' => 'btn btn-red btn-block mb-2')) ?>
                                </div>
                                <div class="col-sm-2">
                                    <?= $this->html->link('戻る', array('controller' => 'Routes', 'action' => 'SearchByRoutes'), array('class' => 'btn btn-return btn-block mb-2')) ?>
                                </div>      
                            </div>
                        
                            <br>
                            <div class="row">
                                <div class="col-sm-6 no-padding mb-2">
                                    <label class="label">店舗名</label>
                                    <?= $this->form->input('', array('class' => 'form-lable' , 'label' => false,'div' => false, 'readonly', 'value' => 'REEX')); ?>
                                </div>
                                <div class="col-sm-2">
                                     <?= $this->html->link('備考', array('controller'=>'Store','action'=>'StoreRemarks'), array('class' => 'btn btn-teal btn-block')) ?>
                                </div>
                                <div class="col-sm-4 no-padding"></div>

                            </div>              
                        </div>
                    </div>
                     <div><?php echo $this->Form->end(); ?></div>
                </div>
                

                    <div class="col-sm-10">
                        <section class="attendanceDetail">
                            <br>
                            <div class="text-right mb-2">
                                <?= $this->html->link('個人追加', array('controller'=>'Personal','action' => 'index'), array('class' => 'btn btn-primary' , 'style' => 'width:80px;')) ?>
                            </div>
                            <div class="table">
                                <table class="table table-bordered table-sm table-striped">
                                    <thead style="background: #dde1e3; ">
                                        <tr>
                                            <th>個人</th>
                                            <th class="text-center" width="50">集荷入力</th>
                                            <th class="text-center" width="50">備考</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php for ($i=0; $i <10; $i++) { ?>
                                            <tr>
                                                <td>
                                                    <?= $this->html->link('優希', array(), array('class' => 'links' , 'style' => 'width:80px;')) ?>
                                                </td>
                                                <td class="text-center">
                                                    <?= $this->html->link('集荷入力', array('controller' => 'PickUp', 'action' => 'PickupInput'), array('class' => 'btn btn-primary' , 'style' => 'width:80px;')) ?>
                                                </td>
                                                 <td class="text-center">
                                                    <?= $this->html->link('備考', array('controller'=>'Personal','action' => 'PersonalRemarks'), array('class' => 'btn btn-teal' , 'style' => 'width:80px;')) ?>    
                                                </td>
                                            </tr>
                                        <?php } ?> 
                                    </tbody>
                                </table>
                            </div>
                        </section>
                    </div>
                    <div class="col-sm-2"></div>
                    
                </div>
                </div>
            </div>  
        
            <!-- END PAGE CONTENT-->
    </div>

    


