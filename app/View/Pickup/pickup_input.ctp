<?php echo $this->assign('title', $title); ?>
<h3><?php echo $title ?></h3>
<div class="page-container">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class ="col-sm-6 no-padding"></div>
				<div class="col-sm-2 text-right">
					<?= $this->html->link('戻る', array('controller' => 'Pickup', 'action' => 'index'),array('class' => 'btn btn-gray mb-2', 'style' => 'width:90px;')) ?>
				</div>
				<div class="col-sm-12">
					<div><?php echo $this->Form->create('Shifts'); ?></div>
					
					<div class="contentRow">
						<div class="col-md-12 no-padding">
							
							<div class="row">
								<div class="col-sm-6 no-padding mb-2">
									<label class="label">預り伝票番号</label>
									<?= $this->form->input('', array('class' => 'form-lable' , 'label' => false,'div' => false, 'readonly', 'value' => 'A1143151212114629')); ?>
								</div>
								<div class="col-sm-6"></div>
								<div class="col-sm-4 no-padding mb-2">
									<label class="label">店舗名</label>
									<?= $this->form->input('', array('class' => 'form-lable' , 'label' => false,'div' => false, 'readonly', 'value' => 'REEK')); ?>
								</div>
								<div class="col-sm-8 no-padding">
									<?= $this->form->button('店舗情報', array('class' => 'btn btn-primary mb-2', 'style' => 'width:90px;')) ?>
								</div>
								<div class="col-sm-4 no-padding mb-2">
									<label class="label">配送日指定</label>
									
									<?= $this->form->input('', array('div' => false, 'label' => false, 'options' =>$this->Option->SpecifyDeliveryDateOption() , 'class' => 'drop-width','style' => 'width:90px;' )) ?>
									
								</div>
								<div class="pickerWrap col-sm-8	 no-padding mb-2">
									<div class="input-group date datepicker" id="datepicker2" data-target-input="nearest">
										<?php echo $this->Form->input('', array('div' => false,'label' => false, 'class'=>'form-control datetimepicker-input', 'type' => 'text','default'=>  date('Y/m/d') )); ?>
										<div class="input-group-append" data-target="#datepicker2" data-toggle="datetimepicker"	>
											<div class="input-group-text"><i class="far fa-calendar-alt"></i></div>
										</div>
									</div>
								</div>
								
							</div>
						</div>
						
						
					</div>
				</div>
				<div><?php echo $this->Form->end(); ?></div>
			</div>
			<div class="col-sm-3"></div>
			<div class="col-sm-8">
				
				<div class="row mb-2">
					<div class="col-sm-6 no-padding">
						<?= $this->form->button('削除', array('class' => 'btn btn-red', 'style' => 'width:90px;')) ?>
					</div>
					<div class="col-sm-6 text-right">
						
						<?= $this->html->link('商品追加', array('action' => 'ProductDetails'), array('class' => 'btn btn-primary')) ?>
					</div>
				</div>
				<section class="attendanceDetail">
					<div class="table">
						<table class="table table-bordered table-sm table-striped">
							<thead style="background: #dde1e3; ">
								<tr>
									<th>選択</th>
									<th>商品</th>
									<th>数量	</th>
									<th>詳細</th>
									
								</tr>
							</thead>
							<tbody>
								<?php for ($i=0; $i < 5 ; $i++) { ?>
								
								<tr>
									<td><?= $this->form->checkbox('', array('class' => '' , 'label' => false,'div' => false,'size'=>'4')); ?></td>
									<td>ワイシャツ</td>
									<td><?= $this->form->inp1ut('', array('class' => '' , 'label' => false,'div' => false,'size'=>'4')); ?></td>
									<td class="text-center"><?= $this->html->link('詳細', array('action' => 'ProductDetails'), array('class' => 'btn btn-primary')) ?></td>
								</tr>
								
								<?php } ?>
							</tbody>
						</table>
					</div>
				</section>
			</div>
			<div class="col-sm-2"></div>
		</div>
	</div>
</div>
<!-- END PAGE CONTENT-->
</div>