<!-- <h3><?= $screenTitle ?></h3> -->
<?php echo $this->assign('title', $title); ?>

<div class="page-container">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-sm-9" style="padding: 0px;">
				<div><?php echo $this->Form->create('Shifts'); ?></div>
				
				<div class="contentRow">

                    <div class="col-md-12 no-padding">
                		<h3><b>ルート選択</b></h3>
                		<h3><b>店舗名やコードから検索する</b></h3>


                		<div class="card mb-2">
                			<div class="card-body" style="padding: 10px;">
                				<div class="row">
				                    <div class="col-sm-8 no-padding">
				                    	<label class="label">店舗名・コード</label>
				                    	<?= $this->form->input('', array('div' => false, 'lable' => false,'class' => 'mb-2 form-lg')) ?>
				                    </div>
				                    <div class="col-sm-2 no-padding">
				                    	<?= $this->form->button('検索', array('class' => 'btn btn-primary btn-block ')) ?>
				                    </div>
				                    <div class="col-sm-2"></div>
                				</div>
                			</div>
                		</div>

                		<br><br>
                		<h3><b>ルートから検索する</b></h3>
						<div class="table-responsive">
							<table class="table table-bordered table-striped table-sm">
								<tbody>
								<?php for ($i=0; $i < 10 ; $i++) { ?>
										<tr>
											<td><?= $this->html->link('六本木', array('controller' => 'Routes' ,'action' => 'SearchByRoutes'), array('class' => 'links')) ?></td>

										</tr>
								<?php }?>
								</tbody>
							</table>
						</div>

                    </div>
               	</div>
            	 <div><?php echo $this->Form->end(); ?></div>
				</div>
				<div class="col-sm-2"></div>
			</div>
		</div>
			</div>	
		
			<!-- END PAGE CONTENT-->
	</div>

	

