<!-- <h3><?= $screenTitle ?></h3> -->
<?php echo $this->assign('title', $title); ?>

<div class="page-container">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-sm-12" style="padding: 0px;">
					
					<div><?php echo $this->Form->create('Shifts'); ?></div>
					
					<div class="contentRow">
	                    <div class="col-sm-12">
							<h3><b>店舗処理</b></h3>
							<br>
							<div class="row">
								<div class="col-sm-4 no-padding mb-1 ">
									<label class="label">店舗</label>
									<?= $this->form->input('', array('class' => 'form-lable' , 'label' => false,'div' => false, 'readonly', 'value' => 'REEX')); ?>
								</div>
								<div class="col-sm-4 no-padding mb-1">
									<label class="label">支払区分</label>
									<?= $this->form->input('', array('class' => 'form-lable' , 'label' => false,'div' => false, 'readonly', 'value' => '掛売')); ?>
								</div>
								<div class="col-sm-2  mb-1">
									<?= $this->html->link('次の店', array('controller'=>'Delivery','action' => 'SlipDetails'), array('class' => 'btn btn-primary btn-block')) ?>
								</div>
								<div class="col-sm-2 mb-1 ">
									<?= $this->html->link('戻る', array('controller' => 'Routes', 'action' => 'index'), array('class' => 'btn btn-return btn-block')) ?>
								</div>

							</div>				
	                    </div>
	               	</div>
	            	 <div><?php echo $this->Form->end(); ?></div>
				</div>
				

					<div class="col-sm-10">
						<div class="row">
							<div class="col-sm-4 no-padding mt-4">
								<?= $this->html->link('配送処理', array('controller' => 'Delivery','action' => 'DeliveryProcessing'), array('class' => 'btn btn-lime btn-block')) ?>
							</div>
							<div class="col-sm-8"></div>

							<div class="col-sm-4 no-padding mt-4">
								<?= $this->html->link('集荷処理', array('controller' => 'PickUp','action' => 'index'), array('class' => 'btn btn-teal btn-block')) ?>
							</div>
							<div class="col-sm-8"></div>

							<div class="col-sm-4 no-padding mt-4">
								<?= $this->html->link('集金処理', array('controller' => 'Collections','action' => 'CollectionProcess'), array('class' => 'btn btn-red btn-block')) ?>
							</div>
							<div class="col-sm-8"></div>

							<div class="col-sm-4 no-padding mt-4">
								<?= $this->html->link('メモ登録', array(), array('class' => 'btn btn-orange btn-block')) ?>
							</div>
							<div class="col-sm-8"></div>
						</div>
					</div>
					<div class="col-sm-2"></div>
					
					<div class="col-sm-12">
						<div class="mt-5"></div>
						<hr>
						<div class="mb-2">メモが登録されている場合、ポップアップを表示</div>
						<div class="card">						
							<div class="card-body p-5">
									
								<div class="mb-4">
									１階で鍵を貰い２階に納品したのち1階で集金
								</div>


								<div class="text-center">
										<?= $this->html->link('OK', array(), array('class' => 'btn btn-primary', 'style' => 'width:80px;')) ?>
								
								</div>
							</div>
						</div>
					</div>
				</div>
				</div>
			</div>	
		
			<!-- END PAGE CONTENT-->
	</div>

	


