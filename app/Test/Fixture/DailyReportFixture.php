<?php
/**
 * DailyReport Fixture
 */
class DailyReportFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary', 'comment' => 'ID'),
		'operation_no' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'comment' => '稼働ID'),
		'work_content' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 1024, 'collate' => 'utf8_general_ci', 'comment' => '作業内容', 'charset' => 'utf8'),
		'liaison_matters_problems' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 1024, 'collate' => 'utf8_general_ci', 'comment' => '連絡事項・問題点', 'charset' => 'utf8'),
		'schedule_tomorrow' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 256, 'collate' => 'utf8_general_ci', 'comment' => '明日の予定', 'charset' => 'utf8'),
		'report_date' => array('type' => 'date', 'null' => false, 'default' => null, 'comment' => '報告日'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB', 'comment' => '日報テーブル')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'operation_no' => 1,
			'work_content' => 'Lorem ipsum dolor sit amet',
			'liaison_matters_problems' => 'Lorem ipsum dolor sit amet',
			'schedule_tomorrow' => 'Lorem ipsum dolor sit amet',
			'report_date' => '2019-01-15'
		),
	);

}
