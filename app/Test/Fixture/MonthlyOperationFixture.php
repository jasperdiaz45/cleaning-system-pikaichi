<?php
/**
 * MonthlyOperation Fixture
 */
class MonthlyOperationFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary', 'comment' => 'ID'),
		'company_no' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'comment' => '会社ID'),
		'division_no' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'comment' => '事業部ID'),
		'department_no' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'comment' => '部ID'),
		'section_no' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'comment' => '課ID'),
		'employee_no' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'comment' => '社員ID'),
		'year_month' => array('type' => 'date', 'null' => false, 'default' => null, 'comment' => '年月'),
		'approval' => array('type' => 'boolean', 'null' => true, 'default' => null, 'comment' => '承認'),
		'comment' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 2048, 'collate' => 'utf8_general_ci', 'comment' => 'コメント', 'charset' => 'utf8'),
		'approver_no' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false, 'comment' => '承認者ID'),
		'approval_date' => array('type' => 'datetime', 'null' => true, 'default' => null, 'comment' => '承認日時'),
		'senior_approval' => array('type' => 'boolean', 'null' => true, 'default' => null, 'comment' => '上席承認'),
		'senior_comment' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 2048, 'collate' => 'utf8_general_ci', 'comment' => '上席コメント', 'charset' => 'utf8'),
		'senior_approver_no' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false, 'comment' => '上席承認者ID'),
		'senior_approval_date' => array('type' => 'datetime', 'null' => true, 'default' => null, 'comment' => '上席承認日時'),
		'final_confirmation' => array('type' => 'boolean', 'null' => true, 'default' => null, 'comment' => '最終確認'),
		'final_confirmer_no' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false, 'comment' => '最終確認者ID'),
		'final_confirmation_date' => array('type' => 'datetime', 'null' => true, 'default' => null, 'comment' => '最終確認日時'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB', 'comment' => '月次稼働テーブル')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'company_no' => 1,
			'division_no' => 1,
			'department_no' => 1,
			'section_no' => 1,
			'employee_no' => 1,
			'year_month' => '2019-01-15',
			'approval' => 1,
			'comment' => 'Lorem ipsum dolor sit amet',
			'approver_no' => 1,
			'approval_date' => '2019-01-15 18:27:46',
			'senior_approval' => 1,
			'senior_comment' => 'Lorem ipsum dolor sit amet',
			'senior_approver_no' => 1,
			'senior_approval_date' => '2019-01-15 18:27:46',
			'final_confirmation' => 1,
			'final_confirmer_no' => 1,
			'final_confirmation_date' => '2019-01-15 18:27:46'
		),
	);

}
