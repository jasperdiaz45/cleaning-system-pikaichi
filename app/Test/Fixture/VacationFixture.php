<?php
/**
 * Vacation Fixture
 */
class VacationFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary', 'comment' => 'ID'),
		'employee_no' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'comment' => '社員ID'),
		'year' => array('type' => 'date', 'null' => false, 'default' => null, 'comment' => '年度'),
		'number_of_working_days' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false, 'comment' => '所定勤務日数'),
		'number_of_days_that_were_taken_during_the_year' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false, 'comment' => '当年有休取得数'),
		'annual_number' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false, 'comment' => '年次回数'),
		'days_remaining_on_holiday' => array('type' => 'decimal', 'null' => true, 'default' => null, 'length' => '3,1', 'unsigned' => false, 'comment' => '有休日数'),
		'days_on_holiday' => array('type' => 'decimal', 'null' => true, 'default' => null, 'length' => '3,1', 'unsigned' => false, 'comment' => '取得有休数'),
		'absents_without_notice' => array('type' => 'decimal', 'null' => true, 'default' => null, 'length' => '3,1', 'unsigned' => false, 'comment' => '欠勤日数'),
		'holiday_working_days' => array('type' => 'decimal', 'null' => true, 'default' => null, 'length' => '3,1', 'unsigned' => false, 'comment' => '休日出勤日数'),
		'compensation_day_off' => array('type' => 'decimal', 'null' => true, 'default' => null, 'length' => '3,1', 'unsigned' => false, 'comment' => '代休日数'),
		'time_off' => array('type' => 'decimal', 'null' => true, 'default' => null, 'length' => '3,1', 'unsigned' => false, 'comment' => '時間休'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB', 'comment' => '休暇テーブル')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'employee_no' => 1,
			'year' => '2019-01-15',
			'number_of_working_days' => 1,
			'number_of_days_that_were_taken_during_the_year' => 1,
			'annual_number' => 1,
			'days_remaining_on_holiday' => '',
			'days_on_holiday' => '',
			'absents_without_notice' => '',
			'holiday_working_days' => '',
			'compensation_day_off' => '',
			'time_off' => ''
		),
	);

}
