<?php
/**
 * Holiday Fixture
 */
class HolidayFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary', 'comment' => 'ID'),
		'company_no' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'comment' => '会社ID'),
		'year' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'comment' => '年度'),
		'month' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'comment' => '月'),
		'day' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'comment' => '日'),
		'holiday_division' => array('type' => 'boolean', 'null' => false, 'default' => null, 'comment' => '休日区分'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB', 'comment' => '休日テーブル')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'company_no' => 1,
			'year' => 1,
			'month' => 1,
			'day' => 1,
			'holiday_division' => 1
		),
	);

}
