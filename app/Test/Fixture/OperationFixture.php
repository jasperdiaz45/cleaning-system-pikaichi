<?php
/**
 * Operation Fixture
 */
class OperationFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary', 'comment' => 'ID'),
		'company_no' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'comment' => '会社ID'),
		'division_no' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'comment' => '事業部ID'),
		'department_no' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'comment' => '部ID'),
		'section_no' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'comment' => '課ID'),
		'employee_no' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'comment' => '社員ID'),
		'monthly_operation_no' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false, 'comment' => '月次稼働ID'),
		'employment_date' => array('type' => 'date', 'null' => true, 'default' => null, 'comment' => '出社日'),
		'start_datetime' => array('type' => 'datetime', 'null' => true, 'default' => null, 'comment' => '稼働開始日時'),
		'stamp_start_datetime' => array('type' => 'datetime', 'null' => true, 'default' => null, 'comment' => '打刻開始日時'),
		'reason_change_start_datetime' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 256, 'collate' => 'utf8_general_ci', 'comment' => '開始日時変更理由', 'charset' => 'utf8'),
		'end_datetime' => array('type' => 'datetime', 'null' => true, 'default' => null, 'comment' => '稼働終了日時'),
		'stamp_end_datetime' => array('type' => 'datetime', 'null' => true, 'default' => null, 'comment' => '打刻終了日時'),
		'reason_change_end_datetime' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 256, 'collate' => 'utf8_general_ci', 'comment' => '終了日時変更理由', 'charset' => 'utf8'),
		'overtime' => array('type' => 'decimal', 'null' => true, 'default' => null, 'length' => '3,1', 'unsigned' => false, 'comment' => '残業時間'),
		'late_night_overtime' => array('type' => 'decimal', 'null' => true, 'default' => null, 'length' => '3,1', 'unsigned' => false, 'comment' => '深夜残業'),
		'vacation_division' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false, 'comment' => '休暇区分'),
		'time_holiday' => array('type' => 'decimal', 'null' => true, 'default' => null, 'length' => '3,1', 'unsigned' => false, 'comment' => '時間有休'),
		'actual_operating_time' => array('type' => 'decimal', 'null' => true, 'default' => null, 'length' => '3,1', 'unsigned' => false, 'comment' => '実稼働時間'),
		'operation_approval' => array('type' => 'boolean', 'null' => false, 'default' => '0', 'comment' => '稼働承認'),
		'operation_approver' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false, 'comment' => '稼働承認者（FK）'),
		'date_approved' => array('type' => 'datetime', 'null' => true, 'default' => null, 'comment' => '稼働承認日時'),
		'tmp_insert' => array('type' => 'boolean', 'null' => false, 'default' => '0', 'comment' => '一時保存'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB', 'comment' => '稼働テーブル')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'company_no' => 1,
			'division_no' => 1,
			'department_no' => 1,
			'section_no' => 1,
			'employee_no' => 1,
			'monthly_operation_no' => 1,
			'employment_date' => '2019-01-15',
			'start_datetime' => '2019-01-15 18:27:46',
			'stamp_start_datetime' => '2019-01-15 18:27:46',
			'reason_change_start_datetime' => 'Lorem ipsum dolor sit amet',
			'end_datetime' => '2019-01-15 18:27:46',
			'stamp_end_datetime' => '2019-01-15 18:27:46',
			'reason_change_end_datetime' => 'Lorem ipsum dolor sit amet',
			'overtime' => '',
			'late_night_overtime' => '',
			'vacation_division' => 1,
			'time_holiday' => '',
			'actual_operating_time' => '',
			'operation_approval' => 1,
			'operation_approver' => 1,
			'date_approved' => '2019-01-15 18:27:46',
			'tmp_insert' => 1
		),
	);

}
