<?php
/**
 * VWorkOperationSummary Fixture
 */
class VWorkOperationSummaryFixture extends CakeTestFixture {

/**
 * Table name
 *
 * @var string
 */
	public $table = 'v_work_operation_summarys';

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'division_no' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'comment' => '事業部ID'),
		'department_no' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'comment' => '部ID'),
		'section_no' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'comment' => '課ID'),
		'employee_no' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'comment' => '社員ID'),
		'employment_year' => array('type' => 'integer', 'null' => true, 'default' => null, 'length' => 4, 'unsigned' => false),
		'April' => array('type' => 'decimal', 'null' => true, 'default' => null, 'length' => '25,1', 'unsigned' => false),
		'May' => array('type' => 'decimal', 'null' => true, 'default' => null, 'length' => '25,1', 'unsigned' => false),
		'Jun' => array('type' => 'decimal', 'null' => true, 'default' => null, 'length' => '25,1', 'unsigned' => false),
		'July' => array('type' => 'decimal', 'null' => true, 'default' => null, 'length' => '25,1', 'unsigned' => false),
		'August' => array('type' => 'decimal', 'null' => true, 'default' => null, 'length' => '25,1', 'unsigned' => false),
		'September' => array('type' => 'decimal', 'null' => true, 'default' => null, 'length' => '25,1', 'unsigned' => false),
		'October' => array('type' => 'decimal', 'null' => true, 'default' => null, 'length' => '25,1', 'unsigned' => false),
		'November' => array('type' => 'decimal', 'null' => true, 'default' => null, 'length' => '25,1', 'unsigned' => false),
		'December' => array('type' => 'decimal', 'null' => true, 'default' => null, 'length' => '25,1', 'unsigned' => false),
		'January' => array('type' => 'decimal', 'null' => true, 'default' => null, 'length' => '25,1', 'unsigned' => false),
		'February' => array('type' => 'decimal', 'null' => true, 'default' => null, 'length' => '25,1', 'unsigned' => false),
		'March' => array('type' => 'decimal', 'null' => true, 'default' => null, 'length' => '25,1', 'unsigned' => false),
		'total_time' => array('type' => 'decimal', 'null' => true, 'default' => null, 'length' => '25,1', 'unsigned' => false),
		'indexes' => array(
			
		),
		'tableParameters' => array('comment' => 'VIEW')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'division_no' => 1,
			'department_no' => 1,
			'section_no' => 1,
			'employee_no' => 1,
			'employment_year' => 1,
			'April' => '',
			'May' => '',
			'Jun' => '',
			'July' => '',
			'August' => '',
			'September' => '',
			'October' => '',
			'November' => '',
			'December' => '',
			'January' => '',
			'February' => '',
			'March' => '',
			'total_time' => ''
		),
	);

}
