<?php
/**
 * LeaveMidway Fixture
 */
class LeaveMidwayFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary', 'comment' => 'ID'),
		'operation_no' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'comment' => '稼働ID'),
		'stamp_leave_time' => array('type' => 'time', 'null' => true, 'default' => null, 'comment' => '打刻退出時刻'),
		'leave_time' => array('type' => 'time', 'null' => true, 'default' => null, 'comment' => '退出時刻'),
		'stamp_return_time' => array('type' => 'time', 'null' => true, 'default' => null, 'comment' => '打刻戻り時刻'),
		'return_time' => array('type' => 'time', 'null' => true, 'default' => null, 'comment' => '戻り時刻'),
		'non_working_times' => array('type' => 'decimal', 'null' => true, 'default' => null, 'length' => '3,1', 'unsigned' => false, 'comment' => '非稼働時間'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB', 'comment' => '途中退出テーブル')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'operation_no' => 1,
			'stamp_leave_time' => '18:27:45',
			'leave_time' => '18:27:45',
			'stamp_return_time' => '18:27:45',
			'return_time' => '18:27:45',
			'non_working_times' => ''
		),
	);

}
