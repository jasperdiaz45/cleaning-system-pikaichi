<?php
/**
 * Employee Fixture
 */
class EmployeeFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary', 'comment' => 'ID'),
		'section_no' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'comment' => '課ID'),
		'authority_no' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false, 'comment' => '権限'),
		'responsibility' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false, 'comment' => '職責'),
		'name' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 256, 'collate' => 'utf8_general_ci', 'comment' => '氏名', 'charset' => 'utf8'),
		'mail_address' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 256, 'collate' => 'utf8_general_ci', 'comment' => 'メールアドレス', 'charset' => 'utf8'),
		'password' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 100, 'collate' => 'utf8_general_ci', 'comment' => 'パスワード', 'charset' => 'utf8'),
		'effectiveness' => array('type' => 'boolean', 'null' => false, 'default' => '1', 'comment' => '有効'),
		'job_entry_date' => array('type' => 'date', 'null' => true, 'default' => null, 'comment' => '入社年月日'),
		'ic_card_key' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false, 'comment' => 'ICカードキー'),
		'position_no' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false, 'comment' => '役職ID'),
		'retirement' => array('type' => 'boolean', 'null' => false, 'default' => '0', 'comment' => '退職'),
		'retirement_date' => array('type' => 'date', 'null' => true, 'default' => null, 'comment' => '退職日'),
		'on_leave' => array('type' => 'boolean', 'null' => false, 'default' => '0', 'comment' => '休職中'),
		'attendance_adjustment' => array('type' => 'boolean', 'null' => false, 'default' => '0', 'comment' => '就業調整有り'),
		'start_time' => array('type' => 'time', 'null' => true, 'default' => null, 'comment' => '始業時刻'),
		'closing_time' => array('type' => 'time', 'null' => true, 'default' => null, 'comment' => '終業時刻'),
		'basic_uptime' => array('type' => 'decimal', 'null' => true, 'default' => null, 'length' => '3,1', 'unsigned' => false, 'comment' => '基本稼働時間'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB', 'comment' => '社員テーブル')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'section_no' => 1,
			'authority_no' => 1,
			'responsibility' => 1,
			'name' => 'Lorem ipsum dolor sit amet',
			'mail_address' => 'Lorem ipsum dolor sit amet',
			'password' => 'Lorem ipsum dolor sit amet',
			'effectiveness' => 1,
			'job_entry_date' => '2019-01-15',
			'ic_card_key' => 1,
			'position_no' => 1,
			'retirement' => 1,
			'retirement_date' => '2019-01-15',
			'on_leave' => 1,
			'attendance_adjustment' => 1,
			'start_time' => '18:27:45',
			'closing_time' => '18:27:45',
			'basic_uptime' => ''
		),
	);

}
