<?php
/**
 * BasicAttendanceInformation Fixture
 */
class BasicAttendanceInformationFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary', 'comment' => 'ID'),
		'company_no' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'comment' => '会社ID'),
		'opening_time' => array('type' => 'time', 'null' => false, 'default' => null, 'comment' => '始業時刻'),
		'closing_time' => array('type' => 'time', 'null' => false, 'default' => null, 'comment' => '終業時刻'),
		'rest_time' => array('type' => 'decimal', 'null' => false, 'default' => null, 'length' => '3,1', 'unsigned' => false, 'comment' => '休憩時間'),
		'overtime_start_time' => array('type' => 'time', 'null' => false, 'default' => null, 'comment' => '残業開始時刻'),
		'late_night_overtime_start_time' => array('type' => 'time', 'null' => false, 'default' => null, 'comment' => '深夜残業開始時刻'),
		'basic_working_hours' => array('type' => 'decimal', 'null' => false, 'default' => null, 'length' => '3,1', 'unsigned' => false, 'comment' => '基本就業時間'),
		'closing_date' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 2, 'unsigned' => false, 'comment' => '締日'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB', 'comment' => '就業基本情報テーブル')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'company_no' => 1,
			'opening_time' => '18:27:30',
			'closing_time' => '18:27:30',
			'rest_time' => '',
			'overtime_start_time' => '18:27:30',
			'late_night_overtime_start_time' => '18:27:30',
			'basic_working_hours' => '',
			'closing_date' => 1
		),
	);

}
