<?php
/**
 * Product Fixture
 */
class ProductFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary', 'comment' => 'ID'),
		'project_no' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'comment' => '商談ID'),
		'item_code' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 2, 'collate' => 'utf8_general_ci', 'comment' => '商品コード', 'charset' => 'utf8'),
		'name' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 1024, 'collate' => 'utf8_general_ci', 'comment' => '名称', 'charset' => 'utf8'),
		'deleted' => array('type' => 'boolean', 'null' => true, 'default' => null, 'comment' => '削除'),
		'deleted_date' => array('type' => 'date', 'null' => true, 'default' => null, 'comment' => '削除日'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB', 'comment' => '商品テーブル')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'project_no' => 1,
			'item_code' => '',
			'name' => 'Lorem ipsum dolor sit amet',
			'deleted' => 1,
			'deleted_date' => '2019-01-15'
		),
	);

}
