<?php
/**
 * VacationApplication Fixture
 */
class VacationApplicationFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary', 'comment' => 'ID'),
		'type' => array('type' => 'boolean', 'null' => false, 'default' => null, 'comment' => '種別'),
		'operation_no' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'comment' => '稼働ID'),
		'application_date' => array('type' => 'date', 'null' => false, 'default' => null, 'comment' => '申請日'),
		'start_date' => array('type' => 'datetime', 'null' => false, 'default' => null, 'comment' => '開始日時'),
		'end_date' => array('type' => 'datetime', 'null' => false, 'default' => null, 'comment' => '終了日時'),
		'reason' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 256, 'collate' => 'utf8_general_ci', 'comment' => '理由', 'charset' => 'utf8'),
		'remarks' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 256, 'collate' => 'utf8_general_ci', 'comment' => '備考', 'charset' => 'utf8'),
		'approval' => array('type' => 'boolean', 'null' => true, 'default' => null, 'comment' => '承認'),
		'comment' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 256, 'collate' => 'utf8_general_ci', 'comment' => 'コメント', 'charset' => 'utf8'),
		'authorize' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false, 'comment' => '承認者'),
		'authorize_datetime' => array('type' => 'datetime', 'null' => true, 'default' => null, 'comment' => '承認日時'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB', 'comment' => '休暇申請テーブル')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'type' => 1,
			'operation_no' => 1,
			'application_date' => '2019-01-15',
			'start_date' => '2019-01-15 18:27:49',
			'end_date' => '2019-01-15 18:27:49',
			'reason' => 'Lorem ipsum dolor sit amet',
			'remarks' => 'Lorem ipsum dolor sit amet',
			'approval' => 1,
			'comment' => 'Lorem ipsum dolor sit amet',
			'authorize' => 1,
			'authorize_datetime' => '2019-01-15 18:27:49'
		),
	);

}
