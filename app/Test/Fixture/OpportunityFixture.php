<?php
/**
 * Opportunity Fixture
 */
class OpportunityFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary', 'comment' => 'ID'),
		'parent_project_no' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'comment' => '案件ID'),
		'department_no' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'comment' => '部門ID'),
		'name' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 256, 'collate' => 'utf8_general_ci', 'comment' => '名称', 'charset' => 'utf8'),
		'client' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 256, 'collate' => 'utf8_general_ci', 'comment' => '顧客', 'charset' => 'utf8'),
		'project_code' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 8, 'collate' => 'utf8_general_ci', 'comment' => '商談コード', 'charset' => 'utf8'),
		'item_code' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false, 'comment' => '商品コード'),
		'sfaid' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 10, 'collate' => 'utf8_general_ci', 'comment' => 'SFAID', 'charset' => 'utf8'),
		'insert_user' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'comment' => '登録者'),
		'insert_date' => array('type' => 'date', 'null' => true, 'default' => null, 'comment' => '登録日'),
		'deleted' => array('type' => 'boolean', 'null' => false, 'default' => '0', 'comment' => '削除'),
		'deleted_date' => array('type' => 'date', 'null' => true, 'default' => null, 'comment' => '削除日'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB', 'comment' => '商談テーブル')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'parent_project_no' => 1,
			'department_no' => 1,
			'name' => 'Lorem ipsum dolor sit amet',
			'client' => 'Lorem ipsum dolor sit amet',
			'project_code' => 'Lorem ',
			'item_code' => 1,
			'sfaid' => 'Lorem ip',
			'insert_user' => 1,
			'insert_date' => '2019-01-15',
			'deleted' => 1,
			'deleted_date' => '2019-01-15'
		),
	);

}
