<?php
/**
 * Position Fixture
 */
class PositionFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary', 'comment' => 'ID'),
		'company_no' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'comment' => '会社ID'),
		'name' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 256, 'collate' => 'utf8_general_ci', 'comment' => '名称', 'charset' => 'utf8'),
		'approved_job_title_no' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'comment' => '承認役職ID'),
		'managerial_classification' => array('type' => 'boolean', 'null' => false, 'default' => null, 'comment' => '管理職区分'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB', 'comment' => '役職テーブル')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'company_no' => 1,
			'name' => 'Lorem ipsum dolor sit amet',
			'approved_job_title_no' => 1,
			'managerial_classification' => 1
		),
	);

}
