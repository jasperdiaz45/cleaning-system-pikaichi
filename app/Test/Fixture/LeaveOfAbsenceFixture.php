<?php
/**
 * LeaveOfAbsence Fixture
 */
class LeaveOfAbsenceFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary', 'comment' => 'ID'),
		'employee_no' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'comment' => '社員ID'),
		'leave_of_absence_type' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 256, 'collate' => 'utf8_general_ci', 'comment' => '休職種別', 'charset' => 'utf8'),
		'start_date' => array('type' => 'date', 'null' => true, 'default' => null, 'comment' => '開始日'),
		'end_date' => array('type' => 'date', 'null' => true, 'default' => null, 'comment' => '終了日'),
		'notes' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 1024, 'collate' => 'utf8_general_ci', 'comment' => 'メモ', 'charset' => 'utf8'),
		'return_date' => array('type' => 'date', 'null' => true, 'default' => null, 'comment' => '復帰日'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB', 'comment' => '休職テーブル')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'employee_no' => 1,
			'leave_of_absence_type' => 'Lorem ipsum dolor sit amet',
			'start_date' => '2019-01-15',
			'end_date' => '2019-01-15',
			'notes' => 'Lorem ipsum dolor sit amet',
			'return_date' => '2019-01-15'
		),
	);

}
