<?php
/**
 * VAttendanceManagement Fixture
 */
class VAttendanceManagementFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'company_no' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'division_no' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'department_no' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'section_no' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'comment' => '課ID'),
		'employee_no' => array('type' => 'integer', 'null' => false, 'default' => '0', 'unsigned' => false, 'comment' => 'ID'),
		'employment_month' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 6, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'operations_dates' => array('type' => 'biginteger', 'null' => false, 'default' => '0', 'length' => 21, 'unsigned' => false),
		'total_actual_operating_time' => array('type' => 'decimal', 'null' => true, 'default' => null, 'length' => '25,1', 'unsigned' => false),
		'total_overtime' => array('type' => 'decimal', 'null' => true, 'default' => null, 'length' => '25,1', 'unsigned' => false),
		'total_late_night_overtime' => array('type' => 'decimal', 'null' => true, 'default' => null, 'length' => '25,1', 'unsigned' => false),
		'holiday_work_count' => array('type' => 'biginteger', 'null' => false, 'default' => '0', 'length' => 21, 'unsigned' => false),
		'compensation_count' => array('type' => 'biginteger', 'null' => true, 'default' => '0', 'length' => 21, 'unsigned' => false),
		'paid_holiday_count' => array('type' => 'biginteger', 'null' => true, 'default' => '0', 'length' => 21, 'unsigned' => false),
		'indexes' => array(
			
		),
		'tableParameters' => array('comment' => 'VIEW')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'company_no' => 1,
			'division_no' => 1,
			'department_no' => 1,
			'section_no' => 1,
			'employee_no' => 1,
			'employment_month' => 'Lore',
			'operations_dates' => '',
			'total_actual_operating_time' => '',
			'total_overtime' => '',
			'total_late_night_overtime' => '',
			'holiday_work_count' => '',
			'compensation_count' => '',
			'paid_holiday_count' => ''
		),
	);

}
