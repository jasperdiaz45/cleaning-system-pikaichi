<?php
/**
 * BusinessDescriptionByOpportunity Fixture
 */
class BusinessDescriptionByOpportunityFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary', 'comment' => 'ID'),
		'operation_no' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'comment' => '稼働ID'),
		'parent_project_no' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'comment' => '案件ID'),
		'project_no' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'comment' => '商談ID'),
		'operating_time' => array('type' => 'decimal', 'null' => true, 'default' => null, 'length' => '3,1', 'unsigned' => false, 'comment' => '稼働時間'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB', 'comment' => '商談別稼働明細テーブル')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'operation_no' => 1,
			'parent_project_no' => 1,
			'project_no' => 1,
			'operating_time' => ''
		),
	);

}
