<?php
/**
 * Message Fixture
 */
class MessageFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary', 'comment' => 'ID'),
		'sender_no' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'comment' => '送信者ID'),
		'recipient_no' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'comment' => '受信者ID'),
		'message' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 256, 'collate' => 'utf8_general_ci', 'comment' => 'メッセージ', 'charset' => 'utf8'),
		'insert_datetime' => array('type' => 'datetime', 'null' => false, 'default' => null, 'comment' => '登録日時'),
		'daily_report_no' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false, 'comment' => '日報ID'),
		'read_datetime' => array('type' => 'datetime', 'null' => true, 'default' => null, 'comment' => '既読日時'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB', 'comment' => 'メッセージテーブル')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'sender_no' => 1,
			'recipient_no' => 1,
			'message' => 'Lorem ipsum dolor sit amet',
			'insert_datetime' => '2019-01-15 18:27:46',
			'daily_report_no' => 1,
			'read_datetime' => '2019-01-15 18:27:46'
		),
	);

}
