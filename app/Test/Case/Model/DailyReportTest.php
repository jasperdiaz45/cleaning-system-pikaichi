<?php
App::uses('DailyReport', 'Model');

/**
 * DailyReport Test Case
 */
class DailyReportTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.daily_report'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->DailyReport = ClassRegistry::init('DailyReport');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->DailyReport);

		parent::tearDown();
	}

}
