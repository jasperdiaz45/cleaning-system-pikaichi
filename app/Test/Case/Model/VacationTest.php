<?php
App::uses('Vacation', 'Model');

/**
 * Vacation Test Case
 */
class VacationTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.vacation'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Vacation = ClassRegistry::init('Vacation');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Vacation);

		parent::tearDown();
	}

}
