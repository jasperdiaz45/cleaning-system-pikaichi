<?php
App::uses('BasicAttendanceInformation', 'Model');

/**
 * BasicAttendanceInformation Test Case
 */
class BasicAttendanceInformationTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.basic_attendance_information'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->BasicAttendanceInformation = ClassRegistry::init('BasicAttendanceInformation');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->BasicAttendanceInformation);

		parent::tearDown();
	}

}
