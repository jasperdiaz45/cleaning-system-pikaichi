<?php
App::uses('VWorkOperationSummary', 'Model');

/**
 * VWorkOperationSummary Test Case
 */
class VWorkOperationSummaryTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.v_work_operation_summary'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->VWorkOperationSummary = ClassRegistry::init('VWorkOperationSummary');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->VWorkOperationSummary);

		parent::tearDown();
	}

}
