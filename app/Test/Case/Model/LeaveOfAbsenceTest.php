<?php
App::uses('LeaveOfAbsence', 'Model');

/**
 * LeaveOfAbsence Test Case
 */
class LeaveOfAbsenceTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.leave_of_absence'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->LeaveOfAbsence = ClassRegistry::init('LeaveOfAbsence');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->LeaveOfAbsence);

		parent::tearDown();
	}

}
