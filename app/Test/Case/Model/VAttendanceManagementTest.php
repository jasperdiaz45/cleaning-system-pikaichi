<?php
App::uses('VAttendanceManagement', 'Model');

/**
 * VAttendanceManagement Test Case
 */
class VAttendanceManagementTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.v_attendance_management'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->VAttendanceManagement = ClassRegistry::init('VAttendanceManagement');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->VAttendanceManagement);

		parent::tearDown();
	}

}
