<?php
App::uses('LeaveMidway', 'Model');

/**
 * LeaveMidway Test Case
 */
class LeaveMidwayTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.leave_midway'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->LeaveMidway = ClassRegistry::init('LeaveMidway');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->LeaveMidway);

		parent::tearDown();
	}

}
