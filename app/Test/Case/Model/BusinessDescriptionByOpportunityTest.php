<?php
App::uses('BusinessDescriptionByOpportunity', 'Model');

/**
 * BusinessDescriptionByOpportunity Test Case
 */
class BusinessDescriptionByOpportunityTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.business_description_by_opportunity'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->BusinessDescriptionByOpportunity = ClassRegistry::init('BusinessDescriptionByOpportunity');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->BusinessDescriptionByOpportunity);

		parent::tearDown();
	}

}
