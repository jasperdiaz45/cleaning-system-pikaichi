<?php
App::uses('MonthlyOperation', 'Model');

/**
 * MonthlyOperation Test Case
 */
class MonthlyOperationTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.monthly_operation'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->MonthlyOperation = ClassRegistry::init('MonthlyOperation');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->MonthlyOperation);

		parent::tearDown();
	}

}
