<?php
App::uses('Proposal', 'Model');

/**
 * Proposal Test Case
 */
class ProposalTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.proposal'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Proposal = ClassRegistry::init('Proposal');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Proposal);

		parent::tearDown();
	}

}
