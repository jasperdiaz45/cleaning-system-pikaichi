<?php
App::uses('OvertimeApplication', 'Model');

/**
 * OvertimeApplication Test Case
 */
class OvertimeApplicationTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.overtime_application'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->OvertimeApplication = ClassRegistry::init('OvertimeApplication');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->OvertimeApplication);

		parent::tearDown();
	}

}
