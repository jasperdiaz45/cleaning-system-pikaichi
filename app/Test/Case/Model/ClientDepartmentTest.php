<?php
App::uses('ClientDepartment', 'Model');

/**
 * ClientDepartment Test Case
 */
class ClientDepartmentTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.client_department'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->ClientDepartment = ClassRegistry::init('ClientDepartment');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ClientDepartment);

		parent::tearDown();
	}

}
