<?php
App::uses('VacationApplication', 'Model');

/**
 * VacationApplication Test Case
 */
class VacationApplicationTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.vacation_application'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->VacationApplication = ClassRegistry::init('VacationApplication');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->VacationApplication);

		parent::tearDown();
	}

}
