<?php
App::uses('LeaveMidwaysController', 'Controller');

/**
 * LeaveMidwaysController Test Case
 */
class LeaveMidwaysControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.leave_midway'
	);

}
