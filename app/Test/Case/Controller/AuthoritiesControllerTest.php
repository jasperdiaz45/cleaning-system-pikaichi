<?php
App::uses('AuthoritiesController', 'Controller');

/**
 * AuthoritiesController Test Case
 */
class AuthoritiesControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.authority'
	);

}
