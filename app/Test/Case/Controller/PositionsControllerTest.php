<?php
App::uses('PositionsController', 'Controller');

/**
 * PositionsController Test Case
 */
class PositionsControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.position'
	);

}
