<?php
App::uses('VacationApplicationsController', 'Controller');

/**
 * VacationApplicationsController Test Case
 */
class VacationApplicationsControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.vacation_application'
	);

}
