<?php
App::uses('EmployeesController', 'Controller');

/**
 * EmployeesController Test Case
 */
class EmployeesControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.employee'
	);

}
