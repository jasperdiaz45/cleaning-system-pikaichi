<?php
App::uses('BasicAttendanceInformationsController', 'Controller');

/**
 * BasicAttendanceInformationsController Test Case
 */
class BasicAttendanceInformationsControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.basic_attendance_information'
	);

}
