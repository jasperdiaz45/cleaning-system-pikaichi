<?php
App::uses('DivisionsController', 'Controller');

/**
 * DivisionsController Test Case
 */
class DivisionsControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.division'
	);

}
