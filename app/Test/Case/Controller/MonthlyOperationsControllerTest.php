<?php
App::uses('MonthlyOperationsController', 'Controller');

/**
 * MonthlyOperationsController Test Case
 */
class MonthlyOperationsControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.monthly_operation'
	);

}
