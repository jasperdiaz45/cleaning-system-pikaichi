<?php
App::uses('OperationsController', 'Controller');

/**
 * OperationsController Test Case
 */
class OperationsControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.operation'
	);

}
