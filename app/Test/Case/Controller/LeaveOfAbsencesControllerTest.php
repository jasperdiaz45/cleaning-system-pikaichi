<?php
App::uses('LeaveOfAbsencesController', 'Controller');

/**
 * LeaveOfAbsencesController Test Case
 */
class LeaveOfAbsencesControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.leave_of_absence'
	);

}
