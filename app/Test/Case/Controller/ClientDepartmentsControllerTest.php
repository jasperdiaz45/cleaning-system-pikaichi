<?php
App::uses('ClientDepartmentsController', 'Controller');

/**
 * ClientDepartmentsController Test Case
 */
class ClientDepartmentsControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.client_department'
	);

}
