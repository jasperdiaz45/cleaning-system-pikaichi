<?php
App::uses('BusinessDescriptionByOpportunitiesController', 'Controller');

/**
 * BusinessDescriptionByOpportunitiesController Test Case
 */
class BusinessDescriptionByOpportunitiesControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.business_description_by_opportunity'
	);

}
