<?php
App::uses('OvertimeApplicationsController', 'Controller');

/**
 * OvertimeApplicationsController Test Case
 */
class OvertimeApplicationsControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.overtime_application'
	);

}
