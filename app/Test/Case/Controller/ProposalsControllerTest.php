<?php
App::uses('ProposalsController', 'Controller');

/**
 * ProposalsController Test Case
 */
class ProposalsControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.proposal'
	);

}
