<?php
App::uses('DailyReportsController', 'Controller');

/**
 * DailyReportsController Test Case
 */
class DailyReportsControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.daily_report'
	);

}
