<?php
App::uses('DocumentsController', 'Controller');

/**
 * DocumentsController Test Case
 */
class DocumentsControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.document'
	);

}
