<?php
App::uses('AppModel', 'Model');
/**
 * WorkReport Model
 *
 */
class AttendanceManagements extends AppModel {
	
	public $actsAs = array(
        'SoftDelete'
    );
    
    public $name = 'AttendanceManagements';
    
    /**
     * Validation rules
     *
     * @var array
     */
	public $validate = array(
		'work_date' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
			),
			'date' => array(
				'rule' => array('date', 'ymd'),
			),
			'isUnique' => array(
				'rule' => array('checkUnique'),
				'message' => '既に登録されています。'
			),
		),
		'report' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
			),
		),
		'schedule' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
			),
		),
		'list-matter' => array(
			'notBlank' => array(
				'rule' => array('checkMatterList'),
				'message' => 'エラー'
			),
		),

	);
	
	/**
	 * 日報データ重複チェック
	 */
	public function checkUnique($check) {
		$result = false;
		$this->recursive = -1;
		$date = $this->data['WorkReport']['work_date'];
		$userId = $this->data['WorkReport']['user_id'];
		$cnt = $this->find('count', 
						array('conditions'=>array('user_id'=>$userId,
												  'work_date'=>$date,),
							)); 
		if ( $cnt == 0 ) {
			$result = true;
		}
		return $result;
	}
	
	public function checkMatterList($check) {
		return false;
	}
}
