<?php
App::uses('AppModel', 'Model');
/**
 * WorkReport Model
 *
 */
class Inputs extends AppModel {
    
    public $name = 'Inputs';
    
    
    /**
     * Validation rules
     *
     * @var array
     */
	public $validate = array(
		'operation_start_datetime' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
			),
		),
	);
	
}
