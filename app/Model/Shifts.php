<?php
App::uses('AppModel', 'Model');
/**
 * WorkReport Model
 *
 */
class Shifts extends AppModel {
	
	public $name = 'Shifts';
    
    /**
     * Validation rules
     *
     * @var array
     */
	public $validate = array(
		'employee_no' => array(
			'numeric' => array(
				'rule' => array('numeric'),
			),
		),
	);
	
	public function getBasicUptime($start,$end,$deduction){
		$t1  = new DateTime($start);
		$t2 = new DateTime($end);
		$hour = floor($deduction);
		$mins = $deduction - $hour;
		$mins = str_pad(round($mins * 60,0),2,0,STR_PAD_LEFT);
		$deduction = strval($hour) . ':' . $mins;
		$t3 = new DateTime($deduction);
		$diff = $t1->diff($t2);
		$t4 = new DateTime($diff->format('%H:%I'));
		$basic_uptime =  $t3->diff($t4);
		return $basic_uptime->format('%h:%I');
	}
	
	public function time_to_decimal($time){
		 $timeArr = explode(':', $time);
		 $hour = $timeArr[0];
		 $mins = $timeArr[1]/60;
    	 $decTime = $hour + floatval($mins);
 
    	 return $decTime;
	}
}
