<?php
App::uses('AppModel', 'Model');
/**
 * OvertimeApplication Model
 *
 */
class OvertimeApplication extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'type' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'operation_no' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'application_date' => array(
			'date' => array(
				'rule' => array('date'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'working_date' => array(
			'date' => array(
				'rule' => array('date'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'overtime_hours' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'reason' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'overtime_start' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
			),
		),
		'overtime_end_date' => array(
			'notBlank' => array(
				'rule' => array('ruleOvertimeEndDate', 'type'),
			),
		),
		'overtime_end_time' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
			),
		),
		'compensation_date' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
			),
		),
	);
	
	public function ruleOvertimeEndDate($check, $type) {
		return !(empty($check['overtime_end_date']) && $type == OVERTIME_KIND_NIGHT_SHIFT);
	}

	//
	// overtime calculation
	//
	
	public function overtimeCalculation($workingDate, $employeeNo) {
		$this->Behaviors->load('Master');
		$Operation = ClassRegistry::init('Operation');

		$Operations = $Operation->find('all', array('conditions' => array('employment_date' => $workingDate, 'employee_no' => $employeeNo)));
		if (count($Operations) > 0) {
			$companyNo = $Operations[0]['Operation']['company_no'];
			$startDatetime = substr($Operations[0]['Operation']['start_datetime'], 0, 16);
			$endDatetime = substr($Operations[0]['Operation']['end_datetime'], 0, 16);
			if ($startDatetime != '' && $endDatetime != '') {
				$dispOvertime = $this->getOvertime($employeeNo, $workingDate, $companyNo, $startDatetime, $endDatetime, OVERTIME_KIND_NORMAL);
				$overtimeStartArray = explode(":", $dispOvertime);
				$overtime = $overtimeStartArray[0] + $overtimeStartArray[1] / 60;
				if ($overtime == 0) {
					$overtime = null;
					$dispOvertime = null;
				}
				$lateNightOvertime = $this->getOvertime($employeeNo, $workingDate, $companyNo, $startDatetime, $endDatetime, OVERTIME_KIND_NIGHT_SHIFT);
				$lateNightOvertimeArray = explode(":", $lateNightOvertime);
				$lateNightOvertime = $lateNightOvertimeArray[0] + $lateNightOvertimeArray[1] / 60;
				if ($lateNightOvertime == 0) $lateNightOvertime = null;
				$Operation->save(array(
					'id' => $Operations[0]['Operation']['id'],
					'overtime' => $overtime,
					'disp_overtime' => $dispOvertime,
					'late_night_overtime' => $lateNightOvertime
				));
			}
		}
	}

	public function isHolidayWorkOK($userId, $dateNow) {
		$Shift = ClassRegistry::init('Shift');
		$ShiftCnt = $Shift->find('count', array('conditions' => array(
											'employee_no' => $userId,
											'date' => $dateNow
										)));
		$holidayWorkcnt = $this->find('count', array('conditions' => array(
											'employee_no' => $userId,
											'working_date' => $dateNow,
											'type' => OVERTIME_KIND_HOLIDAY_WORK,
											'approval' => APPROVAL_ALREADY
										)));
		return ($ShiftCnt == 0 && $holidayWorkcnt > 0) || $ShiftCnt > 0;
	}
}
