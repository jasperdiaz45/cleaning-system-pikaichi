<?php
App::uses('AppModel', 'Model');
/**
 * Operation Model
 *
 */
class Operation extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'company_no' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'division_no' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'department_no' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'section_no' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'employee_no' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'operation_approval' => array(
			'boolean' => array(
				'rule' => array('boolean'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'tmp_insert' => array(
			'boolean' => array(
				'rule' => array('boolean'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//
	// 強制退勤打刻
	//
	
	public function forcedOutStamp($params) {

		$this->Behaviors->load('Master');
		$Shift = ClassRegistry::init('Shift');	

		// get operation info
		$operations = $this->find('first', array(
            	'conditions' => array('employee_no' => $params['employee_no'], 'start_datetime IS NOT NULL', 'end_datetime IS NULL'),
            	'order' => array('employment_date' => 'desc')
            ));
		if (count($operations) == 0) return true;
		$operationId = $operations['Operation']['id'];
		$employmentDate = $operations['Operation']['employment_date'];
		$startDatetime = substr($operations['Operation']['start_datetime'], 0, 16);

		// get closing time
		$shifts = $Shift->find('first', array('conditions' => array('employee_no' => $params['employee_no'], 'date' => $employmentDate)));
		if (count($shifts) == 0) return false;
		$closingTime = $employmentDate . ' ' . substr($shifts['Shift']['closing_time'], 0, 5);

		// actual_operating_time calculation.
		$actualOperatingTime = $this->getDatetimeDiff($startDatetime, $closingTime);
		$actualOperatingTime -= $this->getDefaultBreakTime($params['employee_no'], $employmentDate);

		// save operation info.
		return $this->save(
			array(
      			'id' => $operationId,
      			'end_datetime' => $closingTime,
				'actual_operating_time' => $actualOperatingTime,
				'auto_end_stamp' => 1
      			)
      		);
	}
}

