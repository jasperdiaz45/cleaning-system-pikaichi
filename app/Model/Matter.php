<?php
App::uses('AppModel', 'Model');
/**
 * Matter Model
 *
 */
class Matter extends AppModel {
	
	public $actsAs = array(
		'SoftDelete',
	);
	
	public $order = "Matter.id DESC";

	public function afterFind($results, $primary = false) {
		// 日付フォーマットの変換
		$datefields = array('created',);
		$results = $this->dateFormatAfterFind($results, $datefields, 'Y/m/d H:i');
		return $results;
	}

	public $virtualFields = array(
	    'created_name'   => 'CONCAT(User.last_name, \'　\', User.first_name)',
	);


	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public $validate = array(
		'customer_name' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
			),
		),
		'matter_name' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
			),
		),
		'identifier' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
			),
			'pattern' => array(
				'rule' => array('custom', '/^[a-z].[a-z0-9-_]*$/'),
				'message' => 'identifier',
			),
		),
		'maintenance_year_month' => array(
			'date' => array(
				'rule' => array('custom', '/^(\d{4})\/(0[1-9]|1[0-2])$/'),
				'message' => 'date',
				'allowEmpty' => true,
			),
		),
		'estimate_count' => array(
			'numeric' => array(
				'rule' => array('custom', '/^[0-9]+$/'),
				'message' => 'naturalNumber',
				'allowEmpty' => true,
			),
		),
	);


	/**
	 * belongsTo associations
	 *
	 * @var array
	 */
	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => false,
			'conditions' => 'User.id = Matter.user_id'
		)
	);

}