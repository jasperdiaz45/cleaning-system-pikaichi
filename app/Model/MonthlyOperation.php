<?php
App::uses('AppModel', 'Model');
/**
 * MonthlyOperation Model
 *
 */
class MonthlyOperation extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'company_no' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'division_no' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'department_no' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'section_no' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'employee_no' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'year_month' => array(
			'date' => array(
				'rule' => array('date'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);
	
	public function generate($yearMonth, $companyNo) {

		$Employee = ClassRegistry::init('Employee');
		$nowDatetime = date("Y-m-d H:i:s");

		$employees = $Employee->find('all', array('conditions' => array('company_no' => $companyNo)));

		foreach ($employees as $employee) {

			$cnt = $this->find('count', array('conditions' => array('employee_no' => $employee['Employee']['id'], 'yearMonth' => $yearMonth)));
			if ($cnt == 0) {

				$this->Create();
				$this->save(
					array(
						'company_no' => $companyNo,
						'division_no' => $employee['Employee']['division_no'],
						'department_no' => $employee['Employee']['department_no'],
						'section_no' => $employee['Employee']['section_no'],
						'employee_no' => $employee['Employee']['id'],
						'yearMonth' => $yearMonth,
						'approval' => APPROVAL_ALREADY,
						'approver_no' => APPROVAL_SECTION_DEFAULT_APPROVER_NO,
						'approval_date' => $nowDatetime
					)
				);
			}
		}
	}
}
