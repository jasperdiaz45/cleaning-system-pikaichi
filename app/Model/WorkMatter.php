<?php
App::uses('AppModel', 'Model');
/**
 * WorkMatter Model
 *
 */
class WorkMatter extends AppModel {
	
	public $actsAs = array(
        'SoftDelete'
    );
    
    public $name = 'WorkMatters';

    /**
	 * belongsTo associations
	 *
	 * @var array
	 */
	public $belongsTo = array(
		'WorkReport' => array(
			'className' => 'WorkReport',
			'foreignKey' => false,
			'conditions' => 'WorkReport.id = WorkMatter.work_report_id and WorkReport.deleted = \'0\' ',
			'type' => ' INNER',
		),
		'Matter' => array(
			'className' => 'Matter',
			'foreignKey' => false,
			'conditions' => 'Matter.id = WorkMatter.matter_id and Matter.deleted = \'0\' ',
			'type' => ' INNER',
		),
	);

	/**
	 * 集計：年プルダウン作成
	 */
	public function getTargetYear() {
		$workReport = new WorkReport();
		$yearArray = [];
		$workReportss = $workReport->find('all', 
						array('fields' => array('DISTINCT DATE_FORMAT(WorkReports.work_date,"%Y") as work_date'),
							  'conditions' => array ('WorkReports.work_date >= \'2015/01/01\' and WorkReports.deleted = \'0\''),
							  'order'  => array('work_date' => 'DESC')));
		
		foreach ($workReportss as $index => $data) {
			$yearArray[$data[0]['work_date']] = $data[0]['work_date'];
		}
		return $yearArray;
	}
	
	/**
	 * 集計：ソート対象月をreturn
	 */
	public function getSortTargetMonth($targetYear) {
		$thisYear = DateTimeUtil::format('Y');
		if ($thisYear > $targetYear) {
			// 前年以前の場合は12月分でソート
			return 12;
		}
		
		$this->virtualFields['work_count'] = 'sum(work_count)';
		$firstDay = DateTimeUtil::format('Y/m/01');
		$lastDay = DateTimeUtil::format('Y/m/t');
		
		// 今月の件数を取得
		$thisMonthCount = $this->find('first', array(
			'fields' => array('work_count'),
			'conditions' => array('WorkReport.work_date BETWEEN ? AND ?' => array($firstDay,$lastDay))
		));
		
		$month = DateTimeUtil::format('n', $firstDay);
		if ($month > 1 && empty($thisMonthCount['WorkMatter']['work_count'])) {
			// 今月の工数合計が0の場合前月分でソート（1月は前年12月分を取得していない為除外）
			$month = $month - 1;
		}
		
		return $month;
	}
	
}
