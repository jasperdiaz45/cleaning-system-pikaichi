<?php

class MasterBehavior extends ModelBehavior {

	public function overtimeApplicationMst(Model $model) {

		$overtimeApplicationMst = array(
			null => '',
			OVERTIME_KIND_NORMAL => '残業',
			OVERTIME_KIND_NIGHT_SHIFT => '夜勤',
			OVERTIME_KIND_HOLIDAY_WORK => '休出'
		);
	
    	return $overtimeApplicationMst;
	}

	public function vacationApplicationMst(Model $model) {

		$vacationApplicationMst = array(
			null => '',
			VACATION_KIND_SALARY => '有休',
			VACATION_KIND_SALARY_AM => '午前半休',
			VACATION_KIND_SALARY_PM => '午後半休',
			VACATION_KIND_TIME => '時間休',
			VACATION_KIND_COMPENSATION => '代休',
			VACATION_KIND_CONGRA_CONDOL => '慶弔休暇',
			VACATION_KIND_SUMMER => '夏期休暇',
			VACATION_KIND_END_YEAR => '年末休暇',
			VACATION_KIND_ABSENCE => '欠勤'
		);

		return	$vacationApplicationMst;
	}
	
	
	public function opportunitiesListStatus(Model $model) {

		$opportunitiesListStatus = array(
			'' => '',
			1 => '商談中',
			2 => '進行中',
			9 => '完了'
		);

		return	$opportunitiesListStatus;
	}

	public function reportConfirmedMst(Model $model) {

		$reportConfirmedMst = array(
			null => '未読',
			0 => '未読',
			1 => '既読'
		);

		return $reportConfirmedMst;
	}
	
	public function employeeStatusMst(Model $model) {
	
		$employeeStatusMst = array(
			0 => '在籍',
			1 => '退職'
		);
	}
	
	public function approvalMst(Model $model) {
	
		$approvalMst = array(
			'' => '',
			0 => '申請中',
			1 => '承認済',
			2 => '<p>差戻し</p>'
		);
		
		return $approvalMst;
	}
	
	public function approvalStatusMst(Model $model) {
	
		$approvalMst = array(
			0 => '未承認',
			1 => '承認済',
			2 => '差戻し'
		);
		
		return $approvalMst;
	}

	public function messageKindMst(Model $model) {

		$messageKindMst = array(
			0 => '送信',
			1 => '受信'
		);

		return $messageKindMst;
	}

	public function messageStatusMst(Model $model) {

		$messageStatusMst = array(
			null => '未読',
			0 => '未読',
			1 => '既読'
		);

		return $messageStatusMst;
	}

	public function jpDateNameMst(Model $model) {

		$jpDateNameMst = array(
			'Mon' => '月',
			'Tue' => '火',
			'Wed' => '水',
			'Thu' => '木',
			'Fri' => '金',
			'Sat' => '土',
			'Sun' => '日'
		);

		return $jpDateNameMst;
	}
	
	public function reasonChangeStartDatetimeMst(Model $model) {

		$reasonChangeStartDatetimeMst = array(
			null => '',
			'交通機関遅延' => '交通機関遅延',
			'時計設定不備' => '時計設定不備',
			'PC操作不備' => 'PC操作不備',
			'災害のため' => '災害のため',
			'深夜作業のため' => '深夜作業のため',
			'遅刻申請' => '遅刻申請',
			'直行'=>'直行',
			'直帰'=>'直帰',
			'出張'=>'出張',
			'打刻忘れ'=>'打刻忘れ'
		);

		return $reasonChangeStartDatetimeMst;
	}

/*	public function listResponsibilityMst(Model $model) {
		$listResponsibilityMst = array(
			null => '',
			'1' => '管理職',
			'2' => '一般社員',
			'3' => '嘱託',
			'4' => 'パート',
			'5' => 'アルバイト'
		);
		return $listResponsibilityMst;
	}
*/	
	public function responsibilityMst(Model $model){
		$responsibility = array(
			'0' => '一般',
			'1' => '上席承認者',
			'2' => '最終承認者'
		);
		return $responsibility;
	}

	public function authorityMst(Model $model){
		$authority = array(
			'1' => 'システム管理者',
			'2' => '一般ユーザー'
		);
		return $authority;
	}

	public function employeeCategoryMst(Model $model){
		$employeeCategory = array(
			'1' => '社員',
			'2' => 'パート',
			'3' => '嘱託',
			'4' => '役員'
	);
	return $employeeCategory;
	}
	
	public function positionMst(Model $model){
		$position = array(
			'1' => '一般社員',
			'2' => '取締役',
			'3' => '専務取締役',
			'4' => '代表取締役'
		);
		return $position;
	}
	
	
	//下記２つ、どちらかを消す
	public function leaveOfAbsenceMst(Model $model) {
		$leaveOfAbsenceMst = array(
			null => '',
			1 => '病気',
			2 => '産休・育休',
			3 => '介護'
		);
		return	$leaveOfAbsenceMst;
	}

	public function reasonLeaveMst(Model $mode){
		$reason = array(
			null => '',
			'1' => '病気',
			'2' => '産休・育休',
			'3' => '介護',
		);
		return $reason;
	}
	
	public function reasonChangeEndDatetimeMst(Model $model) {

		$reasonChangeEndDatetimeMst = array(
			null => '',
			'災害のため' => '災害のため',
			'時計設定不備' => '時計設定不備',
			'PC操作不備' => 'PC操作不備',
			'深夜作業のため' => '深夜作業のため',
			'早退申請' => '早退申請',
			'直行'=>'直行',
			'直帰'=>'直帰',
			'出張'=>'出張',
			'打刻忘れ'=> '打刻忘れ'
		);

		return $reasonChangeEndDatetimeMst;
	}

	
	public function getEmployeeNoNameForId(Model $model, $id) {
		$Employee = ClassRegistry::init('Employee');

		$employees = $Employee->find('first', array('conditions' => array('id' => $id)));
		$employeeName = $employees['Employee']['last_name'].' '.$employees['Employee']['first_name'];

		return $employeeName;
	}

	public function isHoliday(Model $model, $employeeId, $date) {
		
		$Employee = ClassRegistry::init('Employee');
		$Holiday = ClassRegistry::init('Holiday');

		$employees = $Employee->find('first', array('conditions' => array('id' => $employeeId)));
		$holidayPattern = $employees['Employee']['holiday_pattern'];
		
		$cnt = $Holiday->find('count', array('conditions' => array('holiday_date' => $date)));

		return $cnt > 0 ? true : false;
	}

	
	public function getCompanyClosingTime(Model $model, $id) {

		$Business = ClassRegistry::init('BasicAttendanceInformation');

		$business_bus = $Business->find('first', array('conditions' => array('id' => $id)));
		$closing_time = $business_bus['BasicAttendanceInformation']['closing_time'];

		return $closing_time;
	}
	
	
	public function get_time_difference(Model $model,$time1,$time2,$date,$userid){
		$Business = ClassRegistry::init('BasicAttendanceInformation');
		
		$OverApp = ClassRegistry::init('OvertimeApplication');
		$date_param = $date[0].$date[1].$date[2].$date[3].'-'.$date[5].$date[6].'-'.$date[8].$date[9];
		
		$over_app = $OverApp->find('all', array('condition' => array('employee_no' => $userid),'group' => 'working_date'));
		
		for ($i = 0; $i < count($over_app); $i++) {
			if ($date_param == $over_app[$i]['OvertimeApplication']['working_date']) {
				$new_time = $over_app[$i]['OvertimeApplication']['overtime_end'];
				
				$time_1 = date_create($new_time);
				$time_2 = date_create($time2);
				
				$t1 = date_format($time_1,'Hi');
				$t2 = date_format($time_2,'Hi');
				
				if ((intval($t2) + 15) > intval($t1) && intval($t1) < (intval($t2) - 15) ) {
					return 'red';
				}
				else
				{
					return '';
				}
			}
			else
			{
				$time_1 = date_create($time1);
				$time_2 = date_create($time2);
				
				$t1 = date_format($time_1,'Hi');
				$t2 = date_format($time_2,'Hi');
				
				if ((intval($t2) + 15) > intval($t1) && intval($t1) < (intval($t2) - 15)) {
					return 'red';
				}
				else
				{
					return '';
				}
			}
		
		}
	}

	public function getDivisionName(Model $model, $id) {

		$Division = ClassRegistry::init('Division');

		$divisions = $Division->find('first', array('conditions' => array('id' => $id)));
		$divisionName = $divisions['Division']['name'];

		return $divisionName;
	}

	public function getDepartmentName(Model $model, $id) {

		$Department = ClassRegistry::init('Department');

		$departments = $Department->find('first', array('conditions' => array('id' => $id)));
		$departmentName = $departments['Department']['name'];

		return $departmentName;
	}
	
	
	public function getProposalsName(Model $model, $id) {

		$Proposals = ClassRegistry::init('Proposals');

		$proposals = $Proposals->find('first', array('conditions' => array('id' => $id)));
		$proposalsName = $proposals['Proposals']['name'];

		return $proposalsName;
	}
	
	public function getClientDepartmentName(Model $model, $id) {

		$ClientDepartments = ClassRegistry::init('ClientDepartments');

		$clientDepartments = $ClientDepartments->find('first', array('conditions' => array('id' => $id)));
		$clientDepartmentsName = $clientDepartments['ClientDepartments']['name'];

		return $clientDepartmentsName;
	}

	public function getSectionName(Model $model, $id) {

		$Section = ClassRegistry::init('Section');

		$sections = $Section->find('first', array('conditions' => array('id' => $id)));
		$sectionName = $sections['Section']['name'];

		return $sectionName;
	}

	//指定月の出勤日数取得
	public function getMonthlyOperationCount(Model $model,$id,$YM) {

		$Operation = ClassRegistry::init('Operation');

		$operation_count = $Operation->find('count', array('conditions' => array('employee_no' => $id,'DATE_FORMAT(employment_date,"%Y%m")' => $YM)));

		return $operation_count;
	}

	//指定月の出勤日数取得
	public function getMonthlyOperationCount21(Model $model,$id,$YM) {

		$Operation = ClassRegistry::init('Operation');
		
		$businessYmd = $this->getBusinessYmd($model,$YM);

		
		$operation_count = $Operation->find('count', array(
			'conditions' => array(
				'employee_no' => $id,
				'employment_date >=' => $businessYmd[0],
				'employment_date <=' => $businessYmd[1],
				)));

		return $operation_count;
	}

	//201803 -> 2018-03-01,2018-03-31
	public function getBusinessYmd(Model $model,$YM){
		$Year = substr($YM, 0, 4);
		$Month = substr($YM, 4, 2);

		$fromDay = $Year . '-' . $Month . '-01';
		$toDay = $Year . '-' . $Month . '-' . $this->get_last_date($model,$Year,$Month);
		$businessYmd = array($fromDay,$toDay);
		return $businessYmd;
		
/*

		if ($Month == '1'){
			$FromYear = strval(intval($Year) - 1);
			$FromMonth = '12';
		}else{
			$FromYear = $Year;
			$FromMonth = str_pad(intval($Month) - 1,2,0,STR_PAD_LEFT);
		}
			$fromDay = $FromYear . '-' . $FromMonth . '-21';
			$toDay = $Year . '-' . $Month . '-20';
		$businessYmd = array($fromDay,$toDay);
*/
	}

	//指定月の稼働時間、残業時間、深夜残業時間
	public function getMonthlyOperationInfo(Model $model,$id,$YM) {

		$Operation = ClassRegistry::init('Operation');

		$businessYmd = $this->getBusinessYmd($model,$YM);

		$conditions = array(
			'employee_no' => $id,
			'employment_date >=' => $businessYmd[0],
			'employment_date <=' => $businessYmd[1]
		);
/*
		$Year = substr($YM, 0, 4);
		$Month = substr($YM, 4, 2);

		if ($Month == '1'){
			$FromYear = strval(intval($Year) - 1);
			$FromMonth = '12';
		}else{
			$FromYear = $Year;
			$FromMonth = str_pad(intval($Month) - 1,2,0,STR_PAD_LEFT);
		}
		$conditions = array(
			'employee_no' => $id,
			'employment_date >=' => $FromYear . '-' . $FromMonth . '-21',
			'employment_date <=' => substr($YM, 0, 4) . '-' . substr($YM, 4, 2) . '-20'
		);
*/

		$monthly_operations = $Operation->find('first', array(
			'fields' => array('sum(actual_operating_time) as total_actual_operating_time',
				'sum(overtime) as total_overtime',
				'sum(late_night_overtime) as total_late_night_overtime'),
			'conditions' => $conditions));
//			'group' => array('employee_no','DATE_FORMAT(employment_date,"%Y%m")')));
		if (count($monthly_operations) == 0) {
			$monthly_operations[0] = array('total_actual_operating_time' => 0, 'total_overtime' => 0, 'total_late_night_overtime' => 0);
		} else {
			if ($monthly_operations[0]['total_actual_operating_time'] == null) $monthly_operations[0]['total_actual_operating_time'] = 0;
			if ($monthly_operations[0]['total_overtime'] == null) $monthly_operations[0]['total_overtime'] = 0;
			if ($monthly_operations[0]['total_late_night_overtime'] == null) $monthly_operations[0]['total_late_night_overtime'] = 0;
		}

		return $monthly_operations;
	}

	//月初から指定日までの残業時間
	public function getMonthlyOvertimeHours(Model $model,$id,$Ymd) {
		$Operation = ClassRegistry::init('Operation');

		$FromDate = substr($Ymd,0,8) . '01';

		$operations = $Operation->find('first', array(
			'fields' => array('sum(overtime) as total_overtime'),
			'conditions' => array('employee_no' => $id,
			'date_format(employment_date,"%Y-%m-%d") >=' => $FromDate,
			'date_format(employment_date,"%Y-%m-%d") <=' => $Ymd
			)));

		if ($operations[0]['total_overtime'] == null) $operations[0]['total_overtime'] = '0.00';

		$monthlyOvertimeHours = $operations[0]['total_overtime'];

		return $monthlyOvertimeHours;

	}

	//指定月の休出回数
	public function getMonthlyHolidayWorkCount(Model $model,$id,$YM) {

		$OvertimeApplication = ClassRegistry::init('OvertimeApplication');

		$monthly_HolidayWorkCount = $OvertimeApplication->find('count', array(
			'conditions' => array('employee_no' => $id,
			'DATE_FORMAT(working_date,"%Y%m")' => $YM,
			'type' => OVERTIME_KIND_HOLIDAY_WORK,
			'approval' => APPROVAL_ALREADY)));

		return $monthly_HolidayWorkCount;
	}

	//指定月の休出回数
	public function getMonthlyHolidayWorkCount21(Model $model,$id,$YM) {

		$OvertimeApplication = ClassRegistry::init('OvertimeApplication');
		
		$YmdAarray = $this->getBusinessYmd($model,$YM);

		$monthly_HolidayWorkCount = $OvertimeApplication->find('count', array(
			'conditions' => array(
				'employee_no' => $id,
				'working_date >=' => $YmdAarray[0],
				'working_date <=' => $YmdAarray[1],
				'type' => OVERTIME_KIND_HOLIDAY_WORK,
				'approval' => APPROVAL_ALREADY)));

		return $monthly_HolidayWorkCount;
	}

	//指定月の休出・代休（引数３：休暇申請種別の値）
	//指定月の休出回数
	public function getMonthlyVacationCount(Model $model,$id,$YM,$type) {

		$VacationApplication = ClassRegistry::init('VacationApplication');

		$monthly_VacationCount = $VacationApplication->find('count', array('conditions' => array(
				'employee_no' => $id,
				'DATE_FORMAT(vacation_date,"%Y%m")' => $YM,
				'type' => $type,
				'approval' => APPROVAL_ALREADY)));

		return $monthly_VacationCount;
	}
	
	//指定月の休出・代休（引数３：休暇申請種別の値）
	//指定月の休出回数
	public function getMonthlyVacationCount21(Model $model,$id,$YM,$type) {

		$VacationApplication = ClassRegistry::init('VacationApplication');

		$YmdAarray = $this->getBusinessYmd($model,$YM);

		$monthly_VacationCount = $VacationApplication->find('count', array('conditions' => array(
				'employee_no' => $id,
				'vacation_date >=' => $YmdAarray[0],
				'vacation_date <=' => $YmdAarray[1],
				'type' => $type,
				'approval' => APPROVAL_ALREADY)));

		return $monthly_VacationCount;
	}
	
	//指定月の遅刻早退件数
	public function getMonthlyDelayOrLeaveEarly(Model $model,$id,$YM) {

		$Operation = ClassRegistry::init('Operation');

		$Operation->bindModel(array(
				'belongsTo' => array(
				'Shift' => array(
					'foreignKey' => false,
					'conditions' => array('Operation.employee_no = Shift.employee_no')))), true);

    	$MonthlyDelayOrLeaveEarly = $Operation->find('count',array(
    		'conditions' => array('Operation.employee_no' => $id,
    						'DATE_FORMAT(employment_date,"%Y%m")' => $YM,
    						'OR' => array(
    									array(
    										'employment_date = date',
    										'CONVERT(DATE_FORMAT(start_datetime, \'%H:%i:%S\') USING binary) > start_time',
    									),
		    							array(
    										'employment_date = date',
    										'CONVERT(DATE_FORMAT(end_datetime, \'%H:%i:%S\') USING binary) < closing_time'
		    							)
		    						)
    						)));

		return $MonthlyDelayOrLeaveEarly;
	}

/*	//指定月の遅刻早退件数
	public function getMonthlyDelayOrLeaveEarly21(Model $model,$id,$YM) {
		$Operation = ClassRegistry::init('Operation');

		$YmdAarray = $this->getBusinessYmd($model,$YM);

		$Operation->bindModel(array(
				'belongsTo' => array(
				'Shift' => array(
					'foreignKey' => false,
					'conditions' => array('Operation.employee_no = Shift.employee_no')))), true);

    	$MonthlyDelayOrLeaveEarly = $Operation->find('count',array(
    		'conditions' => array('Operation.employee_no' => $id,
				    		'employment_date >=' => $YmdAarray[0],
				    		'employment_date <=' => $YmdAarray[1],
    						'OR' => array(
    									array(
    										'employment_date' => 'date',
		    								'CONVERT(DATE_FORMAT(start_datetime, \'%H:%i:%S\') USING binary) > start_time'
		    							),
		    							array(
    										'employment_date' => 'date',
    										'CONVERT(DATE_FORMAT(end_datetime, \'%H:%i:%S\') USING binary) < closing_time'
		    							)
    								)
    						)));

		return $MonthlyDelayOrLeaveEarly;
	}
*/	
	//ClientDepartment
	public function getClientDepartmentList(Model $model) {

		$Operation = ClassRegistry::init('ClientDepartments');
		
    	$clientDepartmentList = $Operation->find('list',array(
			'fields' => array('id','name'),
    		'conditions' => array('deleted' => 0),
    		'recursive' => 0));

    	$emptyArray = array('0' => '');
    	$emptyArray += $clientDepartmentList;
	   	$clientDepartmentList = $emptyArray;

		return $clientDepartmentList;
	}

	//部門
	public function getDepartmentList(Model $model, $divisionNo = '', $companyNo = '') {
		return $this->getDepartmentListIgnoreId($model, $divisionNo, $companyNo);
	}

	public function getDepartmentListIgnoreId(Model $model, $divisionNo = '', $companyNo = '', $ignoreIdList = array()) {

		$Operation = ClassRegistry::init('Department');

		if ($divisionNo != ''){
			$Conditions = array('division_no' => $divisionNo,'deleted' => 0);
		}else{
			$Conditions = array('deleted' => 0);
		}
		if ($companyNo != '') {

			if ($divisionNo != ''){
				$Conditions = array('Department.division_no' => $divisionNo, 'Department.deleted' => 0);
			}else{
				$Conditions = array('Department.deleted' => 0);
			}
			$Conditions += array('Division.company_no' => $companyNo);
			
			$Operation->bindModel(array(
				'belongsTo' => array(
					'Division' => array('foreignKey' => false, 'conditions' => array('Division.id = Department.division_no')),
				),
			), true);
		}
		
		if (count($ignoreIdList) > 0) {
			$Conditions += array('NOT' => array('Department.id' => $ignoreIdList));
		}

    	$DepartmentList = $Operation->find('list',array(
			'fields' => array('id','name'),
    		'conditions' => $Conditions,
    		'recursive' => 0));

    	$emptyArray = array('0' => '');
    	$emptyArray += $DepartmentList;
	   	$DepartmentList = $emptyArray;

		return $DepartmentList;
	}
	
	
	//client Department list
	public function getListDepartment(Model $model){
		$Operation = ClassRegistry::init('ClientDepartment');
		
		$Operation->virtualFields = array(
		    'departments' => "CONCAT(ClientDepartment.id, ' : ', ClientDepartment.name)"
		);
		
		$DepartmentList = $Operation->find('list',array(
			'fields' => array('id','departments'),
    		'conditions' => array('deleted' => 0),
    		'recursive' => 0));
    		
		$emptyArray = array('' => '');
		
    	$emptyArray += $DepartmentList;
	   	$DepartmentList = $emptyArray;
	   	
   		return $DepartmentList;
	}
	
	//Division list
	public function getDivissionList(Model $model,$divisionNo = '',$companyNo = '') {

		$Operation = ClassRegistry::init('Division');

		if ($divisionNo != ''){
			$Conditions = array('division_no' => $divisionNo,'deleted' => 0);
		}else{
			$Conditions = array('deleted' => 0);
		}
		if ($companyNo != '') $Conditions += array('company_no' => $companyNo);
		
    	$DivisionList = $Operation->find('list',array(
			'fields' => array('id','name'),
    		'conditions' => $Conditions,
    		'recursive' => 0));

    	$emptyArray = array('0' => '');
    	$emptyArray += $DivisionList;
	   	$DivisionList = $emptyArray;

		return $DivisionList;
	}
	
	//課
	public function getSectionList(Model $model,$departmentNo = '') {

		$Operation = ClassRegistry::init('Section');

		if ($departmentNo != ''){
			$Conditions = array('department_no' => $departmentNo,'deleted' => 0);
		}else{
			$Conditions = array('deleted' => 0);
		}
		
    	$SectionList = $Operation->find('list',array(
			'fields' => array('id','name'),
    		'conditions' => $Conditions,
    		'recursive' => 0));

    	$emptyArray = array('0' => '');
    	$emptyArray += $SectionList;
	   	$SectionList = $emptyArray;

		return $SectionList;
	}
	
	//承認者　※上長を特定する手段がないため要件等。　仮に社員全員をリスト出力している。
	public function getAuthorizeList(Model $model, $employeeNo) {

		$Employee = ClassRegistry::init('Employee');

		$employees = $Employee->find('all');
		
		$authorizeList = array();
		for ($i = 0; $i < count($employees); $i++) {
			$employeeName = $employees[$i]['Employee']['last_name'] . $employees[$i]['Employee']['first_name'];
			$authorizeList[$employees[$i]['Employee']['id']] = $employeeName;
		}

		return $authorizeList;
	}
	
	//宛先　※何らかの条件で宛先を絞り込む必要あり？
	public function getRecipientList(Model $model, $employeeNo, $companyNo) {

		$Employee = ClassRegistry::init('Employee');

		$employees = $Employee->find('all', ['conditions' => ['id !=' => $employeeNo, 'company_no' => $companyNo ]]);
		
		$recipientList = array();
		for ($i = 0; $i < count($employees); $i++) {
			$employeeName = $employees[$i]['Employee']['last_name'] . $employees[$i]['Employee']['first_name'];
			$recipientList[$employees[$i]['Employee']['id']] = $employeeName;
		}

		return $recipientList;
	}

	//データのある年月リスト(1日〜月末バージョン)
	public function getYearMonthList(Model $model,$t_name,$col_name,$employeeNo = '') {

		$Operation = ClassRegistry::init($t_name);

		if ($employeeNo != ''){
			$employeeNo = array('employee_no' => $employeeNo); 
		}

    	$yearMonthList = $Operation->find('all',array(
			'fields' => array('DISTINCT DATE_FORMAT(' .  $col_name . ',"%Y%m") as YearMonthId',
			'DATE_FORMAT(' .  $col_name . ',"%Y/%m") as YearMonth'),
			'conditions' => $employeeNo,
			'order' => 'YearMonth asc'));

		$yearMonthforList = array();
		for ($i = 0; $i < count($yearMonthList); $i++) {
			$yearMonthforList[$yearMonthList[$i][0]['YearMonthId']] = $yearMonthList[$i][0]['YearMonth'];
		}
		return $yearMonthforList;
	}
/*
	//データのある年月リスト(21日〜20日バージョン)
	public function getBusinessYMList(Model $model,$t_name,$col_name,$employeeNo = '') {

		$Operation = ClassRegistry::init($t_name);

		if ($employeeNo != ''){
			$employeeNo = array('employee_no' => $employeeNo); 
		}

    	$getRecord = $Operation->find('first',array(
			'fields' => array(
				'MIN(' .  $col_name . ') as MinDate',
				'MAX(' .  $col_name . ') as MaxDate'),
			'conditions' => $employeeNo));

		$businessYMList = null;
		
		if ($getRecord[0]['MinDate'] != NULL) {
			
			$minDate = $getRecord[0]['MinDate'];
			$minYear = substr($minDate, 0, 4);
			$minMonth = substr($minDate, 5, 2);
			$minDay = substr($minDate,8,2);
			
			$maxDate = $getRecord[0]['MaxDate'];
			$maxYear = substr($maxDate, 0, 4);
			$maxMonth = substr($maxDate, 5, 2);
			$maxDay = substr($maxDate,8,2);

			if(intval($minDay) > 20){
				if($minMonth == '12'){
					$minYear = intval($minYear) + 1;
				}
				$minMonth = intval($minMonth) + 1;
			}
			if(intval($maxDay) > 20){
				if($maxMonth == '12'){
					$maxYear = intval($maxYear) + 1;
				}
				$maxMonth = intval($maxMonth) + 1;
			}

			$businessYMList = array();

			for($y = $minYear; $y <= $maxYear; $y++){
				if($y == $maxYear){
					if($minYear == $maxYear){
						$mc = $minMonth;
					}else{
						$mc = 1;
					}
					for($m = $mc; $m <= $maxMonth; $m++){
						$businessYMList[strval($y) . str_pad(intval($m),2,0,STR_PAD_LEFT)] = strval($y) . '/' . str_pad(intval($m),2,0,STR_PAD_LEFT);
					}
				}elseif($y == $minYear){
					for($m = $minMonth; $m <= 12; $m++){
						$businessYMList[strval($y) . str_pad(intval($m),2,0,STR_PAD_LEFT)] = strval($y) . '/' . str_pad(intval($m),2,0,STR_PAD_LEFT);
					}
				}else{
					for($m = 1; $m <= 12; $m++){
						$businessYMList[strval($y) . str_pad(intval($m),2,0,STR_PAD_LEFT)] = strval($y) . '/' . str_pad(intval($m),2,0,STR_PAD_LEFT);
					}
				}
			}
		}
		return ($businessYMList);
	}
*/
	public function getCurrentBusinessYM(Model $model,$Ymd) {
		$Year = substr($Ymd, 0, 4);
		$Month = substr($Ymd, 4, 2);
		$Day = substr($Ymd,6,2);
		
		$currentBusinessYM = $Year . $Month;
		return $currentBusinessYM;
	}
	
	//データのある年リスト
	public function getYearList(Model $model,$t_name,$col_name,$employeeNo = '') {

		$Operation = ClassRegistry::init($t_name);

		if ($employeeNo != ''){
			$employeeNo = array('employee_no' => $employeeNo); 
		}

    	$yearList = $Operation->find('all',array(
			'fields' => array('DISTINCT DATE_FORMAT(' .  $col_name . ',"%Y") as Years'),
			'conditions' => $employeeNo,
			'order' => 'Years asc'));

		if(count($yearList) > 0){
			for ($i = 0; $i < count($yearList); $i++) {
				$yearList[$yearList[$i][0]['Years']] = $yearList[$i][0]['Years'];
			}
		}
		return $yearList;
	}
	
	//データのある年リスト_案件別稼働集計関連
	public function getYearList_View(Model $model,$t_name,$col_name) {

		$Operation = ClassRegistry::init($t_name);

    	$yearList = $Operation->find('all',array(
			'fields' => array('DISTINCT '.  $col_name . ' as Years'),
			'order' => 'Years asc'));

		$ret_year_list = array();
		for ($i = 0; $i < count($yearList); $i++) {
			$Year = $yearList[$i][$t_name]['Years'];
			$ret_year_list[$Year] = $Year;
		}
		return $ret_year_list;
	}
	
		public function retirementMst(Model $model) {
	
		$retirementMst = array(
			0 => '在籍',
			1 => '退職'
		);
		
		return $retirementMst;
	}

	public function getEmployeeList(Model $model,$employeeNo) {

		$Employee = ClassRegistry::init('Employee');

		$employees = $Employee->find('all');
		
		$employeeList = array();
		for ($i = 0; $i < count($employees); $i++) {
			
			$employeeList[$employees[$i]['Employee']['id']] = $employees[$i]['Employee']['id'];;
		}

		return $employeeList;
	}

//↓有給休暇関係
	//社員の就業開始年度始まりを取得
	public function getFirstBusinessYear(Model $model,$employeeNo){
		$Employees = ClassRegistry::init('Employee');

		//引数で渡された社員の情報をemployeesから取得する
		$employees = $Employees->find('first', array('conditions' => array('id' => $employeeNo)));

		//当該社員の年度開始日を取得
		$jobEntryDate = $employees['Employee']['job_entry_date'];

		if(intval(substr($jobEntryDate,5,2)) > 4 ){
			$jobEntryDate = strval(intval(substr($jobEntryDate,0,4)) - 1) . '0401';
		}else{
			$jobEntryDate = substr($jobEntryDate,0,4) . '0401';
		}
		return $jobEntryDate;
	}

	//年間有給取得可能数
	public function getHolidayWithSalaryDaysForYear(Model $model,$employeeNo,$Y_M) {

		$Y_M = $Y_M . '01';

		$Employees = ClassRegistry::init('Employee');
		$jobEntryDate = $this->getFirstBusinessYear($model,$employeeNo);

		//４月１日をベースに起算する場合
		$bindModelCondition = array('PaidHolidaysMaster.working_years = timestampdiff(YEAR,str_to_date(' . $jobEntryDate . ',"%Y%m%d"),str_to_date(' . $Y_M . ',"%Y%m%d") ) + 1');

		$Employees->bindModel(array(
				'belongsTo' => array(
				'PaidHolidaysMaster' => array(
					'foreignKey' => false,
					'conditions' => $bindModelCondition))), true);

    	$holidayWithSalaryDaysForYear = $Employees->find('first',array(
			'fields' => array('PaidHolidaysMaster.dates_on_year as dates_on_year'),
    		'conditions' => array('Employee.id' => $employeeNo)
    		));
	
		return $holidayWithSalaryDaysForYear;
	}
	
	//取得済有給数取得
	public function getGotHolidayWithSalaryDaysCount(Model $model,$employeeNo,$Y_M) {

		if(intval(substr($Y_M,-2)) < 4 ){
			$From_Date = strval(intval(substr($Y_M,0,4)) - 1) . '0401';
		}else{
			$From_Date = substr($Y_M,0,4) . '0401';
		}

		$Y_M = $Y_M . '01';
		
		$VacationApplication = ClassRegistry::init('VacationApplication');

		$bindModelCondition = array("VacationApplication.employee_no = Employee.id");

		//４月１日から指定月までの有給取得数を取得
		//Fromの条件がcastしないといけない理由不明
		$condition_item = array(
			"VacationApplication.employee_no" => $employeeNo,
			"cast(date_format(VacationApplication.vacation_date,'%Y%m%d') as SIGNED) >=" => "cast(". $From_Date . " as SIGNED)",
			"VacationApplication.vacation_date <=" => "str_to_date('" . $Y_M . "','%Y%m%d')"
		);

		$fields_item = 'sum(case when VacationApplication.type = 1 then 1 when VacationApplication.type in (2,3)  then 0.5 else 0 end) as cnt';

		$VacationApplication->bindModel(array(
				'belongsTo' => array(
				'Employee' => array(
					'foreignKey' => false,
					'conditions' => $bindModelCondition))), true);

    	$gotHolidayWithSalaryDaysCount = $VacationApplication->find('first',array(
			'fields' => $fields_item,
    		'conditions' => $condition_item
    		));
		
		return $gotHolidayWithSalaryDaysCount;
	}
	
	//有給残数取得
	public function getHavingHolidayWithSalary(Model $model,$employeeNo,$Y_M) {

		$Vacations = ClassRegistry::init('Vacation');

		$Vacations = $Vacations->find('first', array(
			'conditions' => array(
				'employee_no' => $employeeNo,
				'year' => substr($Y_M,0,4)
			)));

		return $Vacations['Vacation']['days_remaining_on_holiday'];
	}
	
	//時間休の残り時間
	public function getHavingTimeHolidayWithSalary(Model $model,$employeeNo,$Y_M) {
		$Employees = ClassRegistry::init('Employee');

		//引数で渡された社員の情報をemployeesから取得する
		$employees = $Employees->find('first', array('conditions' => array('id' => $employeeNo)));

		//当該社員の１年で取得可能な時間休を算出
		$TimeHolidayWithSalaryForYear = floatval($employees['Employee']['basic_uptime']) * 5;

		if(intval(substr($Y_M,-2)) < 4 ){
			$From_Date = strval(intval(substr($Y_M,0,4)) - 1) . '0401';
		}else{
			$From_Date = substr($Y_M,0,4) . '0401';
		}

		$Operations = ClassRegistry::init('Operation');

		$Y_M = $Y_M . '01';

		$condition_item = array(
			"employee_no" => $employeeNo,
			"cast(date_format(employment_date,'%Y%m%d') as SIGNED) >=" => "cast('". $From_Date . "' as SIGNED)",
			"employment_date <=" => "str_to_date('" . $Y_M . "','%Y%m%d')"
		);

		$fields_item = "sum(time_holiday) as cnt";

    	$sumTimeHoliday = $Operations->find('first',array(
			'fields' => $fields_item,
    		'conditions' => $condition_item
    		));
		
		$havingTimeHolidayWithSalary = $TimeHolidayWithSalaryForYear - floatval($sumTimeHoliday[0]['cnt']);

		return $havingTimeHolidayWithSalary;
		
	}	
//↑有給休暇関係

	
	
	
	
	//代休残数取得
	public function getHavingCompensationDayoff(Model $model,$employeeNo,$Y_M) {
		$Vacations = ClassRegistry::init('Vacation');

		$Vacations = $Vacations->find('first', array(
			'conditions' => array(
				'employee_no' => $employeeNo,
				'year' => substr($Y_M,0,4)
			)));

		return $Vacations['Vacation']['compensation_day_off'];

/*		$YM[0] = substr($Y_M, 0, 4);
		$YM[1] = substr($Y_M, 4, 2);

		$last_year = strval(intval($YM[0] - 1));

	//＊＊＊＊＊＊12/21 - 6/20 => 9/20までに申請
	//＊＊＊＊＊＊6/21 - 12/20 => 3/20までに申請
	//＊＊＊＊＊＊指定年月は何月度として捉える　つまり2019/12は2019/12/20まで
		//指定期間内休日出勤回数取得 : condition1
		//指定期間内取得代休回数取得 : condition2
		//１〜３月：前年７月〜当年３月の休日出勤
		if (intval($YM[1]) >= 1 && intval($YM[1]) <= 3 ) {
			$Conditions1 = array(
				'CAST(DATE_FORMAT(working_date,"%Y%m") as SIGNED) >=' => intval($last_year . '07') ,
				'CAST(DATE_FORMAT(working_date,"%Y%m") as SIGNED) <=' => intval($YM[0] . $YM[1]));
			$Conditions2 = array(
				'CAST(DATE_FORMAT(application_date,"%Y%m") as SIGNED) >=' => intval($last_year . '07') ,
				'CAST(DATE_FORMAT(application_date,"%Y%m") as SIGNED) <=' => intval($YM[0] . $YM[1]));
		//４〜６月：１月〜６月の休日出勤
		}else if(intval($YM[1]) >= 4 && intval($YM[1]) <= 6 ){
			$Conditions1 = array(
				'CAST(DATE_FORMAT(working_date,"%Y%m") as SIGNED) >=' => intval($YM[0] . '01') ,
				'CAST(DATE_FORMAT(working_date,"%Y%m") as SIGNED) <=' => intval($YM[0] . $YM[1]));
			$Conditions2 = array(
				'CAST(DATE_FORMAT(application_date,"%Y%m") as SIGNED) >=' => intval($YM[0] . '01') ,
				'CAST(DATE_FORMAT(application_date,"%Y%m") as SIGNED) <=' => intval($YM[0] . $YM[1]));
		//７〜９月：１月〜当年９月の休日出勤
		}else if(intval($YM[1]) >= 7 && intval($YM[1]) <= 9 ){
			$Conditions1 = array(
				'CAST(DATE_FORMAT(working_date,"%Y%m") as SIGNED) >=' => intval($YM[0] . '01') ,
				'CAST(DATE_FORMAT(working_date,"%Y%m") as SIGNED) <=' => intval($YM[0] . $YM[1]));
			$Conditions2 = array(
				'CAST(DATE_FORMAT(application_date,"%Y%m") as SIGNED) >=' => intval($YM[0] . '01') ,
				'CAST(DATE_FORMAT(application_date,"%Y%m") as SIGNED) <=' => intval($YM[0] . $YM[1]));
		//１０〜１２月：７月〜１２月の休日出勤
		}else{
			$Conditions1 = array(
				'CAST(DATE_FORMAT(working_date,"%Y%m") as SIGNED) >=' => intval($YM[0] . '07') ,
				'CAST(DATE_FORMAT(working_date,"%Y%m") as SIGNED) <=' => intval($YM[0] . $YM[1]));
			$Conditions2 = array(
				'CAST(DATE_FORMAT(application_date,"%Y%m") as SIGNED) >=' => intval($YM[0] . '07') ,
				'CAST(DATE_FORMAT(application_date,"%Y%m") as SIGNED) <=' => intval($YM[0] . $YM[1]));
		}

		//社員No,Type条件
		$Conditions1 = $Conditions1 + array(
			'employee_no' => $employeeNo,
			'type' => '3'
		);

		$Conditions2 = $Conditions2 + array(
			'employee_no' => $employeeNo,
			'type' => '5'
		);

		$OvertimeApplication = ClassRegistry::init('overtime_applications');
		$VacationApplication = ClassRegistry::init('vacation_applications');

    	$OvertimeDays = $OvertimeApplication->find('count',array('conditions' => $Conditions1)
		);

    	$GetHolidays = $VacationApplication->find('count',array('conditions' => $Conditions2)
		);

		$HavingCompensationDayoff = intval($OvertimeDays) - intval($GetHolidays);

		return $HavingCompensationDayoff;
*/
	}
	
	public function get_last_date(Model $model,$year,$month){
		$d = new DateTime();
	    $d->setDate($year, $month, 1);
		$last_date = $d->format('t');
		return $last_date;
	}
	
	public function getRedCalender(Model $model,$from_date,$to_date,$employeeId){
		$Employee = ClassRegistry::init('Employee');
		$Holiday = ClassRegistry::init('Holiday');

		$employees = $Employee->find('first', array('conditions' => array('id' => $employeeId)));
		$holidayPattern = $employees['Employee']['holiday_pattern'];

		$Conditions = array(
			'CAST(DATE_FORMAT(holiday_date,"%Y%m%d") as SIGNED) >=' => intval($from_date) ,
			'CAST(DATE_FORMAT(holiday_date,"%Y%m%d") as SIGNED) <=' => intval($to_date) ,
			"pattern" => $holidayPattern
		);
    	$redCalenders = $Holiday->find('all',array(
			'fields' => array('DATE_FORMAT(holiday_date,"%Y%m%d") as holiday_date'),
			'conditions' => $Conditions,
			'order' => 'holiday_date asc'));

		return $redCalenders;
	}
	
	public function timeRoundDown(Model $model, $time){

		$timeArray = explode(':',$time);

		if(intval($timeArray[1]) <= 14){
			$retTime = $timeArray[0] . ':00';
		}elseif(intval($timeArray[1]) <= 29){
			$retTime = $timeArray[0] . ':15';
		}elseif(intval($timeArray[1]) <= 44){
			$retTime = $timeArray[0] . ':30';
		}else{
			$retTime = $timeArray[0] . ':45';
		}
		return $retTime;
	}

	public function timeRoundUp(Model $model, $time){

		$timeArray = explode(':',$time);

		if(intval($timeArray[1]) == 0){
			$retTime = $timeArray[0] . ':00';
		}elseif(intval($timeArray[1]) <= 15){ //1 - 15 => 15
			$retTime = $timeArray[0] . ':15';
		}elseif(intval($timeArray[1]) <= 30){ //16 - 30 => 30
			$retTime = $timeArray[0] . ':30';
		}elseif(intval($timeArray[1]) <= 45){
			$retTime = $timeArray[0] . ':45';
		}else{
			$retTime = strval(intval($timeArray[0] + 1)) . ':00';
		}
		return $retTime;

	}

	// startTime と endTime の間に休憩時間がまたいでいた場合はその休憩時間を小数点以下2位まで返す。(例：1時間30分 → 1.5)
	public function getDecimalBreakTime(Model $model, $startTime, $endTime){

		$startTimeArray = explode(':',$startTime);
		$endTimeArray = explode(':',$endTime);

		if(intval($startTimeArray[0]) <= 12 && intval($endTimeArray[0]) >= 12){
			if($startTimeArray[0] == 12){
				$start_time = $startTime;
			}else{
				$start_time = '12:00';
			}

			if ($endTimeArray[0] == 12){
				$end_time = $endTime;
			}else{
				$end_time = '13:00';
			}

			$retTime = $this->getTimeDiff($model,$start_time,$end_time);
		}else{
			$retTime = 0;
		}
		
		return $retTime;
	}
	
	public function getTimeDiff(Model $model, $startTime, $endTime){

		if($startTime == $endTime){
			return 0;
		}

		$startTime = $this->timeRoundUp($model,$startTime);
		$endTime = $this->timeRoundDown($model,$endTime);
		$startSec = strtotime($startTime);
		$endSec   = strtotime($endTime);
		
		$diff = $endSec - $startSec;
		if($diff != 0){
			$diff = $diff / 3600;
			if($diff < 0){
				$diff = $diff * -1;
			}
		}
		return $diff;
	}
	
	public function getTimeDiffDownToUp(Model $model, $startTime, $endTime){

		if($startTime == $endTime){
			return 0;
		}

		$startTime = $this->timeRoundDown($model,$startTime);
		$endTime = $this->timeRoundUp($model,$endTime);
		$startSec = strtotime($startTime);
		$endSec   = strtotime($endTime);
		
		$diff = $endSec - $startSec;
		if($diff != 0){
			$diff = $diff / 3600;
			if($diff < 0){
				$diff = $diff * -1;
			}
		}
		return $diff;
	}
	
	//Datetime 経過時間取得：日跨ぎ対応（分）
	public function getDatetimeDiff(Model $model, $startDatetime, $endDatetime){
		$mins = $this->getDatetimeDiff_nonUpDown($model, strtotime($startDatetime . ':00'), strtotime($endDatetime . ':00'));
		
		$ret = $mins / 60;
		
		return $ret;
	}	
	
	//Datetime 経過時間取得_downToUp：日跨ぎ対応（分）
	public function getDatetimeDiff_DownToUp(Model $model, $startDatetime, $endDatetime){
		$startTime = $this->timeRoundDown($model,substr($startDatetime,11,5));
		$endTime = $this->timeRoundUp($model,substr($endDatetime,11,5));
		$startDatetime = new DateTime(substr($startDatetime,0,11) . $startTime);
		$endDatetime = new DateTime(substr($endDatetime,0,11) . $endTime);

		$dateTimeDiff = $startDatetime->diff($endDatetime);

		$ret = $dateTimeDiff->d * 24 + $dateTimeDiff->h + $dateTimeDiff->i / 60;

		return $ret;
	}
	
	// startTime と endTime の間に休憩時間がまたいでいた場合はその休憩時間を小数点以下2位まで返す。(例：1時間30分 → 1.5)
	public function getBreakTime(Model $model, $employmentDate, $startDatetime, $endDatetime){

		$startTime = $this->timeRoundDown($model,substr($startDatetime,11,5));
		$endTime = $this->timeRoundUp($model,substr($endDatetime,11,5));
		$startDate = substr($startDatetime,0,10);
		$endDate = substr($endDatetime,0,10);

		//$startDatetimeと$endDatetimeの差分を取得
		$ret_all = $this->getDatetimeDiff_DownToUp($model,$startDatetime,$endDatetime);

		//稼働日と途中退出日が同一
		if($startDate == $employmentDate){
		
			$startTimeArray = explode(':',$startTime);
			$endTimeArray = explode(':',$endTime);
	
			//12時台かその前　〜　１２時台かその後
			if(intval($startTimeArray[0]) <= 12 && intval($endTimeArray[0]) >= 12){
				//１２時台
				if($startTimeArray[0] == 12){
					$start_time = $startTime;
				}else{
					$start_time = '12:00';
				}
	
				if ($endTimeArray[0] == 12){
					$end_time = $endTime;
				}else{
					$end_time = '13:00';
				}
				//重なってる昼休憩時間を取得
				$ret_lunch = $this->getTimeDiff($model,$start_time,$end_time);
			}else{
				$ret_lunch = 0;
			}
			return($ret_all - $ret_lunch);
		}
	}

	// デフォルトの休憩（ランチタイム）を返す。(例：1時間30分 → 1.5)
	public function getDefaultBreakTime(Model $model, $employeeId, $date){

		$Shift = ClassRegistry::init('Shift');

		$shifts = $Shift->find('first', array('conditions' => array('employee_no' => $employeeId, 'date' => $date)));

		if(count($shifts) == 1){
			$ret_lunch = $shifts['Shift']['break_deduction'];
		}else{
			$ret_lunch = 0;
		}
		return($ret_lunch);
	}

	public function getEmployeeAllInfo(Model $model, $id) {

		$Employee = ClassRegistry::init('Employee');

		$employees = $Employee->find('first', array('conditions' => array('id' => $id)));

		return $employees;
	}

//	//指定日が営業日か否かを取得(0:休業日 1:営業日)
//	public function judgeWorkingDate(Model $model, $companyNo ,$Date) {
//		return 1;
//	}

	//get overtime.
	public function getOvertime(Model $model, $employeeNo, $employmentDate, $companyNo, $startDateTime, $endDateTime, $type) {

		$startDateTime = $startDateTime . ':00';
		$endDateTime = $endDateTime . ':00';
		//就業基本情報取得
//		$BAI = ClassRegistry::init('BasicAttendanceInformation');
		$OA = ClassRegistry::init('OvertimeApplication');

//		$BAIdata = $BAI->find('all', array('condition' => array('company_no' => $companyNo)));
		
		//就業基本情報の開始時刻、終了時刻、残業開始時刻
//		$openigTime = $BAIdata[0]['BasicAttendanceInformation']['opening_time'];
//		$closingTime = $BAIdata[0]['BasicAttendanceInformation']['closing_time'];
//		$overtimeStartTime = $BAIdata[0]['BasicAttendanceInformation']['overtime_start_time'];
		
		//休日出勤判定(0:休業日 1:営業日)
//		$employmentDateKbn = $this->judgeWorkingDate($model, $companyNo, $employmentDate);

		//残業申請情報取得（承認済みのみ）・・・複数レコードあり
		$condition =
			array(
				"OvertimeApplication.employee_no" => $employeeNo,
				"OvertimeApplication.working_date" => $employmentDate,
				"OvertimeApplication.type" => $type,
				"OvertimeApplication.approval" => "1"
			);

    	$OAData = $OA->find('all',array(
    		'conditions' => $condition));

		$totalOT_i = 0;

		for ($i = 0; $i < count($OAData); $i++) {
			$OTStart = $OAData[$i]['OvertimeApplication']['overtime_start'];
			$OTEnd = $OAData[$i]['OvertimeApplication']['overtime_end'];
			$OTStartDate = $OAData[$i]['OvertimeApplication']['overtime_start'];
			$OTEndDate = $OAData[$i]['OvertimeApplication']['overtime_end'];
			
			$retVal = $this->getDuplicateTimes($model, $startDateTime, $endDateTime, $OTStart, $OTEnd);
			$totalOT_i = $totalOT_i + $retVal['minutes'];
			//ランチ休憩を減算
//			$breakTimegetDefaultBreakTime = $this->getDefaultBreakTime($model, $retVal['startDatetime'], $retVal['endDatetime']);
//			$totalOT_i = $totalOT_i - $breakTimegetDefaultBreakTime * 60;
//
//			if($employmentDateKbn == 0){
//				//見なし残業減算
//				//見なし残業スタート時間
//				$noCountOTStart = $employmentDate . ' ' . $closingTime . ':00';
//				$noCountOTEnd = $employmentDate . ' ' . $overtimeStartTime . ':00';
//	
//				
//				//見なし残業期間中の残業時間を取得
//				$noCountOT = $this->getDuplicateTimes($model, $noCountOTStart, $noCountOTEnd, $retVal['startDatetime'], $retVal['endDatetime']);
//				$totalOT_i = $totalOT_i - $noCountOT;
//			}
		}
		if($totalOT_i != 0){
			$totalOT = strval(floor($totalOT_i /60)) . ':' . str_pad($totalOT_i % 60,2,0,STR_PAD_LEFT);
		}else{
			$totalOT = '0:00';
		}

		return $totalOT;
	}
	
	//２つの期間で重なっている部分の分を取得
	public function getDuplicateTimes(Model $model, $startDatetime1, $endDatetime1, $startDatetime2, $endDatetime2){

		$timestamp_start1 = strtotime($startDatetime1);
		$timestamp_start2 = strtotime($startDatetime2);
		$timestamp_end1 = strtotime($endDatetime1);
		$timestamp_end2 = strtotime($endDatetime2);
		
		//set fromDatetime
		$startDiff = $timestamp_start2 - $timestamp_start1;
		//$startDatetime2 >= $startDatetime1
		//2 が 1より後　ーーー　２を採用
		if($startDiff >= 0){
			//
			$tmpdiff = $timestamp_end1 - $timestamp_start2;
			if($tmpdiff > 0){
				//use $startDatetime2
				$fromDateTime = $timestamp_start2;
			}else{
				return array('minutes' => 0);
			}
		}else{ //１が２より後　ーーー　１を採用
			$tmpdiff = $timestamp_end2 - $timestamp_start1;
			if($tmpdiff > 0){ //start1よりend2が後
				$fromDateTime = $timestamp_start1;
			}else{
				return array('minutes' => 0);
			}
		}
		
		//set toDatetime
		$endDiff = $timestamp_end2 - $timestamp_end1;

		//１ より ２が後　ーーー　１を採用
		if($endDiff >= 0){
			$tmpdiff = $timestamp_start2 - $timestamp_end1;
			if($tmpdiff >= 0){ //end1よりstart2が後　ーーー　重なりなし
				return array('minutes' => 0);
			}else{
				//use $endDatetime1
				$toDateTime = $timestamp_end1;
			}
		}else{
			$tmpdiff = $timestamp_start1 - $timestamp_end2;
			if($tmpdiff >= 0){ //end2よりstart1が後　ーーー　重なりなし
				return array('minutes' => 0);
			}else{
				$toDateTime = $timestamp_end2;
			}
		}

		$retValue = array(
			'minutes' => $this->getDatetimeDiff_nonUpDown($model, $fromDateTime, $toDateTime),
			'startDatetime' => date("Y/m/d H:i:s",$fromDateTime),
			'endDatetime' => date("Y/m/d H:i:s",$toDateTime));
		
		return $retValue;
	}
	
	//Datetime 経過時間取得：日跨ぎ対応（分）まるめなし
	public function getDatetimeDiff_nonUpDown(Model $model, $startDatetime, $endDatetime){

		$startDatetime = new DateTime(date("Y/m/d H:i:s",$startDatetime));
		$endDatetime = new DateTime(date("Y/m/d H:i:s",$endDatetime));

		$dateTimeDiff = $startDatetime->diff($endDatetime);
		$ret = ($dateTimeDiff->d * 24 + $dateTimeDiff->h) * 60 + $dateTimeDiff->i;
		return $ret;
	}	
	
	public function getvacationsInfo(Model $model, $employee_no, $year){

		$Vacation = ClassRegistry::init('Vacation');

    	$vacationData = $Vacation->find('all',array(
			'conditions' => array('employee_no' => $employee_no,'year' => $year)));

		return $vacationData;
	}

	public function getVacationApplicationNameByDate(Model $model, $employee_no, $date){

		$vcationApplication = ClassRegistry::init('VacationApplication');
		$vacationApplicationMst = $this->vacationApplicationMst($model);

    	$vcationApplications = $vcationApplication->find('all', array(
			'conditions' => array('employee_no' => $employee_no, 'vacation_date' => $date)));

		$vacationApplicationName = '';
		if (count($vcationApplications) > 0) {
			$vacationApplicationName = $vacationApplicationMst[$vcationApplications[0]['VacationApplication']['type']];
		}

		return $vacationApplicationName;
	}

	public function pikaGetOvertime(Model $model, $operationId){
		$Operation = ClassRegistry::init('Operation');

    	$operationData = $Operation->find('all',array(
			'conditions' => array('id' => $operationId)));
		
		for ($i = 0; $i < count($operationData); $i++) {
			//日次承認済みチェック
			if($operationData[$i]['Operation']['operation_approval'] == 1){
				//シフト情報を取得
				
				//シフトと重複している時間帯を外す

			}else{
				return  0;
			}
		}
	}
	
	//通常残業と深夜残業を別けて算出（２２：００が境界線）
	public function getOTandLOT(Model $model, $operationNo){

		//Operations
		$OP = ClassRegistry::init('Operation');

		$OPdata = $OP->find('first', array('conditions' => array('id' => $operationNo)));

		if(count($OPdata) == 0){
			$retVal = array('over_time' => '0:00',
			'over_time_decimal' => 0,
			'late_over_time' => '0:00',
			'late_over_time_decimal' => 0);
			return $retVal;
		}else{
			$employeeNo = $OPdata['Operation']['employee_no'];
			$employmentDate = $OPdata['Operation']['employment_date'];
			$companyNo = $OPdata['Operation']['company_no'];
			$startDateTime = substr($OPdata['Operation']['start_datetime'],0,16);
			$endDateTime = substr($OPdata['Operation']['end_datetime'],0,16);
		}

		//深夜残業開始時間 ・・・　稼働日の22:00
		$startLOT = $employmentDate . ' ' . '22:00';
		
		$normalOT_start = $startDateTime;
		$normalOT_end = $endDateTime;
		$lateOT_start = null;
		$lateOT_end = null;
		
		if($endDateTime > $startLOT){
			$normalOT_start = $startDateTime;
			$normalOT_end = $employmentDate . ' ' . '22:00';
			$lateOT_start = $employmentDate . ' ' . '22:00';
			$lateOT_end = $endDateTime;
		}

		$retOT = $this->getOvertime($model, $employeeNo, $employmentDate, $companyNo, $normalOT_start, $normalOT_end, OVERTIME_KIND_NORMAL);
		$retLNOT = $this->getOvertime($model, $employeeNo, $employmentDate, $companyNo, $lateOT_start, $lateOT_end, OVERTIME_KIND_NORMAL);

		$separate_retOT = explode(':',$retOT);
		$separate_retLNOT = explode(':',$retLNOT);

		$over_time_decimal = intval($separate_retOT[0]) * 60 + $separate_retOT[1];
		
		$late_over_time_decimal = intval($separate_retLNOT[0]) * 60 + $separate_retLNOT[1];
		
		$retVal = array('over_time' => $retOT,
			'over_time_decimal' => round($over_time_decimal / 60,2),
			'late_over_time' => $retLNOT,
			'late_over_time_decimal' => round($late_over_time_decimal / 60,2));
		return $retVal;
	}

	// 稼働に対応する時間休時間と対象休出日を取得する
	public function getTimeVacationAndHolidayWorkInfo(Model $model,$employeeNo,$EmploymentDate){
		$retValues = array('type' => '','value' => '');

		$VA = ClassRegistry::init('VacationApplication');
		$OTA = ClassRegistry::init('OvertimeApplication');

		$VAdata = $VA->find('all', array('conditions' => array(
			'employee_no' => $employeeNo,
			'vacation_date' => $EmploymentDate,
			'approval' => APPROVAL_ALREADY)));

		for ($i = 0; $i < count($VAdata); $i++) {
			$Type = $VAdata[$i]['VacationApplication']['type'];
			$retValues['type'] = $this->vacationApplicationMst($model)[$Type];

			if($Type == VACATION_KIND_TIME){
				$retValues['value'] = strval($VAdata[$i]['VacationApplication']['vacation_hours']) . '時間';
			}elseif($Type == VACATION_KIND_COMPENSATION){
				$OTAdata = $OTA->find('first', array('conditions' => array(
					'employee_no' => $employeeNo,
					'type' => OVERTIME_KIND_HOLIDAY_WORK,
					'plan_of_dayoff' => $EmploymentDate,
					'approval' => APPROVAL_ALREADY)));

				if(count($OTAdata) > 0){
					$retValues['value'] = strval($OTAdata['OvertimeApplication']['working_date']) . 'の代休';
				}
			}
		}
		return $retValues;
	}
	
	public function getActualOperatingTime(Model $model,$operationNo){
		$retVal = array('startTime' => '0000-00-00 00:00:00','actualOperatingTime' => 0.0);

		$OP = ClassRegistry::init('Operation');
		$SHIFT = ClassRegistry::init('Shift');
		
		$OPdata = $OP->find('first', array('conditions' => array('id' => $operationNo)));

		if(count($OPdata) == 0){
			return $retVal;
		}elseif($OPdata['Operation']['end_datetime'] == null){
			return $retVal;
		}

		$SHIFTdata = $SHIFT->find('first', array(
			'conditions' => array(
				'employee_no' => $OPdata['Operation']['employee_no'],
				'date' => $OPdata['Operation']['employment_date']
			)));

		$startDateTime = $OPdata['Operation']['start_datetime'];
		$endDateTime = $OPdata['Operation']['end_datetime'];
		if(count($SHIFTdata) == 0){
			$shift_startDateTime = $startDateTime;
			$shift_endDateTime = $endDateTime;
		}else{
			$shift_startDateTime = $OPdata['Operation']['employment_date'] . ' ' . $SHIFTdata['Shift']['start_time'];
			$shift_endDateTime = $OPdata['Operation']['employment_date'] . ' ' . $SHIFTdata['Shift']['closing_time'];
		}
		$ShiftWorkInfo = $this->getDuplicateTimes($model, $startDateTime, $endDateTime, $shift_startDateTime, $shift_endDateTime);

		if($ShiftWorkInfo['minutes'] == 0){
			$retVal['startTime'] = $startDateTime;
			$retVal['actualOperatingTime'] = 0;
		}else{
			$retVal['startTime'] = str_replace('/','-',$ShiftWorkInfo['startDatetime']);
			
			$retVal['actualOperatingTime'] = $totalOT = strval(floor($ShiftWorkInfo['minutes'] /60)) . ':' . str_pad($ShiftWorkInfo['minutes'] % 60,2,0,STR_PAD_LEFT);
		}

		return $retVal;
	}
	
}
