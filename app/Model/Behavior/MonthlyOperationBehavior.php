<?php

class MonthlyOperationBehavior extends ModelBehavior {

	public function tmp_getBusinessYmd(Model $model,$YM){
		$Year = substr($YM, 0, 4);
		$Month = substr($YM, 4, 2);

		if ($Month == '1'){
			$FromYear = strval(intval($Year) - 1);
			$FromMonth = '12';
		}else{
			$FromYear = $Year;
			$FromMonth = str_pad(intval($Month) - 1,2,0,STR_PAD_LEFT);
		}
			$fromDay = $FromYear . '-' . $FromMonth . '-21';
			$toDay = $Year . '-' . $Month . '-20';
		$businessYmd = array($fromDay,$toDay);
		return $businessYmd;
	}

	//日次承認が１ヵ月分終わっているかチェック
	public function get_all_daily_approval(Model $model,$year_month,$employeeNo){
		$Operation = ClassRegistry::init('Operations');

		//あとで生かす
		//$Operation->Behaviors->load('Master');
		//$businessYmd = $Operation->getBusinessYmd($model,$year_month);

		//あとでチェンジする
		$businessYmd = $this->tmp_getBusinessYmd($model,$year_month);

		$conditions = array(
				'employee_no' => $employeeNo,
				'employment_date >=' => $businessYmd[0],
				'employment_date <=' => $businessYmd[1]);

		$Operations = $Operation->find('first', $conditions);

		$Holiday = ClassRegistry::init('Holidays');
		
/*		$conditions = array(
				'company_no' => $this->Auth->user('company_no'),
				'holiday_date >=' => $businessYmd[0],
				'holiday_date <=' => $businessYmd[1]);
		var_dump($conditions);die;
*/
		return true;
	}
	
	//休暇承認が１ヵ月分終わっているかチェック
	public function get_all_vacation_approval(Model $model,$year_month,$employeeNo){
		return true;
	}
	
	//時間外承認が１ヵ月分終わっているかチェック
	public function get_all_overtime_approval(Model $model,$year_month,$employeeNo){
		return true;
	}
	
	//月次稼働作成--NGの時は既存月次稼働レコード削除
	public function create_monthly_approval(Model $model,$year_month,$employeeNo){
		$MonthlyOperation = ClassRegistry::init('MonthlyOperation');
		
		$MonthlyOperation->Behaviors->load('Master');

		//このメソッドが呼ばれたということは、月内の個別承認が行われたタイミングのため、月次レコードは無条件に削除する
		$conditions = array('employee_no' => $employeeNo,'yearMonth' => $year_month);
		$MonthlyOperation->deleteAll($conditions);
		
		$insertJudge = true;
		if(!$this->get_all_daily_approval($model,$year_month,$employeeNo)){
			$insertJudge = false;
		}
		if(!$this->get_all_vacation_approval($model,$year_month,$employeeNo)){
			$insertJudge = false;
		}
		if(!$this->get_all_overtime_approval($model,$year_month,$employeeNo)){
			$insertJudge = false;
		}

		if($insertJudge == true){
			//Insert monthly_operations
			$MonthlyOperation->create();
	
			$empData = $MonthlyOperation->getEmployeeAllInfo($employeeNo);
	
			$items = array(
				'company_no' => $empData['Employee']['company_no'],
				'division_no' => $empData['Employee']['division_no'],
				'department_no' => $empData['Employee']['department_no'],
				'section_no' => $empData['Employee']['section_no'],
				'employee_no' => $employeeNo,
				'yearMonth' => $year_month
			);
	    	$MonthlyOperation->save($items);
		}
	}

}