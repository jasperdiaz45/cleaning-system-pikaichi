<?php
App::uses('AppModel', 'Model');
/**
 * Employee Model
 *
 */
class Employee extends AppModel {

	public function beforeSave($options = array()) {
		
		if (empty($this->data[$this->alias]['password'])) {
			unset($this->data[$this->alias]['password']);
		}
		if (isset($this->data[$this->alias]['password'])) {
			App::uses('AuthComponent',  'Controller/Component');
			$this->data[$this->alias]['password'] = AuthComponent::password($this->data[$this->alias]['password']);
		} else if (isset($this->data['password'])) {
			App::uses('AuthComponent',  'Controller/Component');
			$this->data['password'] = AuthComponent::password($this->data[$this->alias]['password']);
		} else if (isset($this->data[$this->alias]['new_password']) && !empty($this->data[$this->alias]['new_password'])) {
			App::uses('AuthComponent',  'Controller/Component');
			$this->data[$this->alias]['password'] = AuthComponent::password($this->data[$this->alias]['new_password']);
		} else if (isset($this->data['new_password']) && !empty($this->data['new_password'])) {
			App::uses('AuthComponent',  'Controller/Component');
			$this->data['password'] = AuthComponent::password($this->data[$this->alias]['new_password']);
		}
		return true;
	}
	
	public $virtualFields = array(
	    'username'	=> 'CONCAT(Employee.last_name, "　", Employee.first_name)'
	);
	
/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'section_no' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'name' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'mail_address' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'password' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'effectiveness' => array(
			'boolean' => array(
				'rule' => array('boolean'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'retirement' => array(
			'boolean' => array(
				'rule' => array('boolean'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'on_leave' => array(
			'boolean' => array(
				'rule' => array('boolean'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'attendance_adjustment' => array(
			'boolean' => array(
				'rule' => array('boolean'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);
	
	public function getLatestReportData() {
		$condDays = Configure::read('lasterReportDays');
		
		$toDay = DateTimeUtil::format('Y/m/d');
		$fromDay = DateTimeUtil::calcDate('Y/m/d', '-'.$condDays.' days', $toDay);
		
		$query = 'SELECT User.id, User.last_name, User.first_name, Matter.matter_name, Matter.id, Matter.identifier, WorkReport.sum_work_count ';
		$query .= 'FROM users User ';
		$query .= 'LEFT JOIN ( ';
		$query .= ' SELECT SUM(wm.work_count) AS sum_work_count, wm.matter_id AS matter_id, wr.user_id AS user_id FROM work_matters wm ';
		$query .= ' LEFT JOIN work_reports wr ON wm.work_report_id = wr.id ';
		$query .= ' WHERE wr.work_date BETWEEN "' . $fromDay . '" AND "' . $toDay . '" AND wm.deleted = 0 AND wr.deleted = 0 ';
		$query .= ' GROUP BY wr.user_id, wm.matter_id ';
		$query .= ') WorkReport ON User.id = WorkReport.user_id ';
		$query .= 'LEFT JOIN matters Matter ON WorkReport.matter_id = Matter.id AND Matter.deleted = 0 ';
		$query .= 'WHERE User.deleted = 0 AND User.role <> "1" ';
		$query .= 'ORDER BY User.id ASC, WorkReport.sum_work_count DESC, Matter.id ASC;';
		
		$data = $this->query($query);
		
		return $data;
	}

}
