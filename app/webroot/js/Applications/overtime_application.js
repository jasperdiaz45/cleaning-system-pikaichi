var OVERTIME_KIND_NIGHT_SHIFT;

$(function() {
	chgDisplayStateEndDate();
	chgLabelPlanOfDayoff();
});

function chgDisplayStateEndDate() {
	if ($('#overtime-application-type').val() == OVERTIME_KIND_NIGHT_SHIFT) {
		$('#overtime-end-date').attr('disabled', false);
		$('#overtime-end-date-far').attr('disabled', false);
	} else {
		$('#overtime-end-date').attr('disabled', 'disabled');
		$('#overtime-end-date-far').attr('disabled', 'disabled');
		var workingDate = $('#working-date').val();
		if (workingDate == '') workingDate = $('#today-date').val();
		$('#overtime-end-date').val(workingDate);
	}
}

function chgLabelPlanOfDayoff() {
	if ($('#overtime-application-type').val() == $('#overtime-kind-holiday-work').text()) {
		$('#label-plan-of-dayoff').css('display', 'none');
		$('#label-compensation-date').css('display', 'block');
		$('#planOfDayoff').css('display', 'none');
		$('#compensation-date').css('display', 'block');
	} else {
		$('#label-plan-of-dayoff').css('display', 'block');
		$('#label-compensation-date').css('display', 'none');
		$('#planOfDayoff').css('display', 'block');
		$('#compensation-date').css('display', 'none');
	}
}

// サブミット時にタイプが休出以外の時は代休日にダミーの値を入れる。
// 休出以外の時は代休日が空でも良いためバリデーションチェックでかからないようにするため。
function setDummyCompensationDate() {
	if ($('#overtime-application-type').val() != $('#overtime-kind-holiday-work').text()) {
		if ($('#compensation-date').val() == '') {
			$('#compensation-date').val('dummy');
		}
	}
}
