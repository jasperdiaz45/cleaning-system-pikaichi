var g_ajax_url_base_path = '';
var g_opportunity_dialog_page = 1;

$(function(){

	g_ajax_url_base_path = $("#ajax-url-base-path").text();

	// disable submission by enter key.


	// for opportunity dialog
	if ($('#opportunity-idx-max').length){
		// makeAddOpportunityList();
		getMessageSearch(1);
		getDailyReportSearch(1);
	}
	
});




function getMessageSearch(page = null) {

	if (page != null) g_opportunity_dialog_page = page;
	
	var period_from = $('#period_from').val();
	var period_to = $('#period_to').val();
	var reporter = $('#reporter_no').val();
	var transmission = $('#transmission:checked').length ? 1 : 0;
	var is_read = $('#is_read:checked').length ? 1 : 0;
	
	// alert(reporter);
	var count = 0;
	var limit = 0;

	// make ignore opportunity id list 

	// pagination

	$.ajax({
		url: g_ajax_url_base_path + "get_message_sel_page_info",
		async: false,
		type: "POST",
		dataType: "JSON",
		data: {
		    	'page' : g_opportunity_dialog_page,
				'period_from' : period_from,
	  			'period_to' : period_to,
	  			'reporter_no' : reporter,
	  			'transmission' : transmission,
	  			'is_read' : is_read
		},

		success: function (data) {
			count = data.count;
			limit = data.limit;
		}
	});

	var page_max = Math.ceil(count / limit);
	if (g_opportunity_dialog_page > page_max) g_opportunity_dialog_page = page_max;
	var start_idx = (g_opportunity_dialog_page - 1) * limit + 1;
	var end_idx = g_opportunity_dialog_page * limit;
	if (start_idx > count) start_idx = count;
	if (start_idx < 0) start_idx = 0;
	if (end_idx > count) end_idx = count;

	var opportunity_pagination = '<li><a>全' + count + '件｜' + start_idx + '〜' + end_idx + '件表示｜</a></li>'
	for (var idx = 1; idx <= page_max; idx++) {
		var selected = idx == g_opportunity_dialog_page ? ' style="background-color: #90A4AD; border: 1px solid #90A4AD;"' : ' style="background-color: #DDE1E3; border: 1px solid #DDE1E3;"';
		opportunity_pagination += '<li><a href=""' + selected + ' onclick="getMessageSearch(' + idx + '); return false;">' + idx + '</a></li>'
	}

	$('#opportunity-pagination').html(opportunity_pagination);

	// select list
	
	$.ajax({
		url: g_ajax_url_base_path + "get_message_sel_list",
		async: false,
		type: "POST",
		dataType: "JSON",
		data: {
			    'page' : g_opportunity_dialog_page,
			    'period_from' : period_from,
	  			'period_to' : period_to,
	  			'reporter_no' : reporter,
	  			'transmission' : transmission,
	  			'is_read' : is_read
		},
		success: function (data) {

			var message_sel_list = '';
			for (var idx = 0; idx < data.length; idx++) {

				var parentProjectId = data[idx].Message.id;

                if (data[idx].Message.insert_datetime == null) data[idx].Message.insert_datetime = '';
                if (data[idx].Message.kind == null) data[idx].Message.kind = '';
                if (data[idx].Message.message == null) data[idx].Message.message = '';
                if (data[idx].Message.sender_name == null) data[idx].Message.sender_name = '';
                if (data[idx].Message.Status == null) data[idx].Message.Status = '';
                
				message_sel_list += '<tr>';
                message_sel_list += '<td>' + data[idx].Message.insert_datetime + '</td>';
                message_sel_list += '<td>' + data[idx].Message.kind + '</td>';
                message_sel_list += '<td>' + data[idx].Message.message + '</td>';
                message_sel_list += '<td>' + data[idx].Message.sender_name + '</td>';
                message_sel_list += '<td>' + data[idx].Message.Status + '</td>';
                message_sel_list += '<td><a href="message_detail/'+data[idx].Message.id+'" class="btn btn-blue">詳細';
                message_sel_list += '</a></td>';
			    message_sel_list += '</tr>';
			}

			$('#message-sel-list').html(message_sel_list);
		}
	});
}

function getDailyReportSearch(page=null) {
	
	if (page != null) g_opportunity_dialog_page = page;
	
	var period_from = $('#period_from_daily_report').val();
	var period_to = $('#period_to_daily_report').val();
	var reporter = $('#reporter_no_daily_report').val();
	var is_read = $('#is_read_daily_report:checked').length ? 1 : 0;
	
	// alert(reporter);
	var count = 0;
	var limit = 0;
	
	// make ignore opportunity id list 

	// pagination

	$.ajax({
		url: g_ajax_url_base_path + "get_daily_report_sel_page_info",
		async: false,
		type: "POST",
		dataType: "JSON",
		data: {
		    	'page' : g_opportunity_dialog_page,
				'period_from_daily_report' : period_from,
	  			'period_to_daily_report' : period_to,
	  			'reporter_no_daily_report' : reporter,
	  			'is_read_daily_report' : is_read
		},

		success: function (data) {
			count = data.count;
			limit = data.limit;
		}
	});
	
	var page_max = Math.ceil(count / limit);
	if (g_opportunity_dialog_page > page_max) g_opportunity_dialog_page = page_max;
	var start_idx = (g_opportunity_dialog_page - 1) * limit + 1;
	var end_idx = g_opportunity_dialog_page * limit;
	if (start_idx > count) start_idx = count;
	if (start_idx < 0) start_idx = 0;
	if (end_idx > count) end_idx = count;

	var opportunity_pagination = '<li><a>全' + count + '件｜' + start_idx + '〜' + end_idx + '件表示｜</a></li>'
	for (var idx = 1; idx <= page_max; idx++) {
		var selected = idx == g_opportunity_dialog_page ? ' style="background-color: #90A4AD; border: 1px solid #90A4AD;"' : ' style="background-color: #DDE1E3; border: 1px solid #DDE1E3;"';
		opportunity_pagination += '<li><a href=""' + selected + ' onclick="getDailyReportSearch(' + idx + '); return false;">' + idx + '</a></li>'
	}
	
	$('#opportunity-pagination').html(opportunity_pagination);
	
	// select list
	
	$.ajax({
		url: g_ajax_url_base_path + "get_daily_report_sel_list",
		async: false,
		type: "POST",
		dataType: "JSON",
		data: {
		    	'page' : g_opportunity_dialog_page,
				'period_from_daily_report' : period_from,
	  			'period_to_daily_report' : period_to,
	  			'reporter_no_daily_report' : reporter,
	  			'is_read_daily_report' : is_read
		},
		success: function (data) {

			var daily_report_sel_list = '';
			for (var idx = 0; idx < data.length; idx++) {

				var parentProjectId = data[idx].DailyReport.id;

                if (data[idx].DailyReport.report_date == null) data[idx].DailyReport.report_date = '';
                if (data[idx].Employee.last_name == null) data[idx].Employee.last_name = '';
                if (data[idx].DailyReport.work_content == null) data[idx].DailyReport.work_content = '';
                if (data[idx].DailyReport.liaison_matters_problems == null) data[idx].DailyReport.liaison_matters_problems = '';
                if (data[idx].Report.confirmed == null) data[idx].Report.confirmed = '';
                
				daily_report_sel_list += '<tr>';
                daily_report_sel_list += '<td>' + data[idx].DailyReport.report_date + '</td>';
                daily_report_sel_list += '<td>' + data[idx].Employee.last_name + '</td>';
                daily_report_sel_list += '<td>' + data[idx].DailyReport.work_content + '</td>';
                daily_report_sel_list += '<td>' + data[idx].DailyReport.liaison_matters_problems + '</td>';
                daily_report_sel_list += '<td>' + data[idx].Report.confirmed + '</td>';
                daily_report_sel_list += '<td><a href="daily_report_detail/'+data[idx].DailyReport.operation_no+'" class="btn btn-blue">詳細';
                daily_report_sel_list += '</a></td>';
			    daily_report_sel_list += '</tr>';
			}

			$('#daily-report-sel-list').html(daily_report_sel_list);
		}
	});
}

