var collectionList      = [];
var matterCollection    = [];
var element             = {};

$(function() {
    
    /**
     * 案件一覧選択画面：チェックボックス選択時
     */
    $(".modal-body .matterRow label").click(function(event){
        var selectedId          = $(this)[0].id;
        var matterString        = $(event.target).text();
        var selectedCheckBoxId  = "popupmatter-" + selectedId;
        var hiddenUserId        = "hidden-user-matter-" + selectedId;
        var checkboxElement     = document.getElementById(selectedCheckBoxId);
        
        if (matterString != '') {
            if (!checkboxElement.checked) {
                if (!isInArray(selectedId, collectionList)) {
                    // store only id in array
                    collectionList.push(selectedId);
                    // store element as array object
                    element = {'id': selectedId, 'mattername' : matterString};
                    matterCollection.push(element);
                }
            } else {
                var findItem = collectionList.indexOf(selectedId);
                if (findItem > -1) {
                    collectionList.splice(findItem, 1);
                    var stringgifyData = JSON.stringify(matterCollection);
                    var extractCollection = JSON.parse(stringgifyData);
                    var targetRemoveObject = extractCollection.filter(function(el) {
                                return el.id !== selectedId;
                    });
                    matterCollection = targetRemoveObject;
                }
            }
        }
    });

    /**
     * 案件一覧選択画面：閉じるボタン押下時
     */
    $("#btn-close-moal").click(function(){
        var appendMatterIdArray = [];
        
        // clear all with new, before re-render
        $('.new').remove();
                        
        var sizeObject	= Object.size(matterCollection);
        if (sizeObject > 0) {
            Object.keys(matterCollection).forEach(function(key) {
                // start render item
                var updateMatter    = '';
                var updateSelectBox = '';
                var matterId        = matterCollection[key]['id'];
                var matterName      = matterCollection[key]['mattername'];
                
                updateMatter    += matterName;
                updateMatter    += '<input type="hidden" value="' + matterId + '" name="matter_id[]" id="matter_id" />';
                updateMatter    += '<input type="hidden" value="" name="WorkMatter_id[]" id="WorkMatter_id" />';
                
                for (var j = 0; j <= 10; j++) {
                    if (j == 0) {
                        updateSelectBox += '<option value="">-</option>';    
                    } else {
                        updateSelectBox += '<option value="' + j + '">' + j + '</option>';    
                    }
                }
                
                $('#list-matter').append($('<div class="matter-'+ matterId +' new" id="' + matterId + '">')
                    .append($('<span class="matter-text">').append(updateMatter))
                    .append($('<span>').append($('<select id="work_count" name="work_count[]" class="form-control">').append(updateSelectBox)))
                );
            });
        }
    });
    
    /**
     * 一覧：日付・対象者：変更時
     */
    $(document).on('change', '.date-filter', function(event) {
        document.WorkReportIndexForm.submit();
    });
    
});

function isInArray(value, array) {
  return array.indexOf(value) > -1;
}

Object.size = function(obj) {
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
};