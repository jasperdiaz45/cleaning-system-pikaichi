$(function(){
	var bootstrapDatepicker = $.fn.datepicker.noConflict();
	$.fn.bootstrapDP = bootstrapDatepicker;
	
	$(".monthInput").bootstrapDP({
		format: "yyyy/mm",
		language: "ja",
		autoclose: true,
		minViewMode: "months"
	});
});