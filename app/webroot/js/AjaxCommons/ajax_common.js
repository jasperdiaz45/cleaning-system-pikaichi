
var g_ajax_url_base_path = '/';		// ajax url base path
var g_ajax_url_path = g_ajax_url_base_path + 'AjaxCommons/';

var g_company_no = '';
var g_ignore_id_list = '';
var g_ignore_id_list_for_department_list = '';

function setAjaxUrlBasePath(path) {
	g_ajax_url_base_path = path;
	g_ajax_url_path = g_ajax_url_base_path + 'AjaxCommons/';
}

function setCompanyNo(companyNo) {
	g_company_no = companyNo;
} 

function setIgnoreIdListForDepartmentList(ignoreIdList) {
	g_ignore_id_list_for_department_list = ignoreIdList;
}

function getSearchDivisionList(addElmId, selectNo = null) {
	g_ignore_id_list = '';
	getSearchList('get_search_division_list', addElmId, null, selectNo);
}

function getSearchDepartmentList(addElmId, parentElmId = null, selectNo = null) {
	g_ignore_id_list = g_ignore_id_list_for_department_list;
	getSearchList('get_search_department_list', addElmId, parentElmId, selectNo);
}

function getSearchSectionList(addElmId, parentElmId = null, selectNo = null) {
	g_ignore_id_list = '';
	getSearchList('get_search_section_list', addElmId, parentElmId, selectNo);
}

function getSearchList(action, addElmId, parentElmId, selectNo) {

	var parent_id = parentElmId != null ? $('#' +  parentElmId).val() : '';

	$.ajax({
		url: g_ajax_url_path + action,
		async: false,
		type: "POST",
		dataType: "JSON",
		data: {
			'company_no' : g_company_no,
			'parent_id' : parent_id,
			'ignore_id_list' : g_ignore_id_list,
		},
		success: function (data) {
			var search_list = '';
			for (var idx = 0; idx < data.length; idx++) {
				search_list += '<option value="' + data[idx].id + '"' + (data[idx].id == parseInt(selectNo) ? ' selected' : '') + '>' + data[idx].name + '</option>';
			}
	
			$('#' + addElmId).html(search_list);
		}
	});
}
