
$('#button1').click(function(e){
    e.preventDefault();
    // Code goes here
});
$('#button2').click(function(e){
    e.preventDefault();
    // Code goes here
});

$('input,textarea').attr('autocomplete', 'off');


function select_division(get_id,name){
    // alert(get_id);
    $("#department_no").val(get_id);
    $("#department_name").text(name);
    $("#department_name_h").val(name);
    $("#ModalDepartment").modal('hide');
}

function select_case(get_id,name){
    // alert(get_id);
    $("#parent_project_no").val(get_id);
    $("#parent_project_name").text(name);
    $("#parent_project_name_h").val(name);
    $("#ModalOpportunity").modal('hide');
}

function setProductDialog(itemid, itemCode, productName) {

	$('#product-item-id').val(itemid);
	$('#product-item-code').val(itemCode);
	$('#product-name').val(productName);
}
