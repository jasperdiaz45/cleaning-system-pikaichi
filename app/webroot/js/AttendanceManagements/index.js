
$(function() {
	setAjaxUrlBasePath($('#ajax-full-base-url').text() + '/');
	setCompanyNo($('#f-company-no').text());

	getSearchDepartmentList('search-department-id', null, $('#f-department-no').text());
	getSearchSectionList('search-section-id', 'search-department-id', $('#f-section-no').text());
});
