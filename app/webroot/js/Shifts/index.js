function get_time_total(id) {
	var start = $("#start_"+id).val();
	var end = $("#end_"+id).val();
	var brTime = $("#break_"+id).val();
	var basic_uptime = $("#basic_uptime_"+id);

    start = start.split(":");
    end = end.split(":");
    brTime = brTime.split(":");

    var startDate = new Date(0, 0, 0, start[0], start[1], 0);
    var endDate = new Date(0, 0, 0, end[0], end[1], 0);
    
	
	var diff = endDate.getTime() - startDate.getTime();

    var hours = Math.floor(diff / 1000 / 60 / 60);
    diff -= hours * 1000 * 60 * 60;
    var minutes = Math.floor(diff / 1000 / 60);

    var total_1 = (hours < 9 ? "0" : "") + hours + ":" + (minutes < 9 ? "0" : "") + minutes;
	
	total_1 = total_1.split(":");

    var total1 = new Date(0, 0, 0, total_1[0], total_1[1], 0);
    var breakDate = new Date(0, 0, 0, brTime[0], brTime[1], 0);

    var total_hours = total1.getTime() - breakDate.getTime();


    var hours1 = Math.floor(total_hours / 1000 / 60 / 60);
    total_hours -= hours1 * 1000 * 60 * 60;
    var minutes1 = Math.floor(total_hours / 1000 / 60);


    var total_2 = (hours1 < 9 ? "0" : "") + hours1 + ":" + (minutes1 < 9 ? "0" : "") + minutes1;
    if(total_2 != 'NaN:NaN'){
        basic_uptime.text(total_2);
    }
	
}