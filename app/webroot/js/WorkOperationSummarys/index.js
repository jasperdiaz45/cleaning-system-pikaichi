
$(function() {
	setAjaxUrlBasePath('../');
	setCompanyNo($('#f-company-no').text());

	getSearchDivisionList('search-division-id', $('#f-divisiont-no').text());
	getSearchDepartmentList('search-department-id', 'search-division-id', $('#f-department-no').text());
	getSearchSectionList('search-section-id', 'search-department-id', $('#f-section-no').text());
});
