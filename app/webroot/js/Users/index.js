$(document).ready(function(){

	// search processing
	setAjaxUrlBasePath('../');
	setCompanyNo($('#f-company-no').text());
	getSearchDepartmentList('search-department-id', null, $('#f-department-no').text());
	getSearchSectionList('search-section-id', 'search-department-id', $('#f-section-no').text());

    //checkbox adjustmentAttendance
    var ckbox = $('#adjustAttendance');
    
    if(ckbox.val() == 1){
        $(".check_adjustment").attr('disabled', false);
    }
    else
    {
      $(".check_adjustment").attr('disabled', true);
    }
    
     $('input').ready(function () {
        if (ckbox.is(':checked')) {
            $(".check_adjustment").attr('disabled', false);
        } else {
            $(".check_adjustment").attr('disabled', true);
        }
        
    });  
   
   //Onleave Checkbox
    var ckbox2 = $('#on_leave');
     if(ckbox2.val() == 1){
         $(".check_leave").attr('disabled', false);
    }
    else
    {
        $(".check_leave").attr('disabled', true);
    }
    
      $('input').ready(function () {
        if (ckbox2.is(':checked')) {
            $(".check_leave").attr('disabled', false);
        } else {
            $(".check_leave").attr('disabled', true);
        }
        
    });
    
    //Retirement checkbox
    var ckbox3 = $('#retirement_checkbox');
    
     if(ckbox3.val() == 1){
          $(".check_retirement").attr('disabled', false);
    }
    else
    {
         $(".check_retirement").attr('disabled', true);
    }
    
      $('input').on('click',function () {
        if (ckbox3.is(':checked')) {
            // alert();
            $(".check_retirement").attr('disabled', false);
        } else {
            $(".check_retirement").attr('disabled', true);
        }
        
    });
    
     $('input').ready(function () {
        if (ckbox3.is(':checked')) {
            // alert();
            $(".check_retirement").attr('disabled', false);
        } else {
            $(".check_retirement").attr('disabled', true);
        }
        
    });
});
