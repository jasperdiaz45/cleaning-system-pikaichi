function addLeaveOfAbsences() {

	var addHtml = '<div class="col-md-11" style="margin-top: 10px;">';
	addHtml += $('#base-leave-of-absences').html();
	addHtml += '</div>';

	var idxMax = parseInt($("#leave-of-absences-idx-max").val()) + 1;
	$("#leave-of-absences-idx-max").val(idxMax);
	addHtml = addHtml.replace(/XXXX/g, idxMax);

    $('#leave-of-absences').append(addHtml);
    
    $('.datepicker').each(function() {
        $(this).datetimepicker({
    		//debug: true,
    		format: 'YYYY/MM/DD',
    		locale: 'ja',
    		//input領域をクリックでpickerを出す
    		allowInputToggle: true,
    		//ボタン表示
    		buttons: {
    			showToday: true,
    			showClear: true,
    			showClose: true
    		}
    	});
    });

//	$('#leave-of-absences').html(html);
}