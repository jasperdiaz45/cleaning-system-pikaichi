/**********************************************************
 *
 * main.js
 *
 ***********************************************************/
$(function() {

	$('.datepicker').datetimepicker({
		//debug: true,
		format: 'YYYY/MM/DD',
		locale: 'ja',
		//input領域をクリックでpickerを出す
		allowInputToggle: true,
		//ボタン表示
		buttons: {
			showToday: true,
			showClear: true,
			showClose: true
		},
	});

	$('.timepicker').datetimepicker({
		//debug: true,
		format: 'HH:mm',
		locale: 'ja',
		//input領域をクリックでpickerを出す
		allowInputToggle: true,
		//ボタン表示
		buttons: {
			showToday: true,
			showClear: true,
			showClose: true
		}
	});

	$('.dateandtimepicker').datetimepicker({
		//debug: true,
		format: 'YYYY/MM/DD HH:mm',
		locale: 'ja',
		//input領域をクリックでpickerを出す
		allowInputToggle: true,
		//ボタン表示
		buttons: {
			showToday: true,
			showClear: true,
			showClose: true
		}
	});

	$('header button.menuBtn').on('click',function(){
		$(".sidebar").slideToggle(500);
	});









/* モック */
//$("main").after("<a id='mock' href='index.html'>mock</a>");

});

