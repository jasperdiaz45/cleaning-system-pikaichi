// ajax url base path
var g_ajax_url_base_path = '';

var g_opportunity_dialog_page = 1;
var g_reporter_dialog_page = 1;

var g_befChkLeaveDate = -1;
var g_befChkLeaveTime = -1;
var g_befChkOperatingTime = -1;

$(function(){

	g_ajax_url_base_path = $("#ajax-url-base-path").text();

	// disable submission by enter key.
	$("input").keydown(disableEnter);

	// for opportunity dialog
	if ($('#opportunity-idx-max').length){
		makeAddOpportunityList();
		getOpportunitySelList(1);
	}
	
	// for reporter dialog
	if ($('#reporter-idx-max').length){
		getReporterSearchDepartmentList();
		makeAddReporterList();
		getReporterSelList(1);
	}
});

//
// disable submission by enter key.
//

function disableEnter(e) {

	if ((e.which && e.which === 13) || (e.keyCode && e.keyCode === 13)) {
		return false;
	} else {
		return true;
	}
}

//
// out error check
//

const OPERATION_STATUS_ID_NOW_WORK = 2;				// 勤務中
const OPERATION_STATUS_ID_BEF_WORK_FORGOT_OUT = 5;	// 出社前(退勤打刻忘れ)

function chkOutError() {
	
	$('#flashMessage').html('');
	
	var nowDate = new Date();
	var nowYear = nowDate.getFullYear();
	var nowMonth = ("00" + (nowDate.getMonth() + 1)).slice(-2);
	var nowDay = ("00" + nowDate.getDate()).slice(-2);
	var nowDspDate = nowYear + '/' + nowMonth + '/' + nowDay;
	
	var workDatetimes = '';
	var leaveDatetimes = '';
	if ($("#employment_date").val() != '' && $("#worktimes").val() != '') {
		workDatetimes = $("#employment_date").val() + ' ' + $("#worktimes").val();
	}
	if ($("#leavetimes").val() != '') leaveDatetimes = $("#leavetimes").val();	

	// check status
	if (
		(
			$("#operation-status-id").text() != OPERATION_STATUS_ID_NOW_WORK &&
			$("#operation-status-id").text() != OPERATION_STATUS_ID_BEF_WORK_FORGOT_OUT
		) ||
		(
			$("#employment-date").text() != nowDspDate
		)
    ) {
    	$("#msg-indicator").html('勤務中でないため退社できません');
		return false;
	}
	
	// check workTime
	if (!chkValidationWorkTime($('#worktimes').val())) return false;
	
	// check workDatetimes > leaveDatetimes
	if(
		$("#leavetimes").val() != '' && workDatetimes > leaveDatetimes
	) {
		$("#msg-indicator").html('時間終了は時間開始よりも大きくなければなりません。');
		return false;
	}

	// check reason_change_start_datetime.
	if (
		$("#worktimes").val() != $("#stamp_start_datetime").val() &&
		$("#reason_change_start_datetime").val() == ''
	) {
		$("#msg-indicator").html('出勤時刻の修正理由を選択してください');
		return false;
	}

	// check work content.
	if ($("#work-content").val() == '') {
		$("#msg-indicator").html('作業内容を入力してください');
		return false;
	}

	return true;
}

//
// validation check dailyreport value
//

function chkValidatioSsavedailyreportVal() {
	
	$('#flashMessage').html('');
	
	if (!chkValidatioTmpsaveVal()) return false;
	
	// check work content.
	if ($("#work-content").val() == '') {
		$("#msg-indicator").html('作業内容を入力してください');
		return false;
	}
	
	return true;
}

//
// validation check tmpsave value
//

function chkValidatioTmpsaveVal() {
	
	$('#flashMessage').html('');

	var workTime = $('#worktimes').val();
	var leaveDate = $('#leave-date').val();
	var leaveTime = $('#leave-time').val();
	
	if (!chkValidationWorkTime(workTime)) return false;
	if (!chkValidationLeaveDate(leaveDate)) return false;
	if (!chkValidationLeaveTime(leaveTime)) return false;

	return true;
}

//
// validation check workTime
//

function chkValidationWorkTime(workTime) {

	if (workTime == '') {
		$("#msg-indicator").html('出勤時刻を入力してください');
		return false;
	}

	if (!workTime.match(/^[0-9]{2}:[0-9]{2}$/)) {
		alert('出勤時刻は HH:mm の書式で入力してください');
		return false;
	}
	
	return true;
}

//
// validation check leave_date
//

function chkValidationLeaveDate (leaveDate, targetElm = null) {

	if (leaveDate == '') return true;

	var result = true;

	if(!leaveDate.match(/^[0-9]{4}\/[0-9]{2}\/[0-9]{2}$/)) {
		if (leaveDate != g_befChkLeaveDate || targetElm == null) {
			alert('退勤日は YYYY/MM/DD の書式で入力してください');
			result = false;
		}
		if (targetElm != null) targetElm.focus();
	}
	g_befChkLeaveDate = leaveDate;
	
	return result;
}

//
// validation check leave_time
//

function chkValidationLeaveTime(leaveTime, targetElm = null) {

	if (leaveTime == '') return true;

	var result = true;

	if(!leaveTime.match(/^[0-9]{2}:[0-9]{2}$/)) {
		if (leaveTime != g_befChkLeaveTime || targetElm == null) {
			alert('退勤時刻は HH:mm の書式で入力してください');
			result = false;
		}
		if (targetElm != null) targetElm.focus();
	}
	g_befChkLeaveTime = leaveTime;
	
	return result;
}

//
// Validation check of business operating time by opportunity.
// (Up to two decimal places)
//
//

function chkValidationOpportunityOperatingTime(operatingTime, targetElm) {

	if (operatingTime == '' || operatingTime == 0) return;

	if(!operatingTime.match(/^([0-9]+\.[0-9]{1,2}|[0-9]+)$/)) {
		if (operatingTime != g_befChkOperatingTime) {
			alert('数字で入力してください');
		}
		targetElm.focus();
	}
	g_befChkOperatingTime = operatingTime;
}

//
// for opportunity dialog
//

function makeAddOpportunityList() {
	
	var operationId = $("#operation-id").text();
	if (operationId == '') return;

	$.ajax({
		url: g_ajax_url_base_path + "get_opportunity_add_list",
		async: false,
		type: "POST",
		dataType: "JSON",
		data: {
			'operationId' : operationId
		},
		success: function (data) {
			for (var idx = 0; idx < data.length; idx++) {
				
				if (data[idx].Product.name == null) data[idx].Product.name = '';
				if (data[idx].BusinessDescriptionByOpportunity.operating_time == null) data[idx].BusinessDescriptionByOpportunity.operating_time = '';
				
				addOpportunity(
					data[idx].BusinessDescriptionByOpportunity.project_no,
					data[idx].Opportunity.name,
					data[idx].BusinessDescriptionByOpportunity.parent_project_no,
					data[idx].Product.name,
					data[idx].BusinessDescriptionByOpportunity.operating_time
				);
			}
		}
	});
}

//List Box Department
function getOpportunitySearchClientDepartmentList() {

	$.ajax({
		url: g_ajax_url_base_path + "get_opportunity_search_client_department_list",
		async: false,
		type: "POST",
		dataType: "JSON",
		success: function (data) {

			var client_department_list = '';
			for (var idx = 0; idx < data.length; idx++) {
				client_department_list += '<option value="' + data[idx].id + '">' + data[idx].name + '</option>';
			}
	
			$('#opportunity-search-clientDepartment-id').html(client_department_list);
		}
	});
}

function getOpportunitySelList(page = null) {

	if (page != null) g_opportunity_dialog_page = page;
	
	var search_clientDepartment_id = $('#opportunity-search-clientDepartment-id').val();
	var search_clientDepartment_name = $('#opportunity-search-clientDepartment-name').val();
	var search_proposal_id = $('#opportunity-search-proposal-id').val();
	var search_proposal_name = $('#opportunity-search-proposal-name').val();
	var search_opportunity_id = $('#opportunity-search-opportunity-id').val();
	var search_opportunity_name = $('#opportunity-search-opportunity-name').val();
	var search_opportunity_sfaid = $('#opportunity-search-opportunity-sfaid').val();

	var count = 0;
	var limit = 0;

	// make ignore opportunity id list 

	var ignore_opportunity_id_list = '';
	var opportunity_idx_max = parseInt(document.getElementById("opportunity-idx-max").defaultValue);
	for (var idx = 1; idx <= opportunity_idx_max; idx++) {
		var elem_str = '#project-id-input-idx-' + idx;
		if ($(elem_str).length) {
			if (ignore_opportunity_id_list != '') ignore_opportunity_id_list += ',';
			ignore_opportunity_id_list += $(elem_str).val();
		}
	}

	// pagination

	$.ajax({
		url: g_ajax_url_base_path + "get_opportunity_sel_page_info",
		async: false,
		type: "POST",
		dataType: "JSON",
		data: {
			'clientDepartment_id' : search_clientDepartment_id,
			'clientDepartment_name' : search_clientDepartment_name,
			'proposal_id' : search_proposal_id,
			'proposal_name' : search_proposal_name,
			'opportunity_id' : search_opportunity_id,
			'opportunity_name' : search_opportunity_name,
			'opportunity_sfaid' : search_opportunity_sfaid,
			'ignore_opportunity_id_list' : ignore_opportunity_id_list
		},

		success: function (data) {
			count = data.count;
			limit = data.limit;
		}
	});

	var page_max = Math.ceil(count / limit);
	if (g_opportunity_dialog_page > page_max) g_opportunity_dialog_page = page_max;
	var start_idx = (g_opportunity_dialog_page - 1) * limit + 1;
	var end_idx = g_opportunity_dialog_page * limit;
	if (start_idx > count) start_idx = count;
	if (start_idx < 0) start_idx = 0;
	if (end_idx > count) end_idx = count;

	var opportunity_pagination = '<li><a>全' + count + '件｜' + start_idx + '〜' + end_idx + '件表示｜</a></li>'
	for (var idx = 1; idx <= page_max; idx++) {
		var selected = idx == g_opportunity_dialog_page ? ' style="background-color: #90A4AD; border: 1px solid #90A4AD;"' : ' style="background-color: #DDE1E3; border: 1px solid #DDE1E3;"';
		opportunity_pagination += '<li><a href=""' + selected + ' onclick="getOpportunitySelList(' + idx + '); return false;">' + idx + '</a></li>'
	}

	$('#opportunity-pagination').html(opportunity_pagination);

	// select list
	
	$.ajax({
		url: g_ajax_url_base_path + "get_opportunity_sel_list",
		async: false,
		type: "POST",
		dataType: "JSON",
		data: {
			'page' : g_opportunity_dialog_page,
			'clientDepartment_id' : search_clientDepartment_id,
			'clientDepartment_name' : search_clientDepartment_name,
			'proposal_id' : search_proposal_id,
			'proposal_name' : search_proposal_name,
			'opportunity_id' : search_opportunity_id,
			'opportunity_name' : search_opportunity_name,
			'opportunity_sfaid' : search_opportunity_sfaid,
			'ignore_opportunity_id_list' : ignore_opportunity_id_list
		},
		success: function (data) {

			var opportunity_sel_list = '';
			for (var idx = 0; idx < data.length; idx++) {

				var parentProjectId = data[idx].Proposal.id;

                if (data[idx].ClientDepartment.id == null) data[idx].ClientDepartment.id = '';
                if (data[idx].ClientDepartment.name == null) data[idx].ClientDepartment.name = '';
                if (data[idx].Proposal.id == null) data[idx].Proposal.id = '';
                if (data[idx].Proposal.name == null) data[idx].Proposal.name = '';
                if (data[idx].Product.name == null) data[idx].Product.name = '';
                if (data[idx].Opportunity.sfaid == null) data[idx].Opportunity.sfaid = '';

				opportunity_sel_list += '<tr>';
                opportunity_sel_list += '<td>' + data[idx].ClientDepartment.id + '</td>';
                opportunity_sel_list += '<td>' + data[idx].ClientDepartment.name + '</td>';
                opportunity_sel_list += '<td>' + data[idx].Proposal.id + '</td>';
                opportunity_sel_list += '<td>' + data[idx].Proposal.name + '</td>';
                opportunity_sel_list += '<td>' + data[idx].Opportunity.id + '</td>';
                opportunity_sel_list += '<td>' + data[idx].Opportunity.name + '</td>';
                opportunity_sel_list += '<td>' + data[idx].Product.name + '</td>';
                opportunity_sel_list += '<td>' + data[idx].Opportunity.sfaid + '</td>';
                opportunity_sel_list += '<td>';
			    opportunity_sel_list += '<a class="btn btn-blue btn-select"';
			    opportunity_sel_list += ' onclick="addOpportunityFromDialog(' + data[idx].Opportunity.id + ',\'' + data[idx].Opportunity.name + '\', ' + parentProjectId + ', \'' + data[idx].Product.name + '\'); return false;"';
			    opportunity_sel_list += '>選択</a>';
			    opportunity_sel_list += '</td>';
			    opportunity_sel_list += '</tr>';
			}

			$('#opportunity-sel-list').html(opportunity_sel_list);
		}
	});
}

function addOpportunity(projectId, projectName, parentProjectId, productName, operatingTime = '') {
		
	var opportunity_idx_max = parseInt(document.getElementById("opportunity-idx-max").defaultValue) + 1;
	document.getElementById("opportunity-idx-max").defaultValue = opportunity_idx_max;
	var inputId = 'opportunity-value-' + opportunity_idx_max;

	// add reporter
	var opportunity_list = $('#opportunity-list').html();
	
	opportunity_list += '<tr id="opportunity-idx-' + opportunity_idx_max + '">';
	opportunity_list += '<td>' + projectName + ' ' + productName + '</td>';
	opportunity_list += '<td>';
	opportunity_list += '<input type="hidden" name="data[project_id_' + opportunity_idx_max + ']" value="' + projectId + '" class="form-control" id="project-id-input-idx-' + opportunity_idx_max + '">';
	opportunity_list += '<input type="hidden" name="data[parent_project_id_' + opportunity_idx_max + ']" value="' + parentProjectId + '" class="form-control" id="parent-project-id-input-idx-' + opportunity_idx_max + '">';
	opportunity_list += '<div class="input-box"><input name="data[operating_time_' + opportunity_idx_max + ']" type="text" id="' + inputId + '" class="" placeholder="" onBlur="chkValidationOpportunityOperatingTime($\(\'#' + inputId + '\').val(), $\(\'#' + inputId + '\'));" value="' + operatingTime + '"></div>';
	opportunity_list += '</td>';
	opportunity_list += '<td><p>小数点二位まで入力可(例：15分→0.25）</p></td>';
	opportunity_list += '<td><a class="deleteBtn" onclick="delOpportunity(' + opportunity_idx_max + ');"><i class="fas fa-times-circle"></i></a></td>';
	opportunity_list += '</tr>';

	$('#opportunity-list').html(opportunity_list);

	$("#" + inputId).keydown(disableEnter); // disable submission by enter key.
}	
	
function addOpportunityFromDialog(projectId, projectName, parentProjectId, productName) {
	
	addOpportunity(projectId, projectName, parentProjectId, productName);

	// reload list
	getOpportunitySelList();

	// own closing
	$('body').removeClass('modal-open');
	$('.modal-backdrop').remove();
	$('#addBusinessTalkModal').modal('hide');
}
	
function delOpportunity(idx) {
	
	$('#opportunity-idx-' + idx).remove();

	getOpportunitySelList();	// reload list
}

//
// for reporter dialog
//

function getReporterSearchDepartmentList() {
	
	var company_no = $('#company-no').text();

	$.ajax({
		url: g_ajax_url_base_path + "get_reporter_search_department_list",
		async: false,
		type: "POST",
		dataType: "JSON",
		data: {
			'company_no' : company_no,
		},
		success: function (data) {

			var department_list = '';
			for (var idx = 0; idx < data.length; idx++) {
				department_list += '<option value="' + data[idx].id + '">' + data[idx].name + '</option>';
			}
	
			$('#reporter-search-department-id').html(department_list);
		}
	});
}

function getReporterSearchSectionList() {

	var department_id = $('#reporter-search-department-id').val();

	$.ajax({
		url: "/Inputs/get_reporter_search_section_list",
		async: false,
		type: "POST",
		dataType: "JSON",
		data: {
			'department_id' : department_id,
		},
		success: function (data) {

			var section_list = '';
			for (var idx = 0; idx < data.length; idx++) {
				section_list += '<option value="' + data[idx].id + '">' + data[idx].name + '</option>';
			}
	
			$('#reporter-search-section-id').html(section_list);
		}
	});
}

function makeAddReporterList() {
	
	var dailyReportId = $("#daily-report-id").text();
	if (dailyReportId == '') return;

	$.ajax({
		url: g_ajax_url_base_path + "get_reporter_add_list",
		async: false,
		type: "POST",
		dataType: "JSON",
		data: {
			'dailyReportId' : dailyReportId
		},
		success: function (data) {
			for (var idx = 0; idx < data.length; idx++) {
				addReporter(
					data[idx].Employee.id,
					data[idx].Employee.name
				);
			}
		}
	});
}

function getReporterSelList(page = null) {

	if (page != null) g_reporter_dialog_page = page;
	
	var search_department_id = $('#reporter-search-department-id').val();
	var search_section_id = $('#reporter-search-section-id').val();
	var search_name = $('#reporter-search-name').val();

	var count = 0;
	var limit = 0;

	// make ignore reporter id list 

	var ignore_repoter_id_list = '';
	var reporter_idx_max = parseInt(document.getElementById("reporter-idx-max").defaultValue);
	for (var idx = 1; idx <= reporter_idx_max; idx++) {
		var elem_str = '#reporter-input-idx-' + idx;
		if ($(elem_str).length) {
			if (ignore_repoter_id_list != '') ignore_repoter_id_list += ',';
			ignore_repoter_id_list += $(elem_str).val();
		}
	}

	// pagination

	$.ajax({
		url: g_ajax_url_base_path + "get_reporter_sel_page_info",
		async: false,
		type: "POST",
		dataType: "JSON",
		data: {
			'department_id' : search_department_id,
			'section_id' : search_section_id,
			'employee_name' : search_name,
			'ignore_repoter_id_list' : ignore_repoter_id_list
		},

		success: function (data) {
			count = data.count;
			limit = data.limit;
		}
	});

	var page_max = Math.ceil(count / limit);
	if (g_reporter_dialog_page > page_max) g_reporter_dialog_page = page_max;
	var start_idx = (g_reporter_dialog_page - 1) * limit + 1;
	var end_idx = g_reporter_dialog_page * limit;
	if (start_idx > count) start_idx = count;
	if (start_idx < 0) start_idx = 0;
	if (end_idx > count) end_idx = count;

	var reporter_pagination = '<li><a>全' + count + '件｜' + start_idx + '〜' + end_idx + '件表示｜</a></li>'
	for (var idx = 1; idx <= page_max; idx++) {
		var selected = idx == g_reporter_dialog_page ? ' style="background-color: #90A4AD; border: 1px solid #90A4AD;"' : ' style="background-color: #DDE1E3; border: 1px solid #DDE1E3;"';
		reporter_pagination += '<li><a href=""' + selected + ' onclick="getReporterSelList(' + idx + '); return false;">' + idx + '</a></li>'
	}

	$('#reporter-pagination').html(reporter_pagination);

	// select list
	
	$.ajax({
		url: g_ajax_url_base_path + "get_reporter_sel_list",
		async: false,
		type: "POST",
		dataType: "JSON",
		data: {
			'page' : g_reporter_dialog_page,
			'department_id' : search_department_id,
			'section_id' : search_section_id,
			'employee_name' : search_name,
			'ignore_repoter_id_list' : ignore_repoter_id_list
		},
		success: function (data) {
			var reporter_sel_list = '';
			for (var idx = 0; idx < data.length; idx++) {
				reporter_sel_list += '<tr>';
			    reporter_sel_list += '<td>' + data[idx].Department.name + '</td>';
			    reporter_sel_list += '<td>' + data[idx].Section.name + '</td>';
			    reporter_sel_list += '<td>' + data[idx].Employee.name + '</td>';
			    reporter_sel_list += '<td>';
			    reporter_sel_list += '<a class="btn btn-blue btn-select"';
			    reporter_sel_list += ' onclick="addReporterFromDialog(' + data[idx].Employee.id + ',\'' + data[idx].Employee.name + '\'); return false;"';
			    reporter_sel_list += '>選択</a>';
			    reporter_sel_list += '</td>';
			    reporter_sel_list += '</tr>';
			}

			$('#reporter-sel-list').html(reporter_sel_list);
		}
	});
}

function addReporter(id, name) {
		
	var reporter_idx_max = parseInt(document.getElementById("reporter-idx-max").defaultValue) + 1;
	document.getElementById("reporter-idx-max").defaultValue = reporter_idx_max;

	// add reporter
	var reporter_list = $('#reporter-list').html();
		
	reporter_list += '<tr id="reporter-idx-' + reporter_idx_max + '">';
	reporter_list += '<td><label>報告先</label></td>';
	reporter_list += '<td>';
	reporter_list += '<input type="hidden" name="data[reporter_' + reporter_idx_max + ']" value="' + id + '" class="form-control" id="reporter-input-idx-' + reporter_idx_max + '">';
	reporter_list += name;
	reporter_list += '</td>';
	reporter_list += '<td><a class="deleteBtn" onclick="delReporter(' + reporter_idx_max + ');"><i class="fas fa-times-circle"></i></a></td>';
	reporter_list += '</tr>';

	$('#reporter-list').html(reporter_list);
}
	
function addReporterFromDialog(id, name) {
	
	addReporter(id, name);

	// reload list
	getReporterSelList();

	// own closing
	$('body').removeClass('modal-open');
	$('.modal-backdrop').remove();
	$('#addReportDestinationModal').modal('hide');
}
	
function delReporter(idx) {
	
	$('#reporter-idx-' + idx).remove();

	getReporterSelList();	// reload list
}
