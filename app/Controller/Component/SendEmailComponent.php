<?php
App::uses('Component', 'Controller');
App::uses( 'CakeEmail', 'Network/Email');

/**
 * メール送信
 */
class SendEmailComponent extends Component {
	
	/**
	 * メールの設定情報を取得する
	 */
	private function getMailSetting(){
		$Email = new CakeEmail();
		
		// Config ファイルの値によって切替
		$mailSetting = Configure::read('mailSettingProduction');
		if ($mailSetting == 'true') {
			$this->isProduction = true;
		} else {
			$this->isProduction = false;
		}

		if ($this->isProduction) {
			// 本番環境
			$Email->config(array(
				'from'=>array('sample@test.com'=>'日報登録ツール インフォメーション'),
				'host' => 'mail.test.com',
				'port' =>  '587',
				'username' => 'sample@test.com',
				'password' => 'sample',
				'transport' => 'Smtp',
			));
		} else {
			// 開発環境
			$Email->config(array(
				'from'=>array('dev.yakumo@gmail.com'=>'日報登録ツール インフォメーション'),
				'host' => 'ssl://smtp.gmail.com',
				'port' =>  '465',
				'username' => 'dev.yakumo@gmail.com',
				'password' => 'devyakumo.1',
				'transport' => 'Smtp',
			));
		}
		
		return $Email;
	}
	
	/**
	 * 日報報告メール
	 */
	public function sendWorkReport($toMail, $params){
		$paramKeys = array('company_name','first_name', 'work_date', 'last_name', 'matter_info', 'report', 'schedule', 'other', 'manager');
		$title = "稼働報告【%company_name% %last_name% %first_name%】";
		$content = "%last_name% %first_name%　%work_date%
%matter_info%

作業報告：
%report% 

作業予定：
%schedule% 

その他(伝達,課題など)：
%other% 

%manager%";

		$params = $this->parseParams($paramKeys, $params);
		return $this->sendMail($toMail, $content, $title, $params);
	}
	
	/**
	 * サマリー自動送信メール
	 */
	public function sendSummaryReport($toMail, $workMatters, $target) {
		$title = "稼働報告【案件別サマリー】";
		
		try{
			$email = $this->getMailSetting();
			$email->subject($title);
			$email->emailFormat('html');
			$email->template('summary','default');
			$email->viewVars(compact('target', 'workMatters', 'title'));
			
			if (!empty($toMail)) {
				// 送信先を設定
				$email->to($toMail);
				$email->send();
			}
		} catch(Exception $e) {
			//$this->log($e->getMessage());
			return $e->getMessage();
		}
		
		return '';
	}
	
	/**
	 * テストメール
	 */
	public function sendTest($toMail, $params){
		$paramKeys = array('name');

		$content = "テストメール\r\n%name%\r\n";
		$title = 'テストメール';
		$params = $this->parseParams($paramKeys, $params);
		return $this->sendMail($toMail, $content, $title, $params);
	}
	
	/**
	 * メールを送信する
	 */
	private function sendMail($toMail,$content,$title,$params,$bccSend = false){
		$content = $this->replaseBody($content, $params);
		$title = $this->replaseBody($title, $params);
		try{
			$email = $this->getMailSetting();
			$email->subject($title);
			$email->emailFormat('text');
			$email->template('default');
			$email->viewVars(compact('content'));
			
			$bccArray = array_keys($email->from());
	
			if (!empty($bccArray) && !empty($bccSend) && !empty($toMail)) {
				$email->bcc($bccArray);
			}
						
			if (!empty($toMail)) {
				// 送信先を設定
				$email->to($toMail);
				$email->send();
			} else if(!empty($bccArray) && !empty($bccSend)){
				$email->to($bccArray[0]);
				$email->send();				
			}
		} catch(Exception $e) {
			//debug($e->getMessage());
			$this->log($e->getMessage());
			return false;
		}
		
		return true;
	}

	/**
	 * 配列のキーを置換一覧に合わせる
	 */
	private function parseParams($paramsKeys, $params){
		$result = array();
		foreach($paramsKeys as $key){
			$result[$key] = key_exists($key, $params) ? $params[$key] : '';
		}
		return $result;
	}

	/**
	 * paramsのKeyをValueに置換する
	 */
	private function replaseBody($body, $params){
		$patterns = array();
		$replacements = array();
		foreach($params as $key=>$value){
			$patterns[] = '%'.$key.'%';
			$replacements[] = $value;
		}
		return str_replace($patterns, $replacements, $body);
	}
	
	
	// Added by developer from bnj
	public function sendEmail($sender_name,$from,$to,$message,$subject){
	
		$Email = new CakeEmail();
		$Email->config('gmail');
		$Email->from(array($from => ''.$sender_name.''));
		$Email->to($to);
		$Email->subject($subject);
		$result = $Email->send($message);
		// If
		if (count($result) > 0) {
			// die($user_email);
			return true;
		}else{
			return false;
		}
	}
	
}