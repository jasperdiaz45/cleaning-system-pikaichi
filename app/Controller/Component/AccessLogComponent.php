<?php
App::uses('Component', 'Controller');

/**
 * アクセスログ
 *
 * 出力例
 *   2017-05-12 16:21:38 AccessLog: System: Admin, IP: 10.240.0.240, ID: , Method: GET, URL: /admin/admin_users/login, PostData: ,
 *   2017-05-12 16:28:21 AccessLog: System: Admin, IP: 10.240.0.240, ID: , Method: POST, URL: /admin/admin_users/login, PostData: {"_method":"POST","data":{"AdminUser":{"mail":"*****************","password":"","autologin":"1"}}},
 *   2017-05-12 16:29:27 AccessLog: System: Admin, IP: 10.240.0.214, ID: 1, Method: GET, URL: /admin/conferences, PostData: ,
 *
 * コンポーネントの設定
 *   $systemName: システム名。AppControllerの$componentsで設定される。
 *   $secretDataKey: ログ出力をしないkeyの一覧
 *   $asteriskDataKey: *に変換して出力をするkeyの一覧
 *   settingCakeLogConfig(): ログ出力の設定
 *   writeLog(): ログ出力処理
 *
 * 設定
 *   AppController
 *      $componentsの配列の中に以下のように入れる。
 *      Authコンポーネントの後に記述すること。
 *      'AccessLog' => array('systemName' => 'Admin'),
 *   Config
 *      writeAccsessLog が'false'の時はログ出力をしない。
 *      Configure::write('writeAccsessLog', 'false');
 *
 */
class AccessLogComponent extends Component {

	public $systemName = "";

	private $secretDataKey = array('password', 'password_confirm', 'new_password', 'new_password_confirm');

	private $asteriskDataKey = array('email', 'last_name', 'first_name');


	//beforeFileterの後に実行される
	public function startup(Controller $controller){
		$this->controller = $controller;

		$this->settingCakeLogConfig();
		$this->writeLog();
	}

	/**
	 * アクセスログファイルの設定
	 */
	private function settingCakeLogConfig(){
		CakeLog::config('accessLog', array(
			'engine' => 'FileLog',
			'types' => array('accessLog'),
			'file' => 'accessLog_'.date( 'Ymd'),
			'size' => false
		));
	}

	/**
	 * logの出力
	 */
	private function writeLog(){

		$url = Router::reverse($this->controller->request);
		$url = urldecode($url);

		$log = '';

		$log .= $this->getColumn('System', $this->systemName);
		$log .= $this->getColumn('IP', $_SERVER["REMOTE_ADDR"]);
		$log .= $this->getColumn('ID', $this->controller->Auth->user('id'));
		$log .= $this->getColumn('Method', $this->controller->request->method());
		$log .= $this->getColumn('URL', $url);
		$log .= $this->getColumn('PostData',  $this->getPostData());

		$isLog = Configure::read("writeAccsessLog");
		if($isLog != 'false'){
			$this->log($log, 'accessLog');
		}
	}

	/**
	 * keyとvalueの文字列作成
	 */
	private function getColumn($key, $value){
		$str = $value .', ';
		return $str;
	}

	/**
	 * POSTデータの文字列作成。
	 * 個人情報などは置換処理を行う。
	 */
	private function getPostData(){
		if ($this->controller->request->is(array('post', 'put')) == false) {
			return '';
		}
		$data = $_POST;
		if(is_array($data)){
			$data = $this->replaceArrayData($data);
		}
		return json_encode($data, JSON_UNESCAPED_UNICODE);
	}

	/**
	 * POSTデータの配列を再帰的に置換処理
	 */
	private function replaceArrayData($array, $parentKeys = array()) {
		$resultArr = array();
		foreach ($array as $key => $value) {
			$newParentKeys = $parentKeys;
			$newParentKeys[] = $key;

			if (is_array($value)) {
				$value = $this->replaceArrayData($value, $newParentKeys);
			} else {
				$value = $this->replaceValueData($newParentKeys, $value);
			}
			$resultArr[$key] = $value;
		}
		return $resultArr;
	}

	/**
	 * POSTデータの置換処理
	 */
	private function replaceValueData($parentKeys, $value) {

		foreach ($parentKeys as $parentKey) {
			if(in_array($parentKey, $this->secretDataKey, true)){
				return '';
			}

			if(in_array($parentKey, $this->asteriskDataKey, true)){
				return str_repeat('*', strlen($value));
			}
		}
		return $value;
	}

}