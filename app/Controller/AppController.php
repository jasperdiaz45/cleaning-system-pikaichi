<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');
App::import('Vendor', 'accesslogs');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

	public $helpers = array('Version');

	public $components = array(
		//'Search.Prg',
		'Paginator',
		'Session',
		'Flash',
		'Cookie',
		//'DebugKit.Toolbar',
		'Auth' => array(
			// 認証時の設定
			'authenticate' => array(
				'Form' => array(
					// 認証時に使用するモデル
					'userModel' => 'Employee',
					// 認証時に使用するモデルのユーザ名とパスワードの対象カラム
					'fields' => array('username' => 'mail_address' , 'password'=>'password'),
					'scope' => array('Employee.retirement' => '0', 'Employee.effectiveness' => '1'),
				),
			),
			// ログインに使用するアクションを指定
			'loginAction' => '/users/login',
			// ログイン後のリダイレクト先を指定
			'loginRedirect' => '/Routes/index',
			// ログアウト後のリダイレクト先を指定
			'logoutRedirect' => '/users/login',
			
		),
		'AccessLog' => array('systemName' => 'WorkAdmin'),
	);
	
	public function beforeFilter() {
		parent::beforeFilter();
		AuthComponent::$sessionKey = "Auth.AdminUser";

		//未ログインでアクセスできるアクションを指定
		//これ以外のアクションへのアクセスはloginにリダイレクトされる規約になっている
		$this->Auth->allow('login','change_password');
		$user = $this -> Auth -> user();
		$isLoggedIn =  $this -> Auth -> loggedIn();
		$siteURL = Router::url('/');
		$this->set(compact('employee', 'isLoggedIn', 'siteURL'));
		$this -> layout = 'main';
		
		$this->set('auth',$this->Auth);
		
		$this->paginate=array(
			'limit'=> 20,
		);
		
		// アクセスログ
    	$this->writeAccessLog();
	}

	/**
	 * 権限チェック
	 */
	protected function checkAuthority($function) {
		$role = $this->Auth->user('authority_no');
		$authority = Configure::read('authority.authorityNo'.$role.'.'.$function);
		if($authority === null){
			throw new InternalErrorException('not find authority');
		}
		return $authority;
	}

	/*
	 * アクセスログ
	 */
	protected function writeAccessLog() {

		if(ACCESS_LOG_ENABLE && $this->request->is('post')) {

			$userId = $this->Auth->user('id');
			ob_start();
			var_dump($this->request->data);
			$requestData = ob_get_clean();
	
		 	$msg = $userId . "," . date('Y-m-d H:i:s') . "," . env('REQUEST_URI') . "," . env('HTTP_USER_AGENT') . "\n";
			$msg .= $requestData . "\n";
	 
	    	$filename = ACCESS_LOG_OUT_DIR . date('Ymd') . '.log';
	    	$log = new File($filename);
		    $log->append($msg);

			// delete old files.		    
		    $deletDay = date('Ymd',strtotime("- " . ACCESS_LOG_DAYS_LEFT . " day"));
		    $dir = opendir(ACCESS_LOG_OUT_DIR);
			while (false !== ($file = readdir($dir))){
			  if($file[0] != "."){
			    if ($deletDay > $file) {
			      unlink(ACCESS_LOG_OUT_DIR . $file);
			    }
			  }
			}
			closedir($dir);
		}
	}
}
