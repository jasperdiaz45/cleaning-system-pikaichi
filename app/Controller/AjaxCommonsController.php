<?php
App::uses('AppController', 'Controller');

class AjaxCommonsController extends AppController {

	var $uses = array('Input');

	public function get_search_division_list() {
		
		$this->autoRender = false;
		$this->response->type('json');
		
		$this->Input->Behaviors->load('Master');
		$companyNo = $this->request->data['company_no'];
		$parentId = $this->request->data['parent_id'];
        $searchList = $this->Input->getDivissionList($parentId, $companyNo);

        $idNameList = array();
        $idx = 0;
        foreach($searchList as $key => $value) {
        	$idNameList[$idx++] = array('id' => $key, 'name' => $value);
        }

		return json_encode($idNameList);
	}

	public function get_search_department_list() {
		
		$this->autoRender = false;
		$this->response->type('json');

		$this->Input->Behaviors->load('Master');
		$companyNo = $this->request->data['company_no'];
		$parentId = $this->request->data['parent_id'];
		$ignoreIdList = $this->request->data['ignore_id_list'];
		if ($ignoreIdList != '') $ignoreIdList = explode(',', $this->request->data['ignore_id_list']);
		if ($parentId != '') {
	        $searchList = $this->Input->getDepartmentListIgnoreId($parentId, null, $ignoreIdList);
		} else {
	        $searchList = $this->Input->getDepartmentListIgnoreId($parentId, $companyNo, $ignoreIdList);
		}

        $idNameList = array();
        $idx = 0;
        foreach($searchList as $key => $value) {
        	$idNameList[$idx++] = array('id' => $key, 'name' => $value);
        }

		return json_encode($idNameList);
	}

	public function get_search_section_list() {
		
		$this->autoRender = false;
		$this->response->type('json');
		
		$this->Input->Behaviors->load('Master');
		$parentId = $this->request->data['parent_id'];
        $searchList = $this->Input->getSectionList($parentId);

        $idNameList = array();
        $idx = 0;
        foreach ($searchList as $key => $value) {
        	$idNameList[$idx++] = array('id' => $key, 'name' => $value);
        }

		return json_encode($idNameList);
	}
}
