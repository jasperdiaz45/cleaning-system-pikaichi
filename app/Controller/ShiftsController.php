<?php
App::import('Vendor',  'IOFactory', array('file' => 'PHPExcel/IOFactory.php'));
App::import("Vendor", "phpqrcode/qrlib");
App::uses('AppController', 'Controller');
App::uses('TableRegistry', 'ORM');
App::uses('CakeTime', 'Utility');


/**		$operationMonthInfo['days_remaining_on_holiday'] = floor($vacations[0]['Vacation']['days_remaining_on_holiday'] * 10) / 10;
		$operationMonthInfo['days_remaining_on_holiday_time'] = floor($vacations[0]['Vacation']['time_off']);
		$operationMonthInfo['havingCompensationDayoff'] = $vacations[0]['Vacation']['compensation_day_off'];

 * WorkReports Controller
 *
 * @property WorkReport $WorkReport
 * @property WorkMatter $WorkMatter
 * @property Matter $Matter
 * @property User $User
 * @property FlashComponent $Flash
 */
class ShiftsController extends AppController {


	var $uses = array('Shifts','Operation','Employee');
	
	public function qrcode($text) {
		// $host = Router::url('/', true);
		$host = "http://192.168.1.169/Pikaichi/environment/";
		$server = $host.'img/qrCode/';

		// Don't render a view file
	    $this->autoRender = false; 
      	$file_path = WWW_ROOT .'img'.DS.'qrCode'.DS;
      	$file_name = uniqid().".png";

  		$new_text = $server.$file_name;

	    $file = $file_path.$file_name;
	    // Tell the borwser to download the file (slug it, too)
	    // $this->response->download(Inflector::slug($new_text) . '.png');
	    // Bombs away
	    QRcode::png($new_text, $file, 'L', 10);

	    $url = $file_name;
	    $this->set(compact('url'));
	    return $url;
	    // return $file;
	    // return ;
	}


	public function index() {
		// 一覧
		$server = Router::url('/', true);

		$this->set(compact('server'));

		$title = 'シフト登録';
		$this->set(compact('title'));
		$yearMonth='';
		$relation_emp_no='';
		$userId='';
		$this->Shifts->Behaviors->load('Master');
		$day_format = $this->Shifts->jpDateNameMst();
		if($this->request->is('post')) {
			$yearMonth=$this->request->data['Shifts']['work_date'];
			$YmdAarray = $this->Shifts->getBusinessYmd($yearMonth);
			$year = date('Y',strtotime($YmdAarray[0]));
			$month = date('m',strtotime($YmdAarray[0]));
			$relation_emp_no = (!empty($this->request->data['Shifts']['search']))?$this->request->data['Shifts']['search']:'';
			$employee = $this->Employee->find('all',array('conditions'=>array('relation_emp_no'=>$relation_emp_no)));
			$userId = (!empty($employee)) ? $employee[0]['Employee']['id']:'';
			$employeeName = (!empty($userId)) ? $this->Shifts->getEmployeeNoNameForId($userId) :'';
			if(array_key_exists('update',$this->request->data)){
				$cond = array(array('employee_no' => $userId),array('date >='=>$year.'-'.$month.'-01'),array('date <='=>$year.'-'.$month.'-31'));
				$this->Shifts->deleteAll(array($cond,false));
				$shifts = $this->request->data['Shifts'];
				unset($shifts['search']);
				unset($shifts['work_date']);
				$count=count($shifts);
				for($i=1;$i<=$count;$i++){
					$date = $shifts[$i]['empDate'];
					$start_time=($shifts[$i]['start_time']!='') ? date('h:i',strtotime($shifts[$i]['start_time'])) : '';
	    			$closing_time=($shifts[$i]['closing_time']!='') ? date('H:i',strtotime($shifts[$i]['closing_time'])) : '';
	    			$break_deduction=($shifts[$i]['break_deduction']!='') ? $this->Shifts->time_to_decimal($shifts[$i]['break_deduction']) : '';
	    		
	    			if($start_time !='' && $closing_time != '' && $break_deduction != ''){
		    			$break_deduction=$this->Shifts->time_to_decimal($shifts[$i]['break_deduction']);
						$basic_uptime = $this->Shifts->getBasicUptime($start_time,$closing_time,$break_deduction);
						$basic_uptime = $this->Shifts->time_to_decimal($basic_uptime);
						$data= array('employee_no'=>$userId,'date'=>$date,'start_time'=>$start_time,'closing_time'=>$closing_time,'basic_uptime'=>round($basic_uptime,2),'break_deduction'=>$break_deduction);
	    				$this->Shifts->saveAll($data);
	    			}
				}
				
			}
		}else{
			$yearMonth = $this->getYM();
		}
		$shifts=array();
		$this->set(compact('yearMonth'));
		$this->Shifts->Behaviors->load('Master');
		$yearMonthList = $this->Shifts->getYearMonthList('Shifts','date');
		$YmdAarray = $this->Shifts->getBusinessYmd($yearMonth);
		$yearMonth=date('Y-m',strtotime($YmdAarray[0]));
		$conditions = array('employee_no' => $userId,array('date_format(date,"%Y-%m")'=>$yearMonth));
		$shifts = $this->Shifts->find('all',array('conditions' => $conditions,'order'=>array('date ASC')));
		$this->set(compact('YmdAarray'));
		$this->set(compact('shifts','day_format','employeeName'));
		$this->set(compact('yearMonthList'));
	}
	
	public function getYM() {

		$YM = date('Ym');

		return $YM;
	}
	
	public function upload(){
		$title = 'シフト登録';
		$this->set(compact('title'));
		if($this->request->is('post')) {
		 	$userId = $this->Auth->user('id');
			$csv = $this->request->data['upload']['CSVfile'];
		    $file = $csv['tmp_name'];
		    $inputFileType = PHPExcel_IOFactory::identify($file);
		    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
		    $objReader->setReadDataOnly(true);
    		$objPHPExcel = $objReader->load($file);
    		$sheet = $objPHPExcel->getSheet(0);
			$highestRow = $sheet->getHighestDataRow();
			$highestColumn = $sheet->getHighestColumn();
    		for ($row = 1; $row <= $highestRow; $row++) {
    			$year;
		 		$month;
    			$date;
			    $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, 
			                                    null, true, false);
			   if($row==1){
				$year = $rowData[0][1];
			   }
			   
			   if($row==2){
			   	$month = $rowData[0][1];
			   	$cond = array(array('date >='=>$year.'-'.$month.'-01'),array('date <='=>$year.'-'.$month.'-31'));
				$this->Shifts->deleteAll(array($cond,false));
			   }
			   
			   if($row>4){
			   	if($rowData[0][2] != null){
			   		$data = array();
			   		$start_time;
	    			$closing_time;
	    			$break_deduction;
	    			$basic_uptime;
	    			$employee_no;
				   	$days = cal_days_in_month(CAL_GREGORIAN, $month, $year);
				   	$employees = $this->Employee->find('all',array('conditions'=>array('relation_emp_no' =>  $rowData[0][2])));
					if($employees){
						$employee_no = $employees[0]['Employee']['id'];
							for($i = 1; $i<=$days;$i++){
								$day_value = $rowData[0][$i + 3];
								switch($day_value){
									case 'A':
										$start_time='9:00';
		    							$closing_time='17:00';
		    							$break_deduction='1.50';
		    							break;
		    						case 'B':
		    							$start_time='9:00';
		    							$closing_time='12:30';
		    							$break_deduction='0.50';
		    							break;
		    						case 'C':
		    							$start_time='9:00';
		    							$closing_time='14:00';
		    							$break_deduction='1.00';
		    							break;
		    						case 'D':
		    							$start_time='9:00';
		    							$closing_time='16:00';
		    							$break_deduction='1.00';
		    							break;
		    						case 'E':
		    							$start_time='9:00';
		    							$closing_time='15:00';
		    							$break_deduction='1.00';
		    							break;
		    						case 'F':
		    							$start_time='14:00';
		    							$closing_time='17:00';
		    							$break_deduction='0.50';
		    							break;
		    						case 'G':
		    							$start_time='9:00';
		    							$closing_time='12:00';
		    							$break_deduction='1.50';
		    							break;
		    						case '/':
		    							$start_time='';
		    							$closing_time='';
		    							$break_deduction='';
		    							break;
		    						default:
		    							$start_time = ($rowData[0][0]!='') ? PHPExcel_Style_NumberFormat::toFormattedString($rowData[0][0],'hh:mm'):'';
					   					$closing_time =($rowData[0][1]!='') ? PHPExcel_Style_NumberFormat::toFormattedString($rowData[0][1],'hh:mm'):'';
		    							$break_deduction='1.50';
								}
								if($start_time !='' && $closing_time != '' && $break_deduction != ''){
									$basic_uptime = $this->Shifts->getBasicUptime($start_time,$closing_time,$break_deduction);
									$basic_uptime = $this->Shifts->time_to_decimal($basic_uptime);
									$data[] = array('start_time'=>$start_time,'closing_time'=>$closing_time,'basic_uptime'=>$basic_uptime,'break_deduction'=>$break_deduction,'employee_no'=>$employee_no,'date'=>$year.'-'.$month.'-'.$i);
								}
							} 	
						}
						$this->Shifts->saveAll($data);
			   		}
				}
    		}
    		return $this->redirect(array('action' => 'index'));
		}
	}
	
}