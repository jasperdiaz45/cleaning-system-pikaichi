<?php
App::uses('AppController', 'Controller');
App::uses('CakeTime', 'Utility');
/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 * @property FlashComponent $Flash
 */
class UsersController extends AppController {
	
	public $components = array('SendEmail');
	
	var $uses = array('Employee','Operation','Department', 'Division','Authority','Section','LeaveOfAbsence');
	
	/**
	 * ログイン
	 */
	public function login(){
		// 社員管理
		$title = '社員管理';
		$this->set(compact('title'));
		
		$this->layout = "login";
		
		$this -> Auth -> logout();
		if ($this->request->is('post')) {
			if ($this -> Auth -> login()) {
//				if($this->request->data['User']['autologin'] == '1'){
//					$this->Cookie->write( 'UserLogin', $this->request->data, true, '+1 year');
//				}else{
					$this->Cookie->delete('UserLogin');
//				}
				return $this->redirect($this->Auth->loginRedirect);
			} else {
				$this->Flash->set(__d('message','loginError'));
			}
		} else {
			$this->request->data = $this->Cookie->read('UserLogin');
		}
	}
	
	/**
	 * ログアウト処理
	 */
	public function logout() {
		$this->Cookie->delete('UserLogin');
		$this -> Auth -> logout();
		$this->redirect($this->Auth->logoutRedirect);
	}
	
	public function change_password(){
		$title = 'パスワードを変更する';
		$this->set(compact('title'));
		$this->layout = "login";
		$color = false;
		if ($this->request->is('post')) {
			
			if ($this -> Auth -> login()) {
			$requestData = $this->request->data;
			$email = $requestData['Employee']['mail_address'];
			$password=$requestData['Employee']['password'];
			$confirm=$requestData['Employee']['confirm_password'];
			$newpassword=$requestData['Employee']['new_password'];
			$employee = $this->Employee->find('all', ['conditions' => ['mail_address' =>$email]]);
			if($newpassword == ''){
					$this->Flash->error(__('新しいパスワードが無効です'));
				} elseif($confirm == '') {
					$this->Flash->error(__('新しいパスワードが無効です'));
				}else{
			
			if(count($employee)>0){
				$id=$employee[0]['Employee']['id'];
					
					
						
					
					if($confirm==$newpassword){
					$this->Employee->save (
						array(
							'id' => $id,
							'password' => $newpassword,
							'new_password' => $newpassword
						)
					);
					$this->Cookie->delete('UserLogin');
					$this -> Auth -> logout();
					$this->Flash->success(__('パスワードを変更しました'));
					
					$color = true;
					} else{
						$this->Flash->error(__('新しいパスワードが一致しません'));
					}
					
				
			}
				}
			} else {
				$this->Flash->set(__d('message','loginError'));
			}
		} else {
			$this->request->data = $this->Cookie->read('UserLogin');
		}
		
		$this->set(compact('color'));
		
		
	}
	

	/**
	 * index method
	 *
	 * @return void
	 */
	public function index() {
		// 社員管理
		$title = '社員管理';
		$this->set(compact('title'));
		
		$this->Operation->Behaviors->load('Master');
	
		$retirementMst = $this->Operation->retirementMst();
		
		//list responsibility
		$responsibility = $this->Operation->responsibilityMst();
		$this->set(compact('responsibility'));
		
		//position
		//$position = $this->Operation->positionMst();

		$f_department_no = '';
		$f_divisiont_no ='';
		$f_retirement = '';
		$f_responsibility = '0';
		$conditions =  array('retirement' => 0);
		if ($this->request->is('post')) {
			//検索条件取得
			$formInputInfo = $this->request->data;
			$f_department_no = $formInputInfo['department_no'];
			$f_section_no = $formInputInfo['section_no'];
			$f_retirement = $formInputInfo['Employee']['retirement'];
			$f_responsibility = $formInputInfo['Employee']['responsibility'];
			
			$conditions =  array('effectiveness' => 1);
			if ($f_department_no != '0')	{
				$conditions = $conditions + array('department_no' => $f_department_no);
			}
			if ($f_section_no != '0')	{
				$conditions = $conditions + array('section_no' => $f_section_no);
			}
			if ($f_retirement != '0')	{
				$conditions = $conditions + array('retirement' => $f_retirement);
			}
			if ($f_responsibility != '0' && $f_responsibility != null)	{
				$conditions = $conditions + array('responsibility' => $f_responsibility);
			}
				
			$this->set(compact('conditions'));
		}
		$this->set('f_company_no', $this->Auth->user('company_no'));
		$this->set(compact('f_department_no'));
		$this->set(compact('f_section_no'));
		
		//get position name
		$position = $this->Operation->positionMst();
		
		$employee = $this->Employee->find('all',['conditions' => $conditions]);
		for ($i = 0; $i < count($employee); $i++) {
			
			$num_pos = 1;
			if (!empty($employee[$i]['Employee']['position_no'])) {
				$num_pos = intval($employee[$i]['Employee']['position_no']);
			}
			
    		$employee[$i]['Employee']['department_name'] =  $this->Operation->getDepartmentName($employee[$i]['Employee']['department_no']);
    		$employee[$i]['Employee']['section_name'] =  $this->Operation->getSectionName($employee[$i]['Employee']['section_no']);
    		$employee[$i]['Employee']['retirement'] =  $retirementMst[$employee[$i]['Employee']['retirement']];
    		$employee[$i]['Employee']['position_no'] = $position[$num_pos];
		}
		$this->set(compact('employee'));
		
		//ON LEAVE EMPLOYEES
		$employeeLeaveOfAbsence = $this->Employee->find('all',['conditions' => ['on_leave' => 1]]);

		$positionLst = $this->Operation->positionMst;
		for ($i = 0; $i < count($employeeLeaveOfAbsence); $i++) {
			$employeeLeaveOfAbsence[$i]['Employee']['position_no'] = $position[$employeeLeaveOfAbsence[$i]['Employee']['position_no']];
			$employeeLeaveOfAbsence[$i]['Employee']['division_no'] = $this->Operation->getDivisionName($employeeLeaveOfAbsence[$i]['Employee']['division_no']);
			$employeeLeaveOfAbsence[$i]['Employee']['department_no'] = $this->Operation->getDepartmentName($employeeLeaveOfAbsence[$i]['Employee']['department_no']);
			$employeeLeaveOfAbsence[$i]['Employee']['section_no'] = $this->Operation->getSectionName($employeeLeaveOfAbsence[$i]['Employee']['section_no']);
		}

		$this->set(compact('employeeLeaveOfAbsence'));
		
		// アクセス権限チェック
		//if ( !$this->checkAuthority('users') ) {
		//	throw new NotFoundException();
		//}
		//$this->User->recursive = 0;
		$this->set('users', $this->Paginator->paginate());
	}

	/**
	 * view method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function view($id = null) {
			// 社員管理
		$title = '社員管理';
		$this->set(compact('title'));
		
		// アクセス権限チェック
		if ( !$this->checkAuthority('users') ) {
			throw new NotFoundException();
		}
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
		$this->set('user', $this->User->find('first', $options));
	}
	

	public function add() {
		// 社員管理
		$title = 'ユーザー詳細';
		$this->set(compact('title'));
		
		$this->Operation->Behaviors->load('Master');
		
		$leaveOptions = $this->Operation->reasonLeaveMst();
		$this->set(compact('leaveOptions'));
		//Employee_category
		$employee_category = $this->Operation->employeeCategoryMst();
		$this->set(compact('employee_category'));
		//Position
		$position_no = $this->Operation->positionMst();
		$this->set(compact('position_no'));
		//Responsibility
		$responsibility = $this->Operation->responsibilityMst();
		$this->set(compact('responsibility'));
		//Division No
		$divisionLists = $this->Division->find('list',['conditions' => array('deleted' => 0)]);
		$this->set(compact('divisionLists'));
		//Department No
		$departmentLists = $this->Department->find('list',['conditions' => array('deleted' => 0)]);
		$this->set(compact('departmentLists'));
		//Section No
		$sectionLists = $this->Section->find('list',['conditions' => array('deleted' => 0)]);
		$this->set(compact('sectionLists'));
		
		$Userid = $this->request->params['pass'];
		// アクセス権限チェック
		if ( !$this->checkAuthority('users') ) {
			throw new NotFoundException();
		}
		
		if ($this->request->is(array('post', 'put'))) {

			$Employee = $this->request->data;
			$Employee['Employee']['company_no'] = $this->Auth->user('company_no');

		
		 if($this->Employee->save($Employee)) {
		 		$id = $this->Employee->id;
		 		
		 		//get data for leaveOfAbsences from form
		 		$checked = $this->request->data['Employee']['on_leave'];
		 		//Variable for leavOfAbsences
		 		$leaveType = null;
		 		$startDate = null;
		 		$EndDate = null;
		 		// $Employee->start_time = time('H:i:s');
		 		// $StartTime = $this->request->data['Employee']['start_time'] = CakeTime::format('now', '%H:%M');
		 		//Set data for leaveOfAbsences if its checked
		 		if ($checked) {
		 			$leaveType = $this->request->data['Employee']['leave_of_absence_type'];
			 		$startDate = $this->request->data['Employee']['start_date'];
			 		$EndDate = $this->request->data['Employee']['end_date'];
		 		}
				
				//Set data for leaveOfAbsences
				$option = array('employee_no' => $id, 'leave_of_absence_type' => $leaveType, 'start_date' => $startDate, 'end_date' => $EndDate);
		 		$this->LeaveOfAbsence->save($option);
				// $this->Flash->success(__('The user has been saved.'));
				$this->Flash->success(__('ユーザーが保存されました。'));
				// $this->Flash->success(__($id));
				return $this->redirect(array('action' => 'index'));
			} else {
				// ユーザーを保存できませんでした。
				// $this->Flash->error(__('The user could not be saved. Please, try again.'));
				$this->Flash->error(__('ユーザーを保存できませんでした。'));
			}
		}
		
		//leaveoptions
    	$this->set('leaveoptions',$leaveOptions);
	}
	
	/**
	 * edit method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function edit($id = null) {
		// 社員管理
		$title = '社員詳細';
		$this->set(compact('title'));
		
		$this->Operation->Behaviors->load('Master');
		$responsibility = $this->Operation->responsibilityMst();
		$employee_category = $this->Operation->employeeCategoryMst();
		$this->set(compact('employee_category'));
		$position_no = $this->Operation->positionMst();
		$this->set(compact('position_no'));

		$chk_attendance_adjustment = false;

		$leaveOptions = $this->Operation->reasonLeaveMst();
		// アクセス権限チェック
//		if ( !$this->checkAuthority('users') ) {
//			throw new NotFoundException('You are not authorized!');
//		}
		//if id is not found throw exception
		if (!$id) {
            throw new NotFoundException(__('Invalid User!'));
        }
         //get Employee.id
        $EmployeeId = $this->Employee->findById($id);
         //request all data in id
        if (!$this->request->data) {
        	$this->request->data = $EmployeeId;
    	}
    	//update data
		if ($this->request->is(array('post', 'put'))) {
	        $this->Employee->id = $id;
	        $Employee = $this->request->data;
	
			// Set it if the password input field is not empty.
	        if ($Employee['Employee']['new_password'] != '') $Employee['Employee']['password'] = $Employee['Employee']['new_password'];
	        
	        $Employee['Employee']['company_no'] = $this->Auth->user('company_no');
	
			//就業調整
	        if($Employee['adjustAttendance'] == '1'){
	        	$Employee['Employee']['attendance_adjustment'] = $Employee['adjustAttendance'];
	        	$Employee['Employee']['start_time'] = $Employee['start_time'];
	        	$Employee['Employee']['closing_time'] = $Employee['closing_time'];
	        	$Employee['Employee']['basic_uptime'] = $Employee['basic_uptime'];
	 
	        }else{
	        	$Employee['Employee']['attendance_adjustment'] = '0';
	        }

	        for($idx = 1; isset($Employee['Employee']['leave_of_absence_type_' . $idx]); $idx++){
				//Set data for leaveOfAbsences
				$option = array(
					'employee_no' => $id,
					'leave_of_absence_type' => $Employee['Employee']['leave_of_absence_type_' . $idx],
					'start_date' => $Employee['Employee']['start_date_' . $idx],
					'end_date' => $Employee['Employee']['end_date_' . $idx],
					'return_date' => $Employee['Employee']['return_date_' . $idx]
				);
				$leaveOfAbsenceId = $Employee['Employee']['leave_of_absence_id_' . $idx];
				if ($leaveOfAbsenceId != '') $option += array('id' => $leaveOfAbsenceId);
				$this->LeaveOfAbsence->create();
				$this->LeaveOfAbsence->save($option);
	        }

	        if ($this->Employee->save($Employee)) {
	            $this->Flash->success(__('社員詳細を更新しました'));
	            $this->redirect($this->referer());
	        }
        	$this->Flash->error(__('Unable to update Employee.'));
    	}

    	//get associations
    	$employee = $this->Employee->find('all', array('conditions' => array('Employee.id' => $id)));
    	$this->set(compact('employee'));
    	//get division options
    	$this->set('division', $this->Division->find('list'));
    	//get department opstion
    	$this->set('department',$this->Department->find('list'));
    	//get section options
    	$this->set('section', $this->Section->find('list'));
    	$this->set('employee_category',$employee_category);
//get position_no
    	$this->set('position_no',$position_no);
    	//responsibility options
    	$this->set('responsibility',$responsibility);
    	//leaveoptions
    	$this->set('leaveoptions',$leaveOptions);

    	//就業調整チェックボックス
    	if($employee[0]['Employee']['attendance_adjustment'] == 1){
    		$chk_attendance_adjustment = true;
		}
		$this->set(compact('chk_attendance_adjustment'));
    	
    	//休職中チェックボックス
		$chk_on_leave = false;
    	if($employee[0]['Employee']['on_leave'] == 1){
    		$chk_on_leave = true;
		}
		$this->set(compact('chk_on_leave'));

    	//退職チェックボックス
		$chk_retirement = false;
    	if($employee[0]['Employee']['retirement'] == 1){
    		$chk_retirement = true;
		}
		$this->set(compact('chk_retirement'));

    	//get leave of absence
    	$leaveOfAbsences = $this->LeaveOfAbsence->find('all', array('conditions'=> array('LeaveOfAbsence.employee_no'=>$id)));
/*		for ($i = 0; $i < count($leaveOfAbsences); $i++) {
	    	$leaveOfAbsences[$i]['LeaveOfAbsence']['id'] =  $leaveOfAbsences[$employee[$i]['LeaveOfAbsence']['id']];
	    	$leaveOfAbsences[$i]['LeaveOfAbsence']['leave_of_absence_type'] =  $leaveOfAbsences[$employee[$i]['LeaveOfAbsence']['leave_of_absence_type']];
	    	$leaveOfAbsences[$i]['LeaveOfAbsence']['start_date'] =  $leaveOfAbsences[$employee[$i]['LeaveOfAbsence']['start_date']];
	    	$leaveOfAbsences[$i]['LeaveOfAbsence']['end_date'] =  $leaveOfAbsences[$employee[$i]['LeaveOfAbsence']['end_date']];
		}
  */  	$this->set(compact('leaveOfAbsences'));
    	
/*    	$leave_of_absence_type = '';
    	$start_date = '';
    	$end_date = '';
		//get leave of absence type if its not empty
    	if (!empty($leaveOfAbsences[0]['LeaveOfAbsence']['leave_of_absence_type'])) {
    		$leave_of_absence_type = $leaveOfAbsences[0]['LeaveOfAbsence']['leave_of_absence_type'];
    	}
    	//get leave of absence start_date if its not empty
    	if (!empty($leaveOfAbsences[0]['LeaveOfAbsence']['start_date'])) {
    		$start_date = $leaveOfAbsences[0]['LeaveOfAbsence']['start_date'];
    	}
    	//get leave of absence end_date if its not empty
    	if (!empty($leaveOfAbsences[0]['LeaveOfAbsence']['end_date'])) {
    		$end_date = $leaveOfAbsences[0]['LeaveOfAbsence']['end_date'];
    	}
    	
		$this->set(compact('leave_of_absence_type'));
		$this->set(compact('start_date'));
		$this->set(compact('end_date'));
 */
	}
	
	/**
	 * delete method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function delete($id = null) {
		// アクセス権限チェック
		if ( !$this->checkAuthority('users') ) {
			throw new NotFoundException();
		}
		
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid user'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->User->delete()) {
			$this->Flash->success(__('The user has been deleted.'));
		} else {
			$this->Flash->error(__('The user could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
	
	public function test_mail() {
		$toMail = 'sofuku@p-nt.com';
		$param = array('name'=>'鈴木 太郎');
		$this->SendEmail->sendTest($toMail, $param);
		$this->Flash->set(__d('message', 'mailSended', 'テストアカウント'));
		return $this->redirect(array('action'=>'index'));
	
	}
	
	
}
