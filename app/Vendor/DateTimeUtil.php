<?php

class DateTimeUtil {
	
	/*
	 * $formatで指定した形式で日付を取得
	 * $dateは未指定の場合当日日付を取得
	 */
	public static function format($format, $date = null) {
		$datetime = DateTimeUtil::getDateTime($date);
		return $datetime->format($format);
	}
	
	/*
	 * formatで指定した形式かつ$calc（例：'+1 days'）で指定した形式で算出した日付を取得
	 * $dateは未指定の場合当日日付を元に算出する
	 */
	public static function calcDate($format, $calc, $date = null) {
		$datetime = DateTimeUtil::getDateTime($date);
		$result = $datetime->modify($calc)->format($format);
		if (strpos($calc,'year') !== false || strpos($calc,'month') !== false) {
			// 年、月の算出時に月がずれた場合(yyyy-03-31の-1 month など)
			$firstDate = DateTimeUtil::format('Y-m-01', $date);
			$firstDatetime = DateTimeUtil::getDateTime($firstDate);
			$firstResult = $firstDatetime->modify($calc)->format($format);
			
			$firstYearMonth = DateTimeUtil::format('Y-m',$firstResult);
			$resultYearMonth = DateTimeUtil::format('Y-m',$result);
			if ($firstYearMonth != $resultYearMonth) {
				$modify = DateTimeUtil::format('Y-m-t',$firstResult);
				$result = DateTimeUtil::format($format,$modify);
			}
		}
		return $result;
	}
	
	private static function getDateTime($date = null) {
		$datetime;
		if (empty($date)) {
			$datetime = new DateTime();
		} else {
			$datetime = new DateTime($date);
		}
		return $datetime;
	}
	
	public static function getTimeArray($start_date, $interval, $finish_date, $format) {
		$results = array();
		$start = new DateTime($start_date);
		// ※ DatePeriodのバグ対応 PHP5.3.3ではfinishの時間が00:00:00の場合イテレーションに含まれないため+1 secondを指定
		$finish = new DateTime($finish_date . ' +1 second');
		$date_interval = new DateInterval($interval);
		$date_period = new DatePeriod($start, $date_interval, $finish);
		foreach($date_period as $key=>$val){
			array_push($results, $val->format($format));
		}
		// 日付の配列を返却
		return $results;
	}
	
	public static function getWarekiYear($seireki = null, $label = false){
		if (empty($seireki)) {
			$seireki = DateTimeUtil::format('Y');
		}
		$nengo_year = array('平成' => 1989 , '昭和' => 1926 , '大正' => 1912 , '明治' => 1868 );
		foreach($nengo_year as $nengo => $year){
			if ($seireki >= $year){
				$wareki = $seireki - $year + 1;
				if ($label) {
					return $nengo . $wareki;
				} else {
					return $wareki;
				}
			}
		}
		return "";
	}
	
	public static function getThisMonthAndWeekLastDay($format, $date = null) {
		// 指定日付の同月内かつ同一週の最終日（月～日の順）を取得
		$firstDayWeekday = intval(DateTimeUtil::format('w', $date));
		$addDay = 0;
		if ($firstDayWeekday > 0) {
			$addDay = 7 - $firstDayWeekday;
		}
		
		$calc = '+' . $addDay . ' day';
		$returnDate = DateTimeUtil::calcDate($format, $calc, $date);
		
		$lastMonthDay = DateTimeUtil::format('Y/m/t', $date);
		if ($returnDate > $lastMonthDay) {
			$returnDate = $lastMonthDay;
		}
		
		return $returnDate;
	}
	
	public static function isGroupLessonCancel($date, $time) {
		$reserveDateTime = $date . ' ' . $time;
		$reserveTime = DateTimeUtil::format('Y/m/d H:i', $reserveDateTime);
		$nowTime = DateTimeUtil::format('Y/m/d H:i');
		
		$resutlts = DateTimeUtil::getTimeArray($nowTime,'PT1H1M',$reserveDateTime,'Y/m/d H:i');
		
		return count($resutlts) > 1;
	}
	
	public static function isParsonalLessonCancel($date, $time) {
		$reserveDateTime = $date . ' ' . $time;
		$reserveTime = DateTimeUtil::format('Y/m/d H:i', $reserveDateTime);
		$nowTime = DateTimeUtil::format('Y/m/d H:i');
		$hours = Configure::read('PLcancelHours');
		
		$resutlts = DateTimeUtil::getTimeArray($nowTime,'PT'.$hours.'H1M',$reserveDateTime,'Y/m/d H:i');
		
		return count($resutlts) > 1;
	}
	
	public static function getMinuteDifference($fromTime, $toTime) {
		if (empty($fromTime) || empty($toTime)) {
			return 0;
		}
		
		$resutlts = array();
		try {
			$day = DateTimeUtil::format('Y/m/d');
			$from = DateTimeUtil::format('Y/m/d H:i', $day.' '.$fromTime);
			$to = DateTimeUtil::format('Y/m/d H:i', $day.' '.$toTime);
			$resutlts = DateTimeUtil::getTimeArray($from,'PT1M1S',$to,'Y/m/d H:i');
		} catch (Exception $e) {
			CakeLog::write('error', $e->getMessage());
		}
		
		return count($resutlts);
	}
	
	public static function getJpWeekday($date = null) {
		$weekdays = array('0'=>'日','1'=>'月','2'=>'火','3'=>'水','4'=>'木','5'=>'金','6'=>'土');
		$weekday = DateTimeUtil::format('w', $date);
		
		return $weekdays[$weekday];
	}
	
	//***
	// この関数は後で＜＜＜削除＞＞＞する
	//***
	// Calculate the difference between start time and end time.
	public static function calcDiffHour($startTime, $endTime) {
		$startSec = strtotime($startTime);
		$endSec = strtotime($endTime);
		$diffSec = 0;
		if ($startSec < $endSec) {
			$diffSec = $endSec - $startSec;
		} elseif ($startSec > $endSec) {
			$diffSec = DAY - $startSec + $endSec;
		}
		$diffHour = floor($diffSec / HOUR);
		$diffMin = ($diffSec - $diffHour * HOUR) / MINUTE;
		$diffMin = floor($diffMin / 15) * 15;			// Round every 15 minutes.
		$diffMin = $diffMin * (100 / MINUTE) / 100;		// Convert to percentage.
		$diffHour += $diffMin;
		
		return $diffHour;
	}

	// decimal to time (for example : 1.5h -> 1:30)
	public static function decimaltoTime($decimal) {
		
		$retVal = '';

		$decimalStr = strval($decimal);
		if (strpos($decimalStr, '.')) {
			$timeArray = explode('.', rtrim($decimalStr, '0'));
			$retVal = sprintf('%02d:%02d', $timeArray[0], substr(strval(intval($timeArray[1]) * 6),0,2));
		} else {
			$retVal = $decimalStr . ':00';
		}

		return $retVal;
	}

	// overtime calculation.
	public static function calcOvertime($actualOperatingTime) {
		$overtime = $actualOperatingTime > 8 ? $actualOperatingTime - 8 : 0;

		return $overtime;
	}

	// late_night_overtime calculation.
	public static function calcLateNightOvertime($actualOperatingTime) {
		$lateNightOvertime = $actualOperatingTime > 12.5 ? $actualOperatingTime - 12.5 : 0;

		return $lateNightOvertime;
	}

	//***
	// この関数は後で＜＜＜削除＞＞＞する
	//***
	// conversion number to time. (for example : 1.5h -> 1:30)
	public static function conversionNumberToTime($numTime) {
		$hour = floor($numTime);
		$numMin = ($numTime - $hour) * 10;
		$retTime = sprintf("%d:%02d", $hour, MINUTE / 10 * $numMin);

		return $retTime;
	}
}
