<?php

class CountMeterUtil {
	
	/*
	 * $workCntは実稼働
	 * $estimateCntは予定稼働
	 * メーターとパーセントを連結し、文字列として返却
	 */
	public static function meterAndPer($workCnt, $estimateCnt) {
		// どちらも0の場合、0%
		if (empty($workCnt) && empty($estimateCnt)) {
			return '□□□□□ 0%';
		}
		
		// 予定稼働が0の場合、100%
		if (empty($estimateCnt)) {
			return '■■■■■ 100%';
		}
		
		$calcper = ($workCnt / $estimateCnt) * 100;
		$per = round($calcper); // 小数点以下を四捨五入
		if ($per == 0 && $calcper > 0) {
			$per = 1;
		}
		
		$meter = '';
		if ($per >= 100) {
			$meter = '■■■■■';
			
			$calcper = $per - 100;
			$meterPer = floor($calcper / 20); // 5段階 切り捨てで算出
			for ($i = 0; $i < $meterPer; $i++) {
				$meter .= '◆';
			}
		} else if ($per > 0) {
			$meterPer = floor($per / 20); // 5段階 切り捨てで算出
			for ($i = 0; $i < 5; $i++) {
				if ($i < $meterPer) {
					$meter .= '■';
				} else {
					$meter .= '□';
				}
			}
		} else {
			$meter = '□□□□□';
		}
		
		return $meter . ' ' . $per . '%';
	}
	
}
