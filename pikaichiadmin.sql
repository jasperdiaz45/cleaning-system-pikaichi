-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- ホスト: 127.0.0.1
-- 生成日時: 2019 年 6 月 30 日 12:36
-- サーバのバージョン: 5.5.57-0ubuntu0.14.04.1
-- PHP のバージョン: 5.5.9-1ubuntu4.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- データベース: `pikaichiadmin`
--

-- --------------------------------------------------------

--
-- テーブルの構造 `authorities`
--

CREATE TABLE IF NOT EXISTS `authorities` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `authority_name` varchar(256) NOT NULL COMMENT '権限名',
  `authority_rank` int(2) NOT NULL COMMENT '権限ランク',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='権限テーブル' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `business_description_by_opportunities`
--

CREATE TABLE IF NOT EXISTS `business_description_by_opportunities` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `operation_no` int(11) NOT NULL COMMENT '稼働ID',
  `parent_project_no` int(11) NOT NULL COMMENT '案件ID',
  `project_no` int(11) NOT NULL COMMENT '商談ID',
  `operating_time` decimal(4,2) DEFAULT NULL COMMENT '稼働時間',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商談別稼働明細テーブル' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `cake_sessions`
--

CREATE TABLE IF NOT EXISTS `cake_sessions` (
  `id` varchar(255) NOT NULL DEFAULT '',
  `data` text NOT NULL,
  `expires` bigint(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- テーブルのデータのダンプ `cake_sessions`
--

INSERT INTO `cake_sessions` (`id`, `data`, `expires`) VALUES('07e7e41ltmdskcn3atc393emk5', 'Config|a:3:{s:9:"userAgent";s:32:"d14c664f2b60198160b9a86162b3a6a9";s:4:"time";i:4710297911;s:9:"countdown";i:10;}Message|a:1:{s:4:"auth";a:4:{s:7:"message";s:47:"You are not authorized to access that location.";s:3:"key";s:4:"auth";s:7:"element";s:13:"Flash/default";s:6:"params";a:0:{}}}Auth|a:1:{s:9:"AdminUser";a:26:{s:2:"id";s:1:"8";s:10:"company_no";s:1:"1";s:11:"division_no";s:1:"1";s:13:"department_no";s:1:"3";s:10:"section_no";s:1:"3";s:12:"authority_no";s:1:"1";s:14:"responsibility";s:1:"2";s:10:"first_name";s:6:"亜耶";s:9:"last_name";s:6:"廣田";s:12:"mail_address";s:25:"pikaichi.hirota@gmail.com";s:13:"effectiveness";s:1:"1";s:14:"job_entry_date";s:10:"2014-11-10";s:11:"ic_card_key";s:4:"1007";s:11:"position_no";s:1:"1";s:17:"employee_category";s:1:"1";s:10:"retirement";s:1:"0";s:15:"retirement_date";N;s:8:"on_leave";s:1:"0";s:21:"attendance_adjustment";s:1:"0";s:10:"shift_work";s:1:"1";s:10:"start_time";N;s:12:"closing_time";N;s:8:"birthday";N;s:12:"basic_uptime";N;s:15:"holiday_pattern";s:1:"9";s:8:"username";s:15:"廣田　亜耶";}}', 4710297911);
INSERT INTO `cake_sessions` (`id`, `data`, `expires`) VALUES('1infavnnj4pj7op18do2l3gh07', 'Config|a:3:{s:9:"userAgent";s:32:"e04d3a82e71ee78f9f03601fde6e2750";s:4:"time";i:4711487121;s:9:"countdown";i:10;}', 4711487121);
INSERT INTO `cake_sessions` (`id`, `data`, `expires`) VALUES('1rkj9jr69itqag24kaujj1htd1', 'Config|a:3:{s:9:"userAgent";s:32:"977101f4456b7509d781fe16fd59104f";s:4:"time";i:4715223276;s:9:"countdown";i:10;}', 4715223276);
INSERT INTO `cake_sessions` (`id`, `data`, `expires`) VALUES('1vn09ij84g3ivq2oued30ckpp7', 'Config|a:3:{s:9:"userAgent";s:32:"c18f97aefc462939b2f3d3cdc8d91f2f";s:4:"time";i:4715200398;s:9:"countdown";i:10;}', 4715200398);
INSERT INTO `cake_sessions` (`id`, `data`, `expires`) VALUES('22kgb3em2rsm8q71d6vlbfnv61', 'Config|a:3:{s:9:"userAgent";s:32:"9836fe4fc2a0d52439cac29efde007fb";s:4:"time";i:4714021653;s:9:"countdown";i:10;}Message|a:1:{s:4:"auth";a:4:{s:7:"message";s:47:"You are not authorized to access that location.";s:3:"key";s:4:"auth";s:7:"element";s:13:"Flash/default";s:6:"params";a:0:{}}}Auth|a:1:{s:9:"AdminUser";a:27:{s:2:"id";s:2:"10";s:10:"company_no";s:1:"1";s:11:"division_no";s:1:"1";s:13:"department_no";s:1:"1";s:10:"section_no";s:1:"1";s:12:"authority_no";s:1:"2";s:14:"responsibility";s:1:"0";s:10:"first_name";s:6:"好広";s:9:"last_name";s:6:"野田";s:12:"mail_address";s:24:"pikaichi.noday@gmail.com";s:13:"effectiveness";s:1:"1";s:14:"job_entry_date";s:10:"2015-07-01";s:11:"ic_card_key";s:4:"1011";s:11:"position_no";s:1:"1";s:17:"employee_category";s:1:"1";s:10:"retirement";s:1:"0";s:15:"retirement_date";N;s:8:"on_leave";s:1:"0";s:21:"attendance_adjustment";s:1:"0";s:10:"shift_work";s:1:"1";s:10:"start_time";N;s:12:"closing_time";N;s:8:"birthday";N;s:12:"basic_uptime";N;s:15:"holiday_pattern";s:1:"9";s:15:"relation_emp_no";s:4:"1011";s:8:"username";s:15:"野田　好広";}}', 4714021653);
INSERT INTO `cake_sessions` (`id`, `data`, `expires`) VALUES('2jm6tpo0af2porne3u7mhomse2', 'Config|a:3:{s:9:"userAgent";s:32:"ed569cbe4b8fbfe66141b9bb2381ec2a";s:4:"time";i:4715130330;s:9:"countdown";i:10;}', 4715130330);
INSERT INTO `cake_sessions` (`id`, `data`, `expires`) VALUES('2tcvc23vabb1lo7tco2pf7io35', 'Config|a:3:{s:9:"userAgent";s:32:"c18f97aefc462939b2f3d3cdc8d91f2f";s:4:"time";i:4715130393;s:9:"countdown";i:10;}', 4715130393);
INSERT INTO `cake_sessions` (`id`, `data`, `expires`) VALUES('36vd0apcrtgj3gbi8i9k8vg2v0', 'Config|a:3:{s:9:"userAgent";s:32:"ed569cbe4b8fbfe66141b9bb2381ec2a";s:4:"time";i:4715372363;s:9:"countdown";i:10;}Message|a:1:{s:4:"auth";a:4:{s:7:"message";s:47:"You are not authorized to access that location.";s:3:"key";s:4:"auth";s:7:"element";s:13:"Flash/default";s:6:"params";a:0:{}}}Auth|a:1:{s:9:"AdminUser";a:27:{s:2:"id";s:1:"2";s:10:"company_no";s:1:"1";s:11:"division_no";s:1:"4";s:13:"department_no";s:1:"2";s:10:"section_no";s:1:"2";s:12:"authority_no";s:1:"1";s:14:"responsibility";s:1:"2";s:10:"first_name";s:6:"将禎";s:9:"last_name";s:6:"柳田";s:12:"mail_address";s:27:"pikaichi.yanagita@gmail.com";s:13:"effectiveness";s:1:"1";s:14:"job_entry_date";s:10:"2010-08-23";s:11:"ic_card_key";s:4:"0100";s:11:"position_no";s:1:"5";s:17:"employee_category";s:1:"4";s:10:"retirement";s:1:"0";s:15:"retirement_date";N;s:8:"on_leave";s:1:"0";s:21:"attendance_adjustment";s:1:"0";s:10:"shift_work";s:1:"1";s:10:"start_time";N;s:12:"closing_time";N;s:8:"birthday";N;s:12:"basic_uptime";N;s:15:"holiday_pattern";s:1:"9";s:15:"relation_emp_no";s:3:"100";s:8:"username";s:15:"柳田　将禎";}}', 4715372363);
INSERT INTO `cake_sessions` (`id`, `data`, `expires`) VALUES('3l96runpmtutq9i3vh1osev2a7', 'Config|a:3:{s:9:"userAgent";s:32:"05517c6d112d3e2e9c2a2827f13334f3";s:4:"time";i:4715422936;s:9:"countdown";i:10;}', 4715422936);
INSERT INTO `cake_sessions` (`id`, `data`, `expires`) VALUES('3spfgatphq7be5vo6l3ukiek86', 'Config|a:3:{s:9:"userAgent";s:32:"5ca4b7d57b54aa32f60fc0cf3d55f4c3";s:4:"time";i:4715422160;s:9:"countdown";i:10;}Message|a:1:{s:4:"auth";a:4:{s:7:"message";s:47:"You are not authorized to access that location.";s:3:"key";s:4:"auth";s:7:"element";s:13:"Flash/default";s:6:"params";a:0:{}}}Auth|a:1:{s:9:"AdminUser";a:27:{s:2:"id";s:1:"2";s:10:"company_no";s:1:"1";s:11:"division_no";s:1:"4";s:13:"department_no";s:1:"2";s:10:"section_no";s:1:"2";s:12:"authority_no";s:1:"1";s:14:"responsibility";s:1:"2";s:10:"first_name";s:6:"将禎";s:9:"last_name";s:6:"柳田";s:12:"mail_address";s:27:"pikaichi.yanagita@gmail.com";s:13:"effectiveness";s:1:"1";s:14:"job_entry_date";s:10:"2010-08-23";s:11:"ic_card_key";s:4:"0100";s:11:"position_no";s:1:"5";s:17:"employee_category";s:1:"4";s:10:"retirement";s:1:"0";s:15:"retirement_date";N;s:8:"on_leave";s:1:"0";s:21:"attendance_adjustment";s:1:"0";s:10:"shift_work";s:1:"1";s:10:"start_time";N;s:12:"closing_time";N;s:8:"birthday";N;s:12:"basic_uptime";N;s:15:"holiday_pattern";s:1:"9";s:15:"relation_emp_no";s:3:"100";s:8:"username";s:15:"柳田　将禎";}}', 4715422160);
INSERT INTO `cake_sessions` (`id`, `data`, `expires`) VALUES('41jpfkmshbpic94q4qb3jjuge5', 'Config|a:3:{s:9:"userAgent";s:32:"2c47924e9a0c211f9382d4c4d92556ba";s:4:"time";i:4708722318;s:9:"countdown";i:10;}', 4708722318);
INSERT INTO `cake_sessions` (`id`, `data`, `expires`) VALUES('478qhkndd4oakusf1ihla92gk1', 'Config|a:3:{s:9:"userAgent";s:32:"5ca4b7d57b54aa32f60fc0cf3d55f4c3";s:4:"time";i:4715471206;s:9:"countdown";i:10;}Message|a:1:{s:4:"auth";a:4:{s:7:"message";s:47:"You are not authorized to access that location.";s:3:"key";s:4:"auth";s:7:"element";s:13:"Flash/default";s:6:"params";a:0:{}}}Auth|a:1:{s:9:"AdminUser";a:27:{s:2:"id";s:1:"2";s:10:"company_no";s:1:"1";s:11:"division_no";s:1:"4";s:13:"department_no";s:1:"2";s:10:"section_no";s:1:"2";s:12:"authority_no";s:1:"1";s:14:"responsibility";s:1:"2";s:10:"first_name";s:6:"将禎";s:9:"last_name";s:6:"柳田";s:12:"mail_address";s:27:"pikaichi.yanagita@gmail.com";s:13:"effectiveness";s:1:"1";s:14:"job_entry_date";s:10:"2010-08-23";s:11:"ic_card_key";s:4:"0100";s:11:"position_no";s:1:"5";s:17:"employee_category";s:1:"4";s:10:"retirement";s:1:"0";s:15:"retirement_date";N;s:8:"on_leave";s:1:"0";s:21:"attendance_adjustment";s:1:"0";s:10:"shift_work";s:1:"1";s:10:"start_time";N;s:12:"closing_time";N;s:8:"birthday";N;s:12:"basic_uptime";N;s:15:"holiday_pattern";s:1:"9";s:15:"relation_emp_no";s:3:"100";s:8:"username";s:15:"柳田　将禎";}}', 4715471206);
INSERT INTO `cake_sessions` (`id`, `data`, `expires`) VALUES('4m29s09f8622i9at7kdcm4d133', 'Config|a:3:{s:9:"userAgent";s:32:"5ca4b7d57b54aa32f60fc0cf3d55f4c3";s:4:"time";i:4715422485;s:9:"countdown";i:10;}', 4715422485);
INSERT INTO `cake_sessions` (`id`, `data`, `expires`) VALUES('55v18sjefn5lc6stumd090g397', 'Config|a:3:{s:9:"userAgent";s:32:"603f0c2d051dda8ca02921775582f768";s:4:"time";i:4715336200;s:9:"countdown";i:10;}Message|a:1:{s:4:"auth";a:4:{s:7:"message";s:47:"You are not authorized to access that location.";s:3:"key";s:4:"auth";s:7:"element";s:13:"Flash/default";s:6:"params";a:0:{}}}Auth|a:1:{s:9:"AdminUser";a:27:{s:2:"id";s:1:"3";s:10:"company_no";s:1:"1";s:11:"division_no";s:1:"1";s:13:"department_no";s:1:"1";s:10:"section_no";s:1:"1";s:12:"authority_no";s:1:"2";s:14:"responsibility";s:1:"2";s:10:"first_name";s:6:"慎吾";s:9:"last_name";s:6:"坪井";s:12:"mail_address";s:25:"pikaichi.tsuboi@gmail.com";s:13:"effectiveness";s:1:"1";s:14:"job_entry_date";s:10:"2010-11-01";s:11:"ic_card_key";s:4:"0101";s:11:"position_no";s:1:"4";s:17:"employee_category";s:1:"4";s:10:"retirement";s:1:"0";s:15:"retirement_date";N;s:8:"on_leave";s:1:"0";s:21:"attendance_adjustment";s:1:"0";s:10:"shift_work";s:1:"1";s:10:"start_time";N;s:12:"closing_time";N;s:8:"birthday";N;s:12:"basic_uptime";N;s:15:"holiday_pattern";s:1:"9";s:15:"relation_emp_no";s:3:"101";s:8:"username";s:15:"坪井　慎吾";}}', 4715336200);
INSERT INTO `cake_sessions` (`id`, `data`, `expires`) VALUES('5eu7u0lulva9kci01f3iid5365', 'Config|a:3:{s:9:"userAgent";s:32:"c3016251d9fc5bc072a19e5912778677";s:4:"time";i:4715307368;s:9:"countdown";i:10;}Message|a:1:{s:4:"auth";a:4:{s:7:"message";s:47:"You are not authorized to access that location.";s:3:"key";s:4:"auth";s:7:"element";s:13:"Flash/default";s:6:"params";a:0:{}}}Auth|a:1:{s:9:"AdminUser";a:27:{s:2:"id";s:1:"2";s:10:"company_no";s:1:"1";s:11:"division_no";s:1:"4";s:13:"department_no";s:1:"2";s:10:"section_no";s:1:"2";s:12:"authority_no";s:1:"1";s:14:"responsibility";s:1:"2";s:10:"first_name";s:6:"将禎";s:9:"last_name";s:6:"柳田";s:12:"mail_address";s:27:"pikaichi.yanagita@gmail.com";s:13:"effectiveness";s:1:"1";s:14:"job_entry_date";s:10:"2010-08-23";s:11:"ic_card_key";s:4:"0100";s:11:"position_no";s:1:"5";s:17:"employee_category";s:1:"4";s:10:"retirement";s:1:"0";s:15:"retirement_date";N;s:8:"on_leave";s:1:"0";s:21:"attendance_adjustment";s:1:"0";s:10:"shift_work";s:1:"1";s:10:"start_time";N;s:12:"closing_time";N;s:8:"birthday";N;s:12:"basic_uptime";N;s:15:"holiday_pattern";s:1:"9";s:15:"relation_emp_no";s:3:"100";s:8:"username";s:15:"柳田　将禎";}}', 4715307368);
INSERT INTO `cake_sessions` (`id`, `data`, `expires`) VALUES('5ir66r64g4hc0ucmtjpultqij3', 'Config|a:3:{s:9:"userAgent";s:32:"d14c664f2b60198160b9a86162b3a6a9";s:4:"time";i:4708872367;s:9:"countdown";i:10;}Message|a:1:{s:4:"auth";a:4:{s:7:"message";s:47:"You are not authorized to access that location.";s:3:"key";s:4:"auth";s:7:"element";s:13:"Flash/default";s:6:"params";a:0:{}}}Auth|a:1:{s:9:"AdminUser";a:26:{s:2:"id";s:1:"4";s:10:"company_no";s:1:"1";s:11:"division_no";s:1:"1";s:13:"department_no";s:1:"1";s:10:"section_no";s:1:"1";s:12:"authority_no";s:1:"2";s:14:"responsibility";s:1:"1";s:10:"first_name";s:3:"誠";s:9:"last_name";s:6:"秋野";s:12:"mail_address";s:24:"pikaichi.akino@gmail.com";s:13:"effectiveness";s:1:"1";s:14:"job_entry_date";s:10:"2012-06-01";s:11:"ic_card_key";s:4:"0103";s:11:"position_no";s:1:"3";s:17:"employee_category";s:1:"4";s:10:"retirement";s:1:"0";s:15:"retirement_date";N;s:8:"on_leave";s:1:"0";s:21:"attendance_adjustment";s:1:"0";s:10:"shift_work";s:1:"1";s:10:"start_time";N;s:12:"closing_time";N;s:8:"birthday";N;s:12:"basic_uptime";N;s:15:"holiday_pattern";s:1:"9";s:8:"username";s:12:"秋野　誠";}}', 4708872367);
INSERT INTO `cake_sessions` (`id`, `data`, `expires`) VALUES('5ubhah16tsnt9qhor83hboha52', 'Config|a:3:{s:9:"userAgent";s:32:"5ca4b7d57b54aa32f60fc0cf3d55f4c3";s:4:"time";i:4715422882;s:9:"countdown";i:10;}', 4715422882);
INSERT INTO `cake_sessions` (`id`, `data`, `expires`) VALUES('60lb5r2383v3vgqnuv308ga092', 'Config|a:3:{s:9:"userAgent";s:32:"0298b75623a8136ace6c58975711f9ce";s:4:"time";i:4713093010;s:9:"countdown";i:10;}Message|a:1:{s:4:"auth";a:4:{s:7:"message";s:47:"You are not authorized to access that location.";s:3:"key";s:4:"auth";s:7:"element";s:13:"Flash/default";s:6:"params";a:0:{}}}Auth|a:1:{s:9:"AdminUser";a:26:{s:2:"id";s:1:"8";s:10:"company_no";s:1:"1";s:11:"division_no";s:1:"1";s:13:"department_no";s:1:"3";s:10:"section_no";s:1:"3";s:12:"authority_no";s:1:"1";s:14:"responsibility";s:1:"3";s:10:"first_name";s:6:"亜耶";s:9:"last_name";s:6:"廣田";s:12:"mail_address";s:25:"pikaichi.hirota@gmail.com";s:13:"effectiveness";s:1:"1";s:14:"job_entry_date";s:10:"2014-11-10";s:11:"ic_card_key";s:4:"1007";s:11:"position_no";s:1:"1";s:17:"employee_category";s:1:"1";s:10:"retirement";s:1:"0";s:15:"retirement_date";N;s:8:"on_leave";s:1:"0";s:21:"attendance_adjustment";s:1:"0";s:10:"shift_work";s:1:"1";s:10:"start_time";N;s:12:"closing_time";N;s:8:"birthday";N;s:12:"basic_uptime";N;s:15:"holiday_pattern";s:1:"9";s:8:"username";s:15:"廣田　亜耶";}}', 4713093010);
INSERT INTO `cake_sessions` (`id`, `data`, `expires`) VALUES('6ils1ahoufl2694f947h5e3n70', 'Config|a:3:{s:9:"userAgent";s:32:"d14c664f2b60198160b9a86162b3a6a9";s:4:"time";i:4709815993;s:9:"countdown";i:10;}Message|a:1:{s:4:"auth";a:4:{s:7:"message";s:47:"You are not authorized to access that location.";s:3:"key";s:4:"auth";s:7:"element";s:13:"Flash/default";s:6:"params";a:0:{}}}Auth|a:1:{s:9:"AdminUser";a:26:{s:2:"id";s:1:"7";s:10:"company_no";s:1:"1";s:11:"division_no";s:1:"1";s:13:"department_no";s:1:"3";s:10:"section_no";s:1:"3";s:12:"authority_no";s:1:"2";s:14:"responsibility";s:1:"2";s:10:"first_name";s:6:"綾香";s:9:"last_name";s:6:"坪井";s:12:"mail_address";s:25:"pikaichi.uchino@gmail.com";s:13:"effectiveness";s:1:"1";s:14:"job_entry_date";s:10:"2013-10-01";s:11:"ic_card_key";s:4:"1003";s:11:"position_no";s:1:"1";s:17:"employee_category";s:1:"1";s:10:"retirement";s:1:"0";s:15:"retirement_date";N;s:8:"on_leave";s:1:"0";s:21:"attendance_adjustment";s:1:"0";s:10:"shift_work";s:1:"1";s:10:"start_time";N;s:12:"closing_time";N;s:8:"birthday";N;s:12:"basic_uptime";N;s:15:"holiday_pattern";s:1:"9";s:8:"username";s:15:"坪井　綾香";}}', 4709815993);
INSERT INTO `cake_sessions` (`id`, `data`, `expires`) VALUES('6v5nkiuofu112ciekkjs97uge0', 'Config|a:3:{s:9:"userAgent";s:32:"5ca4b7d57b54aa32f60fc0cf3d55f4c3";s:4:"time";i:4715422893;s:9:"countdown";i:10;}', 4715422893);
INSERT INTO `cake_sessions` (`id`, `data`, `expires`) VALUES('805t4otsk08eq9bj2sjm6ln8o2', 'Config|a:3:{s:9:"userAgent";s:32:"5ca4b7d57b54aa32f60fc0cf3d55f4c3";s:4:"time";i:4715423739;s:9:"countdown";i:10;}Message|a:1:{s:4:"auth";a:4:{s:7:"message";s:47:"You are not authorized to access that location.";s:3:"key";s:4:"auth";s:7:"element";s:13:"Flash/default";s:6:"params";a:0:{}}}Auth|a:1:{s:9:"AdminUser";a:27:{s:2:"id";s:1:"2";s:10:"company_no";s:1:"1";s:11:"division_no";s:1:"4";s:13:"department_no";s:1:"2";s:10:"section_no";s:1:"2";s:12:"authority_no";s:1:"1";s:14:"responsibility";s:1:"2";s:10:"first_name";s:6:"将禎";s:9:"last_name";s:6:"柳田";s:12:"mail_address";s:27:"pikaichi.yanagita@gmail.com";s:13:"effectiveness";s:1:"1";s:14:"job_entry_date";s:10:"2010-08-23";s:11:"ic_card_key";s:4:"0100";s:11:"position_no";s:1:"5";s:17:"employee_category";s:1:"4";s:10:"retirement";s:1:"0";s:15:"retirement_date";N;s:8:"on_leave";s:1:"0";s:21:"attendance_adjustment";s:1:"0";s:10:"shift_work";s:1:"1";s:10:"start_time";N;s:12:"closing_time";N;s:8:"birthday";N;s:12:"basic_uptime";N;s:15:"holiday_pattern";s:1:"9";s:15:"relation_emp_no";s:3:"100";s:8:"username";s:15:"柳田　将禎";}}', 4715423739);
INSERT INTO `cake_sessions` (`id`, `data`, `expires`) VALUES('8noi494akn7eo88qi3k54jr3i4', 'Config|a:3:{s:9:"userAgent";s:32:"5ca4b7d57b54aa32f60fc0cf3d55f4c3";s:4:"time";i:4715422254;s:9:"countdown";i:10;}Message|a:1:{s:4:"auth";a:4:{s:7:"message";s:47:"You are not authorized to access that location.";s:3:"key";s:4:"auth";s:7:"element";s:13:"Flash/default";s:6:"params";a:0:{}}}Auth|a:1:{s:9:"AdminUser";a:27:{s:2:"id";s:1:"2";s:10:"company_no";s:1:"1";s:11:"division_no";s:1:"4";s:13:"department_no";s:1:"2";s:10:"section_no";s:1:"2";s:12:"authority_no";s:1:"1";s:14:"responsibility";s:1:"2";s:10:"first_name";s:6:"将禎";s:9:"last_name";s:6:"柳田";s:12:"mail_address";s:27:"pikaichi.yanagita@gmail.com";s:13:"effectiveness";s:1:"1";s:14:"job_entry_date";s:10:"2010-08-23";s:11:"ic_card_key";s:4:"0100";s:11:"position_no";s:1:"5";s:17:"employee_category";s:1:"4";s:10:"retirement";s:1:"0";s:15:"retirement_date";N;s:8:"on_leave";s:1:"0";s:21:"attendance_adjustment";s:1:"0";s:10:"shift_work";s:1:"1";s:10:"start_time";N;s:12:"closing_time";N;s:8:"birthday";N;s:12:"basic_uptime";N;s:15:"holiday_pattern";s:1:"9";s:15:"relation_emp_no";s:3:"100";s:8:"username";s:15:"柳田　将禎";}}', 4715422254);
INSERT INTO `cake_sessions` (`id`, `data`, `expires`) VALUES('931ei2ktfafcg1ne130vmbm2p6', 'Config|a:3:{s:9:"userAgent";s:32:"d14c664f2b60198160b9a86162b3a6a9";s:4:"time";i:4709922042;s:9:"countdown";i:10;}Message|a:1:{s:4:"auth";a:4:{s:7:"message";s:47:"You are not authorized to access that location.";s:3:"key";s:4:"auth";s:7:"element";s:13:"Flash/default";s:6:"params";a:0:{}}}Auth|a:1:{s:9:"AdminUser";a:26:{s:2:"id";s:1:"8";s:10:"company_no";s:1:"1";s:11:"division_no";s:1:"1";s:13:"department_no";s:1:"3";s:10:"section_no";s:1:"3";s:12:"authority_no";s:1:"1";s:14:"responsibility";s:1:"2";s:10:"first_name";s:6:"亜耶";s:9:"last_name";s:6:"廣田";s:12:"mail_address";s:25:"pikaichi.hirota@gmail.com";s:13:"effectiveness";s:1:"1";s:14:"job_entry_date";s:10:"2014-11-10";s:11:"ic_card_key";s:4:"1007";s:11:"position_no";s:1:"1";s:17:"employee_category";s:1:"1";s:10:"retirement";s:1:"0";s:15:"retirement_date";N;s:8:"on_leave";s:1:"0";s:21:"attendance_adjustment";s:1:"0";s:10:"shift_work";s:1:"1";s:10:"start_time";N;s:12:"closing_time";N;s:8:"birthday";N;s:12:"basic_uptime";N;s:15:"holiday_pattern";s:1:"9";s:8:"username";s:15:"廣田　亜耶";}}', 4709922043);
INSERT INTO `cake_sessions` (`id`, `data`, `expires`) VALUES('9kokgs0vvdm6qcfs2gu4q61od2', 'Config|a:3:{s:9:"userAgent";s:32:"b4d321a5c7ab1400ab744663dfa0130b";s:4:"time";i:4709759190;s:9:"countdown";i:10;}Message|a:1:{s:4:"auth";a:4:{s:7:"message";s:47:"You are not authorized to access that location.";s:3:"key";s:4:"auth";s:7:"element";s:13:"Flash/default";s:6:"params";a:0:{}}}Auth|a:1:{s:8:"redirect";s:1:"/";}', 4709759190);
INSERT INTO `cake_sessions` (`id`, `data`, `expires`) VALUES('bivju2d0ob7lpin2acqneren71', 'Config|a:3:{s:9:"userAgent";s:32:"5ca4b7d57b54aa32f60fc0cf3d55f4c3";s:4:"time";i:4715422477;s:9:"countdown";i:10;}', 4715422477);
INSERT INTO `cake_sessions` (`id`, `data`, `expires`) VALUES('bl6q1n84dbsmkr52dr6jt5jvb2', 'Config|a:3:{s:9:"userAgent";s:32:"86f918eb1a334866270ec98313e7b829";s:4:"time";i:4715337143;s:9:"countdown";i:10;}Message|a:1:{s:4:"auth";a:4:{s:7:"message";s:47:"You are not authorized to access that location.";s:3:"key";s:4:"auth";s:7:"element";s:13:"Flash/default";s:6:"params";a:0:{}}}Auth|a:1:{s:9:"AdminUser";a:27:{s:2:"id";s:1:"2";s:10:"company_no";s:1:"1";s:11:"division_no";s:1:"4";s:13:"department_no";s:1:"2";s:10:"section_no";s:1:"2";s:12:"authority_no";s:1:"1";s:14:"responsibility";s:1:"2";s:10:"first_name";s:6:"将禎";s:9:"last_name";s:6:"柳田";s:12:"mail_address";s:27:"pikaichi.yanagita@gmail.com";s:13:"effectiveness";s:1:"1";s:14:"job_entry_date";s:10:"2010-08-23";s:11:"ic_card_key";s:4:"0100";s:11:"position_no";s:1:"5";s:17:"employee_category";s:1:"4";s:10:"retirement";s:1:"0";s:15:"retirement_date";N;s:8:"on_leave";s:1:"0";s:21:"attendance_adjustment";s:1:"0";s:10:"shift_work";s:1:"1";s:10:"start_time";N;s:12:"closing_time";N;s:8:"birthday";N;s:12:"basic_uptime";N;s:15:"holiday_pattern";s:1:"9";s:15:"relation_emp_no";s:3:"100";s:8:"username";s:15:"柳田　将禎";}}', 4715337143);
INSERT INTO `cake_sessions` (`id`, `data`, `expires`) VALUES('bm6ggmaftgt127divm1d247f60', 'Config|a:3:{s:9:"userAgent";s:32:"977101f4456b7509d781fe16fd59104f";s:4:"time";i:4712844009;s:9:"countdown";i:10;}Message|a:1:{s:4:"auth";a:4:{s:7:"message";s:47:"You are not authorized to access that location.";s:3:"key";s:4:"auth";s:7:"element";s:13:"Flash/default";s:6:"params";a:0:{}}}Auth|a:1:{s:9:"AdminUser";a:26:{s:2:"id";s:1:"7";s:10:"company_no";s:1:"1";s:11:"division_no";s:1:"1";s:13:"department_no";s:1:"3";s:10:"section_no";s:1:"3";s:12:"authority_no";s:1:"2";s:14:"responsibility";s:1:"2";s:10:"first_name";s:6:"綾香";s:9:"last_name";s:6:"坪井";s:12:"mail_address";s:25:"pikaichi.uchino@gmail.com";s:13:"effectiveness";s:1:"1";s:14:"job_entry_date";s:10:"2013-10-01";s:11:"ic_card_key";s:4:"1003";s:11:"position_no";s:1:"1";s:17:"employee_category";s:1:"1";s:10:"retirement";s:1:"0";s:15:"retirement_date";N;s:8:"on_leave";s:1:"0";s:21:"attendance_adjustment";s:1:"0";s:10:"shift_work";s:1:"1";s:10:"start_time";N;s:12:"closing_time";N;s:8:"birthday";N;s:12:"basic_uptime";N;s:15:"holiday_pattern";s:1:"9";s:8:"username";s:15:"坪井　綾香";}}', 4712844837);
INSERT INTO `cake_sessions` (`id`, `data`, `expires`) VALUES('bsigvgm23acfsorphm9mkbuii7', 'Config|a:3:{s:9:"userAgent";s:32:"5ca4b7d57b54aa32f60fc0cf3d55f4c3";s:4:"time";i:4715370121;s:9:"countdown";i:10;}Message|a:1:{s:4:"auth";a:4:{s:7:"message";s:47:"You are not authorized to access that location.";s:3:"key";s:4:"auth";s:7:"element";s:13:"Flash/default";s:6:"params";a:0:{}}}Auth|a:1:{s:9:"AdminUser";a:27:{s:2:"id";s:1:"2";s:10:"company_no";s:1:"1";s:11:"division_no";s:1:"4";s:13:"department_no";s:1:"2";s:10:"section_no";s:1:"2";s:12:"authority_no";s:1:"1";s:14:"responsibility";s:1:"2";s:10:"first_name";s:6:"将禎";s:9:"last_name";s:6:"柳田";s:12:"mail_address";s:27:"pikaichi.yanagita@gmail.com";s:13:"effectiveness";s:1:"1";s:14:"job_entry_date";s:10:"2010-08-23";s:11:"ic_card_key";s:4:"0100";s:11:"position_no";s:1:"5";s:17:"employee_category";s:1:"4";s:10:"retirement";s:1:"0";s:15:"retirement_date";N;s:8:"on_leave";s:1:"0";s:21:"attendance_adjustment";s:1:"0";s:10:"shift_work";s:1:"1";s:10:"start_time";N;s:12:"closing_time";N;s:8:"birthday";N;s:12:"basic_uptime";N;s:15:"holiday_pattern";s:1:"9";s:15:"relation_emp_no";s:3:"100";s:8:"username";s:15:"柳田　将禎";}}', 4715370121);
INSERT INTO `cake_sessions` (`id`, `data`, `expires`) VALUES('c1nat21dcvmcf0a9oheje0h2a6', 'Config|a:3:{s:9:"userAgent";s:32:"977101f4456b7509d781fe16fd59104f";s:4:"time";i:4715200075;s:9:"countdown";i:10;}', 4715200075);
INSERT INTO `cake_sessions` (`id`, `data`, `expires`) VALUES('c4bkn757j1jdbp3418aia6avv3', 'Config|a:3:{s:9:"userAgent";s:32:"5ca4b7d57b54aa32f60fc0cf3d55f4c3";s:4:"time";i:4715422893;s:9:"countdown";i:10;}', 4715422893);
INSERT INTO `cake_sessions` (`id`, `data`, `expires`) VALUES('cg8f075i8ol61q61tnhqjqfqj4', 'Config|a:3:{s:9:"userAgent";s:32:"0298b75623a8136ace6c58975711f9ce";s:4:"time";i:4712485477;s:9:"countdown";i:10;}Message|a:1:{s:4:"auth";a:4:{s:7:"message";s:47:"You are not authorized to access that location.";s:3:"key";s:4:"auth";s:7:"element";s:13:"Flash/default";s:6:"params";a:0:{}}}Auth|a:1:{s:9:"AdminUser";a:26:{s:2:"id";s:1:"8";s:10:"company_no";s:1:"1";s:11:"division_no";s:1:"1";s:13:"department_no";s:1:"3";s:10:"section_no";s:1:"3";s:12:"authority_no";s:1:"1";s:14:"responsibility";s:1:"2";s:10:"first_name";s:6:"亜耶";s:9:"last_name";s:6:"廣田";s:12:"mail_address";s:25:"pikaichi.hirota@gmail.com";s:13:"effectiveness";s:1:"1";s:14:"job_entry_date";s:10:"2014-11-10";s:11:"ic_card_key";s:4:"1007";s:11:"position_no";s:1:"1";s:17:"employee_category";s:1:"1";s:10:"retirement";s:1:"0";s:15:"retirement_date";N;s:8:"on_leave";s:1:"0";s:21:"attendance_adjustment";s:1:"0";s:10:"shift_work";s:1:"1";s:10:"start_time";N;s:12:"closing_time";N;s:8:"birthday";N;s:12:"basic_uptime";N;s:15:"holiday_pattern";s:1:"9";s:8:"username";s:15:"廣田　亜耶";}}', 4712485477);
INSERT INTO `cake_sessions` (`id`, `data`, `expires`) VALUES('dg83b2fp14r1c7e44ce2kq87s6', 'Config|a:3:{s:9:"userAgent";s:32:"3b73aec93e80beda47761d181b14d7b1";s:4:"time";i:4711490294;s:9:"countdown";i:10;}Message|a:1:{s:4:"auth";a:4:{s:7:"message";s:47:"You are not authorized to access that location.";s:3:"key";s:4:"auth";s:7:"element";s:13:"Flash/default";s:6:"params";a:0:{}}}Auth|a:1:{s:9:"AdminUser";a:26:{s:2:"id";s:1:"8";s:10:"company_no";s:1:"1";s:11:"division_no";s:1:"1";s:13:"department_no";s:1:"3";s:10:"section_no";s:1:"3";s:12:"authority_no";s:1:"1";s:14:"responsibility";s:1:"2";s:10:"first_name";s:6:"亜耶";s:9:"last_name";s:6:"廣田";s:12:"mail_address";s:25:"pikaichi.hirota@gmail.com";s:13:"effectiveness";s:1:"1";s:14:"job_entry_date";s:10:"2014-11-10";s:11:"ic_card_key";s:4:"1007";s:11:"position_no";s:1:"1";s:17:"employee_category";s:1:"1";s:10:"retirement";s:1:"0";s:15:"retirement_date";N;s:8:"on_leave";s:1:"0";s:21:"attendance_adjustment";s:1:"0";s:10:"shift_work";s:1:"1";s:10:"start_time";N;s:12:"closing_time";N;s:8:"birthday";N;s:12:"basic_uptime";N;s:15:"holiday_pattern";s:1:"9";s:8:"username";s:15:"廣田　亜耶";}}', 4711490294);
INSERT INTO `cake_sessions` (`id`, `data`, `expires`) VALUES('ejhdu3ctnp4d49q19jho3qp4h4', 'Config|a:3:{s:9:"userAgent";s:32:"3b73aec93e80beda47761d181b14d7b1";s:4:"time";i:4711456733;s:9:"countdown";i:10;}Message|a:1:{s:4:"auth";a:4:{s:7:"message";s:47:"You are not authorized to access that location.";s:3:"key";s:4:"auth";s:7:"element";s:13:"Flash/default";s:6:"params";a:0:{}}}Auth|a:1:{s:9:"AdminUser";a:26:{s:2:"id";s:2:"32";s:10:"company_no";s:1:"1";s:11:"division_no";s:1:"1";s:13:"department_no";s:1:"3";s:10:"section_no";s:1:"3";s:12:"authority_no";s:1:"1";s:14:"responsibility";s:1:"2";s:10:"first_name";s:9:"テスト";s:9:"last_name";s:15:"プロネット";s:12:"mail_address";s:16:"pronet@gmail.com";s:13:"effectiveness";s:1:"1";s:14:"job_entry_date";s:10:"2014-11-10";s:11:"ic_card_key";s:4:"1007";s:11:"position_no";s:1:"1";s:17:"employee_category";s:1:"1";s:10:"retirement";s:1:"0";s:15:"retirement_date";N;s:8:"on_leave";s:1:"0";s:21:"attendance_adjustment";s:1:"0";s:10:"shift_work";s:1:"1";s:10:"start_time";N;s:12:"closing_time";N;s:8:"birthday";N;s:12:"basic_uptime";N;s:15:"holiday_pattern";s:1:"9";s:8:"username";s:27:"プロネット　テスト";}}', 4711456733);
INSERT INTO `cake_sessions` (`id`, `data`, `expires`) VALUES('esu5p328gjch5o0cjcctftlbs2', 'Config|a:3:{s:9:"userAgent";s:32:"c18f97aefc462939b2f3d3cdc8d91f2f";s:4:"time";i:4715118449;s:9:"countdown";i:10;}', 4715118449);
INSERT INTO `cake_sessions` (`id`, `data`, `expires`) VALUES('fh3nkicjiih3c1fteq0a0dt235', 'Config|a:3:{s:9:"userAgent";s:32:"5ca4b7d57b54aa32f60fc0cf3d55f4c3";s:4:"time";i:4715337979;s:9:"countdown";i:10;}Message|a:1:{s:4:"auth";a:4:{s:7:"message";s:47:"You are not authorized to access that location.";s:3:"key";s:4:"auth";s:7:"element";s:13:"Flash/default";s:6:"params";a:0:{}}}Auth|a:1:{s:9:"AdminUser";a:27:{s:2:"id";s:1:"2";s:10:"company_no";s:1:"1";s:11:"division_no";s:1:"4";s:13:"department_no";s:1:"2";s:10:"section_no";s:1:"2";s:12:"authority_no";s:1:"1";s:14:"responsibility";s:1:"2";s:10:"first_name";s:6:"将禎";s:9:"last_name";s:6:"柳田";s:12:"mail_address";s:27:"pikaichi.yanagita@gmail.com";s:13:"effectiveness";s:1:"1";s:14:"job_entry_date";s:10:"2010-08-23";s:11:"ic_card_key";s:4:"0100";s:11:"position_no";s:1:"5";s:17:"employee_category";s:1:"4";s:10:"retirement";s:1:"0";s:15:"retirement_date";N;s:8:"on_leave";s:1:"0";s:21:"attendance_adjustment";s:1:"0";s:10:"shift_work";s:1:"1";s:10:"start_time";N;s:12:"closing_time";N;s:8:"birthday";N;s:12:"basic_uptime";N;s:15:"holiday_pattern";s:1:"9";s:15:"relation_emp_no";s:3:"100";s:8:"username";s:15:"柳田　将禎";}}', 4715337979);
INSERT INTO `cake_sessions` (`id`, `data`, `expires`) VALUES('fkbvqvas98ridbeu2sncu684s3', 'Config|a:3:{s:9:"userAgent";s:32:"5ca4b7d57b54aa32f60fc0cf3d55f4c3";s:4:"time";i:4715312038;s:9:"countdown";i:10;}Message|a:1:{s:4:"auth";a:4:{s:7:"message";s:47:"You are not authorized to access that location.";s:3:"key";s:4:"auth";s:7:"element";s:13:"Flash/default";s:6:"params";a:0:{}}}Auth|a:0:{}', 4715312038);
INSERT INTO `cake_sessions` (`id`, `data`, `expires`) VALUES('fp0cmpp43uj5ktuj82q1m42436', 'Config|a:3:{s:9:"userAgent";s:32:"5ca4b7d57b54aa32f60fc0cf3d55f4c3";s:4:"time";i:4715423669;s:9:"countdown";i:10;}Message|a:1:{s:4:"auth";a:4:{s:7:"message";s:47:"You are not authorized to access that location.";s:3:"key";s:4:"auth";s:7:"element";s:13:"Flash/default";s:6:"params";a:0:{}}}Auth|a:1:{s:9:"AdminUser";a:27:{s:2:"id";s:1:"2";s:10:"company_no";s:1:"1";s:11:"division_no";s:1:"4";s:13:"department_no";s:1:"2";s:10:"section_no";s:1:"2";s:12:"authority_no";s:1:"1";s:14:"responsibility";s:1:"2";s:10:"first_name";s:6:"将禎";s:9:"last_name";s:6:"柳田";s:12:"mail_address";s:27:"pikaichi.yanagita@gmail.com";s:13:"effectiveness";s:1:"1";s:14:"job_entry_date";s:10:"2010-08-23";s:11:"ic_card_key";s:4:"0100";s:11:"position_no";s:1:"5";s:17:"employee_category";s:1:"4";s:10:"retirement";s:1:"0";s:15:"retirement_date";N;s:8:"on_leave";s:1:"0";s:21:"attendance_adjustment";s:1:"0";s:10:"shift_work";s:1:"1";s:10:"start_time";N;s:12:"closing_time";N;s:8:"birthday";N;s:12:"basic_uptime";N;s:15:"holiday_pattern";s:1:"9";s:15:"relation_emp_no";s:3:"100";s:8:"username";s:15:"柳田　将禎";}}', 4715423669);
INSERT INTO `cake_sessions` (`id`, `data`, `expires`) VALUES('ft0klsl9id3tlvmpcq27tvt8t5', 'Config|a:3:{s:9:"userAgent";s:32:"c18f97aefc462939b2f3d3cdc8d91f2f";s:4:"time";i:4715223361;s:9:"countdown";i:10;}', 4715223361);
INSERT INTO `cake_sessions` (`id`, `data`, `expires`) VALUES('g9ikng99tbds02nrdkv0b36on2', 'Config|a:3:{s:9:"userAgent";s:32:"5ca4b7d57b54aa32f60fc0cf3d55f4c3";s:4:"time";i:4715300698;s:9:"countdown";i:10;}Message|a:1:{s:4:"auth";a:4:{s:7:"message";s:47:"You are not authorized to access that location.";s:3:"key";s:4:"auth";s:7:"element";s:13:"Flash/default";s:6:"params";a:0:{}}}Auth|a:1:{s:9:"AdminUser";a:27:{s:2:"id";s:1:"2";s:10:"company_no";s:1:"1";s:11:"division_no";s:1:"4";s:13:"department_no";s:1:"2";s:10:"section_no";s:1:"2";s:12:"authority_no";s:1:"1";s:14:"responsibility";s:1:"2";s:10:"first_name";s:6:"将禎";s:9:"last_name";s:6:"柳田";s:12:"mail_address";s:27:"pikaichi.yanagita@gmail.com";s:13:"effectiveness";s:1:"1";s:14:"job_entry_date";s:10:"2010-08-23";s:11:"ic_card_key";s:4:"0100";s:11:"position_no";s:1:"5";s:17:"employee_category";s:1:"4";s:10:"retirement";s:1:"0";s:15:"retirement_date";N;s:8:"on_leave";s:1:"0";s:21:"attendance_adjustment";s:1:"0";s:10:"shift_work";s:1:"1";s:10:"start_time";N;s:12:"closing_time";N;s:8:"birthday";N;s:12:"basic_uptime";N;s:15:"holiday_pattern";s:1:"9";s:15:"relation_emp_no";s:3:"100";s:8:"username";s:15:"柳田　将禎";}}', 4715300698);
INSERT INTO `cake_sessions` (`id`, `data`, `expires`) VALUES('gdes5ioijosakuqdjfr3tgsqk0', 'Config|a:3:{s:9:"userAgent";s:32:"c8bbf9614dd3d782f1f924dd2eca44d8";s:4:"time";i:4715308331;s:9:"countdown";i:10;}Message|a:1:{s:4:"auth";a:4:{s:7:"message";s:47:"You are not authorized to access that location.";s:3:"key";s:4:"auth";s:7:"element";s:13:"Flash/default";s:6:"params";a:0:{}}}Auth|a:0:{}', 4715308331);
INSERT INTO `cake_sessions` (`id`, `data`, `expires`) VALUES('gfcfksth9e43rjl6aarqva9s05', 'Config|a:3:{s:9:"userAgent";s:32:"2490bcb99d1efbaedd63314fbfa4c35c";s:4:"time";i:4715129876;s:9:"countdown";i:10;}Message|a:1:{s:4:"auth";a:4:{s:7:"message";s:47:"You are not authorized to access that location.";s:3:"key";s:4:"auth";s:7:"element";s:13:"Flash/default";s:6:"params";a:0:{}}}Auth|a:1:{s:9:"AdminUser";a:27:{s:2:"id";s:1:"2";s:10:"company_no";s:1:"1";s:11:"division_no";s:1:"4";s:13:"department_no";s:1:"2";s:10:"section_no";s:1:"2";s:12:"authority_no";s:1:"1";s:14:"responsibility";s:1:"2";s:10:"first_name";s:6:"将禎";s:9:"last_name";s:6:"柳田";s:12:"mail_address";s:27:"pikaichi.yanagita@gmail.com";s:13:"effectiveness";s:1:"1";s:14:"job_entry_date";s:10:"2010-08-23";s:11:"ic_card_key";s:4:"0100";s:11:"position_no";s:1:"5";s:17:"employee_category";s:1:"4";s:10:"retirement";s:1:"0";s:15:"retirement_date";N;s:8:"on_leave";s:1:"0";s:21:"attendance_adjustment";s:1:"0";s:10:"shift_work";s:1:"1";s:10:"start_time";N;s:12:"closing_time";N;s:8:"birthday";N;s:12:"basic_uptime";N;s:15:"holiday_pattern";s:1:"9";s:15:"relation_emp_no";s:3:"100";s:8:"username";s:15:"柳田　将禎";}}', 4715129876);
INSERT INTO `cake_sessions` (`id`, `data`, `expires`) VALUES('glifc1nhfcel1etckgg6597o42', 'Config|a:3:{s:9:"userAgent";s:32:"ed569cbe4b8fbfe66141b9bb2381ec2a";s:4:"time";i:4715130667;s:9:"countdown";i:10;}Auth|a:1:{s:9:"AdminUser";a:27:{s:2:"id";s:1:"2";s:10:"company_no";s:1:"1";s:11:"division_no";s:1:"4";s:13:"department_no";s:1:"2";s:10:"section_no";s:1:"2";s:12:"authority_no";s:1:"1";s:14:"responsibility";s:1:"2";s:10:"first_name";s:6:"将禎";s:9:"last_name";s:6:"柳田";s:12:"mail_address";s:27:"pikaichi.yanagita@gmail.com";s:13:"effectiveness";s:1:"1";s:14:"job_entry_date";s:10:"2010-08-23";s:11:"ic_card_key";s:4:"0100";s:11:"position_no";s:1:"5";s:17:"employee_category";s:1:"4";s:10:"retirement";s:1:"0";s:15:"retirement_date";N;s:8:"on_leave";s:1:"0";s:21:"attendance_adjustment";s:1:"0";s:10:"shift_work";s:1:"1";s:10:"start_time";N;s:12:"closing_time";N;s:8:"birthday";N;s:12:"basic_uptime";N;s:15:"holiday_pattern";s:1:"9";s:15:"relation_emp_no";s:3:"100";s:8:"username";s:15:"柳田　将禎";}}', 4715130667);
INSERT INTO `cake_sessions` (`id`, `data`, `expires`) VALUES('gvs09b9f5qom4aj7pdsiv4mr17', 'Config|a:3:{s:9:"userAgent";s:32:"c18f97aefc462939b2f3d3cdc8d91f2f";s:4:"time";i:4715130612;s:9:"countdown";i:10;}', 4715130612);
INSERT INTO `cake_sessions` (`id`, `data`, `expires`) VALUES('h09bvfjprldttugei25pla1845', 'Config|a:3:{s:9:"userAgent";s:32:"077a67061cfa7fb160b6c72b175ce03a";s:4:"time";i:4715336420;s:9:"countdown";i:10;}Auth|a:1:{s:9:"AdminUser";a:27:{s:2:"id";s:1:"3";s:10:"company_no";s:1:"1";s:11:"division_no";s:1:"1";s:13:"department_no";s:1:"1";s:10:"section_no";s:1:"1";s:12:"authority_no";s:1:"2";s:14:"responsibility";s:1:"2";s:10:"first_name";s:6:"慎吾";s:9:"last_name";s:6:"坪井";s:12:"mail_address";s:25:"pikaichi.tsuboi@gmail.com";s:13:"effectiveness";s:1:"1";s:14:"job_entry_date";s:10:"2010-11-01";s:11:"ic_card_key";s:4:"0101";s:11:"position_no";s:1:"4";s:17:"employee_category";s:1:"4";s:10:"retirement";s:1:"0";s:15:"retirement_date";N;s:8:"on_leave";s:1:"0";s:21:"attendance_adjustment";s:1:"0";s:10:"shift_work";s:1:"1";s:10:"start_time";N;s:12:"closing_time";N;s:8:"birthday";N;s:12:"basic_uptime";N;s:15:"holiday_pattern";s:1:"9";s:15:"relation_emp_no";s:3:"101";s:8:"username";s:15:"坪井　慎吾";}}', 4715336420);
INSERT INTO `cake_sessions` (`id`, `data`, `expires`) VALUES('h42jjksuk0phli7nrf09nidgg6', 'Config|a:3:{s:9:"userAgent";s:32:"0298b75623a8136ace6c58975711f9ce";s:4:"time";i:4714020927;s:9:"countdown";i:10;}Message|a:1:{s:4:"auth";a:4:{s:7:"message";s:47:"You are not authorized to access that location.";s:3:"key";s:4:"auth";s:7:"element";s:13:"Flash/default";s:6:"params";a:0:{}}}Auth|a:1:{s:9:"AdminUser";a:27:{s:2:"id";s:2:"31";s:10:"company_no";s:1:"1";s:11:"division_no";s:1:"1";s:13:"department_no";s:1:"3";s:10:"section_no";s:1:"3";s:12:"authority_no";s:1:"2";s:14:"responsibility";s:1:"0";s:10:"first_name";s:9:"ちはる";s:9:"last_name";s:6:"田邉";s:12:"mail_address";s:25:"pikaichi.tanabe@gmail.com";s:13:"effectiveness";s:1:"1";s:14:"job_entry_date";s:10:"2019-02-12";s:11:"ic_card_key";s:4:"2017";s:11:"position_no";N;s:17:"employee_category";s:1:"2";s:10:"retirement";s:1:"0";s:15:"retirement_date";N;s:8:"on_leave";s:1:"0";s:21:"attendance_adjustment";s:1:"0";s:10:"shift_work";s:1:"1";s:10:"start_time";N;s:12:"closing_time";N;s:8:"birthday";N;s:12:"basic_uptime";N;s:15:"holiday_pattern";s:1:"9";s:15:"relation_emp_no";s:4:"2017";s:8:"username";s:18:"田邉　ちはる";}}', 4714020927);
INSERT INTO `cake_sessions` (`id`, `data`, `expires`) VALUES('h4j3io9uib27iis36s5vl1k555', 'Config|a:3:{s:9:"userAgent";s:32:"5ca4b7d57b54aa32f60fc0cf3d55f4c3";s:4:"time";i:4715311412;s:9:"countdown";i:10;}Message|a:1:{s:4:"auth";a:4:{s:7:"message";s:47:"You are not authorized to access that location.";s:3:"key";s:4:"auth";s:7:"element";s:13:"Flash/default";s:6:"params";a:0:{}}}Auth|a:1:{s:9:"AdminUser";a:27:{s:2:"id";s:1:"2";s:10:"company_no";s:1:"1";s:11:"division_no";s:1:"4";s:13:"department_no";s:1:"2";s:10:"section_no";s:1:"2";s:12:"authority_no";s:1:"1";s:14:"responsibility";s:1:"2";s:10:"first_name";s:6:"将禎";s:9:"last_name";s:6:"柳田";s:12:"mail_address";s:27:"pikaichi.yanagita@gmail.com";s:13:"effectiveness";s:1:"1";s:14:"job_entry_date";s:10:"2010-08-23";s:11:"ic_card_key";s:4:"0100";s:11:"position_no";s:1:"5";s:17:"employee_category";s:1:"4";s:10:"retirement";s:1:"0";s:15:"retirement_date";N;s:8:"on_leave";s:1:"0";s:21:"attendance_adjustment";s:1:"0";s:10:"shift_work";s:1:"1";s:10:"start_time";N;s:12:"closing_time";N;s:8:"birthday";N;s:12:"basic_uptime";N;s:15:"holiday_pattern";s:1:"9";s:15:"relation_emp_no";s:3:"100";s:8:"username";s:15:"柳田　将禎";}}', 4715311412);
INSERT INTO `cake_sessions` (`id`, `data`, `expires`) VALUES('h5n9boisl0m7u7tn9iagd5lqf7', 'Config|a:3:{s:9:"userAgent";s:32:"977101f4456b7509d781fe16fd59104f";s:4:"time";i:4712884604;s:9:"countdown";i:10;}Message|a:1:{s:4:"auth";a:4:{s:7:"message";s:47:"You are not authorized to access that location.";s:3:"key";s:4:"auth";s:7:"element";s:13:"Flash/default";s:6:"params";a:0:{}}}Auth|a:1:{s:9:"AdminUser";a:26:{s:2:"id";s:1:"8";s:10:"company_no";s:1:"1";s:11:"division_no";s:1:"1";s:13:"department_no";s:1:"3";s:10:"section_no";s:1:"3";s:12:"authority_no";s:1:"1";s:14:"responsibility";s:1:"2";s:10:"first_name";s:6:"亜耶";s:9:"last_name";s:6:"廣田";s:12:"mail_address";s:25:"pikaichi.hirota@gmail.com";s:13:"effectiveness";s:1:"1";s:14:"job_entry_date";s:10:"2014-11-10";s:11:"ic_card_key";s:4:"1007";s:11:"position_no";s:1:"1";s:17:"employee_category";s:1:"1";s:10:"retirement";s:1:"0";s:15:"retirement_date";N;s:8:"on_leave";s:1:"0";s:21:"attendance_adjustment";s:1:"0";s:10:"shift_work";s:1:"1";s:10:"start_time";N;s:12:"closing_time";N;s:8:"birthday";N;s:12:"basic_uptime";N;s:15:"holiday_pattern";s:1:"9";s:8:"username";s:15:"廣田　亜耶";}}', 4712884608);
INSERT INTO `cake_sessions` (`id`, `data`, `expires`) VALUES('htqvg0fjaaf1756vfaa0nrr1d5', 'Config|a:3:{s:9:"userAgent";s:32:"634190855efde7e3f6eadff3a7437e00";s:4:"time";i:4715312054;s:9:"countdown";i:10;}Message|a:1:{s:4:"auth";a:4:{s:7:"message";s:47:"You are not authorized to access that location.";s:3:"key";s:4:"auth";s:7:"element";s:13:"Flash/default";s:6:"params";a:0:{}}}Auth|a:1:{s:9:"AdminUser";a:27:{s:2:"id";s:1:"7";s:10:"company_no";s:1:"1";s:11:"division_no";s:1:"1";s:13:"department_no";s:1:"3";s:10:"section_no";s:1:"3";s:12:"authority_no";s:1:"2";s:14:"responsibility";s:1:"3";s:10:"first_name";s:6:"綾香";s:9:"last_name";s:6:"坪井";s:12:"mail_address";s:25:"pikaichi.uchino@gmail.com";s:13:"effectiveness";s:1:"1";s:14:"job_entry_date";s:10:"2013-10-01";s:11:"ic_card_key";s:4:"1003";s:11:"position_no";s:1:"1";s:17:"employee_category";s:1:"1";s:10:"retirement";s:1:"0";s:15:"retirement_date";N;s:8:"on_leave";s:1:"0";s:21:"attendance_adjustment";s:1:"0";s:10:"shift_work";s:1:"1";s:10:"start_time";N;s:12:"closing_time";N;s:8:"birthday";N;s:12:"basic_uptime";N;s:15:"holiday_pattern";s:1:"9";s:15:"relation_emp_no";s:4:"1003";s:8:"username";s:15:"坪井　綾香";}}', 4715312054);
INSERT INTO `cake_sessions` (`id`, `data`, `expires`) VALUES('i6pjbq6nmbmpi6d7er81cik5p2', 'Config|a:3:{s:9:"userAgent";s:32:"e04d3a82e71ee78f9f03601fde6e2750";s:4:"time";i:4711485249;s:9:"countdown";i:10;}Message|a:1:{s:4:"auth";a:4:{s:7:"message";s:47:"You are not authorized to access that location.";s:3:"key";s:4:"auth";s:7:"element";s:13:"Flash/default";s:6:"params";a:0:{}}}Auth|a:1:{s:8:"redirect";s:7:"/Lists/";}', 4711485249);
INSERT INTO `cake_sessions` (`id`, `data`, `expires`) VALUES('jddksam18p3gu0ucg4toh2ntl7', 'Config|a:3:{s:9:"userAgent";s:32:"c18f97aefc462939b2f3d3cdc8d91f2f";s:4:"time";i:4715118655;s:9:"countdown";i:10;}', 4715118655);
INSERT INTO `cake_sessions` (`id`, `data`, `expires`) VALUES('juhaq8uocphipk3dqi671bdrp3', 'Config|a:3:{s:9:"userAgent";s:32:"33dc2fced1852a4114d624d5e058f517";s:4:"time";i:4715406130;s:9:"countdown";i:10;}Message|a:1:{s:4:"auth";a:4:{s:7:"message";s:47:"You are not authorized to access that location.";s:3:"key";s:4:"auth";s:7:"element";s:13:"Flash/default";s:6:"params";a:0:{}}}Auth|a:1:{s:9:"AdminUser";a:27:{s:2:"id";s:1:"2";s:10:"company_no";s:1:"1";s:11:"division_no";s:1:"4";s:13:"department_no";s:1:"2";s:10:"section_no";s:1:"2";s:12:"authority_no";s:1:"1";s:14:"responsibility";s:1:"2";s:10:"first_name";s:6:"将禎";s:9:"last_name";s:6:"柳田";s:12:"mail_address";s:27:"pikaichi.yanagita@gmail.com";s:13:"effectiveness";s:1:"1";s:14:"job_entry_date";s:10:"2010-08-23";s:11:"ic_card_key";s:4:"0100";s:11:"position_no";s:1:"5";s:17:"employee_category";s:1:"4";s:10:"retirement";s:1:"0";s:15:"retirement_date";N;s:8:"on_leave";s:1:"0";s:21:"attendance_adjustment";s:1:"0";s:10:"shift_work";s:1:"1";s:10:"start_time";N;s:12:"closing_time";N;s:8:"birthday";N;s:12:"basic_uptime";N;s:15:"holiday_pattern";s:1:"9";s:15:"relation_emp_no";s:3:"100";s:8:"username";s:15:"柳田　将禎";}}', 4715406130);
INSERT INTO `cake_sessions` (`id`, `data`, `expires`) VALUES('k4gl9vc19r087fsioefailjlr6', 'Config|a:3:{s:9:"userAgent";s:32:"c3016251d9fc5bc072a19e5912778677";s:4:"time";i:4715131228;s:9:"countdown";i:10;}Message|a:1:{s:4:"auth";a:4:{s:7:"message";s:47:"You are not authorized to access that location.";s:3:"key";s:4:"auth";s:7:"element";s:13:"Flash/default";s:6:"params";a:0:{}}}Auth|a:1:{s:9:"AdminUser";a:27:{s:2:"id";s:1:"2";s:10:"company_no";s:1:"1";s:11:"division_no";s:1:"4";s:13:"department_no";s:1:"2";s:10:"section_no";s:1:"2";s:12:"authority_no";s:1:"1";s:14:"responsibility";s:1:"2";s:10:"first_name";s:6:"将禎";s:9:"last_name";s:6:"柳田";s:12:"mail_address";s:27:"pikaichi.yanagita@gmail.com";s:13:"effectiveness";s:1:"1";s:14:"job_entry_date";s:10:"2010-08-23";s:11:"ic_card_key";s:4:"0100";s:11:"position_no";s:1:"5";s:17:"employee_category";s:1:"4";s:10:"retirement";s:1:"0";s:15:"retirement_date";N;s:8:"on_leave";s:1:"0";s:21:"attendance_adjustment";s:1:"0";s:10:"shift_work";s:1:"1";s:10:"start_time";N;s:12:"closing_time";N;s:8:"birthday";N;s:12:"basic_uptime";N;s:15:"holiday_pattern";s:1:"9";s:15:"relation_emp_no";s:3:"100";s:8:"username";s:15:"柳田　将禎";}}', 4715131229);
INSERT INTO `cake_sessions` (`id`, `data`, `expires`) VALUES('ke5g0pu5c17j4q7v7rk86hk9v4', 'Config|a:3:{s:9:"userAgent";s:32:"c3016251d9fc5bc072a19e5912778677";s:4:"time";i:4715311587;s:9:"countdown";i:10;}Message|a:1:{s:4:"auth";a:4:{s:7:"message";s:47:"You are not authorized to access that location.";s:3:"key";s:4:"auth";s:7:"element";s:13:"Flash/default";s:6:"params";a:0:{}}}Auth|a:1:{s:9:"AdminUser";a:27:{s:2:"id";s:1:"2";s:10:"company_no";s:1:"1";s:11:"division_no";s:1:"4";s:13:"department_no";s:1:"2";s:10:"section_no";s:1:"2";s:12:"authority_no";s:1:"1";s:14:"responsibility";s:1:"2";s:10:"first_name";s:6:"将禎";s:9:"last_name";s:6:"柳田";s:12:"mail_address";s:27:"pikaichi.yanagita@gmail.com";s:13:"effectiveness";s:1:"1";s:14:"job_entry_date";s:10:"2010-08-23";s:11:"ic_card_key";s:4:"0100";s:11:"position_no";s:1:"5";s:17:"employee_category";s:1:"4";s:10:"retirement";s:1:"0";s:15:"retirement_date";N;s:8:"on_leave";s:1:"0";s:21:"attendance_adjustment";s:1:"0";s:10:"shift_work";s:1:"1";s:10:"start_time";N;s:12:"closing_time";N;s:8:"birthday";N;s:12:"basic_uptime";N;s:15:"holiday_pattern";s:1:"9";s:15:"relation_emp_no";s:3:"100";s:8:"username";s:15:"柳田　将禎";}}', 4715311587);
INSERT INTO `cake_sessions` (`id`, `data`, `expires`) VALUES('l1fkphh76ghonp1ipjgn8vsin3', 'Config|a:3:{s:9:"userAgent";s:32:"0298b75623a8136ace6c58975711f9ce";s:4:"time";i:4713699788;s:9:"countdown";i:10;}Message|a:1:{s:4:"auth";a:4:{s:7:"message";s:47:"You are not authorized to access that location.";s:3:"key";s:4:"auth";s:7:"element";s:13:"Flash/default";s:6:"params";a:0:{}}}Auth|a:1:{s:9:"AdminUser";a:27:{s:2:"id";s:1:"8";s:10:"company_no";s:1:"1";s:11:"division_no";s:1:"1";s:13:"department_no";s:1:"3";s:10:"section_no";s:1:"3";s:12:"authority_no";s:1:"1";s:14:"responsibility";s:1:"3";s:10:"first_name";s:6:"亜耶";s:9:"last_name";s:6:"廣田";s:12:"mail_address";s:25:"pikaichi.hirota@gmail.com";s:13:"effectiveness";s:1:"1";s:14:"job_entry_date";s:10:"2014-11-10";s:11:"ic_card_key";s:4:"1007";s:11:"position_no";s:1:"1";s:17:"employee_category";s:1:"1";s:10:"retirement";s:1:"0";s:15:"retirement_date";N;s:8:"on_leave";s:1:"0";s:21:"attendance_adjustment";s:1:"0";s:10:"shift_work";s:1:"1";s:10:"start_time";N;s:12:"closing_time";N;s:8:"birthday";N;s:12:"basic_uptime";N;s:15:"holiday_pattern";s:1:"9";s:15:"relation_emp_no";s:4:"1007";s:8:"username";s:15:"廣田　亜耶";}}', 4713699788);
INSERT INTO `cake_sessions` (`id`, `data`, `expires`) VALUES('l8goce5jm6j8juqv7u7fpjfvc6', 'Config|a:3:{s:9:"userAgent";s:32:"19989bd7c3484db41ca03b9e14163fe9";s:4:"time";i:4715338469;s:9:"countdown";i:10;}Message|a:1:{s:4:"auth";a:4:{s:7:"message";s:47:"You are not authorized to access that location.";s:3:"key";s:4:"auth";s:7:"element";s:13:"Flash/default";s:6:"params";a:0:{}}}Auth|a:1:{s:9:"AdminUser";a:27:{s:2:"id";s:1:"2";s:10:"company_no";s:1:"1";s:11:"division_no";s:1:"4";s:13:"department_no";s:1:"2";s:10:"section_no";s:1:"2";s:12:"authority_no";s:1:"1";s:14:"responsibility";s:1:"2";s:10:"first_name";s:6:"将禎";s:9:"last_name";s:6:"柳田";s:12:"mail_address";s:27:"pikaichi.yanagita@gmail.com";s:13:"effectiveness";s:1:"1";s:14:"job_entry_date";s:10:"2010-08-23";s:11:"ic_card_key";s:4:"0100";s:11:"position_no";s:1:"5";s:17:"employee_category";s:1:"4";s:10:"retirement";s:1:"0";s:15:"retirement_date";N;s:8:"on_leave";s:1:"0";s:21:"attendance_adjustment";s:1:"0";s:10:"shift_work";s:1:"1";s:10:"start_time";N;s:12:"closing_time";N;s:8:"birthday";N;s:12:"basic_uptime";N;s:15:"holiday_pattern";s:1:"9";s:15:"relation_emp_no";s:3:"100";s:8:"username";s:15:"柳田　将禎";}}', 4715338469);
INSERT INTO `cake_sessions` (`id`, `data`, `expires`) VALUES('lf16h369bmcce77vep1jqatnc7', 'Config|a:3:{s:9:"userAgent";s:32:"5ca4b7d57b54aa32f60fc0cf3d55f4c3";s:4:"time";i:4715312368;s:9:"countdown";i:10;}Message|a:1:{s:4:"auth";a:4:{s:7:"message";s:47:"You are not authorized to access that location.";s:3:"key";s:4:"auth";s:7:"element";s:13:"Flash/default";s:6:"params";a:0:{}}}Auth|a:0:{}', 4715312368);
INSERT INTO `cake_sessions` (`id`, `data`, `expires`) VALUES('lopj24ubben3j96hqkgi0av9c2', 'Config|a:3:{s:9:"userAgent";s:32:"0298b75623a8136ace6c58975711f9ce";s:4:"time";i:4712868375;s:9:"countdown";i:10;}Message|a:1:{s:4:"auth";a:4:{s:7:"message";s:47:"You are not authorized to access that location.";s:3:"key";s:4:"auth";s:7:"element";s:13:"Flash/default";s:6:"params";a:0:{}}}Auth|a:1:{s:9:"AdminUser";a:26:{s:2:"id";s:1:"8";s:10:"company_no";s:1:"1";s:11:"division_no";s:1:"1";s:13:"department_no";s:1:"3";s:10:"section_no";s:1:"3";s:12:"authority_no";s:1:"1";s:14:"responsibility";s:1:"2";s:10:"first_name";s:6:"亜耶";s:9:"last_name";s:6:"廣田";s:12:"mail_address";s:25:"pikaichi.hirota@gmail.com";s:13:"effectiveness";s:1:"1";s:14:"job_entry_date";s:10:"2014-11-10";s:11:"ic_card_key";s:4:"1007";s:11:"position_no";s:1:"1";s:17:"employee_category";s:1:"1";s:10:"retirement";s:1:"0";s:15:"retirement_date";N;s:8:"on_leave";s:1:"0";s:21:"attendance_adjustment";s:1:"0";s:10:"shift_work";s:1:"1";s:10:"start_time";N;s:12:"closing_time";N;s:8:"birthday";N;s:12:"basic_uptime";N;s:15:"holiday_pattern";s:1:"9";s:8:"username";s:15:"廣田　亜耶";}}', 4712868375);
INSERT INTO `cake_sessions` (`id`, `data`, `expires`) VALUES('lr47j975452jrq3rrl937lphn5', 'Config|a:3:{s:9:"userAgent";s:32:"5ca4b7d57b54aa32f60fc0cf3d55f4c3";s:4:"time";i:4715422883;s:9:"countdown";i:10;}Message|a:1:{s:4:"auth";a:4:{s:7:"message";s:47:"You are not authorized to access that location.";s:3:"key";s:4:"auth";s:7:"element";s:13:"Flash/default";s:6:"params";a:0:{}}}Auth|a:1:{s:9:"AdminUser";a:27:{s:2:"id";s:1:"2";s:10:"company_no";s:1:"1";s:11:"division_no";s:1:"4";s:13:"department_no";s:1:"2";s:10:"section_no";s:1:"2";s:12:"authority_no";s:1:"1";s:14:"responsibility";s:1:"2";s:10:"first_name";s:6:"将禎";s:9:"last_name";s:6:"柳田";s:12:"mail_address";s:27:"pikaichi.yanagita@gmail.com";s:13:"effectiveness";s:1:"1";s:14:"job_entry_date";s:10:"2010-08-23";s:11:"ic_card_key";s:4:"0100";s:11:"position_no";s:1:"5";s:17:"employee_category";s:1:"4";s:10:"retirement";s:1:"0";s:15:"retirement_date";N;s:8:"on_leave";s:1:"0";s:21:"attendance_adjustment";s:1:"0";s:10:"shift_work";s:1:"1";s:10:"start_time";N;s:12:"closing_time";N;s:8:"birthday";N;s:12:"basic_uptime";N;s:15:"holiday_pattern";s:1:"9";s:15:"relation_emp_no";s:3:"100";s:8:"username";s:15:"柳田　将禎";}}', 4715422883);
INSERT INTO `cake_sessions` (`id`, `data`, `expires`) VALUES('m00l6ovr2jaq5ebek522d845f0', 'Config|a:3:{s:9:"userAgent";s:32:"c18f97aefc462939b2f3d3cdc8d91f2f";s:4:"time";i:4715223327;s:9:"countdown";i:10;}', 4715223327);
INSERT INTO `cake_sessions` (`id`, `data`, `expires`) VALUES('m3ga8m650diovce7l1g6n3evj1', 'Config|a:3:{s:9:"userAgent";s:32:"ed569cbe4b8fbfe66141b9bb2381ec2a";s:4:"time";i:4715470700;s:9:"countdown";i:10;}Message|a:1:{s:4:"auth";a:4:{s:7:"message";s:47:"You are not authorized to access that location.";s:3:"key";s:4:"auth";s:7:"element";s:13:"Flash/default";s:6:"params";a:0:{}}}Auth|a:1:{s:9:"AdminUser";a:27:{s:2:"id";s:1:"2";s:10:"company_no";s:1:"1";s:11:"division_no";s:1:"4";s:13:"department_no";s:1:"2";s:10:"section_no";s:1:"2";s:12:"authority_no";s:1:"1";s:14:"responsibility";s:1:"2";s:10:"first_name";s:6:"将禎";s:9:"last_name";s:6:"柳田";s:12:"mail_address";s:27:"pikaichi.yanagita@gmail.com";s:13:"effectiveness";s:1:"1";s:14:"job_entry_date";s:10:"2010-08-23";s:11:"ic_card_key";s:4:"0100";s:11:"position_no";s:1:"5";s:17:"employee_category";s:1:"4";s:10:"retirement";s:1:"0";s:15:"retirement_date";N;s:8:"on_leave";s:1:"0";s:21:"attendance_adjustment";s:1:"0";s:10:"shift_work";s:1:"1";s:10:"start_time";N;s:12:"closing_time";N;s:8:"birthday";N;s:12:"basic_uptime";N;s:15:"holiday_pattern";s:1:"9";s:15:"relation_emp_no";s:3:"100";s:8:"username";s:15:"柳田　将禎";}}', 4715470700);
INSERT INTO `cake_sessions` (`id`, `data`, `expires`) VALUES('n6821sttj68u2bvo50enjpeo97', 'Config|a:3:{s:9:"userAgent";s:32:"7d967713f94c53758c440476e8c23072";s:4:"time";i:4711488600;s:9:"countdown";i:10;}Message|a:1:{s:4:"auth";a:4:{s:7:"message";s:47:"You are not authorized to access that location.";s:3:"key";s:4:"auth";s:7:"element";s:13:"Flash/default";s:6:"params";a:0:{}}}Auth|a:1:{s:9:"AdminUser";a:26:{s:2:"id";s:1:"7";s:10:"company_no";s:1:"1";s:11:"division_no";s:1:"1";s:13:"department_no";s:1:"3";s:10:"section_no";s:1:"3";s:12:"authority_no";s:1:"2";s:14:"responsibility";s:1:"2";s:10:"first_name";s:6:"綾香";s:9:"last_name";s:6:"坪井";s:12:"mail_address";s:25:"pikaichi.uchino@gmail.com";s:13:"effectiveness";s:1:"1";s:14:"job_entry_date";s:10:"2013-10-01";s:11:"ic_card_key";s:4:"1003";s:11:"position_no";s:1:"1";s:17:"employee_category";s:1:"1";s:10:"retirement";s:1:"0";s:15:"retirement_date";N;s:8:"on_leave";s:1:"0";s:21:"attendance_adjustment";s:1:"0";s:10:"shift_work";s:1:"1";s:10:"start_time";N;s:12:"closing_time";N;s:8:"birthday";N;s:12:"basic_uptime";N;s:15:"holiday_pattern";s:1:"9";s:8:"username";s:15:"坪井　綾香";}}', 4711488600);
INSERT INTO `cake_sessions` (`id`, `data`, `expires`) VALUES('n6u3tt4iti9sfilm8o3dbd1pd7', 'Config|a:3:{s:9:"userAgent";s:32:"5ca4b7d57b54aa32f60fc0cf3d55f4c3";s:4:"time";i:4715421614;s:9:"countdown";i:10;}Message|a:1:{s:4:"auth";a:4:{s:7:"message";s:47:"You are not authorized to access that location.";s:3:"key";s:4:"auth";s:7:"element";s:13:"Flash/default";s:6:"params";a:0:{}}}Auth|a:1:{s:9:"AdminUser";a:27:{s:2:"id";s:1:"7";s:10:"company_no";s:1:"1";s:11:"division_no";s:1:"1";s:13:"department_no";s:1:"3";s:10:"section_no";s:1:"3";s:12:"authority_no";s:1:"2";s:14:"responsibility";s:1:"3";s:10:"first_name";s:6:"綾香";s:9:"last_name";s:6:"坪井";s:12:"mail_address";s:25:"pikaichi.uchino@gmail.com";s:13:"effectiveness";s:1:"1";s:14:"job_entry_date";s:10:"2013-10-01";s:11:"ic_card_key";s:4:"1003";s:11:"position_no";s:1:"1";s:17:"employee_category";s:1:"1";s:10:"retirement";s:1:"0";s:15:"retirement_date";N;s:8:"on_leave";s:1:"0";s:21:"attendance_adjustment";s:1:"0";s:10:"shift_work";s:1:"1";s:10:"start_time";N;s:12:"closing_time";N;s:8:"birthday";N;s:12:"basic_uptime";N;s:15:"holiday_pattern";s:1:"9";s:15:"relation_emp_no";s:4:"1003";s:8:"username";s:15:"坪井　綾香";}}', 4715421614);
INSERT INTO `cake_sessions` (`id`, `data`, `expires`) VALUES('nu5qsqkjjmtla62953li9igbb4', 'Config|a:3:{s:9:"userAgent";s:32:"e04d3a82e71ee78f9f03601fde6e2750";s:4:"time";i:4711487119;s:9:"countdown";i:10;}', 4711487119);
INSERT INTO `cake_sessions` (`id`, `data`, `expires`) VALUES('o2gd6s1ct38gbgpp6hvsk4bee1', 'Config|a:3:{s:9:"userAgent";s:32:"d6b1da54cbaf4bfaab1d3d7446f794cd";s:4:"time";i:4711453976;s:9:"countdown";i:10;}Message|a:1:{s:4:"auth";a:4:{s:7:"message";s:47:"You are not authorized to access that location.";s:3:"key";s:4:"auth";s:7:"element";s:13:"Flash/default";s:6:"params";a:0:{}}}Auth|a:1:{s:9:"AdminUser";a:26:{s:2:"id";s:2:"32";s:10:"company_no";s:1:"1";s:11:"division_no";s:1:"1";s:13:"department_no";s:1:"3";s:10:"section_no";s:1:"3";s:12:"authority_no";s:1:"1";s:14:"responsibility";s:1:"0";s:10:"first_name";s:9:"テスト";s:9:"last_name";s:15:"プロネット";s:12:"mail_address";s:16:"pronet@gmail.com";s:13:"effectiveness";s:1:"1";s:14:"job_entry_date";s:10:"2014-11-10";s:11:"ic_card_key";s:4:"1007";s:11:"position_no";s:1:"1";s:17:"employee_category";s:1:"1";s:10:"retirement";s:1:"0";s:15:"retirement_date";N;s:8:"on_leave";s:1:"0";s:21:"attendance_adjustment";s:1:"0";s:10:"shift_work";s:1:"1";s:10:"start_time";N;s:12:"closing_time";N;s:8:"birthday";N;s:12:"basic_uptime";N;s:15:"holiday_pattern";s:1:"9";s:8:"username";s:27:"プロネット　テスト";}}', 4711453976);
INSERT INTO `cake_sessions` (`id`, `data`, `expires`) VALUES('o3vsgifidfj7kneemupno3h0t4', 'Config|a:3:{s:9:"userAgent";s:32:"cf9a8155133efd8033b171c3bbc7ec96";s:4:"time";i:4715339939;s:9:"countdown";i:10;}Message|a:1:{s:4:"auth";a:4:{s:7:"message";s:47:"You are not authorized to access that location.";s:3:"key";s:4:"auth";s:7:"element";s:13:"Flash/default";s:6:"params";a:0:{}}}Auth|a:1:{s:9:"AdminUser";a:27:{s:2:"id";s:1:"2";s:10:"company_no";s:1:"1";s:11:"division_no";s:1:"4";s:13:"department_no";s:1:"2";s:10:"section_no";s:1:"2";s:12:"authority_no";s:1:"1";s:14:"responsibility";s:1:"2";s:10:"first_name";s:6:"将禎";s:9:"last_name";s:6:"柳田";s:12:"mail_address";s:27:"pikaichi.yanagita@gmail.com";s:13:"effectiveness";s:1:"1";s:14:"job_entry_date";s:10:"2010-08-23";s:11:"ic_card_key";s:4:"0100";s:11:"position_no";s:1:"5";s:17:"employee_category";s:1:"4";s:10:"retirement";s:1:"0";s:15:"retirement_date";N;s:8:"on_leave";s:1:"0";s:21:"attendance_adjustment";s:1:"0";s:10:"shift_work";s:1:"1";s:10:"start_time";N;s:12:"closing_time";N;s:8:"birthday";N;s:12:"basic_uptime";N;s:15:"holiday_pattern";s:1:"9";s:15:"relation_emp_no";s:3:"100";s:8:"username";s:15:"柳田　将禎";}}', 4715339939);
INSERT INTO `cake_sessions` (`id`, `data`, `expires`) VALUES('okqsgbsat54kuqtgf740nf2ha5', 'Config|a:3:{s:9:"userAgent";s:32:"5ca4b7d57b54aa32f60fc0cf3d55f4c3";s:4:"time";i:4715422439;s:9:"countdown";i:10;}Message|a:1:{s:4:"auth";a:4:{s:7:"message";s:47:"You are not authorized to access that location.";s:3:"key";s:4:"auth";s:7:"element";s:13:"Flash/default";s:6:"params";a:0:{}}}Auth|a:0:{}', 4715422439);
INSERT INTO `cake_sessions` (`id`, `data`, `expires`) VALUES('ot8gnpm3kmq2hg6m2jsgv34bp0', 'Config|a:3:{s:9:"userAgent";s:32:"d14c664f2b60198160b9a86162b3a6a9";s:4:"time";i:4709732862;s:9:"countdown";i:10;}Message|a:1:{s:4:"auth";a:4:{s:7:"message";s:47:"You are not authorized to access that location.";s:3:"key";s:4:"auth";s:7:"element";s:13:"Flash/default";s:6:"params";a:0:{}}}Auth|a:1:{s:9:"AdminUser";a:26:{s:2:"id";s:1:"4";s:10:"company_no";s:1:"1";s:11:"division_no";s:1:"1";s:13:"department_no";s:1:"1";s:10:"section_no";s:1:"1";s:12:"authority_no";s:1:"2";s:14:"responsibility";s:1:"1";s:10:"first_name";s:3:"誠";s:9:"last_name";s:6:"秋野";s:12:"mail_address";s:24:"pikaichi.akino@gmail.com";s:13:"effectiveness";s:1:"1";s:14:"job_entry_date";s:10:"2012-06-01";s:11:"ic_card_key";s:4:"0103";s:11:"position_no";s:1:"3";s:17:"employee_category";s:1:"4";s:10:"retirement";s:1:"0";s:15:"retirement_date";N;s:8:"on_leave";s:1:"0";s:21:"attendance_adjustment";s:1:"0";s:10:"shift_work";s:1:"1";s:10:"start_time";N;s:12:"closing_time";N;s:8:"birthday";N;s:12:"basic_uptime";N;s:15:"holiday_pattern";s:1:"9";s:8:"username";s:12:"秋野　誠";}}', 4709732862);
INSERT INTO `cake_sessions` (`id`, `data`, `expires`) VALUES('p2e9dmp499729glo59cotv18v6', 'Config|a:3:{s:9:"userAgent";s:32:"ed569cbe4b8fbfe66141b9bb2381ec2a";s:4:"time";i:4715130329;s:9:"countdown";i:10;}Message|a:1:{s:4:"auth";a:4:{s:7:"message";s:47:"You are not authorized to access that location.";s:3:"key";s:4:"auth";s:7:"element";s:13:"Flash/default";s:6:"params";a:0:{}}}Auth|a:0:{}', 4715130329);
INSERT INTO `cake_sessions` (`id`, `data`, `expires`) VALUES('pb9ar7p8tcq4nuvgm3mcu8j0p2', 'Config|a:3:{s:9:"userAgent";s:32:"0298b75623a8136ace6c58975711f9ce";s:4:"time";i:4712782198;s:9:"countdown";i:10;}Message|a:1:{s:4:"auth";a:4:{s:7:"message";s:47:"You are not authorized to access that location.";s:3:"key";s:4:"auth";s:7:"element";s:13:"Flash/default";s:6:"params";a:0:{}}}Auth|a:1:{s:9:"AdminUser";a:26:{s:2:"id";s:1:"8";s:10:"company_no";s:1:"1";s:11:"division_no";s:1:"1";s:13:"department_no";s:1:"3";s:10:"section_no";s:1:"3";s:12:"authority_no";s:1:"1";s:14:"responsibility";s:1:"2";s:10:"first_name";s:6:"亜耶";s:9:"last_name";s:6:"廣田";s:12:"mail_address";s:25:"pikaichi.hirota@gmail.com";s:13:"effectiveness";s:1:"1";s:14:"job_entry_date";s:10:"2014-11-10";s:11:"ic_card_key";s:4:"1007";s:11:"position_no";s:1:"1";s:17:"employee_category";s:1:"1";s:10:"retirement";s:1:"0";s:15:"retirement_date";N;s:8:"on_leave";s:1:"0";s:21:"attendance_adjustment";s:1:"0";s:10:"shift_work";s:1:"1";s:10:"start_time";N;s:12:"closing_time";N;s:8:"birthday";N;s:12:"basic_uptime";N;s:15:"holiday_pattern";s:1:"9";s:8:"username";s:15:"廣田　亜耶";}}', 4712782198);
INSERT INTO `cake_sessions` (`id`, `data`, `expires`) VALUES('pohkr8e0eq0c4q0trme19mhhc4', 'Config|a:3:{s:9:"userAgent";s:32:"0298b75623a8136ace6c58975711f9ce";s:4:"time";i:4713175814;s:9:"countdown";i:10;}Message|a:1:{s:4:"auth";a:4:{s:7:"message";s:47:"You are not authorized to access that location.";s:3:"key";s:4:"auth";s:7:"element";s:13:"Flash/default";s:6:"params";a:0:{}}}Auth|a:1:{s:9:"AdminUser";a:26:{s:2:"id";s:2:"25";s:10:"company_no";s:1:"1";s:11:"division_no";s:1:"1";s:13:"department_no";s:1:"3";s:10:"section_no";s:1:"3";s:12:"authority_no";s:1:"2";s:14:"responsibility";s:1:"0";s:10:"first_name";s:6:"晴海";s:9:"last_name";s:6:"浅野";s:12:"mail_address";s:24:"pikaichi.asano@gmail.com";s:13:"effectiveness";s:1:"1";s:14:"job_entry_date";s:10:"2016-11-28";s:11:"ic_card_key";s:4:"2010";s:11:"position_no";s:1:"1";s:17:"employee_category";s:1:"2";s:10:"retirement";s:1:"0";s:15:"retirement_date";N;s:8:"on_leave";s:1:"0";s:21:"attendance_adjustment";s:1:"0";s:10:"shift_work";s:1:"1";s:10:"start_time";N;s:12:"closing_time";N;s:8:"birthday";N;s:12:"basic_uptime";N;s:15:"holiday_pattern";s:1:"9";s:8:"username";s:15:"浅野　晴海";}}', 4713175814);
INSERT INTO `cake_sessions` (`id`, `data`, `expires`) VALUES('pqi8pdjknmp4agb0111t4fr9j6', 'Config|a:3:{s:9:"userAgent";s:32:"d6b1da54cbaf4bfaab1d3d7446f794cd";s:4:"time";i:4710667135;s:9:"countdown";i:10;}Message|a:1:{s:4:"auth";a:4:{s:7:"message";s:47:"You are not authorized to access that location.";s:3:"key";s:4:"auth";s:7:"element";s:13:"Flash/default";s:6:"params";a:0:{}}}Auth|a:1:{s:9:"AdminUser";a:26:{s:2:"id";s:1:"5";s:10:"company_no";s:1:"1";s:11:"division_no";s:1:"1";s:13:"department_no";s:1:"1";s:10:"section_no";s:1:"1";s:12:"authority_no";s:1:"2";s:14:"responsibility";s:1:"0";s:10:"first_name";s:6:"公洋";s:9:"last_name";s:6:"荒鎌";s:12:"mail_address";s:26:"pikaichi.arakama@gmail.com";s:13:"effectiveness";s:1:"1";s:14:"job_entry_date";s:10:"2012-02-20";s:11:"ic_card_key";s:4:"1001";s:11:"position_no";s:1:"1";s:17:"employee_category";s:1:"1";s:10:"retirement";s:1:"0";s:15:"retirement_date";N;s:8:"on_leave";s:1:"0";s:21:"attendance_adjustment";s:1:"0";s:10:"shift_work";s:1:"1";s:10:"start_time";N;s:12:"closing_time";N;s:8:"birthday";N;s:12:"basic_uptime";N;s:15:"holiday_pattern";s:1:"9";s:8:"username";s:15:"荒鎌　公洋";}}', 4710667135);
INSERT INTO `cake_sessions` (`id`, `data`, `expires`) VALUES('q00tpmhsrhtdoe5co0kk8atv54', 'Config|a:3:{s:9:"userAgent";s:32:"fefeb5a9f6f6ae0749f512fcba4fe069";s:4:"time";i:4715339253;s:9:"countdown";i:10;}Message|a:1:{s:4:"auth";a:4:{s:7:"message";s:47:"You are not authorized to access that location.";s:3:"key";s:4:"auth";s:7:"element";s:13:"Flash/default";s:6:"params";a:0:{}}}Auth|a:0:{}', 4715339253);
INSERT INTO `cake_sessions` (`id`, `data`, `expires`) VALUES('q4nesh4oeqhh8do02jpt3ff6v4', 'Config|a:3:{s:9:"userAgent";s:32:"89487d0630936b6373dae7abb18e57f3";s:4:"time";i:4715132712;s:9:"countdown";i:10;}Message|a:1:{s:4:"auth";a:4:{s:7:"message";s:47:"You are not authorized to access that location.";s:3:"key";s:4:"auth";s:7:"element";s:13:"Flash/default";s:6:"params";a:0:{}}}Auth|a:1:{s:9:"AdminUser";a:27:{s:2:"id";s:1:"3";s:10:"company_no";s:1:"1";s:11:"division_no";s:1:"1";s:13:"department_no";s:1:"1";s:10:"section_no";s:1:"1";s:12:"authority_no";s:1:"2";s:14:"responsibility";s:1:"2";s:10:"first_name";s:6:"慎吾";s:9:"last_name";s:6:"坪井";s:12:"mail_address";s:25:"pikaichi.tsuboi@gmail.com";s:13:"effectiveness";s:1:"1";s:14:"job_entry_date";s:10:"2010-11-01";s:11:"ic_card_key";s:4:"0101";s:11:"position_no";s:1:"4";s:17:"employee_category";s:1:"4";s:10:"retirement";s:1:"0";s:15:"retirement_date";N;s:8:"on_leave";s:1:"0";s:21:"attendance_adjustment";s:1:"0";s:10:"shift_work";s:1:"1";s:10:"start_time";N;s:12:"closing_time";N;s:8:"birthday";N;s:12:"basic_uptime";N;s:15:"holiday_pattern";s:1:"9";s:15:"relation_emp_no";s:3:"101";s:8:"username";s:15:"坪井　慎吾";}}', 4715132712);
INSERT INTO `cake_sessions` (`id`, `data`, `expires`) VALUES('qgb47v0hlm3oihoatqkhppkhe7', 'Config|a:3:{s:9:"userAgent";s:32:"d14c664f2b60198160b9a86162b3a6a9";s:4:"time";i:4708850884;s:9:"countdown";i:10;}Message|a:1:{s:4:"auth";a:4:{s:7:"message";s:47:"You are not authorized to access that location.";s:3:"key";s:4:"auth";s:7:"element";s:13:"Flash/default";s:6:"params";a:0:{}}}Auth|a:1:{s:9:"AdminUser";a:26:{s:2:"id";s:2:"11";s:10:"company_no";s:1:"1";s:11:"division_no";s:1:"1";s:13:"department_no";s:1:"1";s:10:"section_no";s:1:"1";s:12:"authority_no";s:1:"2";s:14:"responsibility";s:1:"0";s:10:"first_name";s:6:"信博";s:9:"last_name";s:6:"渡邉";s:12:"mail_address";s:28:"pikaichi.watanaben@gmail.com";s:13:"effectiveness";s:1:"1";s:14:"job_entry_date";s:10:"2015-12-14";s:11:"ic_card_key";s:4:"1015";s:11:"position_no";s:1:"1";s:17:"employee_category";s:1:"1";s:10:"retirement";s:1:"0";s:15:"retirement_date";N;s:8:"on_leave";s:1:"0";s:21:"attendance_adjustment";s:1:"0";s:10:"shift_work";s:1:"1";s:10:"start_time";N;s:12:"closing_time";N;s:8:"birthday";N;s:12:"basic_uptime";N;s:15:"holiday_pattern";s:1:"9";s:8:"username";s:15:"渡邉　信博";}}', 4708850884);
INSERT INTO `cake_sessions` (`id`, `data`, `expires`) VALUES('qkkmhg2orercfb3gk6n0e4fjp5', 'Config|a:3:{s:9:"userAgent";s:32:"0298b75623a8136ace6c58975711f9ce";s:4:"time";i:4712613635;s:9:"countdown";i:10;}Message|a:1:{s:4:"auth";a:4:{s:7:"message";s:47:"You are not authorized to access that location.";s:3:"key";s:4:"auth";s:7:"element";s:13:"Flash/default";s:6:"params";a:0:{}}}Auth|a:1:{s:9:"AdminUser";a:26:{s:2:"id";s:1:"8";s:10:"company_no";s:1:"1";s:11:"division_no";s:1:"1";s:13:"department_no";s:1:"3";s:10:"section_no";s:1:"3";s:12:"authority_no";s:1:"1";s:14:"responsibility";s:1:"2";s:10:"first_name";s:6:"亜耶";s:9:"last_name";s:6:"廣田";s:12:"mail_address";s:25:"pikaichi.hirota@gmail.com";s:13:"effectiveness";s:1:"1";s:14:"job_entry_date";s:10:"2014-11-10";s:11:"ic_card_key";s:4:"1007";s:11:"position_no";s:1:"1";s:17:"employee_category";s:1:"1";s:10:"retirement";s:1:"0";s:15:"retirement_date";N;s:8:"on_leave";s:1:"0";s:21:"attendance_adjustment";s:1:"0";s:10:"shift_work";s:1:"1";s:10:"start_time";N;s:12:"closing_time";N;s:8:"birthday";N;s:12:"basic_uptime";N;s:15:"holiday_pattern";s:1:"9";s:8:"username";s:15:"廣田　亜耶";}}', 4712613635);
INSERT INTO `cake_sessions` (`id`, `data`, `expires`) VALUES('qlp9to01bnhhkoqcj9hueebkr5', 'Config|a:3:{s:9:"userAgent";s:32:"5ca4b7d57b54aa32f60fc0cf3d55f4c3";s:4:"time";i:4715422935;s:9:"countdown";i:10;}', 4715422935);
INSERT INTO `cake_sessions` (`id`, `data`, `expires`) VALUES('qngldiprt3tnl81bpm8l4o3gm2', 'Config|a:3:{s:9:"userAgent";s:32:"847a3db26a4a4f65190c08da740bbac8";s:4:"time";i:4712001101;s:9:"countdown";i:10;}Message|a:1:{s:4:"auth";a:4:{s:7:"message";s:47:"You are not authorized to access that location.";s:3:"key";s:4:"auth";s:7:"element";s:13:"Flash/default";s:6:"params";a:0:{}}}Auth|a:1:{s:9:"AdminUser";a:26:{s:2:"id";s:2:"11";s:10:"company_no";s:1:"1";s:11:"division_no";s:1:"1";s:13:"department_no";s:1:"1";s:10:"section_no";s:1:"1";s:12:"authority_no";s:1:"2";s:14:"responsibility";s:1:"0";s:10:"first_name";s:6:"信博";s:9:"last_name";s:6:"渡邉";s:12:"mail_address";s:28:"pikaichi.watanaben@gmail.com";s:13:"effectiveness";s:1:"1";s:14:"job_entry_date";s:10:"2015-12-14";s:11:"ic_card_key";s:4:"1015";s:11:"position_no";s:1:"1";s:17:"employee_category";s:1:"1";s:10:"retirement";s:1:"0";s:15:"retirement_date";N;s:8:"on_leave";s:1:"0";s:21:"attendance_adjustment";s:1:"0";s:10:"shift_work";s:1:"1";s:10:"start_time";N;s:12:"closing_time";N;s:8:"birthday";N;s:12:"basic_uptime";N;s:15:"holiday_pattern";s:1:"9";s:8:"username";s:15:"渡邉　信博";}}', 4712001101);
INSERT INTO `cake_sessions` (`id`, `data`, `expires`) VALUES('r80j0bc6cu2msqbsmpghom61u4', 'Config|a:3:{s:9:"userAgent";s:32:"077a67061cfa7fb160b6c72b175ce03a";s:4:"time";i:4715210610;s:9:"countdown";i:10;}Message|a:1:{s:4:"auth";a:4:{s:7:"message";s:47:"You are not authorized to access that location.";s:3:"key";s:4:"auth";s:7:"element";s:13:"Flash/default";s:6:"params";a:0:{}}}Auth|a:1:{s:9:"AdminUser";a:27:{s:2:"id";s:1:"3";s:10:"company_no";s:1:"1";s:11:"division_no";s:1:"1";s:13:"department_no";s:1:"1";s:10:"section_no";s:1:"1";s:12:"authority_no";s:1:"2";s:14:"responsibility";s:1:"2";s:10:"first_name";s:6:"慎吾";s:9:"last_name";s:6:"坪井";s:12:"mail_address";s:25:"pikaichi.tsuboi@gmail.com";s:13:"effectiveness";s:1:"1";s:14:"job_entry_date";s:10:"2010-11-01";s:11:"ic_card_key";s:4:"0101";s:11:"position_no";s:1:"4";s:17:"employee_category";s:1:"4";s:10:"retirement";s:1:"0";s:15:"retirement_date";N;s:8:"on_leave";s:1:"0";s:21:"attendance_adjustment";s:1:"0";s:10:"shift_work";s:1:"1";s:10:"start_time";N;s:12:"closing_time";N;s:8:"birthday";N;s:12:"basic_uptime";N;s:15:"holiday_pattern";s:1:"9";s:15:"relation_emp_no";s:3:"101";s:8:"username";s:15:"坪井　慎吾";}}', 4715210610);
INSERT INTO `cake_sessions` (`id`, `data`, `expires`) VALUES('rghmno06he2ot53p5610ojjjs5', 'Config|a:3:{s:9:"userAgent";s:32:"977101f4456b7509d781fe16fd59104f";s:4:"time";i:4715134971;s:9:"countdown";i:10;}', 4715134971);
INSERT INTO `cake_sessions` (`id`, `data`, `expires`) VALUES('rirr1dphrr39oi95bcn29q0r71', 'Config|a:3:{s:9:"userAgent";s:32:"c18f97aefc462939b2f3d3cdc8d91f2f";s:4:"time";i:4715135200;s:9:"countdown";i:10;}', 4715135200);
INSERT INTO `cake_sessions` (`id`, `data`, `expires`) VALUES('rtb6gree51igov6bocunojo3t3', 'Config|a:3:{s:9:"userAgent";s:32:"977101f4456b7509d781fe16fd59104f";s:4:"time";i:4715130867;s:9:"countdown";i:10;}', 4715130867);
INSERT INTO `cake_sessions` (`id`, `data`, `expires`) VALUES('shc0kp5rvn8malon4vu9l0c192', 'Config|a:3:{s:9:"userAgent";s:32:"5ca4b7d57b54aa32f60fc0cf3d55f4c3";s:4:"time";i:4715338459;s:9:"countdown";i:10;}Message|a:1:{s:4:"auth";a:4:{s:7:"message";s:47:"You are not authorized to access that location.";s:3:"key";s:4:"auth";s:7:"element";s:13:"Flash/default";s:6:"params";a:0:{}}}Auth|a:0:{}', 4715338459);
INSERT INTO `cake_sessions` (`id`, `data`, `expires`) VALUES('t205nqgeo61qsldcrr8crma387', 'Config|a:3:{s:9:"userAgent";s:32:"d6b1da54cbaf4bfaab1d3d7446f794cd";s:4:"time";i:4711489719;s:9:"countdown";i:10;}Message|a:1:{s:4:"auth";a:4:{s:7:"message";s:47:"You are not authorized to access that location.";s:3:"key";s:4:"auth";s:7:"element";s:13:"Flash/default";s:6:"params";a:0:{}}}Auth|a:1:{s:9:"AdminUser";a:26:{s:2:"id";s:1:"8";s:10:"company_no";s:1:"1";s:11:"division_no";s:1:"1";s:13:"department_no";s:1:"3";s:10:"section_no";s:1:"3";s:12:"authority_no";s:1:"1";s:14:"responsibility";s:1:"2";s:10:"first_name";s:6:"亜耶";s:9:"last_name";s:6:"廣田";s:12:"mail_address";s:25:"pikaichi.hirota@gmail.com";s:13:"effectiveness";s:1:"1";s:14:"job_entry_date";s:10:"2014-11-10";s:11:"ic_card_key";s:4:"1007";s:11:"position_no";s:1:"1";s:17:"employee_category";s:1:"1";s:10:"retirement";s:1:"0";s:15:"retirement_date";N;s:8:"on_leave";s:1:"0";s:21:"attendance_adjustment";s:1:"0";s:10:"shift_work";s:1:"1";s:10:"start_time";N;s:12:"closing_time";N;s:8:"birthday";N;s:12:"basic_uptime";N;s:15:"holiday_pattern";s:1:"9";s:8:"username";s:15:"廣田　亜耶";}}', 4711489719);
INSERT INTO `cake_sessions` (`id`, `data`, `expires`) VALUES('u6n4gv18jhb6csdtubk1ca2v21', 'Config|a:3:{s:9:"userAgent";s:32:"1afe64c526ba109e6fc3f27f9e1e010d";s:4:"time";i:4715364265;s:9:"countdown";i:10;}Message|a:1:{s:4:"auth";a:4:{s:7:"message";s:47:"You are not authorized to access that location.";s:3:"key";s:4:"auth";s:7:"element";s:13:"Flash/default";s:6:"params";a:0:{}}}Auth|a:1:{s:9:"AdminUser";a:27:{s:2:"id";s:1:"2";s:10:"company_no";s:1:"1";s:11:"division_no";s:1:"4";s:13:"department_no";s:1:"2";s:10:"section_no";s:1:"2";s:12:"authority_no";s:1:"1";s:14:"responsibility";s:1:"2";s:10:"first_name";s:6:"将禎";s:9:"last_name";s:6:"柳田";s:12:"mail_address";s:27:"pikaichi.yanagita@gmail.com";s:13:"effectiveness";s:1:"1";s:14:"job_entry_date";s:10:"2010-08-23";s:11:"ic_card_key";s:4:"0100";s:11:"position_no";s:1:"5";s:17:"employee_category";s:1:"4";s:10:"retirement";s:1:"0";s:15:"retirement_date";N;s:8:"on_leave";s:1:"0";s:21:"attendance_adjustment";s:1:"0";s:10:"shift_work";s:1:"1";s:10:"start_time";N;s:12:"closing_time";N;s:8:"birthday";N;s:12:"basic_uptime";N;s:15:"holiday_pattern";s:1:"9";s:15:"relation_emp_no";s:3:"100";s:8:"username";s:15:"柳田　将禎";}}', 4715364265);
INSERT INTO `cake_sessions` (`id`, `data`, `expires`) VALUES('uhn1ff426opc3nfpbsrgtqt6k6', 'Config|a:3:{s:9:"userAgent";s:32:"5ca4b7d57b54aa32f60fc0cf3d55f4c3";s:4:"time";i:4715311928;s:9:"countdown";i:10;}Message|a:1:{s:4:"auth";a:4:{s:7:"message";s:47:"You are not authorized to access that location.";s:3:"key";s:4:"auth";s:7:"element";s:13:"Flash/default";s:6:"params";a:0:{}}}Auth|a:1:{s:9:"AdminUser";a:27:{s:2:"id";s:1:"7";s:10:"company_no";s:1:"1";s:11:"division_no";s:1:"1";s:13:"department_no";s:1:"3";s:10:"section_no";s:1:"3";s:12:"authority_no";s:1:"2";s:14:"responsibility";s:1:"3";s:10:"first_name";s:6:"綾香";s:9:"last_name";s:6:"坪井";s:12:"mail_address";s:25:"pikaichi.uchino@gmail.com";s:13:"effectiveness";s:1:"1";s:14:"job_entry_date";s:10:"2013-10-01";s:11:"ic_card_key";s:4:"1003";s:11:"position_no";s:1:"1";s:17:"employee_category";s:1:"1";s:10:"retirement";s:1:"0";s:15:"retirement_date";N;s:8:"on_leave";s:1:"0";s:21:"attendance_adjustment";s:1:"0";s:10:"shift_work";s:1:"1";s:10:"start_time";N;s:12:"closing_time";N;s:8:"birthday";N;s:12:"basic_uptime";N;s:15:"holiday_pattern";s:1:"9";s:15:"relation_emp_no";s:4:"1003";s:8:"username";s:15:"坪井　綾香";}}', 4715311928);
INSERT INTO `cake_sessions` (`id`, `data`, `expires`) VALUES('ujtje074f58ltsiu20ku3pon43', 'Config|a:3:{s:9:"userAgent";s:32:"e04d3a82e71ee78f9f03601fde6e2750";s:4:"time";i:4711487211;s:9:"countdown";i:10;}', 4711487211);
INSERT INTO `cake_sessions` (`id`, `data`, `expires`) VALUES('vcr41q8ajqrp7ghm0d0avgndo7', 'Config|a:3:{s:9:"userAgent";s:32:"5ca4b7d57b54aa32f60fc0cf3d55f4c3";s:4:"time";i:4715297368;s:9:"countdown";i:10;}Message|a:1:{s:4:"auth";a:4:{s:7:"message";s:47:"You are not authorized to access that location.";s:3:"key";s:4:"auth";s:7:"element";s:13:"Flash/default";s:6:"params";a:0:{}}}Auth|a:1:{s:9:"AdminUser";a:27:{s:2:"id";s:1:"2";s:10:"company_no";s:1:"1";s:11:"division_no";s:1:"4";s:13:"department_no";s:1:"2";s:10:"section_no";s:1:"2";s:12:"authority_no";s:1:"1";s:14:"responsibility";s:1:"2";s:10:"first_name";s:6:"将禎";s:9:"last_name";s:6:"柳田";s:12:"mail_address";s:27:"pikaichi.yanagita@gmail.com";s:13:"effectiveness";s:1:"1";s:14:"job_entry_date";s:10:"2010-08-23";s:11:"ic_card_key";s:4:"0100";s:11:"position_no";s:1:"5";s:17:"employee_category";s:1:"4";s:10:"retirement";s:1:"0";s:15:"retirement_date";N;s:8:"on_leave";s:1:"0";s:21:"attendance_adjustment";s:1:"0";s:10:"shift_work";s:1:"1";s:10:"start_time";N;s:12:"closing_time";N;s:8:"birthday";N;s:12:"basic_uptime";N;s:15:"holiday_pattern";s:1:"9";s:15:"relation_emp_no";s:3:"100";s:8:"username";s:15:"柳田　将禎";}}', 4715297368);
INSERT INTO `cake_sessions` (`id`, `data`, `expires`) VALUES('vh7f9om4okppfsa98lq4tlk5c4', 'Config|a:3:{s:9:"userAgent";s:32:"077a67061cfa7fb160b6c72b175ce03a";s:4:"time";i:4715336412;s:9:"countdown";i:10;}Message|a:1:{s:4:"auth";a:4:{s:7:"message";s:47:"You are not authorized to access that location.";s:3:"key";s:4:"auth";s:7:"element";s:13:"Flash/default";s:6:"params";a:0:{}}}Auth|a:1:{s:9:"AdminUser";a:27:{s:2:"id";s:1:"3";s:10:"company_no";s:1:"1";s:11:"division_no";s:1:"1";s:13:"department_no";s:1:"1";s:10:"section_no";s:1:"1";s:12:"authority_no";s:1:"2";s:14:"responsibility";s:1:"2";s:10:"first_name";s:6:"慎吾";s:9:"last_name";s:6:"坪井";s:12:"mail_address";s:25:"pikaichi.tsuboi@gmail.com";s:13:"effectiveness";s:1:"1";s:14:"job_entry_date";s:10:"2010-11-01";s:11:"ic_card_key";s:4:"0101";s:11:"position_no";s:1:"4";s:17:"employee_category";s:1:"4";s:10:"retirement";s:1:"0";s:15:"retirement_date";N;s:8:"on_leave";s:1:"0";s:21:"attendance_adjustment";s:1:"0";s:10:"shift_work";s:1:"1";s:10:"start_time";N;s:12:"closing_time";N;s:8:"birthday";N;s:12:"basic_uptime";N;s:15:"holiday_pattern";s:1:"9";s:15:"relation_emp_no";s:3:"101";s:8:"username";s:15:"坪井　慎吾";}}', 4715336413);
INSERT INTO `cake_sessions` (`id`, `data`, `expires`) VALUES('vj5u9of24m9gfkl055in2nmo32', 'Config|a:3:{s:9:"userAgent";s:32:"ed569cbe4b8fbfe66141b9bb2381ec2a";s:4:"time";i:4715130327;s:9:"countdown";i:10;}Message|a:1:{s:4:"auth";a:4:{s:7:"message";s:47:"You are not authorized to access that location.";s:3:"key";s:4:"auth";s:7:"element";s:13:"Flash/default";s:6:"params";a:0:{}}}Auth|a:1:{s:8:"redirect";s:1:"/";}', 4715130327);
INSERT INTO `cake_sessions` (`id`, `data`, `expires`) VALUES('vrnblivcqup73j0dc5vlb9ee87', 'Config|a:3:{s:9:"userAgent";s:32:"5ca4b7d57b54aa32f60fc0cf3d55f4c3";s:4:"time";i:4715496465;s:9:"countdown";i:10;}Message|a:1:{s:4:"auth";a:4:{s:7:"message";s:47:"You are not authorized to access that location.";s:3:"key";s:4:"auth";s:7:"element";s:13:"Flash/default";s:6:"params";a:0:{}}}Auth|a:1:{s:9:"AdminUser";a:27:{s:2:"id";s:1:"8";s:10:"company_no";s:1:"1";s:11:"division_no";s:1:"1";s:13:"department_no";s:1:"3";s:10:"section_no";s:1:"3";s:12:"authority_no";s:1:"1";s:14:"responsibility";s:1:"3";s:10:"first_name";s:6:"亜耶";s:9:"last_name";s:6:"廣田";s:12:"mail_address";s:25:"pikaichi.hirota@gmail.com";s:13:"effectiveness";s:1:"1";s:14:"job_entry_date";s:10:"2014-11-10";s:11:"ic_card_key";s:4:"1007";s:11:"position_no";s:1:"1";s:17:"employee_category";s:1:"1";s:10:"retirement";s:1:"0";s:15:"retirement_date";N;s:8:"on_leave";s:1:"0";s:21:"attendance_adjustment";s:1:"0";s:10:"shift_work";s:1:"1";s:10:"start_time";N;s:12:"closing_time";N;s:8:"birthday";N;s:12:"basic_uptime";N;s:15:"holiday_pattern";s:1:"9";s:15:"relation_emp_no";s:4:"1007";s:8:"username";s:15:"廣田　亜耶";}}', 4715496465);

-- --------------------------------------------------------

--
-- テーブルの構造 `client_departments`
--

CREATE TABLE IF NOT EXISTS `client_departments` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(256) NOT NULL COMMENT '名称',
  `insert_user` int(11) NOT NULL,
  `insert_date` date NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0' COMMENT '削除',
  `deleted_date` date DEFAULT NULL COMMENT '削除日',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='部門テーブル' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `companies`
--

CREATE TABLE IF NOT EXISTS `companies` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(256) NOT NULL COMMENT '名称',
  `english_short_name` varchar(256) NOT NULL COMMENT '英語略名',
  `document_save_destination` varchar(256) NOT NULL COMMENT 'ドキュメント保存先',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='会社テーブル' AUTO_INCREMENT=2 ;

--
-- テーブルのデータのダンプ `companies`
--

INSERT INTO `companies` (`id`, `name`, `english_short_name`, `document_save_destination`) VALUES(1, '株式会社ピカいち', 'pikaichi', '');

-- --------------------------------------------------------

--
-- テーブルの構造 `daily_reports`
--

CREATE TABLE IF NOT EXISTS `daily_reports` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `operation_no` int(11) NOT NULL COMMENT '稼働ID',
  `work_content` varchar(1024) NOT NULL COMMENT '作業内容',
  `liaison_matters_problems` varchar(1024) DEFAULT NULL COMMENT '連絡事項・問題点',
  `schedule_tomorrow` varchar(256) DEFAULT NULL COMMENT '明日の予定',
  `comment` varchar(256) DEFAULT NULL,
  `report_date` date NOT NULL COMMENT '報告日',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='日報テーブル' AUTO_INCREMENT=65 ;

--
-- テーブルのデータのダンプ `daily_reports`
--

INSERT INTO `daily_reports` (`id`, `operation_no`, `work_content`, `liaison_matters_problems`, `schedule_tomorrow`, `comment`, `report_date`) VALUES(1, 1, '５', '', '', NULL, '2019-04-25');
INSERT INTO `daily_reports` (`id`, `operation_no`, `work_content`, `liaison_matters_problems`, `schedule_tomorrow`, `comment`, `report_date`) VALUES(2, 2, 'ああああ', '', '', NULL, '2019-04-25');
INSERT INTO `daily_reports` (`id`, `operation_no`, `work_content`, `liaison_matters_problems`, `schedule_tomorrow`, `comment`, `report_date`) VALUES(3, 3, 'asd', '', '', NULL, '2019-04-25');
INSERT INTO `daily_reports` (`id`, `operation_no`, `work_content`, `liaison_matters_problems`, `schedule_tomorrow`, `comment`, `report_date`) VALUES(4, 4, 'abc', '', '', NULL, '2019-04-25');
INSERT INTO `daily_reports` (`id`, `operation_no`, `work_content`, `liaison_matters_problems`, `schedule_tomorrow`, `comment`, `report_date`) VALUES(5, 5, 'ki', '', '', NULL, '2019-04-25');
INSERT INTO `daily_reports` (`id`, `operation_no`, `work_content`, `liaison_matters_problems`, `schedule_tomorrow`, `comment`, `report_date`) VALUES(6, 6, 'rew', '', '', NULL, '2019-04-25');
INSERT INTO `daily_reports` (`id`, `operation_no`, `work_content`, `liaison_matters_problems`, `schedule_tomorrow`, `comment`, `report_date`) VALUES(7, 7, '222', '', '', NULL, '2019-04-25');
INSERT INTO `daily_reports` (`id`, `operation_no`, `work_content`, `liaison_matters_problems`, `schedule_tomorrow`, `comment`, `report_date`) VALUES(8, 8, 'test001', 'test002', 'test003', 'testtest', '2019-04-26');
INSERT INTO `daily_reports` (`id`, `operation_no`, `work_content`, `liaison_matters_problems`, `schedule_tomorrow`, `comment`, `report_date`) VALUES(9, 9, 'aaaaa', '', '', NULL, '2019-04-26');
INSERT INTO `daily_reports` (`id`, `operation_no`, `work_content`, `liaison_matters_problems`, `schedule_tomorrow`, `comment`, `report_date`) VALUES(10, 10, '1', '', '', NULL, '2019-04-26');
INSERT INTO `daily_reports` (`id`, `operation_no`, `work_content`, `liaison_matters_problems`, `schedule_tomorrow`, `comment`, `report_date`) VALUES(11, 11, '2', '', '', NULL, '2019-04-26');
INSERT INTO `daily_reports` (`id`, `operation_no`, `work_content`, `liaison_matters_problems`, `schedule_tomorrow`, `comment`, `report_date`) VALUES(12, 12, '3', '', '', NULL, '2019-04-26');
INSERT INTO `daily_reports` (`id`, `operation_no`, `work_content`, `liaison_matters_problems`, `schedule_tomorrow`, `comment`, `report_date`) VALUES(13, 13, '4', '', '', NULL, '2019-04-26');
INSERT INTO `daily_reports` (`id`, `operation_no`, `work_content`, `liaison_matters_problems`, `schedule_tomorrow`, `comment`, `report_date`) VALUES(14, 14, '5', '', '', NULL, '2019-04-26');
INSERT INTO `daily_reports` (`id`, `operation_no`, `work_content`, `liaison_matters_problems`, `schedule_tomorrow`, `comment`, `report_date`) VALUES(15, 15, '作業内容', '問題点なし', '明日の予定は休みです', NULL, '2019-04-26');
INSERT INTO `daily_reports` (`id`, `operation_no`, `work_content`, `liaison_matters_problems`, `schedule_tomorrow`, `comment`, `report_date`) VALUES(16, 16, 'aaaaa', 'iiiii', 'uuuuu', 'rareawrareaw', '2019-04-27');
INSERT INTO `daily_reports` (`id`, `operation_no`, `work_content`, `liaison_matters_problems`, `schedule_tomorrow`, `comment`, `report_date`) VALUES(17, 17, 'aaa', 'bbb', 'ccc', NULL, '2019-04-27');
INSERT INTO `daily_reports` (`id`, `operation_no`, `work_content`, `liaison_matters_problems`, `schedule_tomorrow`, `comment`, `report_date`) VALUES(18, 18, 'test001', '', '', NULL, '2019-06-03');
INSERT INTO `daily_reports` (`id`, `operation_no`, `work_content`, `liaison_matters_problems`, `schedule_tomorrow`, `comment`, `report_date`) VALUES(19, 19, 'aaa', '', '', NULL, '2019-05-05');
INSERT INTO `daily_reports` (`id`, `operation_no`, `work_content`, `liaison_matters_problems`, `schedule_tomorrow`, `comment`, `report_date`) VALUES(20, 20, 'asdfadsf', '', '', NULL, '2019-04-10');
INSERT INTO `daily_reports` (`id`, `operation_no`, `work_content`, `liaison_matters_problems`, `schedule_tomorrow`, `comment`, `report_date`) VALUES(21, 21, 'sddfsfs', '', '', NULL, '2019-05-05');
INSERT INTO `daily_reports` (`id`, `operation_no`, `work_content`, `liaison_matters_problems`, `schedule_tomorrow`, `comment`, `report_date`) VALUES(22, 22, 'hjkhjk', '', '', NULL, '2019-04-16');
INSERT INTO `daily_reports` (`id`, `operation_no`, `work_content`, `liaison_matters_problems`, `schedule_tomorrow`, `comment`, `report_date`) VALUES(23, 23, 'a', '', '', NULL, '2019-05-14');
INSERT INTO `daily_reports` (`id`, `operation_no`, `work_content`, `liaison_matters_problems`, `schedule_tomorrow`, `comment`, `report_date`) VALUES(24, 24, 'プロネット　テスト', '', '', NULL, '2019-05-15');
INSERT INTO `daily_reports` (`id`, `operation_no`, `work_content`, `liaison_matters_problems`, `schedule_tomorrow`, `comment`, `report_date`) VALUES(25, 25, '', '', '', NULL, '2019-05-21');
INSERT INTO `daily_reports` (`id`, `operation_no`, `work_content`, `liaison_matters_problems`, `schedule_tomorrow`, `comment`, `report_date`) VALUES(26, 26, '', '', '', NULL, '2019-05-24');
INSERT INTO `daily_reports` (`id`, `operation_no`, `work_content`, `liaison_matters_problems`, `schedule_tomorrow`, `comment`, `report_date`) VALUES(27, 27, 'aa', '', '', NULL, '2019-05-24');
INSERT INTO `daily_reports` (`id`, `operation_no`, `work_content`, `liaison_matters_problems`, `schedule_tomorrow`, `comment`, `report_date`) VALUES(28, 28, 'a', '', '', NULL, '2019-05-31');
INSERT INTO `daily_reports` (`id`, `operation_no`, `work_content`, `liaison_matters_problems`, `schedule_tomorrow`, `comment`, `report_date`) VALUES(29, 29, 'a', '', '', NULL, '2019-05-31');
INSERT INTO `daily_reports` (`id`, `operation_no`, `work_content`, `liaison_matters_problems`, `schedule_tomorrow`, `comment`, `report_date`) VALUES(30, 31, 'a', '', '', NULL, '2019-05-31');
INSERT INTO `daily_reports` (`id`, `operation_no`, `work_content`, `liaison_matters_problems`, `schedule_tomorrow`, `comment`, `report_date`) VALUES(31, 33, 'j', '', '', NULL, '2019-05-31');
INSERT INTO `daily_reports` (`id`, `operation_no`, `work_content`, `liaison_matters_problems`, `schedule_tomorrow`, `comment`, `report_date`) VALUES(32, 32, 'a', '', '', NULL, '2019-05-31');
INSERT INTO `daily_reports` (`id`, `operation_no`, `work_content`, `liaison_matters_problems`, `schedule_tomorrow`, `comment`, `report_date`) VALUES(33, 34, 'a', '', '', NULL, '2019-06-02');
INSERT INTO `daily_reports` (`id`, `operation_no`, `work_content`, `liaison_matters_problems`, `schedule_tomorrow`, `comment`, `report_date`) VALUES(34, 35, 'a', '', '', NULL, '2019-06-02');
INSERT INTO `daily_reports` (`id`, `operation_no`, `work_content`, `liaison_matters_problems`, `schedule_tomorrow`, `comment`, `report_date`) VALUES(35, 36, '報告', '', '', NULL, '2019-06-02');
INSERT INTO `daily_reports` (`id`, `operation_no`, `work_content`, `liaison_matters_problems`, `schedule_tomorrow`, `comment`, `report_date`) VALUES(36, 37, '５月１日稼働分', '', '', NULL, '2019-06-02');
INSERT INTO `daily_reports` (`id`, `operation_no`, `work_content`, `liaison_matters_problems`, `schedule_tomorrow`, `comment`, `report_date`) VALUES(37, 38, '６月１日稼働分', '', '', NULL, '2019-06-02');
INSERT INTO `daily_reports` (`id`, `operation_no`, `work_content`, `liaison_matters_problems`, `schedule_tomorrow`, `comment`, `report_date`) VALUES(38, 39, 'a', '', '', NULL, '2019-06-02');
INSERT INTO `daily_reports` (`id`, `operation_no`, `work_content`, `liaison_matters_problems`, `schedule_tomorrow`, `comment`, `report_date`) VALUES(39, 40, 'a', '', '', NULL, '2019-06-02');
INSERT INTO `daily_reports` (`id`, `operation_no`, `work_content`, `liaison_matters_problems`, `schedule_tomorrow`, `comment`, `report_date`) VALUES(40, 41, 'a', '', '', NULL, '2019-06-02');
INSERT INTO `daily_reports` (`id`, `operation_no`, `work_content`, `liaison_matters_problems`, `schedule_tomorrow`, `comment`, `report_date`) VALUES(41, 51, 'a', '', '', NULL, '2019-06-04');
INSERT INTO `daily_reports` (`id`, `operation_no`, `work_content`, `liaison_matters_problems`, `schedule_tomorrow`, `comment`, `report_date`) VALUES(42, 52, 'a', '', '', NULL, '2019-06-04');
INSERT INTO `daily_reports` (`id`, `operation_no`, `work_content`, `liaison_matters_problems`, `schedule_tomorrow`, `comment`, `report_date`) VALUES(43, 53, 'a', '', '', NULL, '2019-06-04');
INSERT INTO `daily_reports` (`id`, `operation_no`, `work_content`, `liaison_matters_problems`, `schedule_tomorrow`, `comment`, `report_date`) VALUES(44, 49, 'a', '', '', NULL, '2019-06-04');
INSERT INTO `daily_reports` (`id`, `operation_no`, `work_content`, `liaison_matters_problems`, `schedule_tomorrow`, `comment`, `report_date`) VALUES(45, 54, 'a', '', '', NULL, '2019-06-04');
INSERT INTO `daily_reports` (`id`, `operation_no`, `work_content`, `liaison_matters_problems`, `schedule_tomorrow`, `comment`, `report_date`) VALUES(46, 55, 'あ', '', '', NULL, '2019-06-08');
INSERT INTO `daily_reports` (`id`, `operation_no`, `work_content`, `liaison_matters_problems`, `schedule_tomorrow`, `comment`, `report_date`) VALUES(47, 56, 'あ', '', '', NULL, '2019-06-08');
INSERT INTO `daily_reports` (`id`, `operation_no`, `work_content`, `liaison_matters_problems`, `schedule_tomorrow`, `comment`, `report_date`) VALUES(48, 57, 'あ', '', '', NULL, '2019-06-08');
INSERT INTO `daily_reports` (`id`, `operation_no`, `work_content`, `liaison_matters_problems`, `schedule_tomorrow`, `comment`, `report_date`) VALUES(49, 58, 'a', '', '', NULL, '2019-06-08');
INSERT INTO `daily_reports` (`id`, `operation_no`, `work_content`, `liaison_matters_problems`, `schedule_tomorrow`, `comment`, `report_date`) VALUES(50, 60, 'a', '', '', NULL, '2019-06-08');
INSERT INTO `daily_reports` (`id`, `operation_no`, `work_content`, `liaison_matters_problems`, `schedule_tomorrow`, `comment`, `report_date`) VALUES(51, 62, 'a', '', '', NULL, '2019-06-08');
INSERT INTO `daily_reports` (`id`, `operation_no`, `work_content`, `liaison_matters_problems`, `schedule_tomorrow`, `comment`, `report_date`) VALUES(52, 64, 'a', '', '', NULL, '2019-06-08');
INSERT INTO `daily_reports` (`id`, `operation_no`, `work_content`, `liaison_matters_problems`, `schedule_tomorrow`, `comment`, `report_date`) VALUES(53, 66, 'a', '', '', NULL, '2019-06-08');
INSERT INTO `daily_reports` (`id`, `operation_no`, `work_content`, `liaison_matters_problems`, `schedule_tomorrow`, `comment`, `report_date`) VALUES(54, 67, 'a', '', '', NULL, '2019-06-08');
INSERT INTO `daily_reports` (`id`, `operation_no`, `work_content`, `liaison_matters_problems`, `schedule_tomorrow`, `comment`, `report_date`) VALUES(55, 68, 'aaa', '', '', NULL, '2019-06-10');
INSERT INTO `daily_reports` (`id`, `operation_no`, `work_content`, `liaison_matters_problems`, `schedule_tomorrow`, `comment`, `report_date`) VALUES(56, 69, 'a', '', '', NULL, '2019-06-10');
INSERT INTO `daily_reports` (`id`, `operation_no`, `work_content`, `liaison_matters_problems`, `schedule_tomorrow`, `comment`, `report_date`) VALUES(57, 70, '', '', '', NULL, '2019-06-13');
INSERT INTO `daily_reports` (`id`, `operation_no`, `work_content`, `liaison_matters_problems`, `schedule_tomorrow`, `comment`, `report_date`) VALUES(58, 72, 'a', '', '', NULL, '2019-06-26');
INSERT INTO `daily_reports` (`id`, `operation_no`, `work_content`, `liaison_matters_problems`, `schedule_tomorrow`, `comment`, `report_date`) VALUES(59, 73, 'a', '', '', NULL, '2019-06-26');
INSERT INTO `daily_reports` (`id`, `operation_no`, `work_content`, `liaison_matters_problems`, `schedule_tomorrow`, `comment`, `report_date`) VALUES(60, 74, '１', '', '', NULL, '2019-06-26');
INSERT INTO `daily_reports` (`id`, `operation_no`, `work_content`, `liaison_matters_problems`, `schedule_tomorrow`, `comment`, `report_date`) VALUES(61, 75, 'あ', '', '', NULL, '2019-06-26');
INSERT INTO `daily_reports` (`id`, `operation_no`, `work_content`, `liaison_matters_problems`, `schedule_tomorrow`, `comment`, `report_date`) VALUES(62, 71, '', '', '', NULL, '2019-06-29');
INSERT INTO `daily_reports` (`id`, `operation_no`, `work_content`, `liaison_matters_problems`, `schedule_tomorrow`, `comment`, `report_date`) VALUES(63, 76, '', '', '', NULL, '2019-06-29');
INSERT INTO `daily_reports` (`id`, `operation_no`, `work_content`, `liaison_matters_problems`, `schedule_tomorrow`, `comment`, `report_date`) VALUES(64, 77, '', '', '', NULL, '2019-06-29');

-- --------------------------------------------------------

--
-- テーブルの構造 `departments`
--

CREATE TABLE IF NOT EXISTS `departments` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `division_no` int(11) NOT NULL COMMENT '事業部ID',
  `name` varchar(256) NOT NULL COMMENT '名称',
  `deleted` int(1) NOT NULL DEFAULT '0' COMMENT '削除',
  `deleted_date` datetime DEFAULT NULL COMMENT '削除日',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='部テーブル' AUTO_INCREMENT=4 ;

--
-- テーブルのデータのダンプ `departments`
--

INSERT INTO `departments` (`id`, `division_no`, `name`, `deleted`, `deleted_date`) VALUES(1, 1, 'リフォーム事業部', 0, NULL);
INSERT INTO `departments` (`id`, `division_no`, `name`, `deleted`, `deleted_date`) VALUES(2, 1, '不動産事業部', 0, NULL);
INSERT INTO `departments` (`id`, `division_no`, `name`, `deleted`, `deleted_date`) VALUES(3, 1, '管理本部', 0, NULL);

-- --------------------------------------------------------

--
-- テーブルの構造 `divisions`
--

CREATE TABLE IF NOT EXISTS `divisions` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `company_no` int(11) NOT NULL COMMENT '会社ID',
  `name` varchar(256) NOT NULL COMMENT '名称',
  `deleted` int(1) NOT NULL DEFAULT '0' COMMENT '削除',
  `deleted_date` datetime DEFAULT NULL COMMENT '削除日',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='事業部テーブル' AUTO_INCREMENT=2 ;

--
-- テーブルのデータのダンプ `divisions`
--

INSERT INTO `divisions` (`id`, `company_no`, `name`, `deleted`, `deleted_date`) VALUES(1, 1, 'ピカいち', 0, NULL);

-- --------------------------------------------------------

--
-- テーブルの構造 `documents`
--

CREATE TABLE IF NOT EXISTS `documents` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `company_no` int(11) NOT NULL COMMENT '会社ID',
  `name` varchar(256) NOT NULL COMMENT '名称',
  `file_name` varchar(256) NOT NULL COMMENT 'ファイル名',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='ドキュメントテーブル' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `employees`
--

CREATE TABLE IF NOT EXISTS `employees` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `company_no` int(11) NOT NULL,
  `division_no` int(11) NOT NULL,
  `department_no` int(11) NOT NULL,
  `section_no` int(11) NOT NULL COMMENT '課ID',
  `authority_no` int(11) DEFAULT NULL COMMENT '権限',
  `responsibility` int(11) DEFAULT NULL COMMENT '職責',
  `first_name` varchar(128) NOT NULL COMMENT '名前',
  `last_name` varchar(128) NOT NULL COMMENT '苗字',
  `mail_address` varchar(256) NOT NULL COMMENT 'メールアドレス',
  `password` varchar(100) NOT NULL COMMENT 'パスワード',
  `effectiveness` int(1) NOT NULL DEFAULT '1' COMMENT '有効',
  `job_entry_date` date DEFAULT NULL COMMENT '入社年月日',
  `ic_card_key` varchar(256) DEFAULT NULL COMMENT 'ICカードキー',
  `position_no` int(11) DEFAULT NULL COMMENT '役職ID',
  `employee_category` int(2) NOT NULL DEFAULT '1' COMMENT '1:社員 2:パート 3:嘱託 4:役員',
  `retirement` int(1) NOT NULL DEFAULT '0' COMMENT '退職',
  `retirement_date` date DEFAULT NULL COMMENT '退職日',
  `on_leave` int(1) NOT NULL DEFAULT '0' COMMENT '休職中',
  `attendance_adjustment` int(1) NOT NULL DEFAULT '0' COMMENT '就業調整有り',
  `shift_work` int(1) NOT NULL DEFAULT '1' COMMENT 'シフト勤務',
  `start_time` time DEFAULT NULL COMMENT '始業時刻',
  `closing_time` time DEFAULT NULL COMMENT '終業時刻',
  `birthday` date DEFAULT NULL,
  `basic_uptime` decimal(3,1) DEFAULT NULL COMMENT '基本稼働時間',
  `holiday_pattern` int(1) NOT NULL DEFAULT '9' COMMENT '休日パターン（1:パターンA　2:パターンB　9:通常勤務)',
  `relation_emp_no` int(5) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='社員テーブル' AUTO_INCREMENT=33 ;

--
-- テーブルのデータのダンプ `employees`
--

INSERT INTO `employees` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `authority_no`, `responsibility`, `first_name`, `last_name`, `mail_address`, `password`, `effectiveness`, `job_entry_date`, `ic_card_key`, `position_no`, `employee_category`, `retirement`, `retirement_date`, `on_leave`, `attendance_adjustment`, `shift_work`, `start_time`, `closing_time`, `birthday`, `basic_uptime`, `holiday_pattern`, `relation_emp_no`) VALUES(2, 1, 4, 2, 2, 1, 2, '将禎', '柳田', 'pikaichi.yanagita@gmail.com', '14702e12e7fb7a784f64f1d6a1f5ae2373fcdd53', 1, '2010-08-23', '0100', 5, 4, 0, NULL, 0, 0, 1, NULL, NULL, NULL, NULL, 9, 100);
INSERT INTO `employees` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `authority_no`, `responsibility`, `first_name`, `last_name`, `mail_address`, `password`, `effectiveness`, `job_entry_date`, `ic_card_key`, `position_no`, `employee_category`, `retirement`, `retirement_date`, `on_leave`, `attendance_adjustment`, `shift_work`, `start_time`, `closing_time`, `birthday`, `basic_uptime`, `holiday_pattern`, `relation_emp_no`) VALUES(3, 1, 1, 1, 1, 2, 2, '慎吾', '坪井', 'pikaichi.tsuboi@gmail.com', '14702e12e7fb7a784f64f1d6a1f5ae2373fcdd53', 1, '2010-11-01', '0101', 4, 4, 0, NULL, 0, 0, 1, NULL, NULL, NULL, NULL, 9, 101);
INSERT INTO `employees` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `authority_no`, `responsibility`, `first_name`, `last_name`, `mail_address`, `password`, `effectiveness`, `job_entry_date`, `ic_card_key`, `position_no`, `employee_category`, `retirement`, `retirement_date`, `on_leave`, `attendance_adjustment`, `shift_work`, `start_time`, `closing_time`, `birthday`, `basic_uptime`, `holiday_pattern`, `relation_emp_no`) VALUES(4, 1, 1, 1, 1, 2, 2, '誠', '秋野', 'pikaichi.akino@gmail.com', '14702e12e7fb7a784f64f1d6a1f5ae2373fcdd53', 1, '2012-06-01', '0103', 3, 4, 0, NULL, 0, 0, 1, NULL, NULL, NULL, NULL, 9, 103);
INSERT INTO `employees` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `authority_no`, `responsibility`, `first_name`, `last_name`, `mail_address`, `password`, `effectiveness`, `job_entry_date`, `ic_card_key`, `position_no`, `employee_category`, `retirement`, `retirement_date`, `on_leave`, `attendance_adjustment`, `shift_work`, `start_time`, `closing_time`, `birthday`, `basic_uptime`, `holiday_pattern`, `relation_emp_no`) VALUES(5, 1, 1, 1, 1, 2, 0, '公洋', '荒鎌', 'pikaichi.arakama@gmail.com', '14702e12e7fb7a784f64f1d6a1f5ae2373fcdd53', 1, '2012-02-20', '1001', 1, 1, 0, NULL, 0, 0, 1, NULL, NULL, NULL, NULL, 9, 1001);
INSERT INTO `employees` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `authority_no`, `responsibility`, `first_name`, `last_name`, `mail_address`, `password`, `effectiveness`, `job_entry_date`, `ic_card_key`, `position_no`, `employee_category`, `retirement`, `retirement_date`, `on_leave`, `attendance_adjustment`, `shift_work`, `start_time`, `closing_time`, `birthday`, `basic_uptime`, `holiday_pattern`, `relation_emp_no`) VALUES(6, 1, 1, 1, 1, 2, 0, '雅史', '谷本', 'pikaichi.tanimoto@gmail.com', '14702e12e7fb7a784f64f1d6a1f5ae2373fcdd53', 1, '2013-01-01', '1002', 1, 1, 0, NULL, 0, 0, 1, NULL, NULL, NULL, NULL, 9, 1002);
INSERT INTO `employees` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `authority_no`, `responsibility`, `first_name`, `last_name`, `mail_address`, `password`, `effectiveness`, `job_entry_date`, `ic_card_key`, `position_no`, `employee_category`, `retirement`, `retirement_date`, `on_leave`, `attendance_adjustment`, `shift_work`, `start_time`, `closing_time`, `birthday`, `basic_uptime`, `holiday_pattern`, `relation_emp_no`) VALUES(7, 1, 1, 3, 3, 2, 3, '綾香', '坪井', 'pikaichi.uchino@gmail.com', '14702e12e7fb7a784f64f1d6a1f5ae2373fcdd53', 1, '2013-10-01', '1003', 1, 1, 0, NULL, 0, 0, 1, NULL, NULL, NULL, NULL, 9, 1003);
INSERT INTO `employees` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `authority_no`, `responsibility`, `first_name`, `last_name`, `mail_address`, `password`, `effectiveness`, `job_entry_date`, `ic_card_key`, `position_no`, `employee_category`, `retirement`, `retirement_date`, `on_leave`, `attendance_adjustment`, `shift_work`, `start_time`, `closing_time`, `birthday`, `basic_uptime`, `holiday_pattern`, `relation_emp_no`) VALUES(8, 1, 1, 3, 3, 1, 3, '亜耶', '廣田', 'pikaichi.hirota@gmail.com', '14702e12e7fb7a784f64f1d6a1f5ae2373fcdd53', 1, '2014-11-10', '1007', 1, 1, 0, NULL, 0, 0, 1, NULL, NULL, NULL, NULL, 9, 1007);
INSERT INTO `employees` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `authority_no`, `responsibility`, `first_name`, `last_name`, `mail_address`, `password`, `effectiveness`, `job_entry_date`, `ic_card_key`, `position_no`, `employee_category`, `retirement`, `retirement_date`, `on_leave`, `attendance_adjustment`, `shift_work`, `start_time`, `closing_time`, `birthday`, `basic_uptime`, `holiday_pattern`, `relation_emp_no`) VALUES(9, 1, 1, 1, 1, 2, 0, '吉博', '小林', 'pikaichi.kobayashi@gmail.com', '14702e12e7fb7a784f64f1d6a1f5ae2373fcdd53', 1, '2015-02-02', '1008', 1, 1, 0, NULL, 0, 0, 1, NULL, NULL, NULL, NULL, 9, 1008);
INSERT INTO `employees` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `authority_no`, `responsibility`, `first_name`, `last_name`, `mail_address`, `password`, `effectiveness`, `job_entry_date`, `ic_card_key`, `position_no`, `employee_category`, `retirement`, `retirement_date`, `on_leave`, `attendance_adjustment`, `shift_work`, `start_time`, `closing_time`, `birthday`, `basic_uptime`, `holiday_pattern`, `relation_emp_no`) VALUES(10, 1, 1, 1, 1, 2, 0, '好広', '野田', 'pikaichi.noday@gmail.com', '14702e12e7fb7a784f64f1d6a1f5ae2373fcdd53', 1, '2015-07-01', '1011', 1, 1, 0, NULL, 0, 0, 1, NULL, NULL, NULL, NULL, 9, 1011);
INSERT INTO `employees` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `authority_no`, `responsibility`, `first_name`, `last_name`, `mail_address`, `password`, `effectiveness`, `job_entry_date`, `ic_card_key`, `position_no`, `employee_category`, `retirement`, `retirement_date`, `on_leave`, `attendance_adjustment`, `shift_work`, `start_time`, `closing_time`, `birthday`, `basic_uptime`, `holiday_pattern`, `relation_emp_no`) VALUES(11, 1, 1, 1, 1, 2, 0, '信博', '渡邉', 'pikaichi.watanaben@gmail.com', '14702e12e7fb7a784f64f1d6a1f5ae2373fcdd53', 1, '2015-12-14', '1015', 1, 1, 0, NULL, 0, 0, 1, NULL, NULL, NULL, NULL, 9, 1015);
INSERT INTO `employees` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `authority_no`, `responsibility`, `first_name`, `last_name`, `mail_address`, `password`, `effectiveness`, `job_entry_date`, `ic_card_key`, `position_no`, `employee_category`, `retirement`, `retirement_date`, `on_leave`, `attendance_adjustment`, `shift_work`, `start_time`, `closing_time`, `birthday`, `basic_uptime`, `holiday_pattern`, `relation_emp_no`) VALUES(12, 1, 1, 3, 3, 2, 0, '真奈', '勘定', 'pikaichi.kanjyo@gmail.com', '14702e12e7fb7a784f64f1d6a1f5ae2373fcdd53', 1, '2016-06-01', '1018', 1, 1, 0, NULL, 0, 0, 1, NULL, NULL, NULL, NULL, 9, 1018);
INSERT INTO `employees` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `authority_no`, `responsibility`, `first_name`, `last_name`, `mail_address`, `password`, `effectiveness`, `job_entry_date`, `ic_card_key`, `position_no`, `employee_category`, `retirement`, `retirement_date`, `on_leave`, `attendance_adjustment`, `shift_work`, `start_time`, `closing_time`, `birthday`, `basic_uptime`, `holiday_pattern`, `relation_emp_no`) VALUES(13, 1, 1, 1, 1, 2, 0, '祐亮', '海老名', 'pikaichi.ebina@gmail.com', '14702e12e7fb7a784f64f1d6a1f5ae2373fcdd53', 1, '2016-09-26', '1019', 1, 1, 0, NULL, 0, 0, 1, NULL, NULL, NULL, NULL, 9, 1019);
INSERT INTO `employees` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `authority_no`, `responsibility`, `first_name`, `last_name`, `mail_address`, `password`, `effectiveness`, `job_entry_date`, `ic_card_key`, `position_no`, `employee_category`, `retirement`, `retirement_date`, `on_leave`, `attendance_adjustment`, `shift_work`, `start_time`, `closing_time`, `birthday`, `basic_uptime`, `holiday_pattern`, `relation_emp_no`) VALUES(14, 1, 1, 1, 1, 2, 0, '敦', '堀', 'pikaichi.horia@gmail.com', '14702e12e7fb7a784f64f1d6a1f5ae2373fcdd53', 1, '2016-10-17', '1020', 1, 1, 0, NULL, 0, 0, 1, NULL, NULL, NULL, NULL, 9, 1020);
INSERT INTO `employees` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `authority_no`, `responsibility`, `first_name`, `last_name`, `mail_address`, `password`, `effectiveness`, `job_entry_date`, `ic_card_key`, `position_no`, `employee_category`, `retirement`, `retirement_date`, `on_leave`, `attendance_adjustment`, `shift_work`, `start_time`, `closing_time`, `birthday`, `basic_uptime`, `holiday_pattern`, `relation_emp_no`) VALUES(15, 1, 1, 1, 1, 2, 0, '聡', '羽生', 'pikaichi.haniu@gmail.com', '14702e12e7fb7a784f64f1d6a1f5ae2373fcdd53', 1, '2017-01-01', '1022', 1, 1, 0, NULL, 0, 0, 1, NULL, NULL, NULL, NULL, 9, 1022);
INSERT INTO `employees` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `authority_no`, `responsibility`, `first_name`, `last_name`, `mail_address`, `password`, `effectiveness`, `job_entry_date`, `ic_card_key`, `position_no`, `employee_category`, `retirement`, `retirement_date`, `on_leave`, `attendance_adjustment`, `shift_work`, `start_time`, `closing_time`, `birthday`, `basic_uptime`, `holiday_pattern`, `relation_emp_no`) VALUES(16, 1, 1, 1, 1, 2, 0, '隼', '菊地', 'pikaichi.tanaka@gmail.com', '14702e12e7fb7a784f64f1d6a1f5ae2373fcdd53', 1, '2017-01-18', '1023', 1, 1, 0, NULL, 0, 0, 1, NULL, NULL, NULL, NULL, 9, 1023);
INSERT INTO `employees` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `authority_no`, `responsibility`, `first_name`, `last_name`, `mail_address`, `password`, `effectiveness`, `job_entry_date`, `ic_card_key`, `position_no`, `employee_category`, `retirement`, `retirement_date`, `on_leave`, `attendance_adjustment`, `shift_work`, `start_time`, `closing_time`, `birthday`, `basic_uptime`, `holiday_pattern`, `relation_emp_no`) VALUES(17, 1, 1, 3, 3, 2, 0, '夏海', '井桁', 'pikaichi.natsumi@gmail.com', '14702e12e7fb7a784f64f1d6a1f5ae2373fcdd53', 1, '2017-04-01', '1026', 1, 1, 0, NULL, 0, 0, 1, NULL, NULL, NULL, NULL, 9, 1026);
INSERT INTO `employees` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `authority_no`, `responsibility`, `first_name`, `last_name`, `mail_address`, `password`, `effectiveness`, `job_entry_date`, `ic_card_key`, `position_no`, `employee_category`, `retirement`, `retirement_date`, `on_leave`, `attendance_adjustment`, `shift_work`, `start_time`, `closing_time`, `birthday`, `basic_uptime`, `holiday_pattern`, `relation_emp_no`) VALUES(18, 1, 1, 1, 1, 2, 0, '秀明', '宮野', 'pikaichi.miyano@gmail.com', '14702e12e7fb7a784f64f1d6a1f5ae2373fcdd53', 1, '2017-05-08', '1028', 1, 1, 0, NULL, 0, 0, 1, NULL, NULL, NULL, NULL, 9, 1028);
INSERT INTO `employees` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `authority_no`, `responsibility`, `first_name`, `last_name`, `mail_address`, `password`, `effectiveness`, `job_entry_date`, `ic_card_key`, `position_no`, `employee_category`, `retirement`, `retirement_date`, `on_leave`, `attendance_adjustment`, `shift_work`, `start_time`, `closing_time`, `birthday`, `basic_uptime`, `holiday_pattern`, `relation_emp_no`) VALUES(19, 1, 1, 1, 1, 2, 0, '英司', '阿知羅', 'pikaichi.achira@gmail.com', '14702e12e7fb7a784f64f1d6a1f5ae2373fcdd53', 1, '2017-10-10', '1029', 1, 1, 0, NULL, 0, 0, 1, NULL, NULL, NULL, NULL, 9, 1029);
INSERT INTO `employees` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `authority_no`, `responsibility`, `first_name`, `last_name`, `mail_address`, `password`, `effectiveness`, `job_entry_date`, `ic_card_key`, `position_no`, `employee_category`, `retirement`, `retirement_date`, `on_leave`, `attendance_adjustment`, `shift_work`, `start_time`, `closing_time`, `birthday`, `basic_uptime`, `holiday_pattern`, `relation_emp_no`) VALUES(20, 1, 1, 1, 1, 2, 0, '徹洋', '高師', 'pikaichi.tanaka@gmail.com', '14702e12e7fb7a784f64f1d6a1f5ae2373fcdd53', 1, '2017-11-01', '1030', 1, 1, 0, NULL, 0, 0, 1, NULL, NULL, NULL, NULL, 9, 1030);
INSERT INTO `employees` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `authority_no`, `responsibility`, `first_name`, `last_name`, `mail_address`, `password`, `effectiveness`, `job_entry_date`, `ic_card_key`, `position_no`, `employee_category`, `retirement`, `retirement_date`, `on_leave`, `attendance_adjustment`, `shift_work`, `start_time`, `closing_time`, `birthday`, `basic_uptime`, `holiday_pattern`, `relation_emp_no`) VALUES(21, 1, 1, 3, 3, 2, 0, '瑠奈', '大多和', 'pikaichi.otawa@gmail.com', '14702e12e7fb7a784f64f1d6a1f5ae2373fcdd53', 1, '2018-04-01', '1032', 1, 1, 0, NULL, 0, 0, 1, NULL, NULL, NULL, NULL, 9, 1032);
INSERT INTO `employees` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `authority_no`, `responsibility`, `first_name`, `last_name`, `mail_address`, `password`, `effectiveness`, `job_entry_date`, `ic_card_key`, `position_no`, `employee_category`, `retirement`, `retirement_date`, `on_leave`, `attendance_adjustment`, `shift_work`, `start_time`, `closing_time`, `birthday`, `basic_uptime`, `holiday_pattern`, `relation_emp_no`) VALUES(22, 1, 1, 2, 2, 2, 0, '愛理', '蒔田', 'pikaichi.makita@gmail.com', '14702e12e7fb7a784f64f1d6a1f5ae2373fcdd53', 1, '2018-04-01', '1033', 1, 1, 0, NULL, 0, 0, 1, NULL, NULL, NULL, NULL, 9, 1033);
INSERT INTO `employees` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `authority_no`, `responsibility`, `first_name`, `last_name`, `mail_address`, `password`, `effectiveness`, `job_entry_date`, `ic_card_key`, `position_no`, `employee_category`, `retirement`, `retirement_date`, `on_leave`, `attendance_adjustment`, `shift_work`, `start_time`, `closing_time`, `birthday`, `basic_uptime`, `holiday_pattern`, `relation_emp_no`) VALUES(23, 1, 1, 2, 2, 2, 0, 'かつみ', '井上', 'pikaichi.inoue@gmail.com', '14702e12e7fb7a784f64f1d6a1f5ae2373fcdd53', 1, '2014-05-19', '2003', 1, 1, 0, NULL, 0, 0, 1, NULL, NULL, NULL, NULL, 9, 2003);
INSERT INTO `employees` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `authority_no`, `responsibility`, `first_name`, `last_name`, `mail_address`, `password`, `effectiveness`, `job_entry_date`, `ic_card_key`, `position_no`, `employee_category`, `retirement`, `retirement_date`, `on_leave`, `attendance_adjustment`, `shift_work`, `start_time`, `closing_time`, `birthday`, `basic_uptime`, `holiday_pattern`, `relation_emp_no`) VALUES(24, 1, 1, 3, 3, 2, 0, '洋子', '平', 'pikaichi.hira@gmail.com', '14702e12e7fb7a784f64f1d6a1f5ae2373fcdd53', 1, '2015-11-01', '2005', 1, 2, 0, NULL, 0, 0, 1, NULL, NULL, NULL, NULL, 9, 2005);
INSERT INTO `employees` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `authority_no`, `responsibility`, `first_name`, `last_name`, `mail_address`, `password`, `effectiveness`, `job_entry_date`, `ic_card_key`, `position_no`, `employee_category`, `retirement`, `retirement_date`, `on_leave`, `attendance_adjustment`, `shift_work`, `start_time`, `closing_time`, `birthday`, `basic_uptime`, `holiday_pattern`, `relation_emp_no`) VALUES(25, 1, 1, 3, 3, 2, 0, '晴海', '浅野', 'pikaichi.asano@gmail.com', '14702e12e7fb7a784f64f1d6a1f5ae2373fcdd53', 1, '2016-11-28', '2010', 1, 2, 0, NULL, 0, 0, 1, NULL, NULL, NULL, NULL, 9, 2010);
INSERT INTO `employees` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `authority_no`, `responsibility`, `first_name`, `last_name`, `mail_address`, `password`, `effectiveness`, `job_entry_date`, `ic_card_key`, `position_no`, `employee_category`, `retirement`, `retirement_date`, `on_leave`, `attendance_adjustment`, `shift_work`, `start_time`, `closing_time`, `birthday`, `basic_uptime`, `holiday_pattern`, `relation_emp_no`) VALUES(26, 1, 1, 3, 3, 2, 0, '美和', '宮内', 'pikaichi.miyauchi@gmail.com', '14702e12e7fb7a784f64f1d6a1f5ae2373fcdd53', 1, '2017-02-02', '2012', 1, 2, 0, NULL, 0, 0, 1, NULL, NULL, NULL, NULL, 9, 2012);
INSERT INTO `employees` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `authority_no`, `responsibility`, `first_name`, `last_name`, `mail_address`, `password`, `effectiveness`, `job_entry_date`, `ic_card_key`, `position_no`, `employee_category`, `retirement`, `retirement_date`, `on_leave`, `attendance_adjustment`, `shift_work`, `start_time`, `closing_time`, `birthday`, `basic_uptime`, `holiday_pattern`, `relation_emp_no`) VALUES(27, 1, 1, 1, 1, 2, 0, '海周', '田中', 'pikaichi.tanaka@gmail.com', '14702e12e7fb7a784f64f1d6a1f5ae2373fcdd53', 1, '2017-03-30', '2013', 1, 1, 0, NULL, 0, 0, 1, NULL, NULL, NULL, NULL, 9, 2013);
INSERT INTO `employees` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `authority_no`, `responsibility`, `first_name`, `last_name`, `mail_address`, `password`, `effectiveness`, `job_entry_date`, `ic_card_key`, `position_no`, `employee_category`, `retirement`, `retirement_date`, `on_leave`, `attendance_adjustment`, `shift_work`, `start_time`, `closing_time`, `birthday`, `basic_uptime`, `holiday_pattern`, `relation_emp_no`) VALUES(28, 1, 1, 3, 3, 2, 0, '二三男', '中古', 'pikaichi.cyuko@gmail.com', '14702e12e7fb7a784f64f1d6a1f5ae2373fcdd53', 1, '2017-06-20', '2014', 1, 2, 0, NULL, 0, 0, 1, NULL, NULL, NULL, NULL, 9, 2014);
INSERT INTO `employees` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `authority_no`, `responsibility`, `first_name`, `last_name`, `mail_address`, `password`, `effectiveness`, `job_entry_date`, `ic_card_key`, `position_no`, `employee_category`, `retirement`, `retirement_date`, `on_leave`, `attendance_adjustment`, `shift_work`, `start_time`, `closing_time`, `birthday`, `basic_uptime`, `holiday_pattern`, `relation_emp_no`) VALUES(29, 1, 1, 1, 1, 2, 0, '太志', '川畑', 'pikaichi.kawabata@gmail.com', '14702e12e7fb7a784f64f1d6a1f5ae2373fcdd53', 1, '2017-03-30', '2015', 1, 2, 0, NULL, 0, 0, 1, NULL, NULL, NULL, NULL, 9, 2015);
INSERT INTO `employees` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `authority_no`, `responsibility`, `first_name`, `last_name`, `mail_address`, `password`, `effectiveness`, `job_entry_date`, `ic_card_key`, `position_no`, `employee_category`, `retirement`, `retirement_date`, `on_leave`, `attendance_adjustment`, `shift_work`, `start_time`, `closing_time`, `birthday`, `basic_uptime`, `holiday_pattern`, `relation_emp_no`) VALUES(30, 1, 1, 3, 3, 2, 0, '由美', '四辻', 'pikaichi.yotsutsuji@gmail.com', '14702e12e7fb7a784f64f1d6a1f5ae2373fcdd53', 1, '2018-07-01', '2016', 1, 2, 0, NULL, 0, 0, 1, NULL, NULL, NULL, NULL, 9, 2016);
INSERT INTO `employees` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `authority_no`, `responsibility`, `first_name`, `last_name`, `mail_address`, `password`, `effectiveness`, `job_entry_date`, `ic_card_key`, `position_no`, `employee_category`, `retirement`, `retirement_date`, `on_leave`, `attendance_adjustment`, `shift_work`, `start_time`, `closing_time`, `birthday`, `basic_uptime`, `holiday_pattern`, `relation_emp_no`) VALUES(31, 1, 1, 3, 3, 2, 0, 'ちはる', '田邉', 'pikaichi.tanabe@gmail.com', '14702e12e7fb7a784f64f1d6a1f5ae2373fcdd53', 1, '2019-02-12', '2017', NULL, 2, 0, NULL, 0, 0, 1, NULL, NULL, NULL, NULL, 9, 2017);
INSERT INTO `employees` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `authority_no`, `responsibility`, `first_name`, `last_name`, `mail_address`, `password`, `effectiveness`, `job_entry_date`, `ic_card_key`, `position_no`, `employee_category`, `retirement`, `retirement_date`, `on_leave`, `attendance_adjustment`, `shift_work`, `start_time`, `closing_time`, `birthday`, `basic_uptime`, `holiday_pattern`, `relation_emp_no`) VALUES(32, 1, 1, 3, 3, 1, 0, 'テスト', 'プロネット', 'pronet@gmail.com', '14702e12e7fb7a784f64f1d6a1f5ae2373fcdd53', 1, '2014-11-10', '1007', 1, 1, 0, NULL, 0, 0, 1, NULL, NULL, NULL, NULL, 9, 5555);

-- --------------------------------------------------------

--
-- テーブルの構造 `holidays`
--

CREATE TABLE IF NOT EXISTS `holidays` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `pattern` int(1) NOT NULL DEFAULT '9',
  `holiday_date` date NOT NULL,
  `year` int(4) NOT NULL COMMENT '年',
  `month` int(11) NOT NULL COMMENT '月',
  `day` int(2) NOT NULL COMMENT '休日区分',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='休日テーブル' AUTO_INCREMENT=164 ;

--
-- テーブルのデータのダンプ `holidays`
--

INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(4, 9, '2019-04-06', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(5, 9, '2019-04-07', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(6, 9, '2019-04-13', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(7, 9, '2019-04-14', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(8, 9, '2019-04-20', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(9, 9, '2019-04-21', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(10, 9, '2019-04-27', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(11, 9, '2019-04-28', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(12, 9, '2019-04-29', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(13, 9, '2019-05-03', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(14, 9, '2019-05-04', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(15, 9, '2019-05-05', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(16, 9, '2019-05-06', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(17, 9, '2019-05-11', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(18, 9, '2019-05-12', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(19, 9, '2019-05-18', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(20, 9, '2019-05-19', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(21, 9, '2019-05-26', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(22, 9, '2019-06-01', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(23, 9, '2019-06-02', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(24, 9, '2019-06-08', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(25, 9, '2019-06-09', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(26, 9, '2019-06-15', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(27, 9, '2019-06-16', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(28, 9, '2019-06-23', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(29, 9, '2019-06-29', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(30, 9, '2019-06-30', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(31, 9, '2019-07-06', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(32, 9, '2019-07-07', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(33, 9, '2019-07-13', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(34, 9, '2019-07-14', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(35, 9, '2019-07-15', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(36, 9, '2019-07-20', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(37, 9, '2019-07-21', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(38, 9, '2019-07-27', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(39, 9, '2019-07-28', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(40, 9, '2019-08-03', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(41, 9, '2019-08-04', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(42, 9, '2019-08-10', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(43, 9, '2019-08-11', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(44, 9, '2019-08-12', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(45, 9, '2019-08-13', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(46, 9, '2019-08-14', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(47, 9, '2019-08-15', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(48, 9, '2019-08-18', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(49, 9, '2019-08-24', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(50, 9, '2019-08-25', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(51, 9, '2019-08-31', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(52, 9, '2019-09-01', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(53, 9, '2019-09-07', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(54, 9, '2019-09-08', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(55, 9, '2019-09-14', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(56, 9, '2019-09-15', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(57, 9, '2019-09-16', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(58, 9, '2019-09-20', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(59, 9, '2019-09-21', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(60, 9, '2019-09-22', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(61, 9, '2019-09-23', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(62, 9, '2019-09-28', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(63, 9, '2019-09-29', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(64, 1, '2019-04-07', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(65, 1, '2019-04-13', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(66, 1, '2019-04-14', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(67, 1, '2019-04-21', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(68, 1, '2019-04-27', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(69, 1, '2019-04-28', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(70, 1, '2019-04-29', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(71, 1, '2019-05-03', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(72, 1, '2019-05-04', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(73, 1, '2019-05-05', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(74, 1, '2019-05-06', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(75, 1, '2019-05-12', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(76, 1, '2019-05-18', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(77, 1, '2019-05-19', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(78, 1, '2019-05-26', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(79, 1, '2019-06-02', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(80, 1, '2019-06-08', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(81, 1, '2019-06-09', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(82, 1, '2019-06-16', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(83, 1, '2019-06-23', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(84, 1, '2019-06-29', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(85, 1, '2019-06-30', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(86, 1, '2019-07-07', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(87, 1, '2019-07-13', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(88, 1, '2019-07-14', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(89, 1, '2019-07-15', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(90, 1, '2019-07-21', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(91, 1, '2019-07-27', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(92, 1, '2019-07-28', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(93, 1, '2019-08-04', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(94, 1, '2019-08-10', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(95, 1, '2019-08-11', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(96, 1, '2019-08-12', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(97, 1, '2019-08-13', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(98, 1, '2019-08-14', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(99, 1, '2019-08-15', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(100, 1, '2019-08-18', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(101, 1, '2019-08-24', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(102, 1, '2019-08-25', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(103, 1, '2019-09-01', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(104, 1, '2019-09-07', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(105, 1, '2019-09-08', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(106, 1, '2019-09-15', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(107, 1, '2019-09-16', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(108, 1, '2019-09-21', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(109, 1, '2019-09-22', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(110, 1, '2019-09-23', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(111, 1, '2019-09-29', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(112, 2, '2019-04-06', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(113, 2, '2019-04-07', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(114, 2, '2019-04-14', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(115, 2, '2019-04-20', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(116, 2, '2019-04-21', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(117, 2, '2019-04-28', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(118, 2, '2019-04-29', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(119, 2, '2019-05-03', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(120, 2, '2019-05-04', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(121, 2, '2019-05-05', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(122, 2, '2019-05-06', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(123, 2, '2019-05-11', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(124, 2, '2019-05-12', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(125, 2, '2019-05-19', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(126, 2, '2019-05-26', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(127, 2, '2019-06-01', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(128, 2, '2019-06-02', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(129, 2, '2019-06-09', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(130, 2, '2019-06-15', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(131, 2, '2019-06-16', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(132, 2, '2019-06-23', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(133, 2, '2019-06-30', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(134, 2, '2019-07-06', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(135, 2, '2019-07-07', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(136, 2, '2019-07-14', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(137, 2, '2019-07-15', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(138, 2, '2019-07-20', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(139, 2, '2019-07-21', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(140, 2, '2019-07-28', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(141, 2, '2019-08-03', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(142, 2, '2019-08-04', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(143, 2, '2019-08-10', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(144, 2, '2019-08-11', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(145, 2, '2019-08-12', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(146, 2, '2019-08-13', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(147, 2, '2019-08-14', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(148, 2, '2019-08-15', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(149, 2, '2019-08-18', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(150, 2, '2019-08-25', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(151, 2, '2019-08-31', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(152, 2, '2019-09-01', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(153, 2, '2019-09-08', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(154, 2, '2019-09-14', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(155, 2, '2019-09-15', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(156, 2, '2019-09-16', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(157, 2, '2019-09-20', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(158, 2, '2019-09-22', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(159, 2, '2019-09-23', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(160, 2, '2019-09-28', 0, 0, 0);
INSERT INTO `holidays` (`id`, `pattern`, `holiday_date`, `year`, `month`, `day`) VALUES(161, 2, '2019-09-29', 0, 0, 0);

-- --------------------------------------------------------

--
-- テーブルの構造 `leave_midways`
--

CREATE TABLE IF NOT EXISTS `leave_midways` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `operation_no` int(11) NOT NULL COMMENT '稼働ID',
  `stamp_leave_time` datetime DEFAULT NULL COMMENT '打刻退出時刻',
  `leave_time` datetime DEFAULT NULL COMMENT '退出時刻',
  `stamp_return_time` datetime DEFAULT NULL COMMENT '打刻戻り時刻',
  `return_time` datetime DEFAULT NULL COMMENT '戻り時刻',
  `non_working_times` decimal(4,2) DEFAULT NULL COMMENT '非稼働時間',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='途中退出テーブル' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `leave_of_absences`
--

CREATE TABLE IF NOT EXISTS `leave_of_absences` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `employee_no` int(11) NOT NULL COMMENT '社員ID',
  `leave_of_absence_type` varchar(256) DEFAULT NULL COMMENT '休職種別',
  `start_date` date DEFAULT NULL COMMENT '開始日',
  `end_date` date DEFAULT NULL COMMENT '終了日',
  `notes` varchar(1024) DEFAULT NULL COMMENT 'メモ',
  `return_date` date DEFAULT NULL COMMENT '復帰日',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='休職テーブル' AUTO_INCREMENT=3 ;

--
-- テーブルのデータのダンプ `leave_of_absences`
--

INSERT INTO `leave_of_absences` (`id`, `employee_no`, `leave_of_absence_type`, `start_date`, `end_date`, `notes`, `return_date`) VALUES(1, 5, '1', '2019-04-18', '2019-04-27', NULL, '2019-04-27');
INSERT INTO `leave_of_absences` (`id`, `employee_no`, `leave_of_absence_type`, `start_date`, `end_date`, `notes`, `return_date`) VALUES(2, 6, '2', '2019-04-19', '2019-05-17', NULL, '2019-04-30');

-- --------------------------------------------------------

--
-- テーブルの構造 `messages`
--

CREATE TABLE IF NOT EXISTS `messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `sender_no` int(11) NOT NULL COMMENT '送信者ID',
  `recipient_no` int(11) NOT NULL COMMENT '受信者ID',
  `message` varchar(256) NOT NULL COMMENT 'メッセージ',
  `insert_datetime` datetime NOT NULL COMMENT '登録日時',
  `daily_report_no` int(11) DEFAULT NULL COMMENT '日報ID',
  `read_datetime` datetime DEFAULT NULL COMMENT '既読日時',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='メッセージテーブル' AUTO_INCREMENT=15 ;

--
-- テーブルのデータのダンプ `messages`
--

INSERT INTO `messages` (`id`, `sender_no`, `recipient_no`, `message`, `insert_datetime`, `daily_report_no`, `read_datetime`) VALUES(1, 3, 5, 'test001', '2019-04-26 03:53:54', NULL, NULL);
INSERT INTO `messages` (`id`, `sender_no`, `recipient_no`, `message`, `insert_datetime`, `daily_report_no`, `read_datetime`) VALUES(2, 3, 12, 'test002', '2019-04-26 03:54:09', NULL, NULL);
INSERT INTO `messages` (`id`, `sender_no`, `recipient_no`, `message`, `insert_datetime`, `daily_report_no`, `read_datetime`) VALUES(3, 3, 2, 'test003', '2019-04-26 03:54:47', NULL, NULL);
INSERT INTO `messages` (`id`, `sender_no`, `recipient_no`, `message`, `insert_datetime`, `daily_report_no`, `read_datetime`) VALUES(4, 3, 2, 'test004', '2019-04-26 03:54:59', NULL, NULL);
INSERT INTO `messages` (`id`, `sender_no`, `recipient_no`, `message`, `insert_datetime`, `daily_report_no`, `read_datetime`) VALUES(5, 3, 2, 'test005', '2019-04-26 03:55:08', NULL, NULL);
INSERT INTO `messages` (`id`, `sender_no`, `recipient_no`, `message`, `insert_datetime`, `daily_report_no`, `read_datetime`) VALUES(6, 3, 2, 't006', '2019-04-26 03:55:23', NULL, NULL);
INSERT INTO `messages` (`id`, `sender_no`, `recipient_no`, `message`, `insert_datetime`, `daily_report_no`, `read_datetime`) VALUES(7, 3, 2, 't007', '2019-04-26 03:55:31', NULL, NULL);
INSERT INTO `messages` (`id`, `sender_no`, `recipient_no`, `message`, `insert_datetime`, `daily_report_no`, `read_datetime`) VALUES(8, 3, 2, 't008', '2019-04-26 03:55:38', NULL, NULL);
INSERT INTO `messages` (`id`, `sender_no`, `recipient_no`, `message`, `insert_datetime`, `daily_report_no`, `read_datetime`) VALUES(9, 3, 2, 't009', '2019-04-26 03:55:46', NULL, NULL);
INSERT INTO `messages` (`id`, `sender_no`, `recipient_no`, `message`, `insert_datetime`, `daily_report_no`, `read_datetime`) VALUES(10, 3, 2, 't010', '2019-04-26 03:55:57', NULL, NULL);
INSERT INTO `messages` (`id`, `sender_no`, `recipient_no`, `message`, `insert_datetime`, `daily_report_no`, `read_datetime`) VALUES(11, 3, 2, 't011', '2019-04-26 03:56:07', NULL, NULL);
INSERT INTO `messages` (`id`, `sender_no`, `recipient_no`, `message`, `insert_datetime`, `daily_report_no`, `read_datetime`) VALUES(12, 3, 8, 'test003', '2019-04-26 04:18:39', 8, NULL);
INSERT INTO `messages` (`id`, `sender_no`, `recipient_no`, `message`, `insert_datetime`, `daily_report_no`, `read_datetime`) VALUES(13, 18, 15, 'test', '2019-04-27 02:08:01', 16, '2019-04-27 02:08:42');
INSERT INTO `messages` (`id`, `sender_no`, `recipient_no`, `message`, `insert_datetime`, `daily_report_no`, `read_datetime`) VALUES(14, 15, 18, 'aaaaa', '2019-04-27 02:08:35', 16, '2019-04-27 02:09:43');

-- --------------------------------------------------------

--
-- テーブルの構造 `monthly_operations`
--

CREATE TABLE IF NOT EXISTS `monthly_operations` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `company_no` int(11) NOT NULL COMMENT '会社ID',
  `division_no` int(11) NOT NULL COMMENT '事業部ID',
  `department_no` int(11) NOT NULL COMMENT '部ID',
  `section_no` int(11) NOT NULL COMMENT '課ID',
  `employee_no` int(11) NOT NULL COMMENT '社員ID',
  `yearMonth` char(6) NOT NULL COMMENT '年月',
  `approval` int(1) NOT NULL DEFAULT '0' COMMENT '承認',
  `comment` varchar(2048) DEFAULT NULL COMMENT 'コメント',
  `approver_no` int(11) DEFAULT NULL COMMENT '承認者ID',
  `approval_date` datetime DEFAULT NULL COMMENT '承認日時',
  `senior_approval` int(1) NOT NULL DEFAULT '0' COMMENT '上席承認',
  `senior_comment` varchar(2048) DEFAULT NULL COMMENT '上席コメント',
  `senior_approver_no` int(11) DEFAULT NULL COMMENT '上席承認者ID',
  `senior_approval_date` datetime DEFAULT NULL COMMENT '上席承認日時',
  `final_confirmation` int(1) NOT NULL DEFAULT '0' COMMENT '最終確認',
  `final_confirmer_no` int(11) DEFAULT NULL COMMENT '最終確認者ID',
  `final_confirmation_date` datetime DEFAULT NULL COMMENT '最終確認日時',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='月次稼働テーブル' AUTO_INCREMENT=63 ;

--
-- テーブルのデータのダンプ `monthly_operations`
--

INSERT INTO `monthly_operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `yearMonth`, `approval`, `comment`, `approver_no`, `approval_date`, `senior_approval`, `senior_comment`, `senior_approver_no`, `senior_approval_date`, `final_confirmation`, `final_confirmer_no`, `final_confirmation_date`) VALUES(1, 1, 4, 2, 2, 2, '201905', 1, NULL, 2, '2019-05-31 19:04:19', 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO `monthly_operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `yearMonth`, `approval`, `comment`, `approver_no`, `approval_date`, `senior_approval`, `senior_comment`, `senior_approver_no`, `senior_approval_date`, `final_confirmation`, `final_confirmer_no`, `final_confirmation_date`) VALUES(2, 1, 1, 1, 1, 3, '201905', 1, NULL, 2, '2019-05-31 19:04:19', 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO `monthly_operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `yearMonth`, `approval`, `comment`, `approver_no`, `approval_date`, `senior_approval`, `senior_comment`, `senior_approver_no`, `senior_approval_date`, `final_confirmation`, `final_confirmer_no`, `final_confirmation_date`) VALUES(3, 1, 1, 1, 1, 4, '201905', 1, NULL, 2, '2019-05-31 19:04:19', 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO `monthly_operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `yearMonth`, `approval`, `comment`, `approver_no`, `approval_date`, `senior_approval`, `senior_comment`, `senior_approver_no`, `senior_approval_date`, `final_confirmation`, `final_confirmer_no`, `final_confirmation_date`) VALUES(4, 1, 1, 1, 1, 5, '201905', 1, NULL, 2, '2019-05-31 19:04:19', 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO `monthly_operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `yearMonth`, `approval`, `comment`, `approver_no`, `approval_date`, `senior_approval`, `senior_comment`, `senior_approver_no`, `senior_approval_date`, `final_confirmation`, `final_confirmer_no`, `final_confirmation_date`) VALUES(5, 1, 1, 1, 1, 6, '201905', 1, NULL, 2, '2019-05-31 19:04:19', 1, NULL, 3, '2019-05-31 19:52:00', 0, NULL, NULL);
INSERT INTO `monthly_operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `yearMonth`, `approval`, `comment`, `approver_no`, `approval_date`, `senior_approval`, `senior_comment`, `senior_approver_no`, `senior_approval_date`, `final_confirmation`, `final_confirmer_no`, `final_confirmation_date`) VALUES(6, 1, 1, 3, 3, 7, '201905', 1, NULL, 2, '2019-05-31 19:04:19', 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO `monthly_operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `yearMonth`, `approval`, `comment`, `approver_no`, `approval_date`, `senior_approval`, `senior_comment`, `senior_approver_no`, `senior_approval_date`, `final_confirmation`, `final_confirmer_no`, `final_confirmation_date`) VALUES(7, 1, 1, 3, 3, 8, '201905', 1, NULL, 2, '2019-05-31 19:04:19', 1, NULL, 3, '2019-06-02 22:16:17', 0, NULL, NULL);
INSERT INTO `monthly_operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `yearMonth`, `approval`, `comment`, `approver_no`, `approval_date`, `senior_approval`, `senior_comment`, `senior_approver_no`, `senior_approval_date`, `final_confirmation`, `final_confirmer_no`, `final_confirmation_date`) VALUES(8, 1, 1, 1, 1, 9, '201905', 1, NULL, 2, '2019-05-31 19:04:19', 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO `monthly_operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `yearMonth`, `approval`, `comment`, `approver_no`, `approval_date`, `senior_approval`, `senior_comment`, `senior_approver_no`, `senior_approval_date`, `final_confirmation`, `final_confirmer_no`, `final_confirmation_date`) VALUES(9, 1, 1, 1, 1, 10, '201905', 1, NULL, 2, '2019-05-31 19:04:19', 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO `monthly_operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `yearMonth`, `approval`, `comment`, `approver_no`, `approval_date`, `senior_approval`, `senior_comment`, `senior_approver_no`, `senior_approval_date`, `final_confirmation`, `final_confirmer_no`, `final_confirmation_date`) VALUES(10, 1, 1, 1, 1, 11, '201905', 1, NULL, 2, '2019-05-31 19:04:19', 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO `monthly_operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `yearMonth`, `approval`, `comment`, `approver_no`, `approval_date`, `senior_approval`, `senior_comment`, `senior_approver_no`, `senior_approval_date`, `final_confirmation`, `final_confirmer_no`, `final_confirmation_date`) VALUES(11, 1, 1, 3, 3, 12, '201905', 1, NULL, 2, '2019-05-31 19:04:19', 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO `monthly_operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `yearMonth`, `approval`, `comment`, `approver_no`, `approval_date`, `senior_approval`, `senior_comment`, `senior_approver_no`, `senior_approval_date`, `final_confirmation`, `final_confirmer_no`, `final_confirmation_date`) VALUES(12, 1, 1, 1, 1, 13, '201905', 1, NULL, 2, '2019-05-31 19:04:19', 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO `monthly_operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `yearMonth`, `approval`, `comment`, `approver_no`, `approval_date`, `senior_approval`, `senior_comment`, `senior_approver_no`, `senior_approval_date`, `final_confirmation`, `final_confirmer_no`, `final_confirmation_date`) VALUES(13, 1, 1, 1, 1, 14, '201905', 1, NULL, 2, '2019-05-31 19:04:19', 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO `monthly_operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `yearMonth`, `approval`, `comment`, `approver_no`, `approval_date`, `senior_approval`, `senior_comment`, `senior_approver_no`, `senior_approval_date`, `final_confirmation`, `final_confirmer_no`, `final_confirmation_date`) VALUES(14, 1, 1, 1, 1, 15, '201905', 1, NULL, 2, '2019-05-31 19:04:19', 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO `monthly_operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `yearMonth`, `approval`, `comment`, `approver_no`, `approval_date`, `senior_approval`, `senior_comment`, `senior_approver_no`, `senior_approval_date`, `final_confirmation`, `final_confirmer_no`, `final_confirmation_date`) VALUES(15, 1, 1, 1, 1, 16, '201905', 1, NULL, 2, '2019-05-31 19:04:19', 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO `monthly_operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `yearMonth`, `approval`, `comment`, `approver_no`, `approval_date`, `senior_approval`, `senior_comment`, `senior_approver_no`, `senior_approval_date`, `final_confirmation`, `final_confirmer_no`, `final_confirmation_date`) VALUES(16, 1, 1, 3, 3, 17, '201905', 1, NULL, 2, '2019-05-31 19:04:19', 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO `monthly_operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `yearMonth`, `approval`, `comment`, `approver_no`, `approval_date`, `senior_approval`, `senior_comment`, `senior_approver_no`, `senior_approval_date`, `final_confirmation`, `final_confirmer_no`, `final_confirmation_date`) VALUES(17, 1, 1, 1, 1, 18, '201905', 1, NULL, 2, '2019-05-31 19:04:19', 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO `monthly_operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `yearMonth`, `approval`, `comment`, `approver_no`, `approval_date`, `senior_approval`, `senior_comment`, `senior_approver_no`, `senior_approval_date`, `final_confirmation`, `final_confirmer_no`, `final_confirmation_date`) VALUES(18, 1, 1, 1, 1, 19, '201905', 1, NULL, 2, '2019-05-31 19:04:19', 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO `monthly_operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `yearMonth`, `approval`, `comment`, `approver_no`, `approval_date`, `senior_approval`, `senior_comment`, `senior_approver_no`, `senior_approval_date`, `final_confirmation`, `final_confirmer_no`, `final_confirmation_date`) VALUES(19, 1, 1, 1, 1, 20, '201905', 1, NULL, 2, '2019-05-31 19:04:19', 1, NULL, 3, '2019-05-31 21:51:26', 1, 8, '2019-05-31 21:51:51');
INSERT INTO `monthly_operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `yearMonth`, `approval`, `comment`, `approver_no`, `approval_date`, `senior_approval`, `senior_comment`, `senior_approver_no`, `senior_approval_date`, `final_confirmation`, `final_confirmer_no`, `final_confirmation_date`) VALUES(20, 1, 1, 3, 3, 21, '201905', 1, NULL, 2, '2019-05-31 19:04:19', 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO `monthly_operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `yearMonth`, `approval`, `comment`, `approver_no`, `approval_date`, `senior_approval`, `senior_comment`, `senior_approver_no`, `senior_approval_date`, `final_confirmation`, `final_confirmer_no`, `final_confirmation_date`) VALUES(21, 1, 1, 2, 2, 22, '201905', 1, NULL, 2, '2019-05-31 19:04:19', 1, NULL, 3, '2019-05-31 20:33:49', 0, 8, '2019-05-31 20:32:21');
INSERT INTO `monthly_operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `yearMonth`, `approval`, `comment`, `approver_no`, `approval_date`, `senior_approval`, `senior_comment`, `senior_approver_no`, `senior_approval_date`, `final_confirmation`, `final_confirmer_no`, `final_confirmation_date`) VALUES(22, 1, 1, 2, 2, 23, '201905', 1, NULL, 2, '2019-05-31 19:04:19', 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO `monthly_operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `yearMonth`, `approval`, `comment`, `approver_no`, `approval_date`, `senior_approval`, `senior_comment`, `senior_approver_no`, `senior_approval_date`, `final_confirmation`, `final_confirmer_no`, `final_confirmation_date`) VALUES(23, 1, 1, 3, 3, 24, '201905', 1, NULL, 2, '2019-05-31 19:04:19', 1, NULL, 3, '2019-05-31 21:50:06', 0, NULL, NULL);
INSERT INTO `monthly_operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `yearMonth`, `approval`, `comment`, `approver_no`, `approval_date`, `senior_approval`, `senior_comment`, `senior_approver_no`, `senior_approval_date`, `final_confirmation`, `final_confirmer_no`, `final_confirmation_date`) VALUES(24, 1, 1, 3, 3, 25, '201905', 1, NULL, 2, '2019-05-31 19:04:19', 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO `monthly_operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `yearMonth`, `approval`, `comment`, `approver_no`, `approval_date`, `senior_approval`, `senior_comment`, `senior_approver_no`, `senior_approval_date`, `final_confirmation`, `final_confirmer_no`, `final_confirmation_date`) VALUES(25, 1, 1, 3, 3, 26, '201905', 1, NULL, 2, '2019-05-31 19:04:19', 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO `monthly_operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `yearMonth`, `approval`, `comment`, `approver_no`, `approval_date`, `senior_approval`, `senior_comment`, `senior_approver_no`, `senior_approval_date`, `final_confirmation`, `final_confirmer_no`, `final_confirmation_date`) VALUES(26, 1, 1, 1, 1, 27, '201905', 1, NULL, 2, '2019-05-31 19:04:19', 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO `monthly_operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `yearMonth`, `approval`, `comment`, `approver_no`, `approval_date`, `senior_approval`, `senior_comment`, `senior_approver_no`, `senior_approval_date`, `final_confirmation`, `final_confirmer_no`, `final_confirmation_date`) VALUES(27, 1, 1, 3, 3, 28, '201905', 1, NULL, 2, '2019-05-31 19:04:19', 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO `monthly_operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `yearMonth`, `approval`, `comment`, `approver_no`, `approval_date`, `senior_approval`, `senior_comment`, `senior_approver_no`, `senior_approval_date`, `final_confirmation`, `final_confirmer_no`, `final_confirmation_date`) VALUES(28, 1, 1, 1, 1, 29, '201905', 1, NULL, 2, '2019-05-31 19:04:19', 1, NULL, 3, '2019-05-31 20:36:37', 2, 8, NULL);
INSERT INTO `monthly_operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `yearMonth`, `approval`, `comment`, `approver_no`, `approval_date`, `senior_approval`, `senior_comment`, `senior_approver_no`, `senior_approval_date`, `final_confirmation`, `final_confirmer_no`, `final_confirmation_date`) VALUES(29, 1, 1, 3, 3, 30, '201905', 1, NULL, 2, '2019-05-31 19:04:19', 1, NULL, 3, '2019-05-31 20:40:30', 0, 8, '2019-05-31 20:38:53');
INSERT INTO `monthly_operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `yearMonth`, `approval`, `comment`, `approver_no`, `approval_date`, `senior_approval`, `senior_comment`, `senior_approver_no`, `senior_approval_date`, `final_confirmation`, `final_confirmer_no`, `final_confirmation_date`) VALUES(30, 1, 1, 3, 3, 31, '201905', 1, NULL, 2, '2019-05-31 19:04:19', 1, NULL, 3, '2019-05-31 20:41:44', 1, 8, '2019-05-31 20:42:10');
INSERT INTO `monthly_operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `yearMonth`, `approval`, `comment`, `approver_no`, `approval_date`, `senior_approval`, `senior_comment`, `senior_approver_no`, `senior_approval_date`, `final_confirmation`, `final_confirmer_no`, `final_confirmation_date`) VALUES(31, 1, 1, 3, 3, 32, '201905', 1, NULL, 2, '2019-05-31 19:04:19', 1, NULL, 3, '2019-05-31 20:41:44', 1, 8, '2019-05-31 20:42:10');
INSERT INTO `monthly_operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `yearMonth`, `approval`, `comment`, `approver_no`, `approval_date`, `senior_approval`, `senior_comment`, `senior_approver_no`, `senior_approval_date`, `final_confirmation`, `final_confirmer_no`, `final_confirmation_date`) VALUES(32, 1, 4, 2, 2, 2, '201904', 1, NULL, 2, '2019-06-02 13:52:51', 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO `monthly_operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `yearMonth`, `approval`, `comment`, `approver_no`, `approval_date`, `senior_approval`, `senior_comment`, `senior_approver_no`, `senior_approval_date`, `final_confirmation`, `final_confirmer_no`, `final_confirmation_date`) VALUES(33, 1, 1, 1, 1, 3, '201904', 1, NULL, 2, '2019-06-02 13:52:51', 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO `monthly_operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `yearMonth`, `approval`, `comment`, `approver_no`, `approval_date`, `senior_approval`, `senior_comment`, `senior_approver_no`, `senior_approval_date`, `final_confirmation`, `final_confirmer_no`, `final_confirmation_date`) VALUES(34, 1, 1, 1, 1, 4, '201904', 1, NULL, 2, '2019-06-02 13:52:51', 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO `monthly_operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `yearMonth`, `approval`, `comment`, `approver_no`, `approval_date`, `senior_approval`, `senior_comment`, `senior_approver_no`, `senior_approval_date`, `final_confirmation`, `final_confirmer_no`, `final_confirmation_date`) VALUES(35, 1, 1, 1, 1, 5, '201904', 1, NULL, 2, '2019-06-02 13:52:51', 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO `monthly_operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `yearMonth`, `approval`, `comment`, `approver_no`, `approval_date`, `senior_approval`, `senior_comment`, `senior_approver_no`, `senior_approval_date`, `final_confirmation`, `final_confirmer_no`, `final_confirmation_date`) VALUES(36, 1, 1, 1, 1, 6, '201904', 1, NULL, 2, '2019-06-02 13:52:51', 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO `monthly_operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `yearMonth`, `approval`, `comment`, `approver_no`, `approval_date`, `senior_approval`, `senior_comment`, `senior_approver_no`, `senior_approval_date`, `final_confirmation`, `final_confirmer_no`, `final_confirmation_date`) VALUES(37, 1, 1, 3, 3, 7, '201904', 1, NULL, 2, '2019-06-02 13:52:51', 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO `monthly_operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `yearMonth`, `approval`, `comment`, `approver_no`, `approval_date`, `senior_approval`, `senior_comment`, `senior_approver_no`, `senior_approval_date`, `final_confirmation`, `final_confirmer_no`, `final_confirmation_date`) VALUES(38, 1, 1, 3, 3, 8, '201904', 1, NULL, 2, '2019-06-02 13:52:51', 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO `monthly_operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `yearMonth`, `approval`, `comment`, `approver_no`, `approval_date`, `senior_approval`, `senior_comment`, `senior_approver_no`, `senior_approval_date`, `final_confirmation`, `final_confirmer_no`, `final_confirmation_date`) VALUES(39, 1, 1, 1, 1, 9, '201904', 1, NULL, 2, '2019-06-02 13:52:51', 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO `monthly_operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `yearMonth`, `approval`, `comment`, `approver_no`, `approval_date`, `senior_approval`, `senior_comment`, `senior_approver_no`, `senior_approval_date`, `final_confirmation`, `final_confirmer_no`, `final_confirmation_date`) VALUES(40, 1, 1, 1, 1, 10, '201904', 1, NULL, 2, '2019-06-02 13:52:51', 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO `monthly_operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `yearMonth`, `approval`, `comment`, `approver_no`, `approval_date`, `senior_approval`, `senior_comment`, `senior_approver_no`, `senior_approval_date`, `final_confirmation`, `final_confirmer_no`, `final_confirmation_date`) VALUES(41, 1, 1, 1, 1, 11, '201904', 1, NULL, 2, '2019-06-02 13:52:51', 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO `monthly_operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `yearMonth`, `approval`, `comment`, `approver_no`, `approval_date`, `senior_approval`, `senior_comment`, `senior_approver_no`, `senior_approval_date`, `final_confirmation`, `final_confirmer_no`, `final_confirmation_date`) VALUES(42, 1, 1, 3, 3, 12, '201904', 1, NULL, 2, '2019-06-02 13:52:51', 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO `monthly_operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `yearMonth`, `approval`, `comment`, `approver_no`, `approval_date`, `senior_approval`, `senior_comment`, `senior_approver_no`, `senior_approval_date`, `final_confirmation`, `final_confirmer_no`, `final_confirmation_date`) VALUES(43, 1, 1, 1, 1, 13, '201904', 1, NULL, 2, '2019-06-02 13:52:51', 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO `monthly_operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `yearMonth`, `approval`, `comment`, `approver_no`, `approval_date`, `senior_approval`, `senior_comment`, `senior_approver_no`, `senior_approval_date`, `final_confirmation`, `final_confirmer_no`, `final_confirmation_date`) VALUES(44, 1, 1, 1, 1, 14, '201904', 1, NULL, 2, '2019-06-02 13:52:51', 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO `monthly_operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `yearMonth`, `approval`, `comment`, `approver_no`, `approval_date`, `senior_approval`, `senior_comment`, `senior_approver_no`, `senior_approval_date`, `final_confirmation`, `final_confirmer_no`, `final_confirmation_date`) VALUES(45, 1, 1, 1, 1, 15, '201904', 1, NULL, 2, '2019-06-02 13:52:51', 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO `monthly_operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `yearMonth`, `approval`, `comment`, `approver_no`, `approval_date`, `senior_approval`, `senior_comment`, `senior_approver_no`, `senior_approval_date`, `final_confirmation`, `final_confirmer_no`, `final_confirmation_date`) VALUES(46, 1, 1, 1, 1, 16, '201904', 1, NULL, 2, '2019-06-02 13:52:51', 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO `monthly_operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `yearMonth`, `approval`, `comment`, `approver_no`, `approval_date`, `senior_approval`, `senior_comment`, `senior_approver_no`, `senior_approval_date`, `final_confirmation`, `final_confirmer_no`, `final_confirmation_date`) VALUES(47, 1, 1, 3, 3, 17, '201904', 1, NULL, 2, '2019-06-02 13:52:51', 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO `monthly_operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `yearMonth`, `approval`, `comment`, `approver_no`, `approval_date`, `senior_approval`, `senior_comment`, `senior_approver_no`, `senior_approval_date`, `final_confirmation`, `final_confirmer_no`, `final_confirmation_date`) VALUES(48, 1, 1, 1, 1, 18, '201904', 1, NULL, 2, '2019-06-02 13:52:51', 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO `monthly_operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `yearMonth`, `approval`, `comment`, `approver_no`, `approval_date`, `senior_approval`, `senior_comment`, `senior_approver_no`, `senior_approval_date`, `final_confirmation`, `final_confirmer_no`, `final_confirmation_date`) VALUES(49, 1, 1, 1, 1, 19, '201904', 1, NULL, 2, '2019-06-02 13:52:51', 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO `monthly_operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `yearMonth`, `approval`, `comment`, `approver_no`, `approval_date`, `senior_approval`, `senior_comment`, `senior_approver_no`, `senior_approval_date`, `final_confirmation`, `final_confirmer_no`, `final_confirmation_date`) VALUES(50, 1, 1, 1, 1, 20, '201904', 1, NULL, 2, '2019-06-02 13:52:51', 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO `monthly_operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `yearMonth`, `approval`, `comment`, `approver_no`, `approval_date`, `senior_approval`, `senior_comment`, `senior_approver_no`, `senior_approval_date`, `final_confirmation`, `final_confirmer_no`, `final_confirmation_date`) VALUES(51, 1, 1, 3, 3, 21, '201904', 1, NULL, 2, '2019-06-02 13:52:51', 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO `monthly_operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `yearMonth`, `approval`, `comment`, `approver_no`, `approval_date`, `senior_approval`, `senior_comment`, `senior_approver_no`, `senior_approval_date`, `final_confirmation`, `final_confirmer_no`, `final_confirmation_date`) VALUES(52, 1, 1, 2, 2, 22, '201904', 1, NULL, 2, '2019-06-02 13:52:51', 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO `monthly_operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `yearMonth`, `approval`, `comment`, `approver_no`, `approval_date`, `senior_approval`, `senior_comment`, `senior_approver_no`, `senior_approval_date`, `final_confirmation`, `final_confirmer_no`, `final_confirmation_date`) VALUES(53, 1, 1, 2, 2, 23, '201904', 1, NULL, 2, '2019-06-02 13:52:51', 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO `monthly_operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `yearMonth`, `approval`, `comment`, `approver_no`, `approval_date`, `senior_approval`, `senior_comment`, `senior_approver_no`, `senior_approval_date`, `final_confirmation`, `final_confirmer_no`, `final_confirmation_date`) VALUES(54, 1, 1, 3, 3, 24, '201904', 1, NULL, 2, '2019-06-02 13:52:51', 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO `monthly_operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `yearMonth`, `approval`, `comment`, `approver_no`, `approval_date`, `senior_approval`, `senior_comment`, `senior_approver_no`, `senior_approval_date`, `final_confirmation`, `final_confirmer_no`, `final_confirmation_date`) VALUES(55, 1, 1, 3, 3, 25, '201904', 1, NULL, 2, '2019-06-02 13:52:51', 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO `monthly_operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `yearMonth`, `approval`, `comment`, `approver_no`, `approval_date`, `senior_approval`, `senior_comment`, `senior_approver_no`, `senior_approval_date`, `final_confirmation`, `final_confirmer_no`, `final_confirmation_date`) VALUES(56, 1, 1, 3, 3, 26, '201904', 1, NULL, 2, '2019-06-02 13:52:51', 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO `monthly_operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `yearMonth`, `approval`, `comment`, `approver_no`, `approval_date`, `senior_approval`, `senior_comment`, `senior_approver_no`, `senior_approval_date`, `final_confirmation`, `final_confirmer_no`, `final_confirmation_date`) VALUES(57, 1, 1, 1, 1, 27, '201904', 1, NULL, 2, '2019-06-02 13:52:51', 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO `monthly_operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `yearMonth`, `approval`, `comment`, `approver_no`, `approval_date`, `senior_approval`, `senior_comment`, `senior_approver_no`, `senior_approval_date`, `final_confirmation`, `final_confirmer_no`, `final_confirmation_date`) VALUES(58, 1, 1, 3, 3, 28, '201904', 1, NULL, 2, '2019-06-02 13:52:51', 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO `monthly_operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `yearMonth`, `approval`, `comment`, `approver_no`, `approval_date`, `senior_approval`, `senior_comment`, `senior_approver_no`, `senior_approval_date`, `final_confirmation`, `final_confirmer_no`, `final_confirmation_date`) VALUES(59, 1, 1, 1, 1, 29, '201904', 1, NULL, 2, '2019-06-02 13:52:51', 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO `monthly_operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `yearMonth`, `approval`, `comment`, `approver_no`, `approval_date`, `senior_approval`, `senior_comment`, `senior_approver_no`, `senior_approval_date`, `final_confirmation`, `final_confirmer_no`, `final_confirmation_date`) VALUES(60, 1, 1, 3, 3, 30, '201904', 1, NULL, 2, '2019-06-02 13:52:51', 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO `monthly_operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `yearMonth`, `approval`, `comment`, `approver_no`, `approval_date`, `senior_approval`, `senior_comment`, `senior_approver_no`, `senior_approval_date`, `final_confirmation`, `final_confirmer_no`, `final_confirmation_date`) VALUES(61, 1, 1, 3, 3, 31, '201904', 1, NULL, 2, '2019-06-02 13:52:51', 0, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO `monthly_operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `yearMonth`, `approval`, `comment`, `approver_no`, `approval_date`, `senior_approval`, `senior_comment`, `senior_approver_no`, `senior_approval_date`, `final_confirmation`, `final_confirmer_no`, `final_confirmation_date`) VALUES(62, 1, 1, 3, 3, 32, '201904', 1, NULL, 2, '2019-06-02 13:52:51', 0, NULL, NULL, NULL, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- テーブルの構造 `operations`
--

CREATE TABLE IF NOT EXISTS `operations` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `company_no` int(11) NOT NULL COMMENT '会社ID',
  `division_no` int(11) NOT NULL COMMENT '事業部ID',
  `department_no` int(11) NOT NULL COMMENT '部ID',
  `section_no` int(11) NOT NULL COMMENT '課ID',
  `employee_no` int(11) NOT NULL COMMENT '社員ID',
  `monthly_operation_no` int(11) DEFAULT NULL COMMENT '月次稼働ID',
  `employment_date` date DEFAULT NULL COMMENT '出社日',
  `start_datetime` datetime DEFAULT NULL COMMENT '稼働開始日時',
  `stamp_start_datetime` datetime DEFAULT NULL COMMENT '打刻開始日時',
  `stamp_start_location` varchar(128) DEFAULT NULL,
  `reason_change_start_datetime` varchar(256) DEFAULT NULL COMMENT '開始日時変更理由',
  `end_datetime` datetime DEFAULT NULL COMMENT '稼働終了日時',
  `stamp_end_datetime` datetime DEFAULT NULL COMMENT '打刻終了日時',
  `stamp_end_location` varchar(128) DEFAULT NULL,
  `reason_change_end_datetime` varchar(256) DEFAULT NULL COMMENT '終了日時変更理由',
  `overtime` decimal(4,2) DEFAULT NULL COMMENT '残業時間',
  `disp_overtime` varchar(5) DEFAULT NULL,
  `late_night_overtime` decimal(4,2) DEFAULT NULL COMMENT '深夜残業',
  `time_holiday` decimal(4,2) DEFAULT NULL COMMENT '時間有休',
  `actual_operating_time` decimal(4,2) DEFAULT NULL COMMENT '実稼働時間',
  `operation_approval` int(1) NOT NULL DEFAULT '1' COMMENT '稼働承認',
  `operation_approver` int(11) DEFAULT NULL COMMENT '稼働承認者（FK）',
  `comment` varchar(256) DEFAULT NULL COMMENT 'コメント',
  `date_approved` datetime DEFAULT NULL COMMENT '稼働承認日時',
  `tmp_insert` tinyint(1) NOT NULL DEFAULT '0' COMMENT '一時保存',
  `auto_end_stamp` int(1) NOT NULL DEFAULT '0' COMMENT '打刻忘れ-翌日自動退勤：１',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='稼働テーブル' AUTO_INCREMENT=79 ;

--
-- テーブルのデータのダンプ `operations`
--

INSERT INTO `operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `monthly_operation_no`, `employment_date`, `start_datetime`, `stamp_start_datetime`, `stamp_start_location`, `reason_change_start_datetime`, `end_datetime`, `stamp_end_datetime`, `stamp_end_location`, `reason_change_end_datetime`, `overtime`, `disp_overtime`, `late_night_overtime`, `time_holiday`, `actual_operating_time`, `operation_approval`, `operation_approver`, `comment`, `date_approved`, `tmp_insert`, `auto_end_stamp`) VALUES(2, 1, 1, 1, 1, 3, NULL, '2019-04-24', '2019-04-24 09:00:00', NULL, NULL, '時計設定不備', '2019-04-24 19:42:00', NULL, NULL, '深夜作業のため', '1.07', '1:04', '1.13', NULL, '9.00', 1, NULL, NULL, NULL, 0, 0);
INSERT INTO `operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `monthly_operation_no`, `employment_date`, `start_datetime`, `stamp_start_datetime`, `stamp_start_location`, `reason_change_start_datetime`, `end_datetime`, `stamp_end_datetime`, `stamp_end_location`, `reason_change_end_datetime`, `overtime`, `disp_overtime`, `late_night_overtime`, `time_holiday`, `actual_operating_time`, `operation_approval`, `operation_approver`, `comment`, `date_approved`, `tmp_insert`, `auto_end_stamp`) VALUES(7, 1, 1, 1, 1, 3, NULL, '2019-04-25', '2019-04-25 01:30:00', '2019-04-25 04:30:00', NULL, '', '2019-04-25 18:30:00', NULL, NULL, '', NULL, NULL, NULL, NULL, '15.50', 0, NULL, NULL, NULL, 1, 0);
INSERT INTO `operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `monthly_operation_no`, `employment_date`, `start_datetime`, `stamp_start_datetime`, `stamp_start_location`, `reason_change_start_datetime`, `end_datetime`, `stamp_end_datetime`, `stamp_end_location`, `reason_change_end_datetime`, `overtime`, `disp_overtime`, `late_night_overtime`, `time_holiday`, `actual_operating_time`, `operation_approval`, `operation_approver`, `comment`, `date_approved`, `tmp_insert`, `auto_end_stamp`) VALUES(8, 1, 1, 1, 1, 3, NULL, '2019-04-26', '2019-04-26 02:38:00', '2019-04-26 02:38:00', NULL, NULL, '2019-04-26 02:39:00', '2019-04-26 02:39:00', NULL, NULL, NULL, NULL, NULL, NULL, '-1.25', 1, NULL, NULL, NULL, 0, 0);
INSERT INTO `operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `monthly_operation_no`, `employment_date`, `start_datetime`, `stamp_start_datetime`, `stamp_start_location`, `reason_change_start_datetime`, `end_datetime`, `stamp_end_datetime`, `stamp_end_location`, `reason_change_end_datetime`, `overtime`, `disp_overtime`, `late_night_overtime`, `time_holiday`, `actual_operating_time`, `operation_approval`, `operation_approver`, `comment`, `date_approved`, `tmp_insert`, `auto_end_stamp`) VALUES(9, 1, 1, 3, 3, 7, NULL, '2019-04-26', '2019-04-26 03:11:00', '2019-04-26 03:11:00', NULL, NULL, '2019-04-26 03:12:00', '2019-04-26 03:12:00', NULL, NULL, NULL, NULL, NULL, NULL, '-1.25', 1, NULL, NULL, NULL, 0, 0);
INSERT INTO `operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `monthly_operation_no`, `employment_date`, `start_datetime`, `stamp_start_datetime`, `stamp_start_location`, `reason_change_start_datetime`, `end_datetime`, `stamp_end_datetime`, `stamp_end_location`, `reason_change_end_datetime`, `overtime`, `disp_overtime`, `late_night_overtime`, `time_holiday`, `actual_operating_time`, `operation_approval`, `operation_approver`, `comment`, `date_approved`, `tmp_insert`, `auto_end_stamp`) VALUES(10, 1, 1, 1, 1, 3, NULL, '2019-04-03', '2019-04-03 03:58:00', NULL, NULL, '時計設定不備', '2019-04-03 03:58:00', NULL, NULL, '災害のため', NULL, NULL, NULL, NULL, '0.25', 1, NULL, NULL, NULL, 0, 0);
INSERT INTO `operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `monthly_operation_no`, `employment_date`, `start_datetime`, `stamp_start_datetime`, `stamp_start_location`, `reason_change_start_datetime`, `end_datetime`, `stamp_end_datetime`, `stamp_end_location`, `reason_change_end_datetime`, `overtime`, `disp_overtime`, `late_night_overtime`, `time_holiday`, `actual_operating_time`, `operation_approval`, `operation_approver`, `comment`, `date_approved`, `tmp_insert`, `auto_end_stamp`) VALUES(11, 1, 1, 1, 1, 3, NULL, '2019-04-04', '2019-04-04 03:59:00', NULL, NULL, '時計設定不備', '2019-04-04 03:59:00', NULL, NULL, '早退申請', NULL, NULL, NULL, NULL, '0.25', 1, NULL, NULL, NULL, 0, 0);
INSERT INTO `operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `monthly_operation_no`, `employment_date`, `start_datetime`, `stamp_start_datetime`, `stamp_start_location`, `reason_change_start_datetime`, `end_datetime`, `stamp_end_datetime`, `stamp_end_location`, `reason_change_end_datetime`, `overtime`, `disp_overtime`, `late_night_overtime`, `time_holiday`, `actual_operating_time`, `operation_approval`, `operation_approver`, `comment`, `date_approved`, `tmp_insert`, `auto_end_stamp`) VALUES(12, 1, 1, 1, 1, 3, NULL, '2019-04-05', '2019-04-05 03:59:00', NULL, NULL, '時計設定不備', '2019-04-05 03:59:00', NULL, NULL, '深夜作業のため', NULL, NULL, NULL, NULL, '0.25', 1, NULL, NULL, NULL, 0, 0);
INSERT INTO `operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `monthly_operation_no`, `employment_date`, `start_datetime`, `stamp_start_datetime`, `stamp_start_location`, `reason_change_start_datetime`, `end_datetime`, `stamp_end_datetime`, `stamp_end_location`, `reason_change_end_datetime`, `overtime`, `disp_overtime`, `late_night_overtime`, `time_holiday`, `actual_operating_time`, `operation_approval`, `operation_approver`, `comment`, `date_approved`, `tmp_insert`, `auto_end_stamp`) VALUES(13, 1, 1, 1, 1, 3, NULL, '2019-04-06', '2019-04-06 04:00:00', NULL, NULL, '遅刻申請', '2019-04-06 04:00:00', NULL, NULL, '直帰', NULL, NULL, NULL, NULL, '0.00', 1, NULL, NULL, NULL, 0, 0);
INSERT INTO `operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `monthly_operation_no`, `employment_date`, `start_datetime`, `stamp_start_datetime`, `stamp_start_location`, `reason_change_start_datetime`, `end_datetime`, `stamp_end_datetime`, `stamp_end_location`, `reason_change_end_datetime`, `overtime`, `disp_overtime`, `late_night_overtime`, `time_holiday`, `actual_operating_time`, `operation_approval`, `operation_approver`, `comment`, `date_approved`, `tmp_insert`, `auto_end_stamp`) VALUES(14, 1, 1, 1, 1, 3, NULL, '2019-04-07', '2019-04-07 04:00:00', NULL, NULL, '災害のため', '2019-04-07 04:01:00', NULL, NULL, '直帰', NULL, NULL, NULL, NULL, '0.00', 1, NULL, NULL, NULL, 0, 0);
INSERT INTO `operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `monthly_operation_no`, `employment_date`, `start_datetime`, `stamp_start_datetime`, `stamp_start_location`, `reason_change_start_datetime`, `end_datetime`, `stamp_end_datetime`, `stamp_end_location`, `reason_change_end_datetime`, `overtime`, `disp_overtime`, `late_night_overtime`, `time_holiday`, `actual_operating_time`, `operation_approval`, `operation_approver`, `comment`, `date_approved`, `tmp_insert`, `auto_end_stamp`) VALUES(15, 1, 1, 1, 1, 18, NULL, '2019-04-26', '2019-04-26 09:00:00', '2019-04-26 23:29:00', NULL, '交通機関遅延', '2019-04-26 23:34:00', '2019-04-26 23:34:00', NULL, '', NULL, NULL, NULL, NULL, '11.00', 1, NULL, NULL, NULL, 0, 0);
INSERT INTO `operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `monthly_operation_no`, `employment_date`, `start_datetime`, `stamp_start_datetime`, `stamp_start_location`, `reason_change_start_datetime`, `end_datetime`, `stamp_end_datetime`, `stamp_end_location`, `reason_change_end_datetime`, `overtime`, `disp_overtime`, `late_night_overtime`, `time_holiday`, `actual_operating_time`, `operation_approval`, `operation_approver`, `comment`, `date_approved`, `tmp_insert`, `auto_end_stamp`) VALUES(16, 1, 1, 1, 1, 18, NULL, '2019-04-27', '2019-04-27 00:00:00', '2019-04-27 02:05:00', NULL, '打刻忘れ', '2019-04-27 07:00:00', '2019-04-27 02:06:00', NULL, '出張', NULL, NULL, NULL, NULL, '6.75', 1, NULL, NULL, NULL, 0, 0);
INSERT INTO `operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `monthly_operation_no`, `employment_date`, `start_datetime`, `stamp_start_datetime`, `stamp_start_location`, `reason_change_start_datetime`, `end_datetime`, `stamp_end_datetime`, `stamp_end_location`, `reason_change_end_datetime`, `overtime`, `disp_overtime`, `late_night_overtime`, `time_holiday`, `actual_operating_time`, `operation_approval`, `operation_approver`, `comment`, `date_approved`, `tmp_insert`, `auto_end_stamp`) VALUES(17, 1, 1, 3, 3, 7, NULL, '2019-04-27', '2019-04-27 05:08:00', '2019-04-27 05:08:00', NULL, NULL, '2019-04-27 05:09:00', '2019-04-27 05:09:00', NULL, NULL, NULL, NULL, NULL, NULL, '0.25', 1, NULL, NULL, NULL, 0, 0);
INSERT INTO `operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `monthly_operation_no`, `employment_date`, `start_datetime`, `stamp_start_datetime`, `stamp_start_location`, `reason_change_start_datetime`, `end_datetime`, `stamp_end_datetime`, `stamp_end_location`, `reason_change_end_datetime`, `overtime`, `disp_overtime`, `late_night_overtime`, `time_holiday`, `actual_operating_time`, `operation_approval`, `operation_approver`, `comment`, `date_approved`, `tmp_insert`, `auto_end_stamp`) VALUES(18, 1, 1, 1, 1, 3, NULL, '2019-05-03', '2019-05-03 09:06:00', '2019-05-03 17:06:00', NULL, 'PC操作不備', '2019-05-03 23:07:00', '2019-05-03 17:07:00', NULL, '早退申請', '4.00', '4:00', NULL, NULL, '14.02', 1, NULL, NULL, NULL, 0, 0);
INSERT INTO `operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `monthly_operation_no`, `employment_date`, `start_datetime`, `stamp_start_datetime`, `stamp_start_location`, `reason_change_start_datetime`, `end_datetime`, `stamp_end_datetime`, `stamp_end_location`, `reason_change_end_datetime`, `overtime`, `disp_overtime`, `late_night_overtime`, `time_holiday`, `actual_operating_time`, `operation_approval`, `operation_approver`, `comment`, `date_approved`, `tmp_insert`, `auto_end_stamp`) VALUES(19, 1, 4, 2, 2, 2, NULL, '2019-05-05', '2019-05-05 06:00:00', '2019-05-05 22:19:00', NULL, '打刻忘れ', '2019-05-05 17:30:00', '2019-05-05 22:20:00', NULL, '打刻忘れ', NULL, NULL, NULL, NULL, '9.50', 1, NULL, NULL, NULL, 0, 0);
INSERT INTO `operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `monthly_operation_no`, `employment_date`, `start_datetime`, `stamp_start_datetime`, `stamp_start_location`, `reason_change_start_datetime`, `end_datetime`, `stamp_end_datetime`, `stamp_end_location`, `reason_change_end_datetime`, `overtime`, `disp_overtime`, `late_night_overtime`, `time_holiday`, `actual_operating_time`, `operation_approval`, `operation_approver`, `comment`, `date_approved`, `tmp_insert`, `auto_end_stamp`) VALUES(20, 1, 4, 2, 2, 2, NULL, '2019-04-10', '2019-04-10 08:00:00', NULL, NULL, '打刻忘れ', '2019-04-10 17:30:00', NULL, NULL, '打刻忘れ', NULL, NULL, NULL, NULL, '9.50', 1, NULL, NULL, NULL, 0, 0);
INSERT INTO `operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `monthly_operation_no`, `employment_date`, `start_datetime`, `stamp_start_datetime`, `stamp_start_location`, `reason_change_start_datetime`, `end_datetime`, `stamp_end_datetime`, `stamp_end_location`, `reason_change_end_datetime`, `overtime`, `disp_overtime`, `late_night_overtime`, `time_holiday`, `actual_operating_time`, `operation_approval`, `operation_approver`, `comment`, `date_approved`, `tmp_insert`, `auto_end_stamp`) VALUES(21, 1, 4, 2, 2, 2, NULL, '2019-04-15', '2019-04-15 08:00:00', NULL, NULL, '打刻忘れ', '2019-04-15 17:30:00', NULL, NULL, '打刻忘れ', NULL, NULL, NULL, NULL, '8.00', 1, NULL, NULL, NULL, 0, 0);
INSERT INTO `operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `monthly_operation_no`, `employment_date`, `start_datetime`, `stamp_start_datetime`, `stamp_start_location`, `reason_change_start_datetime`, `end_datetime`, `stamp_end_datetime`, `stamp_end_location`, `reason_change_end_datetime`, `overtime`, `disp_overtime`, `late_night_overtime`, `time_holiday`, `actual_operating_time`, `operation_approval`, `operation_approver`, `comment`, `date_approved`, `tmp_insert`, `auto_end_stamp`) VALUES(22, 1, 4, 2, 2, 2, NULL, '2019-04-16', '2019-04-16 08:01:00', NULL, NULL, '打刻忘れ', '2019-04-16 18:51:00', NULL, NULL, '打刻忘れ', NULL, NULL, NULL, NULL, '9.00', 1, NULL, NULL, NULL, 0, 0);
INSERT INTO `operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `monthly_operation_no`, `employment_date`, `start_datetime`, `stamp_start_datetime`, `stamp_start_location`, `reason_change_start_datetime`, `end_datetime`, `stamp_end_datetime`, `stamp_end_location`, `reason_change_end_datetime`, `overtime`, `disp_overtime`, `late_night_overtime`, `time_holiday`, `actual_operating_time`, `operation_approval`, `operation_approver`, `comment`, `date_approved`, `tmp_insert`, `auto_end_stamp`) VALUES(23, 1, 1, 3, 3, 8, NULL, '2019-05-14', '2019-05-14 23:04:00', '2019-05-14 23:04:00', NULL, NULL, '2019-05-14 23:04:00', '2019-05-14 23:04:00', NULL, NULL, NULL, NULL, NULL, NULL, '0.25', 1, NULL, NULL, NULL, 0, 0);
INSERT INTO `operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `monthly_operation_no`, `employment_date`, `start_datetime`, `stamp_start_datetime`, `stamp_start_location`, `reason_change_start_datetime`, `end_datetime`, `stamp_end_datetime`, `stamp_end_location`, `reason_change_end_datetime`, `overtime`, `disp_overtime`, `late_night_overtime`, `time_holiday`, `actual_operating_time`, `operation_approval`, `operation_approver`, `comment`, `date_approved`, `tmp_insert`, `auto_end_stamp`) VALUES(24, 1, 1, 3, 3, 32, NULL, '2019-05-15', '2019-05-15 00:51:00', '2019-05-15 00:51:00', NULL, NULL, '2019-05-15 01:41:00', '2019-05-15 01:41:00', NULL, NULL, NULL, NULL, NULL, NULL, '0.50', 1, NULL, NULL, NULL, 0, 0);
INSERT INTO `operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `monthly_operation_no`, `employment_date`, `start_datetime`, `stamp_start_datetime`, `stamp_start_location`, `reason_change_start_datetime`, `end_datetime`, `stamp_end_datetime`, `stamp_end_location`, `reason_change_end_datetime`, `overtime`, `disp_overtime`, `late_night_overtime`, `time_holiday`, `actual_operating_time`, `operation_approval`, `operation_approver`, `comment`, `date_approved`, `tmp_insert`, `auto_end_stamp`) VALUES(25, 1, 1, 1, 1, 11, NULL, '2019-05-21', '2019-05-21 10:09:00', '2019-05-21 10:09:00', NULL, '', '2020-11-28 10:09:00', NULL, NULL, 'PC操作不備', NULL, NULL, NULL, NULL, '99.99', 1, NULL, NULL, NULL, 1, 0);
INSERT INTO `operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `monthly_operation_no`, `employment_date`, `start_datetime`, `stamp_start_datetime`, `stamp_start_location`, `reason_change_start_datetime`, `end_datetime`, `stamp_end_datetime`, `stamp_end_location`, `reason_change_end_datetime`, `overtime`, `disp_overtime`, `late_night_overtime`, `time_holiday`, `actual_operating_time`, `operation_approval`, `operation_approver`, `comment`, `date_approved`, `tmp_insert`, `auto_end_stamp`) VALUES(28, 1, 1, 3, 3, 7, NULL, '2019-05-24', '2019-05-24 08:14:00', '2019-05-24 08:14:00', NULL, '', '2019-05-24 08:14:00', '2019-05-24 08:14:00', NULL, '災害のため', NULL, NULL, NULL, NULL, '0.25', 1, NULL, NULL, NULL, 0, 0);
INSERT INTO `operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `monthly_operation_no`, `employment_date`, `start_datetime`, `stamp_start_datetime`, `stamp_start_location`, `reason_change_start_datetime`, `end_datetime`, `stamp_end_datetime`, `stamp_end_location`, `reason_change_end_datetime`, `overtime`, `disp_overtime`, `late_night_overtime`, `time_holiday`, `actual_operating_time`, `operation_approval`, `operation_approver`, `comment`, `date_approved`, `tmp_insert`, `auto_end_stamp`) VALUES(29, 1, 1, 1, 1, 11, NULL, '2019-05-07', '2019-05-07 09:35:00', NULL, NULL, '交通機関遅延', NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, '0.00', 1, NULL, NULL, NULL, 0, 0);
INSERT INTO `operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `monthly_operation_no`, `employment_date`, `start_datetime`, `stamp_start_datetime`, `stamp_start_location`, `reason_change_start_datetime`, `end_datetime`, `stamp_end_datetime`, `stamp_end_location`, `reason_change_end_datetime`, `overtime`, `disp_overtime`, `late_night_overtime`, `time_holiday`, `actual_operating_time`, `operation_approval`, `operation_approver`, `comment`, `date_approved`, `tmp_insert`, `auto_end_stamp`) VALUES(31, 1, 1, 1, 1, 11, NULL, '2019-05-25', '2019-05-25 09:39:00', NULL, NULL, '災害のため', NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, '0.00', 1, NULL, NULL, NULL, 0, 0);
INSERT INTO `operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `monthly_operation_no`, `employment_date`, `start_datetime`, `stamp_start_datetime`, `stamp_start_location`, `reason_change_start_datetime`, `end_datetime`, `stamp_end_datetime`, `stamp_end_location`, `reason_change_end_datetime`, `overtime`, `disp_overtime`, `late_night_overtime`, `time_holiday`, `actual_operating_time`, `operation_approval`, `operation_approver`, `comment`, `date_approved`, `tmp_insert`, `auto_end_stamp`) VALUES(32, 1, 1, 1, 1, 11, NULL, '2019-05-31', '2019-05-31 02:40:00', '2019-05-31 02:40:00', NULL, NULL, '2019-05-31 02:41:00', '2019-05-31 02:41:00', NULL, NULL, NULL, NULL, NULL, NULL, '0.25', 1, NULL, NULL, NULL, 0, 0);
INSERT INTO `operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `monthly_operation_no`, `employment_date`, `start_datetime`, `stamp_start_datetime`, `stamp_start_location`, `reason_change_start_datetime`, `end_datetime`, `stamp_end_datetime`, `stamp_end_location`, `reason_change_end_datetime`, `overtime`, `disp_overtime`, `late_night_overtime`, `time_holiday`, `actual_operating_time`, `operation_approval`, `operation_approver`, `comment`, `date_approved`, `tmp_insert`, `auto_end_stamp`) VALUES(33, 1, 1, 1, 1, 11, NULL, '2019-05-26', '2019-05-26 09:40:00', NULL, NULL, '深夜作業のため', NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, '0.00', 1, NULL, NULL, NULL, 0, 0);
INSERT INTO `operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `monthly_operation_no`, `employment_date`, `start_datetime`, `stamp_start_datetime`, `stamp_start_location`, `reason_change_start_datetime`, `end_datetime`, `stamp_end_datetime`, `stamp_end_location`, `reason_change_end_datetime`, `overtime`, `disp_overtime`, `late_night_overtime`, `time_holiday`, `actual_operating_time`, `operation_approval`, `operation_approver`, `comment`, `date_approved`, `tmp_insert`, `auto_end_stamp`) VALUES(34, 1, 1, 1, 1, 3, NULL, '2019-06-02', '2019-06-02 02:52:00', '2019-06-02 02:52:00', NULL, NULL, '2019-06-02 02:54:00', '2019-06-02 02:54:00', NULL, NULL, NULL, NULL, NULL, NULL, '0.25', 1, NULL, NULL, NULL, 0, 0);
INSERT INTO `operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `monthly_operation_no`, `employment_date`, `start_datetime`, `stamp_start_datetime`, `stamp_start_location`, `reason_change_start_datetime`, `end_datetime`, `stamp_end_datetime`, `stamp_end_location`, `reason_change_end_datetime`, `overtime`, `disp_overtime`, `late_night_overtime`, `time_holiday`, `actual_operating_time`, `operation_approval`, `operation_approver`, `comment`, `date_approved`, `tmp_insert`, `auto_end_stamp`) VALUES(35, 1, 1, 1, 1, 3, NULL, '2019-05-07', '2019-05-07 09:34:00', NULL, NULL, '災害のため', '2019-05-07 23:34:00', NULL, NULL, 'PC操作不備', '4.97', '4:58', '3.35', NULL, '13.75', 1, NULL, NULL, NULL, 0, 0);
INSERT INTO `operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `monthly_operation_no`, `employment_date`, `start_datetime`, `stamp_start_datetime`, `stamp_start_location`, `reason_change_start_datetime`, `end_datetime`, `stamp_end_datetime`, `stamp_end_location`, `reason_change_end_datetime`, `overtime`, `disp_overtime`, `late_night_overtime`, `time_holiday`, `actual_operating_time`, `operation_approval`, `operation_approver`, `comment`, `date_approved`, `tmp_insert`, `auto_end_stamp`) VALUES(36, 1, 1, 3, 3, 8, NULL, '2019-06-02', '2019-06-02 08:00:00', '2019-06-02 11:46:00', NULL, '打刻忘れ', '2019-06-02 18:19:00', NULL, NULL, '打刻忘れ', '3.25', '3:15', '7.75', '1.00', '8.82', 1, NULL, NULL, NULL, 0, 0);
INSERT INTO `operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `monthly_operation_no`, `employment_date`, `start_datetime`, `stamp_start_datetime`, `stamp_start_location`, `reason_change_start_datetime`, `end_datetime`, `stamp_end_datetime`, `stamp_end_location`, `reason_change_end_datetime`, `overtime`, `disp_overtime`, `late_night_overtime`, `time_holiday`, `actual_operating_time`, `operation_approval`, `operation_approver`, `comment`, `date_approved`, `tmp_insert`, `auto_end_stamp`) VALUES(37, 1, 1, 3, 3, 8, NULL, '2019-05-01', '2019-05-01 09:00:00', NULL, NULL, '打刻忘れ', '2019-05-01 18:45:00', NULL, NULL, '打刻忘れ', NULL, NULL, NULL, NULL, '9.75', 1, NULL, NULL, NULL, 0, 0);
INSERT INTO `operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `monthly_operation_no`, `employment_date`, `start_datetime`, `stamp_start_datetime`, `stamp_start_location`, `reason_change_start_datetime`, `end_datetime`, `stamp_end_datetime`, `stamp_end_location`, `reason_change_end_datetime`, `overtime`, `disp_overtime`, `late_night_overtime`, `time_holiday`, `actual_operating_time`, `operation_approval`, `operation_approver`, `comment`, `date_approved`, `tmp_insert`, `auto_end_stamp`) VALUES(38, 1, 1, 3, 3, 8, NULL, '2019-06-01', '2019-06-01 09:02:00', NULL, NULL, '打刻忘れ', '2019-06-01 20:45:00', NULL, NULL, '打刻忘れ', '1.75', '1:45', '2.50', '2.00', '10.22', 1, NULL, NULL, NULL, 0, 0);
INSERT INTO `operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `monthly_operation_no`, `employment_date`, `start_datetime`, `stamp_start_datetime`, `stamp_start_location`, `reason_change_start_datetime`, `end_datetime`, `stamp_end_datetime`, `stamp_end_location`, `reason_change_end_datetime`, `overtime`, `disp_overtime`, `late_night_overtime`, `time_holiday`, `actual_operating_time`, `operation_approval`, `operation_approver`, `comment`, `date_approved`, `tmp_insert`, `auto_end_stamp`) VALUES(39, 1, 1, 1, 1, 3, NULL, '2019-05-08', '2019-05-08 11:03:00', NULL, NULL, 'PC操作不備', '2019-05-08 21:03:00', NULL, NULL, '災害のため', NULL, NULL, NULL, NULL, '9.75', 1, NULL, NULL, NULL, 0, 0);
INSERT INTO `operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `monthly_operation_no`, `employment_date`, `start_datetime`, `stamp_start_datetime`, `stamp_start_location`, `reason_change_start_datetime`, `end_datetime`, `stamp_end_datetime`, `stamp_end_location`, `reason_change_end_datetime`, `overtime`, `disp_overtime`, `late_night_overtime`, `time_holiday`, `actual_operating_time`, `operation_approval`, `operation_approver`, `comment`, `date_approved`, `tmp_insert`, `auto_end_stamp`) VALUES(40, 1, 1, 1, 1, 3, NULL, '2019-05-20', '2019-05-20 10:20:00', NULL, NULL, '時計設定不備', '2019-05-20 20:20:00', NULL, NULL, '災害のため', NULL, NULL, NULL, NULL, '8.25', 1, NULL, NULL, NULL, 0, 0);
INSERT INTO `operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `monthly_operation_no`, `employment_date`, `start_datetime`, `stamp_start_datetime`, `stamp_start_location`, `reason_change_start_datetime`, `end_datetime`, `stamp_end_datetime`, `stamp_end_location`, `reason_change_end_datetime`, `overtime`, `disp_overtime`, `late_night_overtime`, `time_holiday`, `actual_operating_time`, `operation_approval`, `operation_approver`, `comment`, `date_approved`, `tmp_insert`, `auto_end_stamp`) VALUES(41, 1, 1, 1, 1, 3, NULL, '2019-05-21', '2019-05-21 07:21:00', NULL, NULL, '災害のため', '2019-05-21 13:21:00', NULL, NULL, '災害のため', NULL, NULL, NULL, NULL, '4.25', 1, NULL, NULL, NULL, 0, 0);
INSERT INTO `operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `monthly_operation_no`, `employment_date`, `start_datetime`, `stamp_start_datetime`, `stamp_start_location`, `reason_change_start_datetime`, `end_datetime`, `stamp_end_datetime`, `stamp_end_location`, `reason_change_end_datetime`, `overtime`, `disp_overtime`, `late_night_overtime`, `time_holiday`, `actual_operating_time`, `operation_approval`, `operation_approver`, `comment`, `date_approved`, `tmp_insert`, `auto_end_stamp`) VALUES(42, 1, 1, 2, 2, 22, NULL, '2019-06-03', '2019-06-03 23:31:00', '2019-06-03 23:31:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 1, 0);
INSERT INTO `operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `monthly_operation_no`, `employment_date`, `start_datetime`, `stamp_start_datetime`, `stamp_start_location`, `reason_change_start_datetime`, `end_datetime`, `stamp_end_datetime`, `stamp_end_location`, `reason_change_end_datetime`, `overtime`, `disp_overtime`, `late_night_overtime`, `time_holiday`, `actual_operating_time`, `operation_approval`, `operation_approver`, `comment`, `date_approved`, `tmp_insert`, `auto_end_stamp`) VALUES(43, 1, 1, 1, 1, 19, NULL, '2019-06-03', '2019-06-03 23:31:00', '2019-06-03 23:31:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 1, 0);
INSERT INTO `operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `monthly_operation_no`, `employment_date`, `start_datetime`, `stamp_start_datetime`, `stamp_start_location`, `reason_change_start_datetime`, `end_datetime`, `stamp_end_datetime`, `stamp_end_location`, `reason_change_end_datetime`, `overtime`, `disp_overtime`, `late_night_overtime`, `time_holiday`, `actual_operating_time`, `operation_approval`, `operation_approver`, `comment`, `date_approved`, `tmp_insert`, `auto_end_stamp`) VALUES(44, 1, 1, 1, 1, 16, NULL, '2019-06-03', '2019-06-03 23:32:00', '2019-06-03 23:32:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 1, 0);
INSERT INTO `operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `monthly_operation_no`, `employment_date`, `start_datetime`, `stamp_start_datetime`, `stamp_start_location`, `reason_change_start_datetime`, `end_datetime`, `stamp_end_datetime`, `stamp_end_location`, `reason_change_end_datetime`, `overtime`, `disp_overtime`, `late_night_overtime`, `time_holiday`, `actual_operating_time`, `operation_approval`, `operation_approver`, `comment`, `date_approved`, `tmp_insert`, `auto_end_stamp`) VALUES(45, 1, 1, 3, 3, 21, NULL, '2019-06-03', '2019-06-03 23:33:00', '2019-06-03 23:33:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 1, 0);
INSERT INTO `operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `monthly_operation_no`, `employment_date`, `start_datetime`, `stamp_start_datetime`, `stamp_start_location`, `reason_change_start_datetime`, `end_datetime`, `stamp_end_datetime`, `stamp_end_location`, `reason_change_end_datetime`, `overtime`, `disp_overtime`, `late_night_overtime`, `time_holiday`, `actual_operating_time`, `operation_approval`, `operation_approver`, `comment`, `date_approved`, `tmp_insert`, `auto_end_stamp`) VALUES(46, 1, 1, 2, 2, 23, NULL, '2019-06-03', '2019-06-03 23:33:00', '2019-06-03 23:33:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 1, 0);
INSERT INTO `operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `monthly_operation_no`, `employment_date`, `start_datetime`, `stamp_start_datetime`, `stamp_start_location`, `reason_change_start_datetime`, `end_datetime`, `stamp_end_datetime`, `stamp_end_location`, `reason_change_end_datetime`, `overtime`, `disp_overtime`, `late_night_overtime`, `time_holiday`, `actual_operating_time`, `operation_approval`, `operation_approver`, `comment`, `date_approved`, `tmp_insert`, `auto_end_stamp`) VALUES(47, 1, 1, 3, 3, 24, NULL, '2019-06-03', '2019-06-03 23:33:00', '2019-06-03 23:33:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 1, 0);
INSERT INTO `operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `monthly_operation_no`, `employment_date`, `start_datetime`, `stamp_start_datetime`, `stamp_start_location`, `reason_change_start_datetime`, `end_datetime`, `stamp_end_datetime`, `stamp_end_location`, `reason_change_end_datetime`, `overtime`, `disp_overtime`, `late_night_overtime`, `time_holiday`, `actual_operating_time`, `operation_approval`, `operation_approver`, `comment`, `date_approved`, `tmp_insert`, `auto_end_stamp`) VALUES(48, 1, 1, 1, 1, 18, NULL, '2019-06-03', '2019-06-03 23:33:00', '2019-06-03 23:33:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 1, 0);
INSERT INTO `operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `monthly_operation_no`, `employment_date`, `start_datetime`, `stamp_start_datetime`, `stamp_start_location`, `reason_change_start_datetime`, `end_datetime`, `stamp_end_datetime`, `stamp_end_location`, `reason_change_end_datetime`, `overtime`, `disp_overtime`, `late_night_overtime`, `time_holiday`, `actual_operating_time`, `operation_approval`, `operation_approver`, `comment`, `date_approved`, `tmp_insert`, `auto_end_stamp`) VALUES(49, 1, 1, 3, 3, 25, NULL, '2019-06-03', '2019-06-03 23:33:00', '2019-06-03 23:33:00', NULL, '', '2019-06-03 23:55:00', NULL, NULL, '打刻忘れ', NULL, NULL, NULL, NULL, '0.37', 1, NULL, NULL, NULL, 0, 0);
INSERT INTO `operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `monthly_operation_no`, `employment_date`, `start_datetime`, `stamp_start_datetime`, `stamp_start_location`, `reason_change_start_datetime`, `end_datetime`, `stamp_end_datetime`, `stamp_end_location`, `reason_change_end_datetime`, `overtime`, `disp_overtime`, `late_night_overtime`, `time_holiday`, `actual_operating_time`, `operation_approval`, `operation_approver`, `comment`, `date_approved`, `tmp_insert`, `auto_end_stamp`) VALUES(50, 1, 1, 3, 3, 17, NULL, '2019-06-03', '2019-06-03 23:33:00', '2019-06-03 23:33:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 1, 0);
INSERT INTO `operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `monthly_operation_no`, `employment_date`, `start_datetime`, `stamp_start_datetime`, `stamp_start_location`, `reason_change_start_datetime`, `end_datetime`, `stamp_end_datetime`, `stamp_end_location`, `reason_change_end_datetime`, `overtime`, `disp_overtime`, `late_night_overtime`, `time_holiday`, `actual_operating_time`, `operation_approval`, `operation_approver`, `comment`, `date_approved`, `tmp_insert`, `auto_end_stamp`) VALUES(51, 1, 1, 1, 1, 15, NULL, '2019-06-04', '2019-06-04 00:08:00', '2019-06-04 00:08:00', NULL, NULL, '2019-06-04 00:09:00', '2019-06-04 00:09:00', NULL, NULL, NULL, NULL, NULL, NULL, '0.02', 1, NULL, NULL, NULL, 0, 0);
INSERT INTO `operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `monthly_operation_no`, `employment_date`, `start_datetime`, `stamp_start_datetime`, `stamp_start_location`, `reason_change_start_datetime`, `end_datetime`, `stamp_end_datetime`, `stamp_end_location`, `reason_change_end_datetime`, `overtime`, `disp_overtime`, `late_night_overtime`, `time_holiday`, `actual_operating_time`, `operation_approval`, `operation_approver`, `comment`, `date_approved`, `tmp_insert`, `auto_end_stamp`) VALUES(52, 1, 1, 1, 1, 19, NULL, '2019-06-04', '2019-06-04 00:21:00', '2019-06-04 00:21:00', NULL, NULL, '2019-06-04 00:24:00', '2019-06-04 00:24:00', NULL, NULL, NULL, NULL, NULL, NULL, '0.05', 1, NULL, NULL, NULL, 0, 0);
INSERT INTO `operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `monthly_operation_no`, `employment_date`, `start_datetime`, `stamp_start_datetime`, `stamp_start_location`, `reason_change_start_datetime`, `end_datetime`, `stamp_end_datetime`, `stamp_end_location`, `reason_change_end_datetime`, `overtime`, `disp_overtime`, `late_night_overtime`, `time_holiday`, `actual_operating_time`, `operation_approval`, `operation_approver`, `comment`, `date_approved`, `tmp_insert`, `auto_end_stamp`) VALUES(53, 1, 1, 1, 1, 14, NULL, '2019-06-04', '2019-06-04 00:24:00', '2019-06-04 00:24:00', NULL, NULL, '2019-06-04 00:25:00', '2019-06-04 00:25:00', NULL, NULL, NULL, NULL, NULL, NULL, '0.02', 1, NULL, NULL, NULL, 0, 0);
INSERT INTO `operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `monthly_operation_no`, `employment_date`, `start_datetime`, `stamp_start_datetime`, `stamp_start_location`, `reason_change_start_datetime`, `end_datetime`, `stamp_end_datetime`, `stamp_end_location`, `reason_change_end_datetime`, `overtime`, `disp_overtime`, `late_night_overtime`, `time_holiday`, `actual_operating_time`, `operation_approval`, `operation_approver`, `comment`, `date_approved`, `tmp_insert`, `auto_end_stamp`) VALUES(54, 1, 1, 3, 3, 25, NULL, '2019-06-04', '2019-06-04 00:29:00', '2019-06-04 00:29:00', NULL, NULL, '2019-06-04 00:30:00', '2019-06-04 00:30:00', NULL, NULL, NULL, NULL, NULL, NULL, '0.02', 1, NULL, NULL, NULL, 0, 0);
INSERT INTO `operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `monthly_operation_no`, `employment_date`, `start_datetime`, `stamp_start_datetime`, `stamp_start_location`, `reason_change_start_datetime`, `end_datetime`, `stamp_end_datetime`, `stamp_end_location`, `reason_change_end_datetime`, `overtime`, `disp_overtime`, `late_night_overtime`, `time_holiday`, `actual_operating_time`, `operation_approval`, `operation_approver`, `comment`, `date_approved`, `tmp_insert`, `auto_end_stamp`) VALUES(66, 1, 1, 1, 1, 3, NULL, '2019-06-07', '2019-06-07 08:25:00', NULL, NULL, '災害のため', '2019-06-07 18:30:00', NULL, NULL, '打刻忘れ', NULL, NULL, NULL, NULL, '8.58', 1, NULL, NULL, NULL, 0, 0);
INSERT INTO `operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `monthly_operation_no`, `employment_date`, `start_datetime`, `stamp_start_datetime`, `stamp_start_location`, `reason_change_start_datetime`, `end_datetime`, `stamp_end_datetime`, `stamp_end_location`, `reason_change_end_datetime`, `overtime`, `disp_overtime`, `late_night_overtime`, `time_holiday`, `actual_operating_time`, `operation_approval`, `operation_approver`, `comment`, `date_approved`, `tmp_insert`, `auto_end_stamp`) VALUES(67, 1, 1, 1, 1, 3, NULL, '2019-06-08', '2019-06-08 19:26:00', '2019-06-08 19:26:00', NULL, NULL, '2019-06-08 19:27:00', '2019-06-08 19:27:00', NULL, NULL, NULL, NULL, NULL, NULL, '0.02', 1, NULL, NULL, NULL, 0, 0);
INSERT INTO `operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `monthly_operation_no`, `employment_date`, `start_datetime`, `stamp_start_datetime`, `stamp_start_location`, `reason_change_start_datetime`, `end_datetime`, `stamp_end_datetime`, `stamp_end_location`, `reason_change_end_datetime`, `overtime`, `disp_overtime`, `late_night_overtime`, `time_holiday`, `actual_operating_time`, `operation_approval`, `operation_approver`, `comment`, `date_approved`, `tmp_insert`, `auto_end_stamp`) VALUES(68, 1, 1, 3, 3, 8, NULL, '2019-06-07', '2019-06-07 09:51:00', NULL, NULL, '打刻忘れ', NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 0, 1);
INSERT INTO `operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `monthly_operation_no`, `employment_date`, `start_datetime`, `stamp_start_datetime`, `stamp_start_location`, `reason_change_start_datetime`, `end_datetime`, `stamp_end_datetime`, `stamp_end_location`, `reason_change_end_datetime`, `overtime`, `disp_overtime`, `late_night_overtime`, `time_holiday`, `actual_operating_time`, `operation_approval`, `operation_approver`, `comment`, `date_approved`, `tmp_insert`, `auto_end_stamp`) VALUES(69, 1, 1, 3, 3, 8, NULL, '2019-06-10', '2019-06-10 02:00:00', '2019-06-10 02:00:00', NULL, NULL, '2019-06-10 02:02:00', '2019-06-10 02:02:00', NULL, NULL, NULL, NULL, NULL, NULL, '-0.97', 1, NULL, NULL, NULL, 0, 0);
INSERT INTO `operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `monthly_operation_no`, `employment_date`, `start_datetime`, `stamp_start_datetime`, `stamp_start_location`, `reason_change_start_datetime`, `end_datetime`, `stamp_end_datetime`, `stamp_end_location`, `reason_change_end_datetime`, `overtime`, `disp_overtime`, `late_night_overtime`, `time_holiday`, `actual_operating_time`, `operation_approval`, `operation_approver`, `comment`, `date_approved`, `tmp_insert`, `auto_end_stamp`) VALUES(70, 1, 1, 1, 1, 10, NULL, '2019-06-13', '2019-06-13 19:18:00', '2019-06-13 19:17:00', NULL, '', NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, '0.00', 1, NULL, NULL, NULL, 1, 0);
INSERT INTO `operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `monthly_operation_no`, `employment_date`, `start_datetime`, `stamp_start_datetime`, `stamp_start_location`, `reason_change_start_datetime`, `end_datetime`, `stamp_end_datetime`, `stamp_end_location`, `reason_change_end_datetime`, `overtime`, `disp_overtime`, `late_night_overtime`, `time_holiday`, `actual_operating_time`, `operation_approval`, `operation_approver`, `comment`, `date_approved`, `tmp_insert`, `auto_end_stamp`) VALUES(71, 1, 4, 2, 2, 2, NULL, '2019-06-26', '2019-06-26 15:28:00', '2019-06-26 15:28:00', NULL, '', '2019-06-26 01:38:00', NULL, NULL, '', NULL, NULL, NULL, NULL, '13.83', 1, NULL, NULL, NULL, 1, 0);
INSERT INTO `operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `monthly_operation_no`, `employment_date`, `start_datetime`, `stamp_start_datetime`, `stamp_start_location`, `reason_change_start_datetime`, `end_datetime`, `stamp_end_datetime`, `stamp_end_location`, `reason_change_end_datetime`, `overtime`, `disp_overtime`, `late_night_overtime`, `time_holiday`, `actual_operating_time`, `operation_approval`, `operation_approver`, `comment`, `date_approved`, `tmp_insert`, `auto_end_stamp`) VALUES(72, 1, 1, 1, 1, 3, NULL, '2019-06-26', '2019-06-26 19:59:00', '2019-06-26 19:59:00', NULL, NULL, '2019-06-26 19:59:00', '2019-06-26 19:59:00', NULL, NULL, NULL, NULL, NULL, NULL, '0.00', 1, NULL, NULL, NULL, 0, 0);
INSERT INTO `operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `monthly_operation_no`, `employment_date`, `start_datetime`, `stamp_start_datetime`, `stamp_start_location`, `reason_change_start_datetime`, `end_datetime`, `stamp_end_datetime`, `stamp_end_location`, `reason_change_end_datetime`, `overtime`, `disp_overtime`, `late_night_overtime`, `time_holiday`, `actual_operating_time`, `operation_approval`, `operation_approver`, `comment`, `date_approved`, `tmp_insert`, `auto_end_stamp`) VALUES(73, 1, 1, 1, 1, 9, NULL, '2019-06-26', '2019-06-26 20:01:00', '2019-06-26 20:01:00', NULL, NULL, '2019-06-26 20:02:00', '2019-06-26 20:02:00', NULL, NULL, NULL, NULL, NULL, NULL, '0.02', 1, NULL, NULL, NULL, 0, 0);
INSERT INTO `operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `monthly_operation_no`, `employment_date`, `start_datetime`, `stamp_start_datetime`, `stamp_start_location`, `reason_change_start_datetime`, `end_datetime`, `stamp_end_datetime`, `stamp_end_location`, `reason_change_end_datetime`, `overtime`, `disp_overtime`, `late_night_overtime`, `time_holiday`, `actual_operating_time`, `operation_approval`, `operation_approver`, `comment`, `date_approved`, `tmp_insert`, `auto_end_stamp`) VALUES(74, 1, 1, 1, 1, 14, NULL, '2019-06-26', '2019-06-26 20:33:00', '2019-06-26 20:33:00', NULL, NULL, '2019-06-26 20:33:00', '2019-06-26 20:33:00', NULL, NULL, NULL, NULL, NULL, NULL, '0.00', 1, NULL, NULL, NULL, 0, 0);
INSERT INTO `operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `monthly_operation_no`, `employment_date`, `start_datetime`, `stamp_start_datetime`, `stamp_start_location`, `reason_change_start_datetime`, `end_datetime`, `stamp_end_datetime`, `stamp_end_location`, `reason_change_end_datetime`, `overtime`, `disp_overtime`, `late_night_overtime`, `time_holiday`, `actual_operating_time`, `operation_approval`, `operation_approver`, `comment`, `date_approved`, `tmp_insert`, `auto_end_stamp`) VALUES(75, 1, 1, 3, 3, 17, NULL, '2019-06-26', '2019-06-26 20:35:00', '2019-06-26 20:35:00', NULL, NULL, '2019-06-26 20:37:00', '2019-06-26 20:37:00', NULL, NULL, NULL, NULL, NULL, NULL, '0.03', 1, NULL, NULL, NULL, 0, 0);
INSERT INTO `operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `monthly_operation_no`, `employment_date`, `start_datetime`, `stamp_start_datetime`, `stamp_start_location`, `reason_change_start_datetime`, `end_datetime`, `stamp_end_datetime`, `stamp_end_location`, `reason_change_end_datetime`, `overtime`, `disp_overtime`, `late_night_overtime`, `time_holiday`, `actual_operating_time`, `operation_approval`, `operation_approver`, `comment`, `date_approved`, `tmp_insert`, `auto_end_stamp`) VALUES(76, 1, 1, 1, 1, 3, NULL, '2019-06-29', NULL, '2019-06-29 23:19:00', NULL, '', NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, '0.00', 1, NULL, NULL, NULL, 1, 0);
INSERT INTO `operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `monthly_operation_no`, `employment_date`, `start_datetime`, `stamp_start_datetime`, `stamp_start_location`, `reason_change_start_datetime`, `end_datetime`, `stamp_end_datetime`, `stamp_end_location`, `reason_change_end_datetime`, `overtime`, `disp_overtime`, `late_night_overtime`, `time_holiday`, `actual_operating_time`, `operation_approval`, `operation_approver`, `comment`, `date_approved`, `tmp_insert`, `auto_end_stamp`) VALUES(77, 1, 1, 1, 1, 3, NULL, '2019-06-29', '2019-06-29 22:28:00', '2019-06-29 23:28:00', NULL, '', NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, '0.00', 1, NULL, NULL, NULL, 1, 0);
INSERT INTO `operations` (`id`, `company_no`, `division_no`, `department_no`, `section_no`, `employee_no`, `monthly_operation_no`, `employment_date`, `start_datetime`, `stamp_start_datetime`, `stamp_start_location`, `reason_change_start_datetime`, `end_datetime`, `stamp_end_datetime`, `stamp_end_location`, `reason_change_end_datetime`, `overtime`, `disp_overtime`, `late_night_overtime`, `time_holiday`, `actual_operating_time`, `operation_approval`, `operation_approver`, `comment`, `date_approved`, `tmp_insert`, `auto_end_stamp`) VALUES(78, 1, 1, 3, 3, 7, NULL, '2019-06-29', '2019-06-29 23:39:00', '2019-06-29 23:39:00', 'Salaysay St., Santiago, 3311 Isabela, Philippines', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 1, 0);

-- --------------------------------------------------------

--
-- テーブルの構造 `opportunities`
--

CREATE TABLE IF NOT EXISTS `opportunities` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `parent_project_no` int(11) NOT NULL COMMENT '案件ID',
  `department_no` int(11) NOT NULL COMMENT '部門ID',
  `name` varchar(256) NOT NULL COMMENT '名称',
  `client` varchar(256) DEFAULT NULL COMMENT '顧客',
  `project_code` char(8) DEFAULT NULL COMMENT '商談コード',
  `item_code` int(11) DEFAULT NULL COMMENT '商品コード',
  `sfaid` char(10) DEFAULT NULL COMMENT 'SFAID',
  `status` int(1) NOT NULL,
  `insert_user` int(11) NOT NULL COMMENT '登録者',
  `insert_date` date DEFAULT NULL COMMENT '登録日',
  `deleted` int(1) NOT NULL DEFAULT '0' COMMENT '削除',
  `deleted_date` date DEFAULT NULL COMMENT '削除日',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商談テーブル' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `overtime_applications`
--

CREATE TABLE IF NOT EXISTS `overtime_applications` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `employee_no` int(11) NOT NULL COMMENT '社員ID',
  `type` int(2) NOT NULL COMMENT '種別',
  `application_date` date NOT NULL COMMENT '申請日',
  `working_date` date NOT NULL COMMENT '出社日',
  `overtime_hours` decimal(4,2) NOT NULL COMMENT '残業時間',
  `reason` varchar(256) NOT NULL COMMENT '理由',
  `plan_of_dayoff` date DEFAULT NULL COMMENT '代休取得予定日',
  `remarks` varchar(256) DEFAULT NULL COMMENT '備考',
  `approval` int(1) NOT NULL DEFAULT '0' COMMENT '承認',
  `comment` varchar(256) DEFAULT NULL COMMENT 'コメント',
  `authorize` int(11) DEFAULT NULL COMMENT '承認者',
  `authorize_datetime` datetime DEFAULT NULL COMMENT '承認日時',
  `overtime_start` datetime DEFAULT NULL COMMENT '残業開始',
  `overtime_end` datetime DEFAULT NULL COMMENT '残業終了',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='残業申請テーブル' AUTO_INCREMENT=29 ;

--
-- テーブルのデータのダンプ `overtime_applications`
--

INSERT INTO `overtime_applications` (`id`, `employee_no`, `type`, `application_date`, `working_date`, `overtime_hours`, `reason`, `plan_of_dayoff`, `remarks`, `approval`, `comment`, `authorize`, `authorize_datetime`, `overtime_start`, `overtime_end`) VALUES(1, 3, 1, '2019-04-15', '2019-04-15', '3.75', '残業します', '2019-05-11', '備考です', 1, '', 3, '2019-04-15 06:39:04', '2019-04-15 18:37:00', '2019-04-15 22:37:00');
INSERT INTO `overtime_applications` (`id`, `employee_no`, `type`, `application_date`, `working_date`, `overtime_hours`, `reason`, `plan_of_dayoff`, `remarks`, `approval`, `comment`, `authorize`, `authorize_datetime`, `overtime_start`, `overtime_end`) VALUES(2, 3, 1, '2019-04-25', '2019-04-24', '3.75', 'aaa', '2019-05-10', '342', 1, '', 3, '2019-04-25 04:09:07', '2019-04-24 18:38:00', '2019-04-24 22:38:00');
INSERT INTO `overtime_applications` (`id`, `employee_no`, `type`, `application_date`, `working_date`, `overtime_hours`, `reason`, `plan_of_dayoff`, `remarks`, `approval`, `comment`, `authorize`, `authorize_datetime`, `overtime_start`, `overtime_end`) VALUES(3, 3, 3, '2019-04-25', '2019-05-01', '13.75', 'aa', '2019-05-11', 'aavb', 1, 'qqq', 3, '2019-04-25 04:06:36', '2019-05-01 04:05:00', '2019-05-01 18:05:00');
INSERT INTO `overtime_applications` (`id`, `employee_no`, `type`, `application_date`, `working_date`, `overtime_hours`, `reason`, `plan_of_dayoff`, `remarks`, `approval`, `comment`, `authorize`, `authorize_datetime`, `overtime_start`, `overtime_end`) VALUES(5, 3, 2, '2019-04-25', '2019-04-24', '4.75', 'fdf', '2019-05-11', 'gh', 1, 'gfh', 3, '2019-04-25 11:40:07', '2019-04-24 18:34:00', '2019-04-24 23:34:00');
INSERT INTO `overtime_applications` (`id`, `employee_no`, `type`, `application_date`, `working_date`, `overtime_hours`, `reason`, `plan_of_dayoff`, `remarks`, `approval`, `comment`, `authorize`, `authorize_datetime`, `overtime_start`, `overtime_end`) VALUES(7, 3, 3, '2019-04-25', '2019-04-07', '5.75', '休みだけど出ます', '2019-04-22', '代休は取ります', 2, 'test006', 3, NULL, '2019-04-07 12:01:00', '2019-04-07 18:01:00');
INSERT INTO `overtime_applications` (`id`, `employee_no`, `type`, `application_date`, `working_date`, `overtime_hours`, `reason`, `plan_of_dayoff`, `remarks`, `approval`, `comment`, `authorize`, `authorize_datetime`, `overtime_start`, `overtime_end`) VALUES(9, 7, 1, '2019-04-27', '2019-04-27', '4.25', 'test', NULL, 'test', 1, '', 7, '2019-04-27 05:20:57', '2019-04-27 19:00:00', '2019-04-27 23:25:00');
INSERT INTO `overtime_applications` (`id`, `employee_no`, `type`, `application_date`, `working_date`, `overtime_hours`, `reason`, `plan_of_dayoff`, `remarks`, `approval`, `comment`, `authorize`, `authorize_datetime`, `overtime_start`, `overtime_end`) VALUES(10, 7, 1, '2019-04-27', '2019-04-16', '3.75', 'aaa', '2019-05-01', 'f', 1, '', 7, '2019-04-27 09:06:33', '2019-04-16 18:05:00', '2019-04-16 22:05:00');
INSERT INTO `overtime_applications` (`id`, `employee_no`, `type`, `application_date`, `working_date`, `overtime_hours`, `reason`, `plan_of_dayoff`, `remarks`, `approval`, `comment`, `authorize`, `authorize_datetime`, `overtime_start`, `overtime_end`) VALUES(11, 3, 1, '2019-05-03', '2019-05-03', '3.75', 'test001', '2019-05-31', 'test002', 1, 'test003', 3, '2019-05-03 17:09:28', '2019-05-03 18:08:00', '2019-05-03 22:08:00');
INSERT INTO `overtime_applications` (`id`, `employee_no`, `type`, `application_date`, `working_date`, `overtime_hours`, `reason`, `plan_of_dayoff`, `remarks`, `approval`, `comment`, `authorize`, `authorize_datetime`, `overtime_start`, `overtime_end`) VALUES(12, 3, 3, '2019-05-03', '2019-05-06', '3.75', 'test007', '2019-05-28', 'test008', 1, 'q', 3, '2019-06-02 12:36:16', '2019-05-06 13:12:00', '2019-05-06 17:13:00');
INSERT INTO `overtime_applications` (`id`, `employee_no`, `type`, `application_date`, `working_date`, `overtime_hours`, `reason`, `plan_of_dayoff`, `remarks`, `approval`, `comment`, `authorize`, `authorize_datetime`, `overtime_start`, `overtime_end`) VALUES(13, 3, 2, '2019-05-03', '2019-05-07', '8.75', 'test008', '2019-07-03', 'test009', 1, 'q', 3, '2019-06-02 03:39:57', '2019-05-07 20:13:00', '2019-05-08 05:13:00');
INSERT INTO `overtime_applications` (`id`, `employee_no`, `type`, `application_date`, `working_date`, `overtime_hours`, `reason`, `plan_of_dayoff`, `remarks`, `approval`, `comment`, `authorize`, `authorize_datetime`, `overtime_start`, `overtime_end`) VALUES(14, 32, 1, '2019-05-15', '2019-05-16', '4.00', '残業テスト', NULL, '', 0, NULL, 32, NULL, '2019-05-16 18:00:00', '2019-05-16 22:00:00');
INSERT INTO `overtime_applications` (`id`, `employee_no`, `type`, `application_date`, `working_date`, `overtime_hours`, `reason`, `plan_of_dayoff`, `remarks`, `approval`, `comment`, `authorize`, `authorize_datetime`, `overtime_start`, `overtime_end`) VALUES(15, 7, 1, '2019-05-24', '2019-05-23', '0.75', 'aaa', NULL, '', 0, NULL, 7, NULL, '2019-05-23 20:32:00', '2019-05-23 21:32:00');
INSERT INTO `overtime_applications` (`id`, `employee_no`, `type`, `application_date`, `working_date`, `overtime_hours`, `reason`, `plan_of_dayoff`, `remarks`, `approval`, `comment`, `authorize`, `authorize_datetime`, `overtime_start`, `overtime_end`) VALUES(16, 3, 1, '2019-06-02', '2019-05-07', '4.75', 'a', NULL, '', 1, 'q', 3, '2019-06-02 03:37:48', '2019-05-07 18:36:00', '2019-05-07 23:36:00');
INSERT INTO `overtime_applications` (`id`, `employee_no`, `type`, `application_date`, `working_date`, `overtime_hours`, `reason`, `plan_of_dayoff`, `remarks`, `approval`, `comment`, `authorize`, `authorize_datetime`, `overtime_start`, `overtime_end`) VALUES(17, 3, 3, '2019-06-02', '2019-05-26', '10.00', 'a', NULL, '', 1, 'q', 3, '2019-06-02 22:09:16', '2019-05-26 10:51:00', '2019-05-26 20:51:00');
INSERT INTO `overtime_applications` (`id`, `employee_no`, `type`, `application_date`, `working_date`, `overtime_hours`, `reason`, `plan_of_dayoff`, `remarks`, `approval`, `comment`, `authorize`, `authorize_datetime`, `overtime_start`, `overtime_end`) VALUES(18, 3, 3, '2019-06-26', '2019-06-29', '9.00', 'いろいろあったので', '2019-07-01', 'あ', 0, NULL, 3, NULL, '2019-06-29 09:31:00', '2019-06-29 18:31:00');
INSERT INTO `overtime_applications` (`id`, `employee_no`, `type`, `application_date`, `working_date`, `overtime_hours`, `reason`, `plan_of_dayoff`, `remarks`, `approval`, `comment`, `authorize`, `authorize_datetime`, `overtime_start`, `overtime_end`) VALUES(24, 3, 3, '2019-06-26', '2019-07-13', '13.00', 'いろいろあったので1', '2019-07-15', 'aa', 0, NULL, 3, NULL, '2019-07-13 09:51:00', '2019-07-13 22:51:00');
INSERT INTO `overtime_applications` (`id`, `employee_no`, `type`, `application_date`, `working_date`, `overtime_hours`, `reason`, `plan_of_dayoff`, `remarks`, `approval`, `comment`, `authorize`, `authorize_datetime`, `overtime_start`, `overtime_end`) VALUES(26, 3, 1, '2019-06-26', '2019-06-26', '3.00', 'a', NULL, '', 0, NULL, 3, NULL, '2019-06-26 19:58:00', '2019-06-26 22:58:00');
INSERT INTO `overtime_applications` (`id`, `employee_no`, `type`, `application_date`, `working_date`, `overtime_hours`, `reason`, `plan_of_dayoff`, `remarks`, `approval`, `comment`, `authorize`, `authorize_datetime`, `overtime_start`, `overtime_end`) VALUES(27, 3, 3, '2019-06-26', '2019-06-26', '14.02', 'asasa', '2019-07-02', 'a', 0, NULL, 3, NULL, '2019-06-26 08:58:00', '2019-06-26 22:59:00');
INSERT INTO `overtime_applications` (`id`, `employee_no`, `type`, `application_date`, `working_date`, `overtime_hours`, `reason`, `plan_of_dayoff`, `remarks`, `approval`, `comment`, `authorize`, `authorize_datetime`, `overtime_start`, `overtime_end`) VALUES(28, 14, 3, '2019-06-26', '2019-06-26', '10.00', 'aass', '2019-07-03', 'ありがとうございました。', 1, 'いいよ', 14, '2019-06-26 20:31:59', '2019-06-26 08:28:00', '2019-06-26 18:28:00');

-- --------------------------------------------------------

--
-- テーブルの構造 `paid_holidays_masters`
--

CREATE TABLE IF NOT EXISTS `paid_holidays_masters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `working_years` int(2) NOT NULL,
  `dates_on_year` int(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `positions`
--

CREATE TABLE IF NOT EXISTS `positions` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `company_no` int(11) NOT NULL COMMENT '会社ID',
  `name` varchar(256) NOT NULL COMMENT '名称',
  `approved_job_title_no` int(11) NOT NULL COMMENT '承認役職ID',
  `managerial_classification` int(2) NOT NULL COMMENT '管理職区分',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='役職テーブル' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `project_no` int(11) NOT NULL COMMENT '商談ID',
  `item_code` char(2) NOT NULL COMMENT '商品コード',
  `name` varchar(1024) NOT NULL COMMENT '名称',
  `deleted` int(1) DEFAULT NULL COMMENT '削除',
  `deleted_date` date DEFAULT NULL COMMENT '削除日',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商品テーブル' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `proposals`
--

CREATE TABLE IF NOT EXISTS `proposals` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(256) NOT NULL COMMENT '名称',
  `proposal_code` char(6) DEFAULT NULL COMMENT '案件コード',
  `deleted` int(1) NOT NULL DEFAULT '0' COMMENT '削除',
  `deleted_date` date DEFAULT NULL COMMENT '削除日',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='案件テーブル' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `reports`
--

CREATE TABLE IF NOT EXISTS `reports` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `daily_report_no` int(11) NOT NULL COMMENT '日報ID',
  `reporter_no` int(11) NOT NULL COMMENT '報告先ID',
  `confirmed` int(1) DEFAULT NULL COMMENT '確認済',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='報告先テーブル' AUTO_INCREMENT=55 ;

--
-- テーブルのデータのダンプ `reports`
--

INSERT INTO `reports` (`id`, `daily_report_no`, `reporter_no`, `confirmed`) VALUES(1, 8, 6, 0);
INSERT INTO `reports` (`id`, `daily_report_no`, `reporter_no`, `confirmed`) VALUES(2, 8, 13, 0);
INSERT INTO `reports` (`id`, `daily_report_no`, `reporter_no`, `confirmed`) VALUES(3, 10, 6, 0);
INSERT INTO `reports` (`id`, `daily_report_no`, `reporter_no`, `confirmed`) VALUES(4, 10, 13, 0);
INSERT INTO `reports` (`id`, `daily_report_no`, `reporter_no`, `confirmed`) VALUES(5, 11, 6, 0);
INSERT INTO `reports` (`id`, `daily_report_no`, `reporter_no`, `confirmed`) VALUES(6, 11, 13, 0);
INSERT INTO `reports` (`id`, `daily_report_no`, `reporter_no`, `confirmed`) VALUES(7, 12, 6, 0);
INSERT INTO `reports` (`id`, `daily_report_no`, `reporter_no`, `confirmed`) VALUES(8, 12, 13, 0);
INSERT INTO `reports` (`id`, `daily_report_no`, `reporter_no`, `confirmed`) VALUES(9, 13, 6, 0);
INSERT INTO `reports` (`id`, `daily_report_no`, `reporter_no`, `confirmed`) VALUES(10, 13, 13, 0);
INSERT INTO `reports` (`id`, `daily_report_no`, `reporter_no`, `confirmed`) VALUES(11, 14, 6, 0);
INSERT INTO `reports` (`id`, `daily_report_no`, `reporter_no`, `confirmed`) VALUES(12, 14, 13, 0);
INSERT INTO `reports` (`id`, `daily_report_no`, `reporter_no`, `confirmed`) VALUES(14, 16, 15, 1);
INSERT INTO `reports` (`id`, `daily_report_no`, `reporter_no`, `confirmed`) VALUES(19, 33, 6, 0);
INSERT INTO `reports` (`id`, `daily_report_no`, `reporter_no`, `confirmed`) VALUES(20, 33, 13, 0);
INSERT INTO `reports` (`id`, `daily_report_no`, `reporter_no`, `confirmed`) VALUES(21, 34, 6, 0);
INSERT INTO `reports` (`id`, `daily_report_no`, `reporter_no`, `confirmed`) VALUES(22, 34, 13, 0);
INSERT INTO `reports` (`id`, `daily_report_no`, `reporter_no`, `confirmed`) VALUES(23, 38, 6, 0);
INSERT INTO `reports` (`id`, `daily_report_no`, `reporter_no`, `confirmed`) VALUES(24, 38, 13, 0);
INSERT INTO `reports` (`id`, `daily_report_no`, `reporter_no`, `confirmed`) VALUES(25, 39, 6, 0);
INSERT INTO `reports` (`id`, `daily_report_no`, `reporter_no`, `confirmed`) VALUES(26, 39, 13, 0);
INSERT INTO `reports` (`id`, `daily_report_no`, `reporter_no`, `confirmed`) VALUES(27, 40, 6, 0);
INSERT INTO `reports` (`id`, `daily_report_no`, `reporter_no`, `confirmed`) VALUES(28, 40, 13, 0);
INSERT INTO `reports` (`id`, `daily_report_no`, `reporter_no`, `confirmed`) VALUES(31, 18, 6, 0);
INSERT INTO `reports` (`id`, `daily_report_no`, `reporter_no`, `confirmed`) VALUES(32, 18, 13, 0);
INSERT INTO `reports` (`id`, `daily_report_no`, `reporter_no`, `confirmed`) VALUES(33, 46, 6, 0);
INSERT INTO `reports` (`id`, `daily_report_no`, `reporter_no`, `confirmed`) VALUES(34, 46, 13, 0);
INSERT INTO `reports` (`id`, `daily_report_no`, `reporter_no`, `confirmed`) VALUES(35, 47, 6, 0);
INSERT INTO `reports` (`id`, `daily_report_no`, `reporter_no`, `confirmed`) VALUES(36, 47, 13, 0);
INSERT INTO `reports` (`id`, `daily_report_no`, `reporter_no`, `confirmed`) VALUES(37, 48, 6, 0);
INSERT INTO `reports` (`id`, `daily_report_no`, `reporter_no`, `confirmed`) VALUES(38, 48, 13, 0);
INSERT INTO `reports` (`id`, `daily_report_no`, `reporter_no`, `confirmed`) VALUES(39, 49, 6, 0);
INSERT INTO `reports` (`id`, `daily_report_no`, `reporter_no`, `confirmed`) VALUES(40, 49, 13, 0);
INSERT INTO `reports` (`id`, `daily_report_no`, `reporter_no`, `confirmed`) VALUES(41, 50, 6, 0);
INSERT INTO `reports` (`id`, `daily_report_no`, `reporter_no`, `confirmed`) VALUES(42, 50, 13, 0);
INSERT INTO `reports` (`id`, `daily_report_no`, `reporter_no`, `confirmed`) VALUES(43, 51, 6, 0);
INSERT INTO `reports` (`id`, `daily_report_no`, `reporter_no`, `confirmed`) VALUES(44, 51, 13, 0);
INSERT INTO `reports` (`id`, `daily_report_no`, `reporter_no`, `confirmed`) VALUES(45, 52, 6, 0);
INSERT INTO `reports` (`id`, `daily_report_no`, `reporter_no`, `confirmed`) VALUES(46, 52, 13, 0);
INSERT INTO `reports` (`id`, `daily_report_no`, `reporter_no`, `confirmed`) VALUES(49, 54, 6, 0);
INSERT INTO `reports` (`id`, `daily_report_no`, `reporter_no`, `confirmed`) VALUES(50, 54, 13, 0);
INSERT INTO `reports` (`id`, `daily_report_no`, `reporter_no`, `confirmed`) VALUES(51, 53, 6, 0);
INSERT INTO `reports` (`id`, `daily_report_no`, `reporter_no`, `confirmed`) VALUES(52, 53, 13, 0);
INSERT INTO `reports` (`id`, `daily_report_no`, `reporter_no`, `confirmed`) VALUES(53, 58, 6, 0);
INSERT INTO `reports` (`id`, `daily_report_no`, `reporter_no`, `confirmed`) VALUES(54, 58, 13, 0);

-- --------------------------------------------------------

--
-- テーブルの構造 `sections`
--

CREATE TABLE IF NOT EXISTS `sections` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `department_no` int(11) NOT NULL COMMENT '部ID',
  `name` varchar(256) NOT NULL COMMENT '名称',
  `deleted` int(1) NOT NULL DEFAULT '0' COMMENT '削除',
  `deleted_date` datetime DEFAULT NULL COMMENT '削除日',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='課テーブル' AUTO_INCREMENT=4 ;

--
-- テーブルのデータのダンプ `sections`
--

INSERT INTO `sections` (`id`, `department_no`, `name`, `deleted`, `deleted_date`) VALUES(1, 1, 'リフォーム事業部', 0, NULL);
INSERT INTO `sections` (`id`, `department_no`, `name`, `deleted`, `deleted_date`) VALUES(2, 2, 'リフォーム事業部', 0, NULL);
INSERT INTO `sections` (`id`, `department_no`, `name`, `deleted`, `deleted_date`) VALUES(3, 3, '管理本部', 0, NULL);

-- --------------------------------------------------------

--
-- テーブルの構造 `shifts`
--

CREATE TABLE IF NOT EXISTS `shifts` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `employee_no` int(11) NOT NULL,
  `date` date NOT NULL COMMENT '日付',
  `start_time` time NOT NULL COMMENT '始業時刻',
  `closing_time` time NOT NULL COMMENT '終業時刻',
  `basic_uptime` decimal(3,2) NOT NULL COMMENT '基本稼働時間',
  `break_deduction` decimal(3,2) NOT NULL COMMENT '休憩控除(昼食 1h or 0.5h)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='休日テーブル' AUTO_INCREMENT=356 ;

--
-- テーブルのデータのダンプ `shifts`
--

INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(4, 9, '2019-04-15', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(5, 9, '2019-04-16', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(6, 9, '2019-04-17', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(7, 9, '2019-04-18', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(8, 9, '2019-04-19', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(9, 9, '2019-04-20', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(10, 9, '2019-04-22', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(11, 9, '2019-04-23', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(12, 9, '2019-04-24', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(13, 9, '2019-04-25', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(14, 9, '2019-04-26', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(15, 9, '2019-04-30', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(16, 11, '2019-04-15', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(17, 11, '2019-04-16', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(18, 11, '2019-04-17', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(19, 11, '2019-04-18', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(20, 11, '2019-04-19', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(21, 11, '2019-04-20', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(22, 11, '2019-04-22', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(23, 11, '2019-04-23', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(24, 11, '2019-04-24', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(25, 11, '2019-04-25', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(26, 11, '2019-04-26', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(27, 11, '2019-04-30', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(28, 14, '2019-04-15', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(29, 14, '2019-04-16', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(30, 14, '2019-04-17', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(31, 14, '2019-04-18', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(32, 14, '2019-04-19', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(33, 14, '2019-04-20', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(34, 14, '2019-04-22', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(35, 14, '2019-04-23', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(36, 14, '2019-04-24', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(37, 14, '2019-04-25', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(38, 14, '2019-04-26', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(39, 14, '2019-04-30', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(40, 10, '2019-04-15', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(41, 10, '2019-04-16', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(42, 10, '2019-04-17', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(43, 10, '2019-04-18', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(44, 10, '2019-04-19', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(45, 10, '2019-04-22', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(46, 10, '2019-04-23', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(47, 10, '2019-04-24', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(48, 10, '2019-04-25', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(49, 10, '2019-04-26', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(50, 10, '2019-04-27', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(51, 10, '2019-04-30', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(52, 13, '2019-04-15', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(53, 13, '2019-04-16', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(54, 13, '2019-04-17', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(55, 13, '2019-04-18', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(56, 13, '2019-04-19', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(57, 13, '2019-04-22', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(58, 13, '2019-04-23', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(59, 13, '2019-04-24', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(60, 13, '2019-04-25', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(61, 13, '2019-04-26', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(62, 13, '2019-04-27', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(63, 13, '2019-04-30', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(64, 19, '2019-04-15', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(65, 19, '2019-04-16', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(66, 19, '2019-04-17', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(67, 19, '2019-04-18', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(68, 19, '2019-04-19', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(69, 19, '2019-04-22', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(70, 19, '2019-04-23', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(71, 19, '2019-04-24', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(72, 19, '2019-04-25', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(73, 19, '2019-04-26', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(74, 19, '2019-04-27', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(75, 19, '2019-04-30', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(76, 18, '2019-04-15', '08:00:00', '17:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(77, 18, '2019-04-16', '08:00:00', '17:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(78, 18, '2019-04-17', '08:00:00', '17:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(79, 18, '2019-04-18', '08:00:00', '17:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(80, 18, '2019-04-19', '08:00:00', '17:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(81, 18, '2019-04-20', '08:00:00', '17:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(82, 18, '2019-04-22', '08:00:00', '17:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(83, 18, '2019-04-23', '08:00:00', '17:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(84, 18, '2019-04-24', '08:00:00', '17:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(85, 18, '2019-04-25', '08:00:00', '17:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(86, 18, '2019-04-26', '08:00:00', '17:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(87, 18, '2019-04-30', '08:00:00', '17:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(88, 16, '2019-04-15', '08:00:00', '17:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(89, 16, '2019-04-16', '08:00:00', '17:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(90, 16, '2019-04-17', '08:00:00', '17:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(91, 16, '2019-04-18', '08:00:00', '17:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(92, 16, '2019-04-19', '08:00:00', '17:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(93, 16, '2019-04-20', '08:00:00', '17:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(94, 16, '2019-04-22', '08:00:00', '17:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(95, 16, '2019-04-23', '08:00:00', '17:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(96, 16, '2019-04-24', '08:00:00', '17:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(97, 16, '2019-04-25', '08:00:00', '17:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(98, 16, '2019-04-26', '08:00:00', '17:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(99, 16, '2019-04-30', '08:00:00', '17:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(100, 27, '2019-04-15', '08:00:00', '17:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(101, 27, '2019-04-16', '08:00:00', '17:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(102, 27, '2019-04-17', '08:00:00', '17:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(103, 27, '2019-04-18', '08:00:00', '17:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(104, 27, '2019-04-19', '08:00:00', '17:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(105, 27, '2019-04-20', '08:00:00', '17:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(106, 27, '2019-04-22', '08:00:00', '17:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(107, 27, '2019-04-23', '08:00:00', '17:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(108, 27, '2019-04-24', '08:00:00', '17:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(109, 27, '2019-04-25', '08:00:00', '17:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(110, 27, '2019-04-26', '08:00:00', '17:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(111, 27, '2019-04-30', '08:00:00', '17:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(112, 5, '2019-04-15', '08:00:00', '17:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(113, 5, '2019-04-16', '08:00:00', '17:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(114, 5, '2019-04-17', '08:00:00', '17:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(115, 5, '2019-04-18', '08:00:00', '17:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(116, 5, '2019-04-19', '08:00:00', '17:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(117, 5, '2019-04-22', '08:00:00', '17:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(118, 5, '2019-04-23', '08:00:00', '17:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(119, 5, '2019-04-24', '08:00:00', '17:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(120, 5, '2019-04-25', '08:00:00', '17:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(121, 5, '2019-04-26', '08:00:00', '17:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(122, 5, '2019-04-27', '08:00:00', '17:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(123, 5, '2019-04-30', '08:00:00', '17:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(124, 15, '2019-04-15', '08:00:00', '17:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(125, 15, '2019-04-16', '08:00:00', '17:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(126, 15, '2019-04-17', '08:00:00', '17:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(127, 15, '2019-04-18', '08:00:00', '17:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(128, 15, '2019-04-19', '08:00:00', '17:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(129, 15, '2019-04-22', '08:00:00', '17:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(130, 15, '2019-04-23', '08:00:00', '17:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(131, 15, '2019-04-24', '08:00:00', '17:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(132, 15, '2019-04-25', '08:00:00', '17:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(133, 15, '2019-04-26', '08:00:00', '17:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(134, 15, '2019-04-27', '08:00:00', '17:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(135, 15, '2019-04-30', '08:00:00', '17:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(136, 20, '2019-04-15', '08:00:00', '17:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(137, 20, '2019-04-16', '08:00:00', '17:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(138, 20, '2019-04-17', '08:00:00', '17:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(139, 20, '2019-04-18', '08:00:00', '17:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(140, 20, '2019-04-19', '08:00:00', '17:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(141, 20, '2019-04-22', '08:00:00', '17:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(142, 20, '2019-04-23', '08:00:00', '17:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(143, 20, '2019-04-24', '08:00:00', '17:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(144, 20, '2019-04-25', '08:00:00', '17:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(145, 20, '2019-04-26', '08:00:00', '17:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(146, 20, '2019-04-27', '08:00:00', '17:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(147, 20, '2019-04-30', '08:00:00', '17:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(148, 7, '2019-04-15', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(149, 7, '2019-04-16', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(150, 7, '2019-04-17', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(151, 7, '2019-04-18', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(152, 7, '2019-04-19', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(153, 7, '2019-04-20', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(154, 7, '2019-04-22', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(155, 7, '2019-04-23', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(156, 7, '2019-04-24', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(157, 7, '2019-04-25', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(158, 7, '2019-04-26', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(159, 7, '2019-04-30', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(160, 17, '2019-04-15', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(161, 17, '2019-04-16', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(162, 17, '2019-04-17', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(163, 17, '2019-04-18', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(164, 17, '2019-04-19', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(165, 17, '2019-04-20', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(166, 17, '2019-04-22', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(167, 17, '2019-04-23', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(168, 17, '2019-04-24', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(169, 17, '2019-04-25', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(170, 17, '2019-04-26', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(171, 17, '2019-04-30', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(172, 21, '2019-04-15', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(173, 21, '2019-04-16', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(174, 21, '2019-04-17', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(175, 21, '2019-04-18', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(176, 21, '2019-04-19', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(177, 21, '2019-04-20', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(178, 21, '2019-04-22', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(179, 21, '2019-04-23', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(180, 21, '2019-04-24', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(181, 21, '2019-04-25', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(182, 21, '2019-04-26', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(183, 21, '2019-04-30', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(184, 8, '2019-04-15', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(185, 8, '2019-04-16', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(186, 8, '2019-04-17', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(187, 8, '2019-04-18', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(188, 8, '2019-04-19', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(189, 8, '2019-04-22', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(190, 8, '2019-04-23', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(191, 8, '2019-04-24', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(192, 8, '2019-04-25', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(193, 8, '2019-04-26', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(194, 8, '2019-04-27', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(195, 8, '2019-04-30', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(196, 12, '2019-04-15', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(197, 12, '2019-04-16', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(198, 12, '2019-04-17', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(199, 12, '2019-04-18', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(200, 12, '2019-04-19', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(201, 12, '2019-04-22', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(202, 12, '2019-04-23', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(203, 12, '2019-04-24', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(204, 12, '2019-04-25', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(205, 12, '2019-04-26', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(206, 12, '2019-04-27', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(207, 12, '2019-04-30', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(208, 23, '2019-04-15', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(209, 23, '2019-04-16', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(210, 23, '2019-04-17', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(211, 23, '2019-04-18', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(212, 23, '2019-04-19', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(213, 23, '2019-04-22', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(214, 23, '2019-04-23', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(215, 23, '2019-04-24', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(216, 23, '2019-04-25', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(217, 23, '2019-04-26', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(218, 23, '2019-04-30', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(219, 22, '2019-04-15', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(220, 22, '2019-04-16', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(221, 22, '2019-04-17', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(222, 22, '2019-04-18', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(223, 22, '2019-04-19', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(224, 22, '2019-04-22', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(225, 22, '2019-04-23', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(226, 22, '2019-04-24', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(227, 22, '2019-04-25', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(228, 22, '2019-04-26', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(229, 22, '2019-04-30', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(230, 18, '2019-04-15', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(231, 18, '2019-04-16', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(232, 18, '2019-04-17', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(233, 18, '2019-04-18', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(234, 18, '2019-04-19', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(235, 18, '2019-04-20', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(236, 18, '2019-04-22', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(237, 18, '2019-04-23', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(238, 18, '2019-04-24', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(239, 18, '2019-04-25', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(240, 18, '2019-04-26', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(241, 18, '2019-04-30', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(242, 24, '2019-04-15', '09:00:00', '15:00:00', '5.50', '0.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(243, 24, '2019-04-16', '09:00:00', '15:00:00', '5.50', '0.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(244, 24, '2019-04-17', '09:00:00', '15:00:00', '5.50', '0.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(245, 24, '2019-04-19', '09:00:00', '15:00:00', '5.50', '0.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(246, 24, '2019-04-22', '09:00:00', '15:00:00', '5.50', '0.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(247, 24, '2019-04-24', '09:00:00', '15:00:00', '5.50', '0.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(248, 24, '2019-04-25', '09:00:00', '15:00:00', '5.50', '0.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(249, 24, '2019-04-26', '09:00:00', '15:00:00', '5.50', '0.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(250, 25, '2019-04-15', '09:00:00', '17:00:00', '7.00', '1.00');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(251, 25, '2019-04-16', '09:00:00', '17:00:00', '7.00', '1.00');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(252, 25, '2019-04-17', '09:00:00', '17:00:00', '7.00', '1.00');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(253, 25, '2019-04-18', '09:00:00', '17:00:00', '7.00', '1.00');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(254, 25, '2019-04-19', '09:00:00', '17:00:00', '7.00', '1.00');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(255, 25, '2019-04-22', '09:00:00', '17:00:00', '7.00', '1.00');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(256, 25, '2019-04-24', '09:00:00', '17:00:00', '7.00', '1.00');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(257, 25, '2019-04-25', '09:00:00', '17:00:00', '7.00', '1.00');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(258, 25, '2019-04-26', '09:00:00', '17:00:00', '7.00', '1.00');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(259, 25, '2019-04-27', '09:00:00', '17:00:00', '7.00', '1.00');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(260, 26, '2019-04-15', '09:00:00', '13:00:00', '4.00', '0.00');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(261, 26, '2019-04-16', '09:00:00', '13:00:00', '4.00', '0.00');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(262, 26, '2019-04-17', '09:00:00', '15:00:00', '5.50', '0.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(263, 26, '2019-04-19', '09:00:00', '15:00:00', '5.50', '0.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(264, 26, '2019-04-22', '09:00:00', '15:00:00', '5.50', '0.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(265, 26, '2019-04-24', '09:00:00', '17:00:00', '7.00', '1.00');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(266, 26, '2019-04-25', '09:00:00', '15:00:00', '5.50', '0.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(267, 26, '2019-04-27', '09:00:00', '15:00:00', '5.50', '0.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(268, 28, '2019-04-15', '09:00:00', '15:00:00', '5.50', '0.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(269, 28, '2019-04-16', '09:00:00', '15:00:00', '5.50', '0.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(270, 28, '2019-04-18', '09:00:00', '15:00:00', '5.50', '0.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(271, 28, '2019-04-19', '09:00:00', '15:00:00', '5.50', '0.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(272, 28, '2019-04-20', '09:00:00', '12:00:00', '3.00', '0.00');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(273, 28, '2019-04-22', '09:00:00', '15:00:00', '5.50', '0.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(274, 28, '2019-04-23', '09:00:00', '15:00:00', '5.50', '0.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(275, 28, '2019-04-25', '09:00:00', '15:00:00', '5.50', '0.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(276, 28, '2019-04-26', '09:00:00', '15:00:00', '5.50', '0.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(277, 28, '2019-04-27', '09:00:00', '12:00:00', '3.00', '0.00');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(278, 28, '2019-04-29', '09:00:00', '15:00:00', '5.50', '0.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(279, 28, '2019-04-30', '09:00:00', '15:00:00', '5.50', '0.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(280, 30, '2019-04-18', '09:00:00', '17:00:00', '7.00', '1.00');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(281, 30, '2019-04-20', '09:00:00', '17:00:00', '7.00', '1.00');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(282, 30, '2019-04-24', '09:00:00', '17:00:00', '7.00', '1.00');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(283, 30, '2019-04-26', '09:00:00', '17:00:00', '7.00', '1.00');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(284, 30, '2019-04-29', '09:00:00', '17:00:00', '7.00', '1.00');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(285, 31, '2019-04-15', '09:00:00', '17:00:00', '7.00', '1.00');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(286, 31, '2019-04-16', '09:00:00', '17:00:00', '7.00', '1.00');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(287, 31, '2019-04-18', '09:00:00', '17:00:00', '7.00', '1.00');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(288, 31, '2019-04-19', '09:00:00', '17:00:00', '7.00', '1.00');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(289, 31, '2019-04-23', '09:00:00', '17:00:00', '7.00', '1.00');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(290, 31, '2019-04-24', '09:00:00', '17:00:00', '7.00', '1.00');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(291, 31, '2019-04-25', '09:00:00', '17:00:00', '7.00', '1.00');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(292, 31, '2019-04-26', '09:00:00', '17:00:00', '7.00', '1.00');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(293, 2, '2019-04-15', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(294, 2, '2019-04-16', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(295, 2, '2019-04-17', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(296, 2, '2019-04-18', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(297, 2, '2019-04-19', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(298, 2, '2019-04-20', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(299, 2, '2019-04-22', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(300, 2, '2019-04-23', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(301, 2, '2019-04-24', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(302, 2, '2019-04-25', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(303, 2, '2019-04-26', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(304, 2, '2019-04-27', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(305, 2, '2019-04-30', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(306, 3, '2019-04-15', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(307, 3, '2019-04-16', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(308, 3, '2019-04-17', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(309, 3, '2019-04-18', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(310, 3, '2019-04-19', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(311, 3, '2019-04-20', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(312, 3, '2019-04-22', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(313, 3, '2019-04-23', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(314, 3, '2019-04-24', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(315, 3, '2019-04-25', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(316, 3, '2019-04-26', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(317, 3, '2019-04-27', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(318, 3, '2019-04-30', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(319, 4, '2019-04-15', '09:00:00', '18:20:00', '7.84', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(320, 4, '2019-04-16', '09:00:00', '18:20:00', '7.84', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(321, 4, '2019-04-17', '09:00:00', '18:20:00', '7.84', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(322, 4, '2019-04-18', '09:00:00', '18:20:00', '7.84', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(323, 4, '2019-04-19', '09:00:00', '18:20:00', '7.84', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(324, 4, '2019-04-20', '09:00:00', '18:20:00', '7.84', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(325, 4, '2019-04-22', '09:00:00', '18:20:00', '7.84', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(326, 4, '2019-04-23', '09:00:00', '18:20:00', '7.84', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(327, 4, '2019-04-24', '09:00:00', '18:20:00', '7.84', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(328, 4, '2019-04-25', '09:00:00', '18:20:00', '7.84', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(329, 4, '2019-04-26', '09:00:00', '18:20:00', '7.84', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(330, 4, '2019-04-27', '09:00:00', '18:20:00', '7.84', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(331, 4, '2019-04-30', '09:00:00', '18:20:00', '7.84', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(332, 32, '2019-04-15', '09:00:00', '18:20:00', '7.84', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(333, 32, '2019-04-16', '09:00:00', '18:20:00', '7.84', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(334, 32, '2019-04-17', '09:00:00', '18:20:00', '7.84', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(335, 32, '2019-04-18', '09:00:00', '18:20:00', '7.84', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(336, 32, '2019-04-19', '09:00:00', '18:20:00', '7.84', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(337, 32, '2019-04-20', '09:00:00', '18:20:00', '7.84', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(338, 32, '2019-04-22', '09:00:00', '18:20:00', '7.84', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(339, 32, '2019-04-23', '09:00:00', '18:20:00', '7.84', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(340, 32, '2019-04-24', '09:00:00', '18:20:00', '7.84', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(341, 32, '2019-04-25', '09:00:00', '18:20:00', '7.84', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(342, 32, '2019-04-26', '09:00:00', '18:20:00', '7.84', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(343, 32, '2019-04-27', '09:00:00', '18:20:00', '7.84', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(344, 32, '2019-04-30', '09:00:00', '18:20:00', '7.84', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(347, 32, '2019-05-15', '09:00:00', '17:30:00', '7.50', '1.00');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(348, 8, '2019-06-01', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(349, 8, '2019-06-02', '09:00:00', '18:20:00', '7.83', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(350, 3, '2019-05-18', '09:00:00', '18:20:00', '7.84', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(351, 3, '2019-05-20', '09:00:00', '18:20:00', '7.84', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(352, 3, '2019-05-21', '09:00:00', '18:20:00', '7.84', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(353, 3, '2019-06-07', '09:00:00', '18:20:00', '7.84', '1.50');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(354, 8, '2019-06-07', '08:00:00', '16:45:00', '8.75', '1.00');
INSERT INTO `shifts` (`id`, `employee_no`, `date`, `start_time`, `closing_time`, `basic_uptime`, `break_deduction`) VALUES(355, 8, '2019-06-10', '08:00:00', '16:45:00', '8.75', '1.00');

-- --------------------------------------------------------

--
-- テーブルの構造 `tmp_business_description_by_opportunities`
--

CREATE TABLE IF NOT EXISTS `tmp_business_description_by_opportunities` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `employee_no` int(11) NOT NULL,
  `parent_project_no` int(11) NOT NULL COMMENT '案件ID',
  `project_no` int(11) NOT NULL COMMENT '商談ID',
  `operating_time` decimal(4,2) DEFAULT NULL COMMENT '稼働時間',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商談別稼働明細テーブル' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- テーブルの構造 `tmp_reporters`
--

CREATE TABLE IF NOT EXISTS `tmp_reporters` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `employee_no` int(11) NOT NULL,
  `reporter_no` int(11) NOT NULL COMMENT '報告先ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='報告先テーブル' AUTO_INCREMENT=57 ;

--
-- テーブルのデータのダンプ `tmp_reporters`
--

INSERT INTO `tmp_reporters` (`id`, `employee_no`, `reporter_no`) VALUES(16, 18, 15);

-- --------------------------------------------------------

--
-- テーブルの構造 `vacations`
--

CREATE TABLE IF NOT EXISTS `vacations` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `employee_no` int(11) NOT NULL COMMENT '社員ID',
  `year` int(4) NOT NULL COMMENT '年度',
  `number_of_working_days` int(11) DEFAULT NULL COMMENT '所定勤務日数',
  `number_of_days_that_were_taken_during_the_year` int(11) DEFAULT NULL COMMENT '当年有休取得数',
  `annual_number` int(11) DEFAULT NULL COMMENT '年次回数',
  `days_remaining_on_holiday` decimal(3,1) DEFAULT NULL COMMENT '有休日数',
  `days_on_holiday` decimal(3,1) DEFAULT NULL COMMENT '取得有休数',
  `absents_without_notice` decimal(3,1) DEFAULT NULL COMMENT '欠勤日数',
  `holiday_working_days` decimal(3,1) DEFAULT NULL COMMENT '休日出勤日数',
  `compensation_day_off` decimal(3,1) DEFAULT NULL COMMENT '代休日数',
  `time_off` decimal(3,1) DEFAULT NULL COMMENT '時間休',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='休暇テーブル' AUTO_INCREMENT=37 ;

--
-- テーブルのデータのダンプ `vacations`
--

INSERT INTO `vacations` (`id`, `employee_no`, `year`, `number_of_working_days`, `number_of_days_that_were_taken_during_the_year`, `annual_number`, `days_remaining_on_holiday`, `days_on_holiday`, `absents_without_notice`, `holiday_working_days`, `compensation_day_off`, `time_off`) VALUES(2, 2, 2019, 0, 0, 0, '0.0', '0.0', '0.0', '0.0', '0.0', '0.0');
INSERT INTO `vacations` (`id`, `employee_no`, `year`, `number_of_working_days`, `number_of_days_that_were_taken_during_the_year`, `annual_number`, `days_remaining_on_holiday`, `days_on_holiday`, `absents_without_notice`, `holiday_working_days`, `compensation_day_off`, `time_off`) VALUES(3, 2, 2019, 0, 0, 0, '0.0', '0.0', '0.0', '0.0', '0.0', '0.0');
INSERT INTO `vacations` (`id`, `employee_no`, `year`, `number_of_working_days`, `number_of_days_that_were_taken_during_the_year`, `annual_number`, `days_remaining_on_holiday`, `days_on_holiday`, `absents_without_notice`, `holiday_working_days`, `compensation_day_off`, `time_off`) VALUES(4, 3, 2019, 0, 0, 0, '0.0', '6.0', '5.0', '-3.0', '1.0', '7.0');
INSERT INTO `vacations` (`id`, `employee_no`, `year`, `number_of_working_days`, `number_of_days_that_were_taken_during_the_year`, `annual_number`, `days_remaining_on_holiday`, `days_on_holiday`, `absents_without_notice`, `holiday_working_days`, `compensation_day_off`, `time_off`) VALUES(5, 4, 2019, 0, 0, 0, '0.0', '0.0', '0.0', '0.0', '0.0', '0.0');
INSERT INTO `vacations` (`id`, `employee_no`, `year`, `number_of_working_days`, `number_of_days_that_were_taken_during_the_year`, `annual_number`, `days_remaining_on_holiday`, `days_on_holiday`, `absents_without_notice`, `holiday_working_days`, `compensation_day_off`, `time_off`) VALUES(6, 5, 2019, 0, 0, 0, '23.0', '0.0', '0.0', '0.0', '0.0', '0.0');
INSERT INTO `vacations` (`id`, `employee_no`, `year`, `number_of_working_days`, `number_of_days_that_were_taken_during_the_year`, `annual_number`, `days_remaining_on_holiday`, `days_on_holiday`, `absents_without_notice`, `holiday_working_days`, `compensation_day_off`, `time_off`) VALUES(7, 6, 2019, 0, 0, 0, '32.0', '0.0', '0.0', '0.0', '0.0', '0.0');
INSERT INTO `vacations` (`id`, `employee_no`, `year`, `number_of_working_days`, `number_of_days_that_were_taken_during_the_year`, `annual_number`, `days_remaining_on_holiday`, `days_on_holiday`, `absents_without_notice`, `holiday_working_days`, `compensation_day_off`, `time_off`) VALUES(8, 7, 2019, 0, 0, 0, '29.5', '0.0', '0.0', '0.0', '0.0', '0.0');
INSERT INTO `vacations` (`id`, `employee_no`, `year`, `number_of_working_days`, `number_of_days_that_were_taken_during_the_year`, `annual_number`, `days_remaining_on_holiday`, `days_on_holiday`, `absents_without_notice`, `holiday_working_days`, `compensation_day_off`, `time_off`) VALUES(9, 8, 2019, 0, 5, 23, '25.5', '0.0', '0.0', '0.0', '2.5', '39.5');
INSERT INTO `vacations` (`id`, `employee_no`, `year`, `number_of_working_days`, `number_of_days_that_were_taken_during_the_year`, `annual_number`, `days_remaining_on_holiday`, `days_on_holiday`, `absents_without_notice`, `holiday_working_days`, `compensation_day_off`, `time_off`) VALUES(10, 9, 2019, 0, 0, 0, '18.0', '0.0', '0.0', '0.0', '0.0', '0.0');
INSERT INTO `vacations` (`id`, `employee_no`, `year`, `number_of_working_days`, `number_of_days_that_were_taken_during_the_year`, `annual_number`, `days_remaining_on_holiday`, `days_on_holiday`, `absents_without_notice`, `holiday_working_days`, `compensation_day_off`, `time_off`) VALUES(11, 10, 2019, 0, 0, 0, '26.0', '0.0', '0.0', '0.0', '0.0', '0.0');
INSERT INTO `vacations` (`id`, `employee_no`, `year`, `number_of_working_days`, `number_of_days_that_were_taken_during_the_year`, `annual_number`, `days_remaining_on_holiday`, `days_on_holiday`, `absents_without_notice`, `holiday_working_days`, `compensation_day_off`, `time_off`) VALUES(12, 11, 2019, 0, 0, 0, '22.0', '0.0', '0.0', '0.0', '0.0', '0.0');
INSERT INTO `vacations` (`id`, `employee_no`, `year`, `number_of_working_days`, `number_of_days_that_were_taken_during_the_year`, `annual_number`, `days_remaining_on_holiday`, `days_on_holiday`, `absents_without_notice`, `holiday_working_days`, `compensation_day_off`, `time_off`) VALUES(13, 12, 2019, 0, 0, 0, '14.5', '0.0', '0.0', '0.0', '0.0', '0.0');
INSERT INTO `vacations` (`id`, `employee_no`, `year`, `number_of_working_days`, `number_of_days_that_were_taken_during_the_year`, `annual_number`, `days_remaining_on_holiday`, `days_on_holiday`, `absents_without_notice`, `holiday_working_days`, `compensation_day_off`, `time_off`) VALUES(14, 13, 2019, 0, 0, 0, '23.0', '0.0', '0.0', '0.0', '0.0', '0.0');
INSERT INTO `vacations` (`id`, `employee_no`, `year`, `number_of_working_days`, `number_of_days_that_were_taken_during_the_year`, `annual_number`, `days_remaining_on_holiday`, `days_on_holiday`, `absents_without_notice`, `holiday_working_days`, `compensation_day_off`, `time_off`) VALUES(15, 14, 2019, 0, 0, 0, '18.0', '0.0', '0.0', '-1.0', '1.0', '0.0');
INSERT INTO `vacations` (`id`, `employee_no`, `year`, `number_of_working_days`, `number_of_days_that_were_taken_during_the_year`, `annual_number`, `days_remaining_on_holiday`, `days_on_holiday`, `absents_without_notice`, `holiday_working_days`, `compensation_day_off`, `time_off`) VALUES(16, 15, 2019, 0, 0, 0, '12.0', '0.0', '0.0', '0.0', '0.0', '0.0');
INSERT INTO `vacations` (`id`, `employee_no`, `year`, `number_of_working_days`, `number_of_days_that_were_taken_during_the_year`, `annual_number`, `days_remaining_on_holiday`, `days_on_holiday`, `absents_without_notice`, `holiday_working_days`, `compensation_day_off`, `time_off`) VALUES(17, 16, 2019, 0, 0, 0, '19.0', '0.0', '0.0', '0.0', '0.0', '0.0');
INSERT INTO `vacations` (`id`, `employee_no`, `year`, `number_of_working_days`, `number_of_days_that_were_taken_during_the_year`, `annual_number`, `days_remaining_on_holiday`, `days_on_holiday`, `absents_without_notice`, `holiday_working_days`, `compensation_day_off`, `time_off`) VALUES(18, 17, 2019, 0, 0, 0, '12.5', '0.0', '0.0', '0.0', '0.0', '0.0');
INSERT INTO `vacations` (`id`, `employee_no`, `year`, `number_of_working_days`, `number_of_days_that_were_taken_during_the_year`, `annual_number`, `days_remaining_on_holiday`, `days_on_holiday`, `absents_without_notice`, `holiday_working_days`, `compensation_day_off`, `time_off`) VALUES(19, 18, 2019, 0, 0, 0, '17.0', '0.0', '0.0', '0.0', '0.0', '0.0');
INSERT INTO `vacations` (`id`, `employee_no`, `year`, `number_of_working_days`, `number_of_days_that_were_taken_during_the_year`, `annual_number`, `days_remaining_on_holiday`, `days_on_holiday`, `absents_without_notice`, `holiday_working_days`, `compensation_day_off`, `time_off`) VALUES(20, 19, 2019, 0, 0, 0, '10.0', '0.0', '0.0', '0.0', '0.0', '0.0');
INSERT INTO `vacations` (`id`, `employee_no`, `year`, `number_of_working_days`, `number_of_days_that_were_taken_during_the_year`, `annual_number`, `days_remaining_on_holiday`, `days_on_holiday`, `absents_without_notice`, `holiday_working_days`, `compensation_day_off`, `time_off`) VALUES(21, 20, 2019, 0, 0, 0, '7.0', '0.0', '0.0', '0.0', '0.0', '0.0');
INSERT INTO `vacations` (`id`, `employee_no`, `year`, `number_of_working_days`, `number_of_days_that_were_taken_during_the_year`, `annual_number`, `days_remaining_on_holiday`, `days_on_holiday`, `absents_without_notice`, `holiday_working_days`, `compensation_day_off`, `time_off`) VALUES(22, 21, 2019, 0, 0, 0, '6.0', '0.0', '0.0', '0.0', '0.0', '0.0');
INSERT INTO `vacations` (`id`, `employee_no`, `year`, `number_of_working_days`, `number_of_days_that_were_taken_during_the_year`, `annual_number`, `days_remaining_on_holiday`, `days_on_holiday`, `absents_without_notice`, `holiday_working_days`, `compensation_day_off`, `time_off`) VALUES(23, 22, 2019, 0, 0, 0, '7.0', '0.0', '0.0', '0.0', '0.0', '0.0');
INSERT INTO `vacations` (`id`, `employee_no`, `year`, `number_of_working_days`, `number_of_days_that_were_taken_during_the_year`, `annual_number`, `days_remaining_on_holiday`, `days_on_holiday`, `absents_without_notice`, `holiday_working_days`, `compensation_day_off`, `time_off`) VALUES(24, 23, 2019, 0, 0, 0, '18.5', '0.0', '0.0', '0.0', '0.0', '0.0');
INSERT INTO `vacations` (`id`, `employee_no`, `year`, `number_of_working_days`, `number_of_days_that_were_taken_during_the_year`, `annual_number`, `days_remaining_on_holiday`, `days_on_holiday`, `absents_without_notice`, `holiday_working_days`, `compensation_day_off`, `time_off`) VALUES(25, 24, 2019, 0, 0, 0, '17.0', '0.0', '0.0', '0.0', '0.0', '0.0');
INSERT INTO `vacations` (`id`, `employee_no`, `year`, `number_of_working_days`, `number_of_days_that_were_taken_during_the_year`, `annual_number`, `days_remaining_on_holiday`, `days_on_holiday`, `absents_without_notice`, `holiday_working_days`, `compensation_day_off`, `time_off`) VALUES(26, 25, 2019, 0, 0, 0, '16.0', '0.0', '0.0', '0.0', '0.0', '0.0');
INSERT INTO `vacations` (`id`, `employee_no`, `year`, `number_of_working_days`, `number_of_days_that_were_taken_during_the_year`, `annual_number`, `days_remaining_on_holiday`, `days_on_holiday`, `absents_without_notice`, `holiday_working_days`, `compensation_day_off`, `time_off`) VALUES(27, 26, 2019, 0, 0, 0, '10.0', '0.0', '0.0', '0.0', '0.0', '0.0');
INSERT INTO `vacations` (`id`, `employee_no`, `year`, `number_of_working_days`, `number_of_days_that_were_taken_during_the_year`, `annual_number`, `days_remaining_on_holiday`, `days_on_holiday`, `absents_without_notice`, `holiday_working_days`, `compensation_day_off`, `time_off`) VALUES(28, 27, 2019, 0, 0, 0, '10.0', '0.0', '0.0', '0.0', '0.0', '0.0');
INSERT INTO `vacations` (`id`, `employee_no`, `year`, `number_of_working_days`, `number_of_days_that_were_taken_during_the_year`, `annual_number`, `days_remaining_on_holiday`, `days_on_holiday`, `absents_without_notice`, `holiday_working_days`, `compensation_day_off`, `time_off`) VALUES(29, 28, 2019, 0, 0, 0, '21.0', '0.0', '0.0', '0.0', '0.0', '0.0');
INSERT INTO `vacations` (`id`, `employee_no`, `year`, `number_of_working_days`, `number_of_days_that_were_taken_during_the_year`, `annual_number`, `days_remaining_on_holiday`, `days_on_holiday`, `absents_without_notice`, `holiday_working_days`, `compensation_day_off`, `time_off`) VALUES(30, 29, 2019, 0, 0, 0, '0.0', '0.0', '0.0', '0.0', '0.0', '0.0');
INSERT INTO `vacations` (`id`, `employee_no`, `year`, `number_of_working_days`, `number_of_days_that_were_taken_during_the_year`, `annual_number`, `days_remaining_on_holiday`, `days_on_holiday`, `absents_without_notice`, `holiday_working_days`, `compensation_day_off`, `time_off`) VALUES(31, 30, 2019, 0, 0, 0, '3.0', '0.0', '0.0', '0.0', '0.0', '0.0');
INSERT INTO `vacations` (`id`, `employee_no`, `year`, `number_of_working_days`, `number_of_days_that_were_taken_during_the_year`, `annual_number`, `days_remaining_on_holiday`, `days_on_holiday`, `absents_without_notice`, `holiday_working_days`, `compensation_day_off`, `time_off`) VALUES(32, 31, 2019, 0, 0, 0, '0.0', '0.0', '0.0', '0.0', '0.0', '0.0');
INSERT INTO `vacations` (`id`, `employee_no`, `year`, `number_of_working_days`, `number_of_days_that_were_taken_during_the_year`, `annual_number`, `days_remaining_on_holiday`, `days_on_holiday`, `absents_without_notice`, `holiday_working_days`, `compensation_day_off`, `time_off`) VALUES(33, 32, 2019, 0, 0, 0, '0.0', '0.0', '0.0', '0.0', '0.0', '0.0');
INSERT INTO `vacations` (`id`, `employee_no`, `year`, `number_of_working_days`, `number_of_days_that_were_taken_during_the_year`, `annual_number`, `days_remaining_on_holiday`, `days_on_holiday`, `absents_without_notice`, `holiday_working_days`, `compensation_day_off`, `time_off`) VALUES(34, 32, 2019, 0, 0, 0, '0.0', '0.0', '0.0', '0.0', '0.0', '0.0');
INSERT INTO `vacations` (`id`, `employee_no`, `year`, `number_of_working_days`, `number_of_days_that_were_taken_during_the_year`, `annual_number`, `days_remaining_on_holiday`, `days_on_holiday`, `absents_without_notice`, `holiday_working_days`, `compensation_day_off`, `time_off`) VALUES(35, 32, 2019, 0, 0, 0, '0.0', '0.0', '0.0', '0.0', '0.0', '0.0');
INSERT INTO `vacations` (`id`, `employee_no`, `year`, `number_of_working_days`, `number_of_days_that_were_taken_during_the_year`, `annual_number`, `days_remaining_on_holiday`, `days_on_holiday`, `absents_without_notice`, `holiday_working_days`, `compensation_day_off`, `time_off`) VALUES(36, 33, 2019, 0, 0, 0, '0.0', '0.0', '0.0', '0.0', '0.0', '0.0');

-- --------------------------------------------------------

--
-- テーブルの構造 `vacation_applications`
--

CREATE TABLE IF NOT EXISTS `vacation_applications` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `employee_no` int(11) NOT NULL COMMENT '社員ID',
  `type` int(2) NOT NULL COMMENT '種別',
  `application_date` date NOT NULL COMMENT '申請日',
  `vacation_date` date NOT NULL COMMENT '日時',
  `vacation_hours` int(1) DEFAULT NULL,
  `reason` varchar(256) NOT NULL COMMENT '理由',
  `remarks` varchar(256) DEFAULT NULL COMMENT '備考',
  `approval` int(1) NOT NULL DEFAULT '0' COMMENT '承認',
  `president_approval` int(1) NOT NULL DEFAULT '0',
  `comment` varchar(256) DEFAULT NULL COMMENT 'コメント',
  `authorize` int(11) DEFAULT NULL COMMENT '承認者',
  `authorize_datetime` datetime DEFAULT NULL COMMENT '承認日時',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='休暇申請テーブル' AUTO_INCREMENT=18 ;

--
-- テーブルのデータのダンプ `vacation_applications`
--

INSERT INTO `vacation_applications` (`id`, `employee_no`, `type`, `application_date`, `vacation_date`, `vacation_hours`, `reason`, `remarks`, `approval`, `president_approval`, `comment`, `authorize`, `authorize_datetime`) VALUES(1, 3, 5, '2019-04-25', '2019-04-18', NULL, '代わりの休みです', '休みます', 1, 0, 'test009', 3, '2019-05-03 17:21:04');
INSERT INTO `vacation_applications` (`id`, `employee_no`, `type`, `application_date`, `vacation_date`, `vacation_hours`, `reason`, `remarks`, `approval`, `president_approval`, `comment`, `authorize`, `authorize_datetime`) VALUES(2, 3, 1, '2019-04-25', '2019-04-11', NULL, '有休です', '有休です', 2, 0, 'test010', 3, NULL);
INSERT INTO `vacation_applications` (`id`, `employee_no`, `type`, `application_date`, `vacation_date`, `vacation_hours`, `reason`, `remarks`, `approval`, `president_approval`, `comment`, `authorize`, `authorize_datetime`) VALUES(3, 3, 9, '2019-04-25', '2019-04-02', NULL, '休みました', '出勤しませんでした', 0, 0, NULL, 3, NULL);
INSERT INTO `vacation_applications` (`id`, `employee_no`, `type`, `application_date`, `vacation_date`, `vacation_hours`, `reason`, `remarks`, `approval`, `president_approval`, `comment`, `authorize`, `authorize_datetime`) VALUES(4, 3, 2, '2019-04-25', '2019-04-24', NULL, '腹痛のため', '腹痛のため', 0, 0, 'ｒｒ', 3, '2019-04-25 12:35:12');
INSERT INTO `vacation_applications` (`id`, `employee_no`, `type`, `application_date`, `vacation_date`, `vacation_hours`, `reason`, `remarks`, `approval`, `president_approval`, `comment`, `authorize`, `authorize_datetime`) VALUES(6, 7, 7, '2019-04-27', '2019-04-18', NULL, 'aa', 'sss', 1, 0, '', 7, '2019-04-27 09:06:37');
INSERT INTO `vacation_applications` (`id`, `employee_no`, `type`, `application_date`, `vacation_date`, `vacation_hours`, `reason`, `remarks`, `approval`, `president_approval`, `comment`, `authorize`, `authorize_datetime`) VALUES(7, 32, 1, '2019-05-15', '2019-05-15', NULL, '有給休暇テスト', '', 0, 0, NULL, 32, NULL);
INSERT INTO `vacation_applications` (`id`, `employee_no`, `type`, `application_date`, `vacation_date`, `vacation_hours`, `reason`, `remarks`, `approval`, `president_approval`, `comment`, `authorize`, `authorize_datetime`) VALUES(8, 3, 5, '2019-06-02', '2019-05-31', NULL, 'a', '', 1, 0, 'eee', 3, '2019-06-02 16:02:53');
INSERT INTO `vacation_applications` (`id`, `employee_no`, `type`, `application_date`, `vacation_date`, `vacation_hours`, `reason`, `remarks`, `approval`, `president_approval`, `comment`, `authorize`, `authorize_datetime`) VALUES(10, 3, 9, '2019-06-02', '2019-05-07', NULL, 'a', '', 1, 0, 'a', 3, '2019-06-02 17:02:34');
INSERT INTO `vacation_applications` (`id`, `employee_no`, `type`, `application_date`, `vacation_date`, `vacation_hours`, `reason`, `remarks`, `approval`, `president_approval`, `comment`, `authorize`, `authorize_datetime`) VALUES(11, 3, 1, '2019-06-02', '2019-05-17', NULL, 'a', '', 1, 0, 'eee', 3, '2019-06-02 22:14:54');
INSERT INTO `vacation_applications` (`id`, `employee_no`, `type`, `application_date`, `vacation_date`, `vacation_hours`, `reason`, `remarks`, `approval`, `president_approval`, `comment`, `authorize`, `authorize_datetime`) VALUES(12, 3, 5, '2019-06-26', '2019-07-01', NULL, '2019/6/29の休出の代休です。', NULL, 0, 0, NULL, NULL, NULL);
INSERT INTO `vacation_applications` (`id`, `employee_no`, `type`, `application_date`, `vacation_date`, `vacation_hours`, `reason`, `remarks`, `approval`, `president_approval`, `comment`, `authorize`, `authorize_datetime`) VALUES(14, 3, 5, '2019-06-26', '2019-07-15', NULL, '2019/7/13の休出の代休です。', NULL, 0, 0, NULL, NULL, NULL);
INSERT INTO `vacation_applications` (`id`, `employee_no`, `type`, `application_date`, `vacation_date`, `vacation_hours`, `reason`, `remarks`, `approval`, `president_approval`, `comment`, `authorize`, `authorize_datetime`) VALUES(15, 3, 5, '2019-06-26', '2019-06-28', NULL, '2019/6/26の休出の代休です。', NULL, 0, 0, NULL, NULL, NULL);
INSERT INTO `vacation_applications` (`id`, `employee_no`, `type`, `application_date`, `vacation_date`, `vacation_hours`, `reason`, `remarks`, `approval`, `president_approval`, `comment`, `authorize`, `authorize_datetime`) VALUES(16, 3, 5, '2019-06-26', '2019-07-02', NULL, '2019/6/26の休出の代休です。', NULL, 0, 0, NULL, NULL, NULL);
INSERT INTO `vacation_applications` (`id`, `employee_no`, `type`, `application_date`, `vacation_date`, `vacation_hours`, `reason`, `remarks`, `approval`, `president_approval`, `comment`, `authorize`, `authorize_datetime`) VALUES(17, 14, 5, '2019-06-26', '2019-07-03', NULL, '2019/6/26の休出の代休です。', NULL, 0, 0, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- ビュー用の代替構造 `v_aggregation_by_business_contacts`
--
CREATE TABLE IF NOT EXISTS `v_aggregation_by_business_contacts` (
`employment_year` int(4)
,`proposals_id` int(11)
,`proposals_name` varchar(256)
,`opportunities_id` int(11)
,`opportunities_name` varchar(256)
,`department_no` int(11)
,`section_no` int(11)
,`employee_no` int(11)
,`April` decimal(26,2)
,`May` decimal(26,2)
,`Jun` decimal(26,2)
,`July` decimal(26,2)
,`August` decimal(26,2)
,`September` decimal(26,2)
,`October` decimal(26,2)
,`November` decimal(26,2)
,`December` decimal(26,2)
,`January` decimal(26,2)
,`February` decimal(26,2)
,`March` decimal(26,2)
,`total_time` decimal(26,2)
);
-- --------------------------------------------------------

--
-- ビュー用の代替構造 `v_person_incharge_summary_ds`
--
CREATE TABLE IF NOT EXISTS `v_person_incharge_summary_ds` (
`employment_year` int(4)
,`employee_no` int(11)
,`proposals_id` int(11)
,`proposals_name` varchar(256)
,`opportunities_id` int(11)
,`opportunities_name` varchar(256)
,`April` decimal(26,2)
,`May` decimal(26,2)
,`Jun` decimal(26,2)
,`July` decimal(26,2)
,`August` decimal(26,2)
,`September` decimal(26,2)
,`October` decimal(26,2)
,`November` decimal(26,2)
,`December` decimal(26,2)
,`January` decimal(26,2)
,`February` decimal(26,2)
,`March` decimal(26,2)
,`total_time` decimal(26,2)
);
-- --------------------------------------------------------

--
-- ビュー用の代替構造 `v_person_incharge_summary_hs`
--
CREATE TABLE IF NOT EXISTS `v_person_incharge_summary_hs` (
`employment_year` int(4)
,`employee_no` int(11)
,`proposals_id` int(11)
,`proposals_name` varchar(256)
,`April` decimal(26,2)
,`May` decimal(26,2)
,`Jun` decimal(26,2)
,`July` decimal(26,2)
,`August` decimal(26,2)
,`September` decimal(26,2)
,`October` decimal(26,2)
,`November` decimal(26,2)
,`December` decimal(26,2)
,`January` decimal(26,2)
,`February` decimal(26,2)
,`March` decimal(26,2)
,`total_time` decimal(26,2)
);
-- --------------------------------------------------------

--
-- ビュー用の代替構造 `v_project_attendances`
--
CREATE TABLE IF NOT EXISTS `v_project_attendances` (
`employment_year` int(4)
,`client_departments_id` int(11)
,`client_departments_name` varchar(256)
,`proposals_id` int(11)
,`proposals_name` varchar(256)
,`opportunities_id` int(11)
,`opportunities_name` varchar(256)
,`April` decimal(26,2)
,`May` decimal(26,2)
,`Jun` decimal(26,2)
,`July` decimal(26,2)
,`August` decimal(26,2)
,`September` decimal(26,2)
,`October` decimal(26,2)
,`November` decimal(26,2)
,`December` decimal(26,2)
,`January` decimal(26,2)
,`February` decimal(26,2)
,`March` decimal(26,2)
,`total_time` decimal(26,2)
);
-- --------------------------------------------------------

--
-- ビュー用の代替構造 `v_project_summaries`
--
CREATE TABLE IF NOT EXISTS `v_project_summaries` (
`employment_year` int(4)
,`proposals_id` int(11)
,`proposals_name` varchar(256)
,`opportunities_id` int(11)
,`opportunities_name` varchar(256)
,`April` decimal(26,2)
,`May` decimal(26,2)
,`Jun` decimal(26,2)
,`July` decimal(26,2)
,`August` decimal(26,2)
,`September` decimal(26,2)
,`October` decimal(26,2)
,`November` decimal(26,2)
,`December` decimal(26,2)
,`January` decimal(26,2)
,`February` decimal(26,2)
,`March` decimal(26,2)
,`total_time` decimal(26,2)
);
-- --------------------------------------------------------

--
-- ビュー用の代替構造 `v_step_of_person_incharge_summaries`
--
CREATE TABLE IF NOT EXISTS `v_step_of_person_incharge_summaries` (
`employment_year` int(4)
,`client_departments_id` int(11)
,`client_departments_name` varchar(256)
,`proposals_id` int(11)
,`proposals_name` varchar(256)
,`opportunities_id` int(11)
,`opportunities_name` varchar(256)
,`employee_no` int(11)
,`April` decimal(26,2)
,`May` decimal(26,2)
,`Jun` decimal(26,2)
,`July` decimal(26,2)
,`August` decimal(26,2)
,`September` decimal(26,2)
,`October` decimal(26,2)
,`November` decimal(26,2)
,`December` decimal(26,2)
,`January` decimal(26,2)
,`February` decimal(26,2)
,`March` decimal(26,2)
,`total_time` decimal(26,2)
);
-- --------------------------------------------------------

--
-- ビュー用の代替構造 `v_work_operation_summarys`
--
CREATE TABLE IF NOT EXISTS `v_work_operation_summarys` (
`company_no` int(11)
,`division_no` int(11)
,`department_no` int(11)
,`section_no` int(11)
,`employee_no` int(11)
,`employment_year` int(4)
,`April` decimal(26,2)
,`May` decimal(26,2)
,`Jun` decimal(26,2)
,`July` decimal(26,2)
,`August` decimal(26,2)
,`September` decimal(26,2)
,`October` decimal(26,2)
,`November` decimal(26,2)
,`December` decimal(26,2)
,`January` decimal(26,2)
,`February` decimal(26,2)
,`March` decimal(26,2)
,`total_time` decimal(26,2)
);
-- --------------------------------------------------------

--
-- ビュー用の構造 `v_aggregation_by_business_contacts`
--
DROP TABLE IF EXISTS `v_aggregation_by_business_contacts`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_aggregation_by_business_contacts` AS select year(`operations`.`employment_date`) AS `employment_year`,`proposals`.`id` AS `proposals_id`,`proposals`.`name` AS `proposals_name`,`opportunities`.`id` AS `opportunities_id`,`opportunities`.`name` AS `opportunities_name`,`operations`.`department_no` AS `department_no`,`operations`.`section_no` AS `section_no`,`operations`.`employee_no` AS `employee_no`,sum((case when (month(`operations`.`employment_date`) = 4) then `business_description_by_opportunities`.`operating_time` else 0 end)) AS `April`,sum((case when (month(`operations`.`employment_date`) = 5) then `business_description_by_opportunities`.`operating_time` else 0 end)) AS `May`,sum((case when (month(`operations`.`employment_date`) = 6) then `business_description_by_opportunities`.`operating_time` else 0 end)) AS `Jun`,sum((case when (month(`operations`.`employment_date`) = 7) then `business_description_by_opportunities`.`operating_time` else 0 end)) AS `July`,sum((case when (month(`operations`.`employment_date`) = 8) then `business_description_by_opportunities`.`operating_time` else 0 end)) AS `August`,sum((case when (month(`operations`.`employment_date`) = 9) then `business_description_by_opportunities`.`operating_time` else 0 end)) AS `September`,sum((case when (month(`operations`.`employment_date`) = 10) then `business_description_by_opportunities`.`operating_time` else 0 end)) AS `October`,sum((case when (month(`operations`.`employment_date`) = 11) then `business_description_by_opportunities`.`operating_time` else 0 end)) AS `November`,sum((case when (month(`operations`.`employment_date`) = 12) then `business_description_by_opportunities`.`operating_time` else 0 end)) AS `December`,sum((case when (month(`operations`.`employment_date`) = 1) then `business_description_by_opportunities`.`operating_time` else 0 end)) AS `January`,sum((case when (month(`operations`.`employment_date`) = 2) then `business_description_by_opportunities`.`operating_time` else 0 end)) AS `February`,sum((case when (month(`operations`.`employment_date`) = 3) then `business_description_by_opportunities`.`operating_time` else 0 end)) AS `March`,sum(`business_description_by_opportunities`.`operating_time`) AS `total_time` from ((((`business_description_by_opportunities` left join `operations` on((`operations`.`id` = `business_description_by_opportunities`.`operation_no`))) left join `opportunities` on((`opportunities`.`id` = `business_description_by_opportunities`.`project_no`))) left join `proposals` on((`proposals`.`id` = `opportunities`.`parent_project_no`))) left join `client_departments` on((`client_departments`.`id` = `opportunities`.`department_no`))) group by year(`operations`.`employment_date`),`proposals`.`id`,`proposals`.`name`,`opportunities`.`id`,`opportunities`.`name`,`operations`.`department_no`,`operations`.`section_no`,`operations`.`employee_no`;

-- --------------------------------------------------------

--
-- ビュー用の構造 `v_person_incharge_summary_ds`
--
DROP TABLE IF EXISTS `v_person_incharge_summary_ds`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_person_incharge_summary_ds` AS select year(`operations`.`employment_date`) AS `employment_year`,`operations`.`employee_no` AS `employee_no`,`proposals`.`id` AS `proposals_id`,`proposals`.`name` AS `proposals_name`,`opportunities`.`id` AS `opportunities_id`,`opportunities`.`name` AS `opportunities_name`,sum((case when (month(`operations`.`employment_date`) = 4) then `business_description_by_opportunities`.`operating_time` else 0 end)) AS `April`,sum((case when (month(`operations`.`employment_date`) = 5) then `business_description_by_opportunities`.`operating_time` else 0 end)) AS `May`,sum((case when (month(`operations`.`employment_date`) = 6) then `business_description_by_opportunities`.`operating_time` else 0 end)) AS `Jun`,sum((case when (month(`operations`.`employment_date`) = 7) then `business_description_by_opportunities`.`operating_time` else 0 end)) AS `July`,sum((case when (month(`operations`.`employment_date`) = 8) then `business_description_by_opportunities`.`operating_time` else 0 end)) AS `August`,sum((case when (month(`operations`.`employment_date`) = 9) then `business_description_by_opportunities`.`operating_time` else 0 end)) AS `September`,sum((case when (month(`operations`.`employment_date`) = 10) then `business_description_by_opportunities`.`operating_time` else 0 end)) AS `October`,sum((case when (month(`operations`.`employment_date`) = 11) then `business_description_by_opportunities`.`operating_time` else 0 end)) AS `November`,sum((case when (month(`operations`.`employment_date`) = 12) then `business_description_by_opportunities`.`operating_time` else 0 end)) AS `December`,sum((case when (month(`operations`.`employment_date`) = 1) then `business_description_by_opportunities`.`operating_time` else 0 end)) AS `January`,sum((case when (month(`operations`.`employment_date`) = 2) then `business_description_by_opportunities`.`operating_time` else 0 end)) AS `February`,sum((case when (month(`operations`.`employment_date`) = 3) then `business_description_by_opportunities`.`operating_time` else 0 end)) AS `March`,sum(`business_description_by_opportunities`.`operating_time`) AS `total_time` from (((`business_description_by_opportunities` left join `operations` on((`operations`.`id` = `business_description_by_opportunities`.`operation_no`))) left join `opportunities` on((`opportunities`.`id` = `business_description_by_opportunities`.`project_no`))) left join `proposals` on((`proposals`.`id` = `business_description_by_opportunities`.`parent_project_no`))) group by year(`operations`.`employment_date`),`operations`.`employee_no`,`proposals`.`id`,`proposals`.`name`,`opportunities`.`id`,`opportunities`.`name`;

-- --------------------------------------------------------

--
-- ビュー用の構造 `v_person_incharge_summary_hs`
--
DROP TABLE IF EXISTS `v_person_incharge_summary_hs`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_person_incharge_summary_hs` AS select year(`operations`.`employment_date`) AS `employment_year`,`operations`.`employee_no` AS `employee_no`,`proposals`.`id` AS `proposals_id`,`proposals`.`name` AS `proposals_name`,sum((case when (month(`operations`.`employment_date`) = 4) then `business_description_by_opportunities`.`operating_time` else 0 end)) AS `April`,sum((case when (month(`operations`.`employment_date`) = 5) then `business_description_by_opportunities`.`operating_time` else 0 end)) AS `May`,sum((case when (month(`operations`.`employment_date`) = 6) then `business_description_by_opportunities`.`operating_time` else 0 end)) AS `Jun`,sum((case when (month(`operations`.`employment_date`) = 7) then `business_description_by_opportunities`.`operating_time` else 0 end)) AS `July`,sum((case when (month(`operations`.`employment_date`) = 8) then `business_description_by_opportunities`.`operating_time` else 0 end)) AS `August`,sum((case when (month(`operations`.`employment_date`) = 9) then `business_description_by_opportunities`.`operating_time` else 0 end)) AS `September`,sum((case when (month(`operations`.`employment_date`) = 10) then `business_description_by_opportunities`.`operating_time` else 0 end)) AS `October`,sum((case when (month(`operations`.`employment_date`) = 11) then `business_description_by_opportunities`.`operating_time` else 0 end)) AS `November`,sum((case when (month(`operations`.`employment_date`) = 12) then `business_description_by_opportunities`.`operating_time` else 0 end)) AS `December`,sum((case when (month(`operations`.`employment_date`) = 1) then `business_description_by_opportunities`.`operating_time` else 0 end)) AS `January`,sum((case when (month(`operations`.`employment_date`) = 2) then `business_description_by_opportunities`.`operating_time` else 0 end)) AS `February`,sum((case when (month(`operations`.`employment_date`) = 3) then `business_description_by_opportunities`.`operating_time` else 0 end)) AS `March`,sum(`business_description_by_opportunities`.`operating_time`) AS `total_time` from ((`business_description_by_opportunities` left join `operations` on((`operations`.`id` = `business_description_by_opportunities`.`operation_no`))) left join `proposals` on((`proposals`.`id` = `business_description_by_opportunities`.`parent_project_no`))) group by year(`operations`.`employment_date`),`operations`.`employee_no`,`proposals`.`id`,`proposals`.`name`;

-- --------------------------------------------------------

--
-- ビュー用の構造 `v_project_attendances`
--
DROP TABLE IF EXISTS `v_project_attendances`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_project_attendances` AS select year(`operations`.`employment_date`) AS `employment_year`,`client_departments`.`id` AS `client_departments_id`,`client_departments`.`name` AS `client_departments_name`,`proposals`.`id` AS `proposals_id`,`proposals`.`name` AS `proposals_name`,`opportunities`.`id` AS `opportunities_id`,`opportunities`.`name` AS `opportunities_name`,sum((case when (month(`operations`.`employment_date`) = 4) then `business_description_by_opportunities`.`operating_time` else 0 end)) AS `April`,sum((case when (month(`operations`.`employment_date`) = 5) then `business_description_by_opportunities`.`operating_time` else 0 end)) AS `May`,sum((case when (month(`operations`.`employment_date`) = 6) then `business_description_by_opportunities`.`operating_time` else 0 end)) AS `Jun`,sum((case when (month(`operations`.`employment_date`) = 7) then `business_description_by_opportunities`.`operating_time` else 0 end)) AS `July`,sum((case when (month(`operations`.`employment_date`) = 8) then `business_description_by_opportunities`.`operating_time` else 0 end)) AS `August`,sum((case when (month(`operations`.`employment_date`) = 9) then `business_description_by_opportunities`.`operating_time` else 0 end)) AS `September`,sum((case when (month(`operations`.`employment_date`) = 10) then `business_description_by_opportunities`.`operating_time` else 0 end)) AS `October`,sum((case when (month(`operations`.`employment_date`) = 11) then `business_description_by_opportunities`.`operating_time` else 0 end)) AS `November`,sum((case when (month(`operations`.`employment_date`) = 12) then `business_description_by_opportunities`.`operating_time` else 0 end)) AS `December`,sum((case when (month(`operations`.`employment_date`) = 1) then `business_description_by_opportunities`.`operating_time` else 0 end)) AS `January`,sum((case when (month(`operations`.`employment_date`) = 2) then `business_description_by_opportunities`.`operating_time` else 0 end)) AS `February`,sum((case when (month(`operations`.`employment_date`) = 3) then `business_description_by_opportunities`.`operating_time` else 0 end)) AS `March`,sum(`business_description_by_opportunities`.`operating_time`) AS `total_time` from ((((`business_description_by_opportunities` left join `operations` on((`operations`.`id` = `business_description_by_opportunities`.`operation_no`))) left join `opportunities` on((`opportunities`.`id` = `business_description_by_opportunities`.`project_no`))) left join `proposals` on((`proposals`.`id` = `opportunities`.`parent_project_no`))) left join `client_departments` on((`client_departments`.`id` = `opportunities`.`department_no`))) group by year(`operations`.`employment_date`),`client_departments`.`id`,`client_departments`.`name`,`proposals`.`id`,`proposals`.`name`,`opportunities`.`id`,`opportunities`.`name`;

-- --------------------------------------------------------

--
-- ビュー用の構造 `v_project_summaries`
--
DROP TABLE IF EXISTS `v_project_summaries`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_project_summaries` AS select year(`operations`.`employment_date`) AS `employment_year`,`proposals`.`id` AS `proposals_id`,`proposals`.`name` AS `proposals_name`,`opportunities`.`id` AS `opportunities_id`,`opportunities`.`name` AS `opportunities_name`,sum((case when (month(`operations`.`employment_date`) = 4) then `business_description_by_opportunities`.`operating_time` else 0 end)) AS `April`,sum((case when (month(`operations`.`employment_date`) = 5) then `business_description_by_opportunities`.`operating_time` else 0 end)) AS `May`,sum((case when (month(`operations`.`employment_date`) = 6) then `business_description_by_opportunities`.`operating_time` else 0 end)) AS `Jun`,sum((case when (month(`operations`.`employment_date`) = 7) then `business_description_by_opportunities`.`operating_time` else 0 end)) AS `July`,sum((case when (month(`operations`.`employment_date`) = 8) then `business_description_by_opportunities`.`operating_time` else 0 end)) AS `August`,sum((case when (month(`operations`.`employment_date`) = 9) then `business_description_by_opportunities`.`operating_time` else 0 end)) AS `September`,sum((case when (month(`operations`.`employment_date`) = 10) then `business_description_by_opportunities`.`operating_time` else 0 end)) AS `October`,sum((case when (month(`operations`.`employment_date`) = 11) then `business_description_by_opportunities`.`operating_time` else 0 end)) AS `November`,sum((case when (month(`operations`.`employment_date`) = 12) then `business_description_by_opportunities`.`operating_time` else 0 end)) AS `December`,sum((case when (month(`operations`.`employment_date`) = 1) then `business_description_by_opportunities`.`operating_time` else 0 end)) AS `January`,sum((case when (month(`operations`.`employment_date`) = 2) then `business_description_by_opportunities`.`operating_time` else 0 end)) AS `February`,sum((case when (month(`operations`.`employment_date`) = 3) then `business_description_by_opportunities`.`operating_time` else 0 end)) AS `March`,sum(`business_description_by_opportunities`.`operating_time`) AS `total_time` from (((`business_description_by_opportunities` left join `operations` on((`operations`.`id` = `business_description_by_opportunities`.`operation_no`))) left join `opportunities` on((`opportunities`.`id` = `business_description_by_opportunities`.`project_no`))) left join `proposals` on((`proposals`.`id` = `opportunities`.`parent_project_no`))) group by year(`operations`.`employment_date`),`proposals`.`id`,`proposals`.`name`,`opportunities`.`id`,`opportunities`.`name`;

-- --------------------------------------------------------

--
-- ビュー用の構造 `v_step_of_person_incharge_summaries`
--
DROP TABLE IF EXISTS `v_step_of_person_incharge_summaries`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_step_of_person_incharge_summaries` AS select year(`operations`.`employment_date`) AS `employment_year`,`client_departments`.`id` AS `client_departments_id`,`client_departments`.`name` AS `client_departments_name`,`proposals`.`id` AS `proposals_id`,`proposals`.`name` AS `proposals_name`,`opportunities`.`id` AS `opportunities_id`,`opportunities`.`name` AS `opportunities_name`,`operations`.`employee_no` AS `employee_no`,sum((case when (month(`operations`.`employment_date`) = 4) then `business_description_by_opportunities`.`operating_time` else 0 end)) AS `April`,sum((case when (month(`operations`.`employment_date`) = 5) then `business_description_by_opportunities`.`operating_time` else 0 end)) AS `May`,sum((case when (month(`operations`.`employment_date`) = 6) then `business_description_by_opportunities`.`operating_time` else 0 end)) AS `Jun`,sum((case when (month(`operations`.`employment_date`) = 7) then `business_description_by_opportunities`.`operating_time` else 0 end)) AS `July`,sum((case when (month(`operations`.`employment_date`) = 8) then `business_description_by_opportunities`.`operating_time` else 0 end)) AS `August`,sum((case when (month(`operations`.`employment_date`) = 9) then `business_description_by_opportunities`.`operating_time` else 0 end)) AS `September`,sum((case when (month(`operations`.`employment_date`) = 10) then `business_description_by_opportunities`.`operating_time` else 0 end)) AS `October`,sum((case when (month(`operations`.`employment_date`) = 11) then `business_description_by_opportunities`.`operating_time` else 0 end)) AS `November`,sum((case when (month(`operations`.`employment_date`) = 12) then `business_description_by_opportunities`.`operating_time` else 0 end)) AS `December`,sum((case when (month(`operations`.`employment_date`) = 1) then `business_description_by_opportunities`.`operating_time` else 0 end)) AS `January`,sum((case when (month(`operations`.`employment_date`) = 2) then `business_description_by_opportunities`.`operating_time` else 0 end)) AS `February`,sum((case when (month(`operations`.`employment_date`) = 3) then `business_description_by_opportunities`.`operating_time` else 0 end)) AS `March`,sum(`business_description_by_opportunities`.`operating_time`) AS `total_time` from ((((`business_description_by_opportunities` left join `operations` on((`operations`.`id` = `business_description_by_opportunities`.`operation_no`))) left join `opportunities` on((`opportunities`.`id` = `business_description_by_opportunities`.`project_no`))) left join `proposals` on((`proposals`.`id` = `opportunities`.`parent_project_no`))) left join `client_departments` on((`client_departments`.`id` = `opportunities`.`department_no`))) group by year(`operations`.`employment_date`),`client_departments`.`id`,`client_departments`.`name`,`proposals`.`id`,`proposals`.`name`,`opportunities`.`id`,`opportunities`.`name`,`operations`.`employee_no`;

-- --------------------------------------------------------

--
-- ビュー用の構造 `v_work_operation_summarys`
--
DROP TABLE IF EXISTS `v_work_operation_summarys`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_work_operation_summarys` AS select `operations`.`company_no` AS `company_no`,`operations`.`division_no` AS `division_no`,`operations`.`department_no` AS `department_no`,`operations`.`section_no` AS `section_no`,`operations`.`employee_no` AS `employee_no`,year(`operations`.`employment_date`) AS `employment_year`,sum((case when (month(`operations`.`employment_date`) = 4) then `operations`.`actual_operating_time` else 0 end)) AS `April`,sum((case when (month(`operations`.`employment_date`) = 5) then `operations`.`actual_operating_time` else 0 end)) AS `May`,sum((case when (month(`operations`.`employment_date`) = 6) then `operations`.`actual_operating_time` else 0 end)) AS `Jun`,sum((case when (month(`operations`.`employment_date`) = 7) then `operations`.`actual_operating_time` else 0 end)) AS `July`,sum((case when (month(`operations`.`employment_date`) = 8) then `operations`.`actual_operating_time` else 0 end)) AS `August`,sum((case when (month(`operations`.`employment_date`) = 9) then `operations`.`actual_operating_time` else 0 end)) AS `September`,sum((case when (month(`operations`.`employment_date`) = 10) then `operations`.`actual_operating_time` else 0 end)) AS `October`,sum((case when (month(`operations`.`employment_date`) = 11) then `operations`.`actual_operating_time` else 0 end)) AS `November`,sum((case when (month(`operations`.`employment_date`) = 12) then `operations`.`actual_operating_time` else 0 end)) AS `December`,sum((case when (month(`operations`.`employment_date`) = 1) then `operations`.`actual_operating_time` else 0 end)) AS `January`,sum((case when (month(`operations`.`employment_date`) = 2) then `operations`.`actual_operating_time` else 0 end)) AS `February`,sum((case when (month(`operations`.`employment_date`) = 3) then `operations`.`actual_operating_time` else 0 end)) AS `March`,sum(`operations`.`actual_operating_time`) AS `total_time` from `operations` group by `operations`.`division_no`,`operations`.`department_no`,`operations`.`section_no`,`operations`.`employee_no`,year(`operations`.`employment_date`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
